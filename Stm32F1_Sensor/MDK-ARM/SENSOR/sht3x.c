#include "sht3x.h"

static int16_t Sht3xAddr = 0x44 << 1; // 读写位0x88
SHT3XDATATYPE Sht3x_Data;

int8_t Sensor_I2C1_Read(uint16_t DevAddr, uint8_t *oData, uint16_t DataLen)
{
	return HAL_I2C_Master_Receive(&hi2c1,DevAddr,oData,DataLen,1000);
}

int8_t Sensor_I2C1_Write(uint16_t DevAddr, uint8_t *iData, uint16_t DataLen)
{
	return HAL_I2C_Master_Transmit(&hi2c1,DevAddr,iData,DataLen,1000);
}

void Sht3x_Read_Data(void)
{
	uint8_t Read_Buf[6];
	float temp = 0, humi = 0;
	
	Read_Buf[0] = 0x24;
	Read_Buf[1] = 0x0b;
	Sensor_I2C1_Write(Sht3xAddr, Read_Buf, 2);
	Sensor_I2C1_Read(Sht3xAddr,Read_Buf, 6);
	
	temp = (uint16_t)((Read_Buf[0]<<8) | Read_Buf[1]);		//温度拼接
	humi= (uint16_t)((Read_Buf[3]<<8) | Read_Buf[4]);			//湿度拼接

	temp = (175.0f * (temp / 65535.0f) - 45.0f);    // T = -45 + 175 * tem / (2^16-1)
	humi = (100.0f * humi / 65535.0f);           		    // RH = hum*100 / (2^16-1)
	
	static float Temp_His = 0,Humi_His = 0;
	
	if((Temp_His * 100.0f + 6) < (temp * 100.0f) || (Temp_His * 100.0f - 6) > (temp * 100.0f))\
	{
		Temp_His = temp;
		Sht3x_Data.Temperature = Temp_His;
	}
	if((Humi_His * 100.0f + 10) < (humi * 100.0f) || (Humi_His * 100.0f - 10) > (humi * 100.0f))\
	{
		Humi_His = humi;
		Sht3x_Data.Humidity = Humi_His;
	}
}

