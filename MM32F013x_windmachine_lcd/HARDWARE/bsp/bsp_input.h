/*
*********************************************************************************************************
*
*	模块名称 : 长时信号输入驱动
*	文件名称 : bsp_input.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C), 2013-2014, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#ifndef __BSP_INPUT_H
#define __BSP_INPUT_H

/*************************************输入函数******************************************************/
typedef enum
{
	L_PWD_LOCK=0,
	
}LONG_INPUT_E;


void bsp_Input_Init(void);
//static void bsp_SingleScan(uint8_t nIndex);
void bsp_InputScan(void);
 uint8_t GetInput(void);
void InputAuthorMonitor(void);

#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
