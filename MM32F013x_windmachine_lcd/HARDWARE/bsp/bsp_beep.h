/*
*********************************************************************************************************
*
*	模块名称 : 蜂鸣器模块
*	文件名称 : bsp_beep.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C), 2012-2013, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#ifndef __BSP_BEEP_H
#define __BSP_BEEP_H

enum BEEP_E
{
    FireCtrlBEEP=0,
    FaultBEEP,
};



/* 供外部调用的函数声明 */
void BEEP_InitHard(void);
void BEEP_Start(uint8_t nIndex,uint32_t _uiFreq, uint16_t _usBeepTime, uint16_t _usStopTime, uint16_t _usCycle);

void BEEP_Stop(uint8_t nIndex);
void BEEP_KeyTone(void);
void BEEP_Pro(void);
uint8_t BEEP_IsEnable(uint8_t nIndex);

#define BEEP_HAVE_POWER	  1	/* 定义此行表示有源蜂鸣器，直接通过GPIO驱动, 无需PWM */

#ifdef	BEEP_HAVE_POWER		/* 有源蜂鸣器 */

	/* PA8 */
	#define GPIO_RCC_BEEP   RCC_AHBPeriph_GPIOB
	#define GPIO_PORT_BEEP1 	GPIOB
	#define GPIO_PIN_BEEP1	GPIO_Pin_2
	
	#define GPIO_PORT_BEEP2 	GPIOB
	#define GPIO_PIN_BEEP2	GPIO_Pin_10

	#define BEEP1_ENABLE()	GPIO_PORT_BEEP1->BSRR = GPIO_PIN_BEEP1		/* 使能蜂鸣器鸣叫 */
	#define BEEP1_DISABLE()	GPIO_PORT_BEEP1->BRR = GPIO_PIN_BEEP1			/* 禁止蜂鸣器鸣叫 */
	#define BEEP2_ENABLE()	GPIO_PORT_BEEP2->BSRR = GPIO_PIN_BEEP2		/* 使能蜂鸣器鸣叫 */
	#define BEEP2_DISABLE()	GPIO_PORT_BEEP2->BRR = GPIO_PIN_BEEP2			/* 禁止蜂鸣器鸣叫 */
	
	#define BEEP1_Troggle()	GPIO_PORT_BEEP1->ODR ^= GPIO_PIN_BEEP1		/* 使能蜂鸣器鸣叫 */
	#define BEEP2_Troggle()	GPIO_PORT_BEEP2->ODR ^= GPIO_PIN_BEEP2			/* 禁止蜂鸣器鸣叫 */

#endif
#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
