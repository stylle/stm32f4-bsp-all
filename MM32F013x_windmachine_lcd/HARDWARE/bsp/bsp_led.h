/*
*********************************************************************************************************
*
*	模块名称 : LED指示灯驱动模块
*	文件名称 : bsp_led.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C), 2013-2014, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#ifndef __BSP_LED_H
#define __BSP_LED_H



#define LED_NUMS  7

typedef enum
{
    LED1=0,
    LED2,
		LED3,
		LED4,
   	LED5,
		LED6,
		LED7,
}LED_Num_T;

typedef enum 
{
	LED_POWER=0, //电源指示
	LED_FIRE_PTR, //消防联动
	LED_RUNNING, 	//运行反馈
	LED_LOW_SPEED,//低速运行
	LED_HIGH_SPEED,//高速运行	
	LED_FAULT	,		//故障指示
	LED_STATE_AUTO, //自动状态
}LED_NAME;


typedef struct _LED_T
{
	uint8_t ucEnalbe;
	uint8_t ucState;//只对关闭时间起作用
	uint16_t usLedTime;
	uint16_t usStopTime;
	uint16_t usCount;
}LED_T;


/* 供外部调用的函数声明 */
void bsp_InitLed(void);

void LED_Start(uint8_t nIndex, uint16_t _usLedTime, uint16_t _usStopTime );
void LED_Stop(uint8_t nIndex);

void led_HardOff(uint8_t _no);
void led_HardOn(uint8_t _no);
void bsp_LedToggle(uint8_t _no);
uint8_t bsp_IsLedOn(uint8_t _no);
void LED_Ctrl(void); //中断中调用或主循环调用
#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
