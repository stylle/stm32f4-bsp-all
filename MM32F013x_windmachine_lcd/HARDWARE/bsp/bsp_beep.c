/*
*********************************************************************************************************
*
*	模块名称 : 蜂鸣器驱动模块
*	文件名称 : bsp_beep.c
*	版    本 : V1.1
*	说    明 : 驱动蜂鸣器.
*
*	修改记录 :
*		版本号  日期        作者     说明
*		V1.0    2014-10-20 armfly  正式发布
*		V1.1    2015-08-30 armfly  增加修改蜂鸣器频率的功能 BEEP_Start() 函数添加频率形参
*
*	Copyright (C), 2015-2016, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#include "bsp.h"

typedef struct _BEEP_T
{
	uint8_t ucEnalbe;
	uint8_t ucState;//只对关闭时间起作用
	uint16_t usBeepTime;
	uint16_t usStopTime;
	uint16_t usCycle;
	uint16_t usCount;
	uint16_t usCycleCount;
	uint32_t uiFreq;
}BEEP_T;

static BEEP_T g_tBeep[2];		/* 定义蜂鸣器全局结构体变量 */


/***************************************蜂鸣器控制函数*************************************************************/

void BEEP_InitHard(void);
void BEEP_Start(uint8_t nIndex,uint32_t _uiFreq, uint16_t _usBeepTime, uint16_t _usStopTime, uint16_t _usCycle);

void BEEP_Stop(uint8_t nIndex);
void BEEP_KeyTone(void);
void BEEP_Pro(void);
uint8_t BEEP_IsEnable(uint8_t nIndex);







/*
*********************************************************************************************************
*	函 数 名: bsp_Beep_Enable
*	功能说明: 初始化蜂鸣器硬件
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_Beep_Enable(uint8_t nIndex)			/* 开始发声 */
{
	switch(nIndex)
	{
		case 0 : {  BEEP1_ENABLE(); break;}  
		case 1 : {  BEEP2_ENABLE(); break;}
		default:break;
	}	
}

/*
*********************************************************************************************************
*	函 数 名: bsp_Beep_Disenable
*	功能说明: 初始化蜂鸣器硬件
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_Beep_Disenable(uint8_t nIndex)			/* 开始发声 */
{
	switch(nIndex)
	{
		case 0 : {  BEEP1_DISABLE(); break;}  
		case 1 : {  BEEP2_DISABLE(); break;}
		default:break;
	}	
}
/*
*********************************************************************************************************
*	函 数 名: BEEP_InitHard
*	功能说明: 初始化蜂鸣器硬件
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_InitHard(void)
{
#ifdef	BEEP_HAVE_POWER		/* 有源蜂鸣器 */
	GPIO_InitTypeDef GPIO_InitStructure;

	/* 打开GPIO的时钟 */
	RCC_AHBPeriphClockCmd(GPIO_RCC_BEEP, ENABLE);

	BEEP1_DISABLE();
	BEEP2_DISABLE();

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	/* 推挽输出模式 */	
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_BEEP1;
	GPIO_Init(GPIO_PORT_BEEP1, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_BEEP2;
	GPIO_Init(GPIO_PORT_BEEP2, &GPIO_InitStructure);
#else
	;	/* 无源蜂鸣器 */
	
#endif
} 

/*
*********************************************************************************************************
*	函 数 名: BEEP_Start
*	功能说明: 启动蜂鸣音。
*	形    参：_uiFreq : 频率 (Hz)
*			  _usBeepTime : 蜂鸣时间，单位10ms; 0 表示不鸣叫
*			  _usStopTime : 停止时间，单位10ms; 0 表示持续鸣叫
*			 _usCycle : 鸣叫次数， 0 表示持续鸣叫
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_Start(uint8_t nIndex,uint32_t _uiFreq, uint16_t _usBeepTime, uint16_t _usStopTime, uint16_t _usCycle)
{
	if (_usBeepTime == 0)
	{
		return;
	}

	g_tBeep[nIndex].uiFreq = _uiFreq;
	g_tBeep[nIndex].usBeepTime = _usBeepTime;
	g_tBeep[nIndex].usStopTime = _usStopTime;
	g_tBeep[nIndex].usCycle = _usCycle;
	g_tBeep[nIndex].usCount = 0;
	g_tBeep[nIndex].usCycleCount = 0;
	g_tBeep[nIndex].ucState = 0;
	g_tBeep[nIndex].ucEnalbe = 1;	/* 设置完全局参数后再使能发声标志 */

	bsp_Beep_Enable(nIndex);			/* 开始发声 */
}

/*
*********************************************************************************************************
*	函 数 名: BEEP_Stop
*	功能说明: 停止蜂鸣音。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_Stop(uint8_t nIndex)
{
	g_tBeep[nIndex].ucEnalbe = 0;

	bsp_Beep_Disenable(nIndex);	/* 必须在清控制标志后再停止发声，避免停止后在中断中又开启 */
}

/*
*********************************************************************************************************
*	函 数 名: BEEP_KeyTone (故障蜂鸣器鸣叫)
*	功能说明: 发送按键音， 固定 1.5KHz
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_KeyTone(void)
{
	BEEP_Start(1,1500, 10, 10, 1);	/* 鸣叫500ms，停500ms， 1次 */
}







/*
*********************************************************************************************************
*	函 数 名: BEEP_Single_Ctrl
*	功能说明: 。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_Single_Ctrl(uint8_t nIndex)
{
		if ((g_tBeep[nIndex].ucEnalbe == 0) || (g_tBeep[nIndex].usStopTime == 0))
		{
			return;
		}

		if (g_tBeep[nIndex].ucState == 0)
		{
			if (g_tBeep[nIndex].usStopTime > 0)	/* 间断发声 */
			{
				if (++g_tBeep[nIndex].usCount >= g_tBeep[nIndex].usBeepTime)
				{
					bsp_Beep_Disenable(nIndex);		/* 停止发声 */
					g_tBeep[nIndex].usCount = 0;
					g_tBeep[nIndex].ucState = 1;
				}
			}
			else
			{
				;	/* 不做任何处理，连续发声 */
			}
		}
		else if (g_tBeep[nIndex].ucState == 1)
		{
			if (++g_tBeep[nIndex].usCount >= g_tBeep[nIndex].usStopTime)
			{
				/* 连续发声时，直到调用stop停止为止 */
				if (g_tBeep[nIndex].usCycle > 0)
				{
					if (++g_tBeep[nIndex].usCycleCount >= g_tBeep[nIndex].usCycle)
					{
						/* 循环次数到，停止发声 */
						g_tBeep[nIndex].ucEnalbe = 0;
					}

					if (g_tBeep[nIndex].ucEnalbe == 0)
					{
						g_tBeep[nIndex].usStopTime = 0;
						return;
					}
				}

				g_tBeep[nIndex].usCount = 0;
				g_tBeep[nIndex].ucState = 0;

				bsp_Beep_Enable(nIndex);			/* 开始发声 */
			}
		}	
}


/*
*********************************************************************************************************
*	函 数 名: BEEP_Pro
*	功能说明: 每隔10ms调用1次该函数，用于控制蜂鸣器发声。该函数在 bsp_timer.c 中被调用。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void BEEP_Pro(void)
{
	uint8_t nIndex=0;
	for(nIndex=0;nIndex<2;nIndex++)
	{
		BEEP_Single_Ctrl(nIndex);
	}
}





/*
*********************************************************************************************************
*	函 数 名: BEEP_IsEnable
*	功能说明: 每隔10ms调用1次该函数，用于控制蜂鸣器发声。该函数在 bsp_timer.c 中被调用。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
uint8_t BEEP_IsEnable(uint8_t nIndex)
{
	if(g_tBeep[nIndex].ucEnalbe ==0)
		return 0;
	else 
		return 1;
	
}
















/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
