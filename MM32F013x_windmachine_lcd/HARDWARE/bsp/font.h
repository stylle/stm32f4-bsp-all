#ifndef __FONT_h_
#define __FONT_h_

extern const unsigned char c_ch_gan16_bold[];
extern const unsigned char c_ch_xie16_bold[];
extern const unsigned char c_ch_shi16_bold[];
extern const unsigned char c_ch_yong16_bold[];
//extern const unsigned char c_ch_dian16_bold[];
//extern const unsigned char c_ch_yuan16_bold[];
//extern const unsigned char c_ch_gao16_bold[];
//extern const unsigned char c_ch_ke16_bold[];
//extern const unsigned char c_ch_kao16_bold[];
//extern const unsigned char c_ch_xing16_bold[];

extern const unsigned char c_ch_zi16_bold[];
extern const unsigned char c_ch_neng16_bold[];
extern const unsigned char c_ch_feng16_bold[];
extern const unsigned char c_ch_ji16_bold[];
extern const unsigned char c_ch_kong16_bold[];
extern const unsigned char c_ch_zhi16_bold[];
extern const unsigned char c_ch_qi16_bold[];

extern const unsigned char c_ch_bao[];//保
//extern const unsigned char c_ch_bao1[];//保
extern const unsigned char c_ch_bai[];//败
extern const unsigned char c_ch_bi[];//毕
/*--  文字:  闭  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char c_ch_bi1    [];
extern const unsigned char c_ch_bu[];//不
extern const unsigned char c_ch_bei[];//背

extern const unsigned char c_ch_can[];//参
extern const unsigned char c_ch_ce[];//测
extern const unsigned char c_ch_cha[];//查
extern const unsigned char c_ch_cheng[];//成
extern const unsigned char c_ch_chang[];//常
//extern const unsigned char c_ch_chang1[];//常
extern const unsigned char c_ch_chang2[];//厂
extern const unsigned char c_ch_ci[];
extern const unsigned char c_ch_chi[];//池
extern const unsigned char c_ch_chong[];//充
extern const unsigned char c_ch_chong1[];//重
extern const unsigned char c_ch_chu[];//出
extern const unsigned char c_ch_cuo[];//错
extern const unsigned char c_ch_cun[];//存

extern const unsigned char c_ch_dan[];//单
extern const unsigned char c_ch_dai[];//待
extern const unsigned char c_ch_dao[];//倒
extern const unsigned char c_ch_dao1[];//到
extern const unsigned char c_ch_dian1[];//电
extern const unsigned char c_ch_dian2[];//点
extern const unsigned char c_ch_ding[];//定
extern const unsigned char c_ch_dong[];//动
extern const unsigned char c_ch_du[];//度
extern const unsigned char c_ch_duan1[];//短
extern const unsigned char c_ch_duan2[];//断
extern const unsigned char c_ch_e[];//额

extern const unsigned char c_ch_fan[];//返
extern const unsigned char c_ch_fang[];//放
extern const unsigned char c_ch_fang2[];//防
extern const unsigned char c_ch_fou[];//否
extern const unsigned char c_ch_fu[];//浮
extern const unsigned char c_ch_fu1[];//复
extern const unsigned char c_ch_fu2[];//辐
/*--  文字:  风  --*/
extern const unsigned char c_ch_feng[];

extern const unsigned char c_ch_gai[];//改
extern const unsigned char c_ch_gong1[];//工
extern const unsigned char c_ch_gong2[];//功
extern const unsigned char c_ch_gong3[];//供
extern const unsigned char c_ch_gu[];//故
extern const unsigned char c_ch_guan[];//关
extern const unsigned char c_ch_guang[];//光
extern const unsigned char c_ch_guo[];//过
extern const unsigned char c_ch_hu1[];//户
extern const unsigned char c_ch_hu2[];//护
extern const unsigned char c_ch_hui[]; //回
extern const unsigned char c_ch_huan[];//换
/*--  文字:  机  --*/
extern const unsigned char c_ch_ji   [];
extern const unsigned char c_ch_ji1[];//急
extern const unsigned char c_ch_ji2[];//记
extern const unsigned char c_ch_ji3[];//机
extern const unsigned char c_ch_ji4[];//计
extern const unsigned char c_ch_jian[];//检
extern const unsigned char c_ch_jian1[];//监
extern const unsigned char c_ch_jian2[];//间
extern const unsigned char c_ch_jiao[];//校
extern const unsigned char c_ch_jin[];//进
extern const unsigned char c_ch_jun[];//均

extern const unsigned char c_ch_kai[];//开
extern const unsigned char c_ch_kuai [];//块
extern const unsigned char c_ch_ke[];//客
extern const unsigned char c_ch_kong[];//控
extern const unsigned char c_ch_kong1[];//空

extern const unsigned char c_ch_liang [];//量
extern const unsigned char c_ch_lei[];//类
extern const unsigned char c_ch_li[];//历
extern const unsigned char c_ch_liu[];//流
extern const unsigned char c_ch_lu1[];//录
extern const unsigned char c_ch_lu2[];//路
extern const unsigned char c_ch_lv[];//率

extern const unsigned char c_ch_ma[];//码
extern const unsigned char c_ch_man[];//满
extern const unsigned char c_ch_mo [];//模
extern const unsigned char c_ch_men[];//门
extern const unsigned char c_ch_mi[];//密

extern const unsigned char c_ch_nian[];//年
extern const unsigned char c_ch_ping[];//屏
/*--  文字:  普  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char c_ch_pu  [];
extern const unsigned char c_ch_qi[];//启
extern const unsigned char c_ch_qi1[];//期
extern const unsigned char c_ch_qu[];//取
extern const unsigned char c_ch_qing[];//请
extern const unsigned char c_ch_qian[];//欠
extern const unsigned char c_ch_qiang[];//强
extern const unsigned char c_ch_que1[];//缺
extern const unsigned char c_ch_que2[];//确
extern const unsigned char c_ch_quan[];//全
	/*--  文字:  权  --*/
extern const unsigned char c_ch_quan1  [];

extern const unsigned char c_ch_ren[];//认
extern const unsigned char c_ch_ru[];//入
extern const unsigned char c_ch_ri[];//日

/*--  文字:  速  --*/
extern const unsigned char c_ch_su[];
extern const unsigned char c_ch_she[];//设
extern const unsigned char c_ch_she2[];//射
extern const unsigned char c_ch_sheng[];//省
extern const unsigned char c_ch_sheng1[];//声
extern const unsigned char c_ch_sheng2[];//剩
extern const unsigned char c_ch_shou1[];//手
extern const unsigned char c_ch_shou2[];//受
	/*--  文字:  授  --*/
extern const unsigned char c_ch_shou3 [];
extern const unsigned char c_ch_shi1[];//史
extern const unsigned char c_ch_shi2[];//示
extern const unsigned char c_ch_shi3[];//是
extern const unsigned char c_ch_shi4[];//时
extern const unsigned char c_ch_shi5[];//失
extern const unsigned char c_ch_shu1[];//输
extern const unsigned char c_ch_shu2[];//数
/*--  文字:  双  --*/
extern const unsigned char c_ch_shuang  [];


extern const unsigned char c_ch_tai[];//态
extern const unsigned char c_ch_tian[];//天
extern const unsigned char c_ch_tiao[];//调
extern const unsigned char c_ch_ting[];//停
extern const unsigned char c_ch_tong[];//统
extern const unsigned char c_ch_tong2[];//通
extern const unsigned char c_ch_wan[];//完
extern const unsigned char c_ch_wei[];//微
extern const unsigned char c_ch_wei2[]; //位
extern const unsigned char c_ch_wen[];//温
extern const unsigned char c_ch_wu1[];//无
extern const unsigned char c_ch_wu2[];//误

extern const unsigned char c_ch_xiu[];//修
extern const unsigned char c_ch_xi1[];//息
extern const unsigned char c_ch_xi2[];//系
extern const unsigned char c_ch_xia[];//下
extern const unsigned char c_ch_xiao[];//消
extern const unsigned char c_ch_xian1[];//显
extern const unsigned char c_ch_xian2[];//限
	/*--  文字:  限  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
extern const unsigned char c_ch_xian3 []; 
/*--  文字:  相  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
extern const unsigned char c_ch_xiang [];//相
extern const unsigned char c_ch_xin[];//信
extern const unsigned char c_ch_xin1[];//新
extern const unsigned char c_ch_xing[];//型
extern const unsigned char c_ch_xuan[];//选
extern const unsigned char c_ch_xun[];//询
extern const unsigned char c_ch_xun1[];

extern const unsigned char c_ch_ya[];//压
extern const unsigned char c_ch_yan[];//验
extern const unsigned char c_ch_yi[];//异
extern const unsigned char c_ch_yi1[];
extern const unsigned char c_ch_yi2[];//已
extern const unsigned char c_ch_yin[];//音
extern const unsigned char c_ch_ying1[];//应
extern const unsigned char c_ch_ying2 [];//因
extern const unsigned char c_ch_yue[];//月
extern const unsigned char c_ch_yue1[];//钥
extern const unsigned char c_ch_yu[];//余

extern const unsigned char c_ch_zai[];//载
extern const unsigned char c_ch_ze1[];//则
extern const unsigned char c_ch_ze2[];//择
extern const unsigned char c_ch_zai1[];//在
extern const unsigned char c_ch_zhang[];//障
extern const unsigned char c_ch_zhi1[];//值
extern const unsigned char c_ch_zhi2[];//只
extern const unsigned char c_ch_zhi3[];//制
extern const unsigned char c_ch_zhi4[];//置
extern const unsigned char c_ch_zhi5[];//支
extern const unsigned char c_ch_zhu[];//主
extern const unsigned char c_ch_zhuan[];//转
extern const unsigned char c_ch_zhuang[];//状
extern const unsigned char c_ch_zheng[];//正
extern const unsigned char c_ch_zi[];//自
extern const unsigned char c_ch_zhi[];//止
extern const unsigned char c_ch_zong[];//总
extern const unsigned char c_ch_zuo[];//作



extern const unsigned char c_ch_celsiu[];//℃
extern const unsigned char c_ll[];
extern const unsigned char c_ch_ye[];/*"液",0*/
extern const unsigned char c_ch_jing[];/*"晶",1*/
extern const unsigned char c_ch_ban[];/*"版",2*/
extern const unsigned char c_ch_ben[];/*"本",3*/
//extern const unsigned char c_ch_ma1[];//马
//extern const unsigned char c_ch_yi1[];//一
//extern const unsigned char c_ch_jin1[];//金
//extern const unsigned char c_ch_mu1[];//木
//extern const unsigned char c_ch_shui1[];//水
extern const unsigned char c_ch_huo1[];//火
//extern const unsigned char c_ch_tu1[];//土

extern const unsigned char c_digits[16][12];//数字0 - 9
//extern const unsigned char c_let_A[];//电流单位“A”
extern const unsigned char c_let_H[];
extern const unsigned char c_let_I[];
extern const unsigned char c_let_K[];
extern const unsigned char c_let_L[];
extern const unsigned char c_let_T[];
extern const unsigned char c_let_V[];//电压单位“V”
extern const unsigned char c_let_W[];
extern const unsigned char c_let_E[];
extern const unsigned char c_let_n[];
extern const unsigned char c_let_t[];
extern const unsigned char c_let_e[];
extern const unsigned char c_let_r[];
extern const unsigned char c_let_s[];
extern const unsigned char c_let_c[];
extern const unsigned char c_let_m[];
extern const unsigned char c_point[];//感叹号！
extern const unsigned char c_ques[];//问号 ？
extern const unsigned char c_minu[];//减号 -
extern const unsigned char c_hash[];//井号 #
extern const unsigned char c_perc[];//百分号 %
extern const unsigned char c_x[];//x
extern const unsigned char c_colon[];//冒号 :
extern const unsigned char c_dot[];
extern const unsigned char c_clear[];
extern const unsigned char c_arrow[];// >
extern const unsigned char c_arrow1[];// >
extern const unsigned char c_arrow2[];//手指图12*12
extern const unsigned char c_ch_yong  [];//用
extern const unsigned char c_ch_hu    [];//户

extern const unsigned char blk[];
extern const unsigned char blk6wid[];



/*--  文字:  高  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_gao[] ;

/*--  文字:  低  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_di[];


/*--  文字:  阀  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_fa[];

/*--  文字:  检  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_jian[];

/*--  文字:  修  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_xiu[];

/*--  文字:  消  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_xiao[];

/*--  文字:  防  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_fang[];

/*--  文字:  联  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_lian[];
/*--  文字:  动  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_dong[];

/*--  文字:  运  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_yun[];

/*--  文字:  行  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern  const unsigned char w_xing[];


/*--  文字:  缺  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=12x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=12x16  --*/
extern const unsigned char w_que[];

/*--  文字:  K  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=6x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=6x16  --*/
extern const unsigned char w_K[];

/*--  文字:  M  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=6x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=6x16  --*/
extern const unsigned char w_M[];

/*--  文字:  S  --*/
/*--  宋体9;  此字体下对应的点阵为：宽x高=6x12   --*/
/*--  高度不是8的倍数，现调整为：宽度x高度=6x16  --*/
extern const unsigned char w_S[];










extern const unsigned char Huafu[];
extern const unsigned char chuanghong[];
extern const unsigned long AuthorizationCode[];

extern const unsigned char welcom[];


#endif
