#ifndef __BSP_LCD12864_H
#define __BSP_LCD12864_H

#include <stdint.h>

#define LCD_Light_PORT    GPIOB
#define LCD_Light_PIN     GPIO_Pin_1

#define LIGHT_ON       GPIO_SetBits(LCD_Light_PORT,LCD_Light_PIN)			//背光关
#define LIGHT_OFF      GPIO_ResetBits(LCD_Light_PORT,LCD_Light_PIN)		//背光开


/* 供外部调用的函数声明 */
void WriteCmdXLCD(uint16_t cmd);
void WriteDataXLCD(uint16_t dat);
void bsp_InitLcd(void);

void OpenXLCD(void);
void PhotoXLCD(const unsigned char *data);
void ClearXLCD(void);
void putblkXLCD(unsigned char x, unsigned char y, unsigned char c_width, unsigned char ccode);
void putsXLCD(unsigned char x, unsigned char y, const unsigned char *discode0, const unsigned char *discode1, const unsigned char *discode2, const unsigned char *discode3, unsigned char c_width, unsigned char n, unsigned char inv);
void putsXLCD2(unsigned char x, unsigned char y, const unsigned char *discode0, const unsigned char *discode1, const unsigned char *discode2, const unsigned char *discode3, unsigned char c_width, unsigned char n, unsigned char invpos);
void putrsXLCD(unsigned char x, unsigned char y, const unsigned char *discode, unsigned char c_width, unsigned char inv);
void putsXLCDD(unsigned char x, unsigned char y, const unsigned char *discode0, const unsigned char *discode1, const unsigned char *discode2,const unsigned char *discode3,  const unsigned char *discode4 ,unsigned char c_width, unsigned char n, unsigned char inv);

#endif
