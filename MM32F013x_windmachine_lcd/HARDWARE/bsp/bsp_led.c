/*
*********************************************************************************************************
*
*	模块名称 : LED指示灯驱动模块
*	文件名称 : bsp_led.c
*	版    本 : V1.0
*	说    明 : 驱动LED指示灯
*
*	修改记录 :
*		版本号  日期        作者     说明
*		V1.0    2013-02-01 armfly  正式发布
*
*	Copyright (C), 2013-2014, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#include "bsp.h"

/*
	该程序适用于安富莱STM32-V4 开发板

	如果用于其它硬件，请修改GPIO定义
	
	如果用户的LED指示灯个数小于4个，可以将多余的LED全部定义为和第1个LED一样，并不影响程序功能
*/


/* 按键口对应的RCC时钟 */
#define RCC_ALL_LED 	(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOB)

#define GPIO_PORT_LED1  GPIOA
#define GPIO_PIN_LED1	GPIO_Pin_12

#define GPIO_PORT_LED2  GPIOA
#define GPIO_PIN_LED2	GPIO_Pin_11

#define GPIO_PORT_LED3  GPIOA
#define GPIO_PIN_LED3	GPIO_Pin_8

#define GPIO_PORT_LED4  GPIOB
#define GPIO_PIN_LED4	GPIO_Pin_15

#define GPIO_PORT_LED5  GPIOB
#define GPIO_PIN_LED5	GPIO_Pin_14

#define GPIO_PORT_LED6  GPIOB
#define GPIO_PIN_LED6	GPIO_Pin_13

#define GPIO_PORT_LED7  GPIOB
#define GPIO_PIN_LED7	GPIO_Pin_12


 LED_T LED_Data[LED_NUMS];
 
//static void led_HardOn(uint8_t _no);
//static void led_HardOff(uint8_t _no);
static void LED_SingleCtrl(uint8_t led_num);

static void led_HardInit(void)
{
	uint8_t i=0;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* 打开GPIO时钟 */
	RCC_AHBPeriphClockCmd(RCC_ALL_LED, ENABLE);

	/*
		配置所有的LED指示灯GPIO为推挽输出模式
		由于将GPIO设置为输出时，GPIO输出寄存器的值缺省是0，因此会驱动LED点亮.
		这是我不希望的，因此在改变GPIO为输出前，先关闭LED指示灯
	*/
	

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	/* 推挽输出模式 */
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED1;
	GPIO_Init(GPIO_PORT_LED1, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED2;
	GPIO_Init(GPIO_PORT_LED2, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED3;
	GPIO_Init(GPIO_PORT_LED3, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED4;
	GPIO_Init(GPIO_PORT_LED4, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED5;
	GPIO_Init(GPIO_PORT_LED5, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED6;
	GPIO_Init(GPIO_PORT_LED6, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED7;
	GPIO_Init(GPIO_PORT_LED7, &GPIO_InitStructure);

	for(i=0;i<LED_NUMS;i++)
	{
		led_HardOff(i);	
//		led_HardOn(i);
		
	}	


}

static void led_DataInit(void)
{
	uint8_t nIndex=0;
	for(nIndex=0;nIndex<LED_NUMS;nIndex++)
	{
		LED_Data[nIndex].ucEnalbe=0;
		LED_Data[nIndex].ucState=0;	
		LED_Data[nIndex].usLedTime=0;
		LED_Data[nIndex].usStopTime=0;	
		LED_Data[nIndex].usCount=0;	
	}
}
/*
*********************************************************************************************************
*	函 数 名: bsp_InitLed
*	功能说明: 配置LED指示灯相关的GPIO,  该函数被 bsp_Init() 调用。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitLed(void)
{
	led_HardInit();
	led_DataInit();
}

/*
*********************************************************************************************************
*	函 数 名: led_HardOff
*	功能说明: 点亮指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 0 - 6
*	返 回 值: 无
*********************************************************************************************************
*/
void led_HardOff(uint8_t _no)
{
//	_no--;

	switch(_no)
	{
		
		case 0:
		{    
//				GPIO_SetBits(GPIO_PORT_LED1,GPIO_PIN_LED1);
			GPIO_PORT_LED1->BRR = GPIO_PIN_LED1;
			break;              
		}                     
		case 1:               
		{         
//			GPIO_SetBits(GPIO_PORT_LED1,GPIO_PIN_LED1);			
			GPIO_PORT_LED2->BRR = GPIO_PIN_LED2;
			break;              
		}                     
		case 2:               
		{                     
			GPIO_PORT_LED3->BRR = GPIO_PIN_LED3;
			break;              
		}                     
		case 3:               
		{                     
			GPIO_PORT_LED4->BRR = GPIO_PIN_LED4;
			break;              
		}                     
		case 4:               
		{                     
			GPIO_PORT_LED5->BRR = GPIO_PIN_LED5;
			break;              
		}                     
		case 5:               
		{                     
			GPIO_PORT_LED6->BRR = GPIO_PIN_LED6;
			break;              
		}                     
		case 6:               
		{                     
			GPIO_PORT_LED7->BRR = GPIO_PIN_LED7;
			break;              
		}
		default:
			break;
	}


}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedOff
*	功能说明: 熄灭指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 无
*********************************************************************************************************
*/

void led_HardOn(uint8_t _no)
{
//	_no--;

	switch(_no)
	{
		
		case 0:
		{
			GPIO_PORT_LED1->BSRR = GPIO_PIN_LED1;
			break;
		}
		case 1:
		{
			GPIO_PORT_LED2->BSRR = GPIO_PIN_LED2;
			break;
		}
		case 2:
		{
			GPIO_PORT_LED3->BSRR = GPIO_PIN_LED3;
			break;
		}
		case 3:
		{
			GPIO_PORT_LED4->BSRR = GPIO_PIN_LED4;
			break;
		}
		case 4:
		{
			GPIO_PORT_LED5->BSRR = GPIO_PIN_LED5;
			break;
		}
		case 5:
		{
			GPIO_PORT_LED6->BSRR = GPIO_PIN_LED6;
			break;
		}
		case 6:
		{
			GPIO_PORT_LED7->BSRR = GPIO_PIN_LED7;
			break;
		}
		default:
			break;
	}	
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedToggle
*	功能说明: 翻转指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 0 - 6
*	返 回 值: 按键代码
*********************************************************************************************************
*/
void bsp_LedToggle(uint8_t _no)
{
//	_no--;
	switch(_no)
	{
		
		case 0:
		{
			GPIO_PORT_LED1->ODR ^= GPIO_PIN_LED1;
			break;
		}
		case 1:
		{
			GPIO_PORT_LED2->ODR ^= GPIO_PIN_LED2;
			break;
		}
		case 2:
		{
			GPIO_PORT_LED3->ODR ^= GPIO_PIN_LED3;
			break;
		}
		case 3:
		{
			GPIO_PORT_LED4->ODR ^= GPIO_PIN_LED4;
			break;
		}
		case 4:
		{
			GPIO_PORT_LED5->ODR ^= GPIO_PIN_LED5;
			break;
		}
		case 5:
		{
			GPIO_PORT_LED6->ODR ^= GPIO_PIN_LED6;
			break;
		}
		case 6:
		{
			GPIO_PORT_LED7->ODR ^= GPIO_PIN_LED7;
			break;
		}
		default:
			break;
	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_IsLedOn
*	功能说明: 判断LED指示灯是否已经点亮。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 1表示已经点亮，0表示未点亮
*********************************************************************************************************
*/
uint8_t bsp_IsLedOn(uint8_t _no)
{
//	_no--;
	switch(_no)
	{		
		case 0:
		{
			if ((GPIO_PORT_LED1->ODR & GPIO_PIN_LED1) == 0)
			{
				return 1;
			}
			return 0;			
		}
		case 1:
		{
			if ((GPIO_PORT_LED2->ODR & GPIO_PIN_LED2) == 0)
			{
				return 1;
			}
			return 0;
		}
		case 2:
		{
			if ((GPIO_PORT_LED3->ODR & GPIO_PIN_LED3) == 0)
			{
				return 1;
			}
			return 0;
		}
		case 3:
		{
			if ((GPIO_PORT_LED4->ODR & GPIO_PIN_LED4) == 0)
			{
				return 1;
			}
			return 0;
		}
		case 4:
		{
			if ((GPIO_PORT_LED5->ODR & GPIO_PIN_LED5) == 0)
			{
				return 1;
			}
			return 0;
		}
		case 5:
		{
			if ((GPIO_PORT_LED6->ODR & GPIO_PIN_LED6) == 0)
			{
				return 1;
			}
			return 0;
		}
		case 6:
		{
			if ((GPIO_PORT_LED7->ODR & GPIO_PIN_LED7) == 0)
			{
				return 1;
			}
			return 0;
		}
		default:
			return 0;
	}
}

/*
*********************************************************************************************************
*	函 数 名: LED_Start
*	功能说明: 启动蜂鸣音。
*	形    参：_uiFreq : 频率 (Hz)
*			  _usBeepTime : 蜂鸣时间，单位10ms; 0 表示不鸣叫
*			  _usStopTime : 停止时间，单位10ms; 0 表示持续鸣叫
*			 _usCycle : 鸣叫次数， 0 表示持续鸣叫
*	返 回 值: 无
*********************************************************************************************************
*/
void LED_Start(uint8_t nIndex, uint16_t _usLedTime, uint16_t _usStopTime )
{
	if (_usLedTime == 0)
	{
		return;
	}

	LED_Data[nIndex].usLedTime = _usLedTime;
	LED_Data[nIndex].usStopTime = _usStopTime;
	LED_Data[nIndex].usCount = 0;
	LED_Data[nIndex].ucState = 0;
	LED_Data[nIndex].ucEnalbe = 1;	/* 设置完全局参数后再使能发声标志 */

	led_HardOn(nIndex);			/* 开始发声 */
}

/*
*********************************************************************************************************
*	函 数 名: LED_Stop
*	功能说明: 停止蜂鸣音。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void LED_Stop(uint8_t nIndex)
{
	LED_Data[nIndex].ucEnalbe = 0;

	led_HardOff(nIndex);	/* 必须在清控制标志后再停止发声，避免停止后在中断中又开启 */
}

/*
*********************************************************************************************************
*	函 数 名: LED_SingleCtrl
*	功能说明: 对led状态进行控制。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 1表示已经点亮，0表示未点亮
*********************************************************************************************************
*/
void LED_SingleCtrl(uint8_t nIndex)
{
	
	if ((LED_Data[nIndex].ucEnalbe == 0) || (LED_Data[nIndex].usStopTime == 0))
	{
		return;
	}

	if (LED_Data[nIndex].ucState == 0)
	{
		if (LED_Data[nIndex].usStopTime > 0)	/* 间断发光 */
		{
			if (++LED_Data[nIndex].usCount >= LED_Data[nIndex].usLedTime)
			{
				led_HardOff(nIndex);		/* 停止发光 */
				LED_Data[nIndex].usCount = 0;
				LED_Data[nIndex].ucState = 1;
			}
		}
		else
		{
			;	/* 不做任何处理，连续发声 */
		}
	}
	else if (LED_Data[nIndex].ucState == 1)
	{
		if (++LED_Data[nIndex].usCount >= LED_Data[nIndex].usStopTime)
		{
			/* 连续发声时，直到调用stop停止为止 */
			
			LED_Data[nIndex].usCount = 0;
			LED_Data[nIndex].ucState = 0;

			led_HardOn(nIndex);			/* 开始发声 */
		}
	}
	
}
/*
*********************************************************************************************************
*	函 数 名: LED_Ctrl
*	功能说明: 对led状态进行控制,每10ms执行一次
*	形    参:  _no : 指示灯序号，范围 1 - 7
*	返 回 值: 1表示已经点亮，0表示未点亮
*********************************************************************************************************
*/
void LED_Ctrl(void) 
{
	uint8_t i =0;
	for(i=0;i<LED_NUMS;i++)
	{
		LED_SingleCtrl(i);
	}
}
