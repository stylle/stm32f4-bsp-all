#include "bsp.h" 



#define LCD_RCC_ALL 				(RCC_AHBPeriph_GPIOB|RCC_AHBPeriph_GPIOA)

#define LCD_CS_PORT       GPIOA
#define LCD_CS_PIN        GPIO_Pin_4

#define LCD_RES_PORT      GPIOA
#define LCD_RES_PIN       GPIO_Pin_5

#define LCD_RS_PORT       GPIOA
#define LCD_RS_PIN        GPIO_Pin_6

#define LCD_SCK_PORT      GPIOA
#define LCD_SCK_PIN       GPIO_Pin_7

#define LCD_SID_PORT      GPIOB
#define LCD_SID_PIN       GPIO_Pin_0




#define CS1 GPIO_SetBits(LCD_CS_PORT,LCD_CS_PIN)
#define CS0 GPIO_ResetBits(LCD_CS_PORT,LCD_CS_PIN)	
#define RS1 GPIO_SetBits(LCD_RS_PORT,LCD_RS_PIN)		
#define RS0 GPIO_ResetBits(LCD_RS_PORT,LCD_RS_PIN)
#define SCK1 GPIO_SetBits(LCD_SCK_PORT,LCD_SCK_PIN)    
#define SCK0 GPIO_ResetBits(LCD_SCK_PORT,LCD_SCK_PIN)
#define SID1 GPIO_SetBits(LCD_SID_PORT,LCD_SID_PIN)    
#define SID0 GPIO_ResetBits(LCD_SID_PORT,LCD_SID_PIN)

#define RES_ON   GPIO_ResetBits(LCD_RES_PORT,LCD_RES_PIN)		//复位开
#define RES_OFF  GPIO_SetBits(LCD_RES_PORT,LCD_RES_PIN)			//复位关


static void Delay(__IO uint32_t nCount)
{
   for(; nCount != 0; nCount--);
}
/*
*********************************************************************************************************
*	函 数 名: bsp_InitLcd
*	功能说明: LCD底层硬件初始化  
*	形    参: 无   
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitLcd()
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(LCD_RCC_ALL, ENABLE);

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;

	GPIO_InitStructure.GPIO_Pin = LCD_SID_PIN;
	GPIO_Init(LCD_SID_PORT, &GPIO_InitStructure);
	   
	GPIO_InitStructure.GPIO_Pin = LCD_CS_PIN;
	GPIO_Init(LCD_CS_PORT, &GPIO_InitStructure);   
	
	GPIO_InitStructure.GPIO_Pin = LCD_RS_PIN;
	GPIO_Init(LCD_RS_PORT, &GPIO_InitStructure);  
	
	GPIO_InitStructure.GPIO_Pin = LCD_SCK_PIN;
	GPIO_Init(LCD_SCK_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LCD_RES_PIN;
	GPIO_Init(LCD_RES_PORT, &GPIO_InitStructure); 

	GPIO_InitStructure.GPIO_Pin = LCD_Light_PIN;
	GPIO_Init(LCD_Light_PORT, &GPIO_InitStructure); 
	
	LIGHT_ON;
	
}
/*
*********************************************************************************************************
*	函 数 名: WriteCmdXLCD
*	功能说明: LCD写命令
*	形    参: cmd：写入的命令   
*	返 回 值: 无
*********************************************************************************************************
*/				   
void WriteCmdXLCD(uint16_t cmd)
{	
	uint8_t i;
	CS0;
	RS0;		//Command
	for(i=0;i<8;i++)
	{
//		SCK1;
		if((cmd&0x80)==0x80) SID1;
		if((cmd&0x80)==0x00) SID0;
		SCK0;
		Delay(60);
    SCK1;
		cmd<<=1;
	}
}
/*
*********************************************************************************************************
*	函 数 名: WriteDataXLCD
*	功能说明: LCD写数据
*	形    参: cmd：写入的数据   
*	返 回 值: 无
*********************************************************************************************************
*/
void WriteDataXLCD(uint16_t data)
{		
	uint8_t i;
	CS0;
	RS1;		//Data
	for(i=0;i<8;i++)
	{
//		SCK1;
		if((data&0x80)==0x80) SID1;
		if((data&0x80)==0x00) SID0;
		SCK0;
		Delay(60);
    SCK1;
		data<<=1;
	}
}









/*******************************************************************************
* 函 数 名: putrsXLCD(unsigned char x, unsigned char y, const unsigned char *discode, unsigned char c_width, unsigned char inv)
* 函数功能: 送数据到LCD12864显示
* 入口参数: x横坐标的值,y纵坐标的值,*discode数组地址,c_width显示宽度,inv反显控制
* 返    回: 无
*******************************************************************************/
void putrsXLCD(unsigned char x, unsigned char y, const unsigned char *discode, unsigned char c_width, unsigned char inv)    //x:0~192 left 2 right y:0~7 top 2 bottom
{
    //显示字的上半屏
    unsigned char i;
    WriteCmdXLCD(0xB0 | y);
    WriteCmdXLCD(0x10 |(x >> 4));
    WriteCmdXLCD(0x00 |(x & 0x0F));
    for(i = 0;i != c_width; i ++)
    {
        WriteDataXLCD(*discode ^ (inv == 0 ? 0x00:0xFC));
        discode ++;
    }
    //显示字的下半屏
    WriteCmdXLCD(0xB0 | y+1);
    WriteCmdXLCD(0x10 |(x >> 4));
    WriteCmdXLCD(0x00 |(x & 0x0F));
    for(;c_width != 0; c_width --)
    {
        WriteDataXLCD(*discode ^ (inv == 0 ? 0x00:0x3F));
        discode ++;
    }
}

/*******************************************************************************
*       Function Name:  putsXLCD
*       Return Value:   void
*       Parameters:     buffer: pointer to string
*       Description:    This routine writes a string of bytes to the
*                       Hitachi KS0108 LCD controller. The user
*                       must check to see if the LCD controller is
*                       busy before calling this routine. The data
*                       is written to the character generator RAM or
*                       the display data RAM depending on what the
*                       previous SetxxRamAddr routine was called.

*******************************************************************************/
void putsXLCD(unsigned char x, unsigned char y, const unsigned char *discode0, const unsigned char *discode1, const unsigned char *discode2, const unsigned char *discode3, unsigned char c_width, unsigned char n, unsigned char inv)
{
    unsigned char i;
    unsigned char const *discode[4];

    discode[0] = discode0;
    discode[1] = discode1;
    discode[2] = discode2;
    discode[3] = discode3;

    for(i = 0; i < n; i++)
    {
        putrsXLCD(x + i * c_width, y, discode[i], c_width, inv);
    }
}
/********************************************************************
*       Function Name:  putblkXLCD
*       Return Value:   void
*       Parameters:     buffer: pointer to string
*       Description:    This routine writes a string of bytes to the
*                       Hitachi K0108 LCD controller. The user
*                       must check to see if the LCD controller is
*                       busy before calling this routine. The data
*                       is written to the character generator RAM or
*                       the display data RAM depending on what the
*                       previous SetxxRamAddr routine was called.
*
********************************************************************/

void putblkXLCD(unsigned char x, unsigned char y, unsigned char c_width, unsigned char ccode)   //c_width*8, x:0~192 left 2 right y:0~7 top 2 bottom
{
    WriteCmdXLCD(0xB0 | y);
    WriteCmdXLCD(0x10 |(x >> 4));
    WriteCmdXLCD(0x00 |(x & 0x0F));
    for(;c_width != 0; c_width --)
    {
        WriteDataXLCD(ccode);
    }
}

/*******************************************************************************
 *		 Function Name:  ClearXLCD									 *
 *		 Return Value:	 void										 *
 *		 Parameters:	 void										 *
 *		 Description:	 Clear ROM to the LCD	

*******************************************************************************/
void ClearXLCD(void)
{
    char i;
    for(i = 0; i < 8; i ++)
    {
        putblkXLCD(0, i, 128, 0);
    }
}

/*******************************************************************************
 *       Function Name:  PhotoXLCD                                   *
*       Return Value:   void                                        *
*       Parameters:     data                                        *
*       Description:    Display Photo to the LCD   
*******************************************************************************/
void PhotoXLCD(const unsigned char *data)
{
    unsigned char i,j;
    unsigned int count = 0;
    for(i = 0; i < 8; i ++)
    {
        for(j = 0; j < 128; j ++)
        {
            putblkXLCD(j, i, 1, data[count]);
            count ++;
        }
    }
}


/********************************************************************
*       Function Name:  OpenXLCD                                    *
*       Return Value:   void                                        *
*       Parameters:     lcdtype: sets the type of LCD (lines)       *
*       Description:    This routine configures the LCD. Based on   *
*                       the Hitachi HD44780 LCD controller. The     *
*                       routine will configure the I/O pins of the  *
*                       microcontroller, setup the LCD for 4- or    *
*                       8-bit mode and clear the display. The user  *
*                       must provide three delay routines:          *
*                       DelayFor18TCY() provides a 18 Tcy delay     *
*                       DelayPORXLCD() provides at least 15ms delay *
*                       DelayXLCD() provides at least 5ms delay     *
********************************************************************/
void OpenXLCD(void)
{
	static uint8_t s_ucfirst=0;
	if(s_ucfirst==0)
	{
		bsp_DelayMS(100);//主要作用是用于防治屏幕花屏，上电后延时一段时间后对液晶屏初始化	
		s_ucfirst=1;
	}
	
	CS0;
	bsp_DelayMS(2);
	
	RES_ON;
	bsp_DelayMS(2);
	RES_OFF;
	

	
	LIGHT_ON;

		 /* 模块厂家提供初始化代码 */
	WriteCmdXLCD(0xAE);	/* 关闭OLED面板显示(休眠) */
	
	WriteCmdXLCD(0xA2);//Select bias setting 1/9
	
	WriteCmdXLCD(0x2F);   //Control built-in power circuit ON/OFF VB VR VF
	WriteCmdXLCD(0x24);  // select resistor ratio Rb/Ra

	
	WriteCmdXLCD(0x10);	/* 设置列地址高4bit */
	WriteCmdXLCD(0x00);	/* 设置列地址低4bit */	
	

	WriteCmdXLCD(0xb0);//set page address
	WriteCmdXLCD(0x40);	/* 设置起始行地址（低5bit 0-63）， 硬件相关*/ //从中间开始
//	WriteCmdXLCD(0x60);//set column address //坐上为 00 

	WriteCmdXLCD(0x81);	/* 设置对比度命令(双字节命令），第1个字节是命令，第2个字节是对比度参数0-255 */
	WriteCmdXLCD(0x30);	/* 设置对比度参数,缺省3F */

	
	OLED_SetDir(OLED_t.OLED_Dir);	/* 设置显示方向 */				
	OLED_SetColoum_Offset(OLED_t.OLED_Dir);	//设置偏移量	

	WriteCmdXLCD(0xA6);	/* A6 : 设置正常显示模式; A7 : 设置为反显模式 */

//	WriteCmdXLCD(0xA8);	/* 设置COM路数 */ 没有这个指令集
//	WriteCmdXLCD(0x3F);	/* 1 ->（63+1）路 */

//	WriteCmdXLCD(0xD3);	/* 设置显示偏移（双字节命令）*/
//	WriteCmdXLCD(0x00);	/* 无偏移 */

	WriteCmdXLCD(0xD5);	/* 设置显示时钟分频系数/振荡频率 */
	WriteCmdXLCD(0x80);	/* 设置分频系数,高4bit是分频系数，低4bit是振荡频率 */

//	WriteCmdXLCD(0xD9);	/* 设置预充电周期 */
//	WriteCmdXLCD(0xF1);	/* [3:0],PHASE 1; [7:4],PHASE 2; */

//	WriteCmdXLCD(0xDA);	/* 设置COM脚硬件接线方式 */
//	WriteCmdXLCD(0x12);

//	WriteCmdXLCD(0xDB);	/* 设置 vcomh 电压倍率 */
//	WriteCmdXLCD(0x40);	/* [6:4] 000 = 0.65 x VCC; 0.77 x VCC (RESET); 0.83 x VCC  */

//	WriteCmdXLCD(0x8D);	/* 设置充电泵（和下个命令结合使用） */
//	WriteCmdXLCD(0x14);	/* 0x14 使能充电泵， 0x10 是关闭 */

	WriteCmdXLCD(0xAF);	/* 打开OLED面板 */
	

	bsp_DelayMS(10);
    /*???????*/

//   ON

	if(s_ucfirst==1)
	{
		bsp_DelayMS(100);//主要作用是用于防治屏幕花屏，上电后延时一段时间后对液晶屏初始化	
		s_ucfirst=2;
	}

}



	 
