#include "bsp.h"

uint16_t g_ucInput_ST;

//持续输入信号处理
#define RCC_ALL_INPUT (RCC_AHBPeriph_GPIOC )
//密码锁
#define GPIO_PORT_IN0  GPIOC
#define GPIO_PIN_IN0	GPIO_Pin_13


static uint8_t IsInputDown0(void) {if ((GPIO_PORT_IN0->IDR & GPIO_PIN_IN0) == 0) return 0;else return 1;}



/*************************************输入函数******************************************************/



void bsp_Input_Init(void);
static void bsp_SingleScan(uint8_t nIndex);
void bsp_InputScan(void);
 uint8_t GetInput(void);
/*
*********************************************************************************************************
*	函 数 名: bsp_Input_Init
*	功能说明: 长时输入初始化
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_Input_Init(void)
{
	
	GPIO_InitTypeDef GPIO_InitStructure;	
	 /* GPIOC Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_ALL_INPUT, ENABLE);
	
	/* 第2步：配置所有的按键GPIO为浮空输入模式 */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_IN0;	
	GPIO_Init(GPIO_PORT_IN0, &GPIO_InitStructure);
	

}




/*
*********************************************************************************************************
*	函 数 名: bsp_SingleScan
*	功能说明: 扫描所有按键。非阻塞，被systick中断周期性的调用
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
static void bsp_SingleScan(uint8_t nIndex)
{
	switch(nIndex)
	{
		case 0: if(IsInputDown0()) {g_ucInput_ST |=(1<<nIndex);} else{g_ucInput_ST &=~(1<<nIndex);} break;		

		default: break;
	}
}



/*
*********************************************************************************************************
*	函 数 名: bsp_InputScan
*	功能说明: 扫描所有长时输入信号，信号周期调用，被systick中断周期性的调用
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/


void bsp_InputScan(void)
{
	uint8_t i;

	for (i = 0; i < 1; i++)
	{		
		bsp_SingleScan(i);
	}
	
}

/*
*********************************************************************************************************
*	函 数 名: GetInput
*	功能说明: 读取长时信号状态
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
 uint8_t GetInput(void)
{
	uint8_t ret;

	/* 因为	g_AdcValue 变量在systick中断中改写，为了避免主程序读变量时被中断程序打乱导致数据错误，因此需要
	关闭中断进行保护 */

	/* 进行临界区保护，关闭中断 */
	__set_PRIMASK(1);  /* 关中断 */

	ret = g_ucInput_ST;

	__set_PRIMASK(0);  /* 开中断 */

	return ret;
}

