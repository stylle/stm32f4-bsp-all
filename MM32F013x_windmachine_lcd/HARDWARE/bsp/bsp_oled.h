/*
*********************************************************************************************************
*
*	模块名称 : TFT液晶显示器驱动模块
*	文件名称 : LCD_tft_lcd.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C), 2010-2011, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/


#ifndef _BSP_OLED_H
#define _BSP_OLED_H

#if 0
	#include "bsp_tft_lcd.h"	/* 仅使用了其中的 FONT_T 结构体 */
#else
	typedef struct
	{
		uint16_t FontCode;	/* 字体代码 0 表示16点阵 */
		uint16_t FrontColor;/* 字体颜色 */
		uint16_t BackColor;	/* 文字背景颜色，透明 */
		uint16_t Space;		/* 文字间距，单位 = 像素 */
	}FONT_T;

extern FONT_T tFont12, tFont16;
	/* 字体代码 */
	typedef enum
	{
		FC_ST_12 = 0,		/* 宋体12x12点阵 （宽x高） */
		FC_ST_16,			/* 宋体15x16点阵 （宽x高） */
		FC_ST_24,			/* 宋体24x24点阵 （宽x高） -- 暂时未支持 */
		FC_ST_32,			/* 宋体32x32点阵 （宽x高） -- 暂时未支持 */	
		
		FC_RA8875_16,		/* RA8875 内置字体 16点阵 */
		FC_RA8875_24,		/* RA8875 内置字体 24点阵 */
		FC_RA8875_32		/* RA8875 内置字体 32点阵 */	
	}FONT_CODE_E;
	
#define LCD_DIR_NOMAL 	0 //正常方向
#define LCD_DIR_REVERSE 1 //翻转180度
#define LCD_DIR_LEFT		2	//左向
#define LCD_DIR_RIGHT 	3	//右向
	typedef struct 
	{
		uint8_t OLED_Dir;//显示方向
		uint8_t OLED_Constract;//对比度
		uint8_t OLED_Disp_Mode;//显示模式 正常显示与反显
		
		uint8_t OLED_Coloum_Offset;//偏移量 方便调整显示方向
		
	
	}OLED_T;
	
	extern OLED_T OLED_t;
	
#endif

/* 可供外部模块调用的函数 */
void OLED_InitHard(void);
void OLED_InitSoft(void);
void OLED_DispOn(void);
void OLED_DispOff(void);
void OLED_SetDir(uint8_t _ucDir);
void OLED_SetColoum_Offset(uint8_t _ucDir);
void OLED_SetContrast(uint8_t ucValue);
void OLED_StartDraw(void);
void OLED_EndDraw(void);
void OLED_ClrScr(uint8_t _ucMode);
void OLED_DispStr(uint16_t _usX, uint16_t _usY, char *_ptr, FONT_T *_tFont);
void OLED_PutPixel(uint16_t _usX, uint16_t _usY, uint8_t _ucColor);
uint8_t OLED_GetPixel(uint16_t _usX, uint16_t _usY);
void OLED_DrawLine(uint16_t _usX1 , uint16_t _usY1 , uint16_t _usX2 , uint16_t _usY2 , uint8_t _ucColor);
void OLED_DrawPoints(uint16_t *x, uint16_t *y, uint16_t _usSize, uint8_t _ucColor);
void OLED_DrawRect(uint16_t _usX, uint16_t _usY, uint8_t _usHeight, uint16_t _usWidth, uint8_t _ucColor);
void OLED_DrawCircle(uint16_t _usX, uint16_t _usY, uint16_t _usRadius, uint8_t _ucColor);
void OLED_DrawBMP(uint16_t _usX, uint16_t _usY, uint16_t _usHeight, uint16_t _usWidth, uint8_t *_ptr);

#endif


