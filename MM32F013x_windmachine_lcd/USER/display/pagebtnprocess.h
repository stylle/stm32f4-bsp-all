/* 
 * File:   pagebtnprocess.h
 * Author: huangzhiming
 *
 * Created on April 17, 2018, 3:44 PM
 */

#ifndef PAGEBTNPROCESS_H
#define	PAGEBTNPROCESS_H

#ifdef	__cplusplus
extern "C" {
#endif



/*********************************************页面按键处理函数函数*****************************************************/
void Key_SoundBtnProcess(uint16_t keycode);
void ParaPageBtnProcess(unsigned int keycode);
void StatePageBtnProcess(unsigned int keycode);
void HistoryPage_BtnProcess(unsigned int keycode);
	
unsigned char UserPasswordBtnProcess(unsigned int keycode);
unsigned char FactoryPasswordBtnProcess(unsigned int keycode);

void FactoryPage_BtnProcess(unsigned int keycode);


void SaveBtnProcess(unsigned int keycode);



#ifdef	__cplusplus
}
#endif

#endif	/* PAGEBTNPROCESS_H */

