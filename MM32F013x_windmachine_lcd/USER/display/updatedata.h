/* 
 * File:   display_fun.h
 * Author: huangzhiming
 *
 * Created on April 16, 2018, 5:08 PM
 */

#ifndef DISPLAY_UPTE_H
#define	DISPLAY_UPTE_H

#ifdef	__cplusplus
extern "C" {
#endif



/*********************************************页面更新显示函数*****************************************************/
void UpdateFactoryPage(unsigned char _page);




void UpdateParaPage(void);

void UpdateStatePage(void);

void UpdateHistoryPage(void);

void UpdateUserInput(void);



#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAY_FUN_H */

