#include "displaypacket.h"
#include "displaypage.h"
#include "display.h"
#include  "data.h"
#include  "bsp.h"
#include "pagebtnprocess.h"



/*********************************************页面按键处理函数函数*****************************************************/
void Key_SoundBtnProcess(uint16_t keycode);
void ParaPageBtnProcess(unsigned int keycode);
void StatePageBtnProcess(unsigned int keycode);
void HistoryPage_BtnProcess(unsigned int keycode);

unsigned char UserPasswordBtnProcess(unsigned int keycode);
unsigned char FactoryPasswordBtnProcess(unsigned int keycode);

void FactoryPage_BtnProcess(unsigned int keycode);


void SaveBtnProcess(unsigned int keycode);





/*
*********************************************************************************************************
*	函 数 名: Key_SoundBtnProcess
*	功能说明: 按键声。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void Key_SoundBtnProcess(uint16_t keycode)
{
    switch(keycode)
    {

    case KEY_1_DOWN:

    case KEY_2_DOWN:

    case KEY_3_DOWN:

    case KEY_4_DOWN:

    case KEY_5_DOWN:

    case KEY_6_DOWN:

    case KEY_7_DOWN:

    case KEY_8_DOWN:

    case KEY_1_LONG:
    {
//					BEEP_KeyTone();
        break;
    }
    default:
        break;
    }
}




/*
*********************************************************************************************************
*	函 数 名: ParaPageBtnProcess
*	功能说明: 参数界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void ParaPageBtnProcess(unsigned int keycode)
{
    switch(keycode)
    {
    case KEY_1_DOWN:    {        DispControler.ucPageNum=USERPASSWORDPAGE;        break;    }
    case KEY_1_LONG:
    {
        if( MachineFlag.Bits.low_Oload == 1
                || MachineFlag.Bits.high_Oload ==1
          )
        {
            Set_Clear_Overload_Switch(1);
        }
        break;
    }
    case KEY_2_DOWN:
    {
#if DISPLAY_HISTORY
        DispControler.ucPageNum=HISTORY_PAGE;
#else
        DispControler.ucPageNum=STATE_PAGE;
#endif

        break;
    }
    case KEY_3_DOWN:    {        DispControler.ucPageNum=STATE_PAGE;        break;    }
    case KEY_4_DOWN:
		{
        if(STAer.ucAuthor==1)
        {
            if(MachineFlag.Bits.auto_bit ==1 )  Set_Auto_Switch(0);
			else Set_Auto_Switch(1);
        }
        break;
    }
    case KEY_5_DOWN:
    {
        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=STATE_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                if(getFactoryPara(MachineType_E) == 1) //双速风机   
                    StartStopLowSpeed(1);              
                else  //单速风机             
                    StartStopHighSpeed(1);           
            }
        }
        break;
    }
    case KEY_6_DOWN:
    {
        if(MachineFlag.Bits.comm_error)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=STATE_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                if(getFactoryPara(MachineType_E) == 1) //双速风机
                {
                    StartStopLowSpeed(0);

                }               
            }
        }
        break;
    }
    case KEY_7_DOWN:
    {
        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=STATE_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                if(getFactoryPara(MachineType_E)== 1) //双速风机
                {
                    StartStopHighSpeed(1);
                }
            }
        }


        break;
    }
    case KEY_8_DOWN:
    {
        if(MachineFlag.Bits.comm_error)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=STATE_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                StartStopHighSpeed(0);
            }
        }

        break;
    }
    case KEY_9_DOWN:
    {
        DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
        break;
    }
    default:
        break;
    }

}


/*
*********************************************************************************************************
*	函 数 名: StatePageBtnProcess
*	功能说明: 状态界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void StatePageBtnProcess(unsigned int keycode)
{
    switch(keycode)
    {
    case KEY_1_DOWN:
    {
        DispControler.ucPageNum=USERPASSWORDPAGE;
        break;
    }
    case KEY_1_LONG:
    {
        if( MachineFlag.Bits.low_Oload == 1
                || MachineFlag.Bits.high_Oload ==1
          )
        {
            Set_Clear_Overload_Switch(1);
        }
        break;
    }
    case KEY_2_DOWN:
    {
        DispControler.ucPageNum=PARAMETER_PAGE;
        break;
    }
    case KEY_3_DOWN:
    {
#if DISPLAY_HISTORY
        DispControler.ucPageNum=HISTORY_PAGE;
#else
        DispControler.ucPageNum=PARAMETER_PAGE;
#endif
        break;
    }
    case KEY_4_DOWN:
    {
        if(STAer.ucAuthor==1)
        {
            if(MachineFlag.Bits.auto_bit  ==1 )
            {
                Set_Auto_Switch(0);
            }
            else
            {
                Set_Auto_Switch(1);
            }
        }
        break;
    }
    case KEY_5_DOWN:
    {
        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=PARAMETER_PAGE;
#endif

        }
        else
        {

            if(STAer.ucAuthor==1)
            {

                if(getFactoryPara(MachineType_E) == 1) //双速风机
                {
                    StartStopLowSpeed(1);

                }
                else  //单速风机
                {
                    StartStopHighSpeed(1);

                }
            }
        }

        break;
    }
    case KEY_6_DOWN:
    {

        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=PARAMETER_PAGE;
#endif

        }
        else
        {

            if(STAer.ucAuthor==1)
            {
                if(getFactoryPara(MachineType_E) == 1) //双速风机
                {
                    StartStopLowSpeed(0);

                }
                else  //单速风机
                {

                }
            }
        }


        break;
    }
    case KEY_7_DOWN:
    {

        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=PARAMETER_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                if(getFactoryPara(MachineType_E) == 1) //双速风机
                {
                    StartStopHighSpeed(1);
                }
            }
        }
        break;
    }
    case KEY_8_DOWN:
    {

        if(MachineFlag.Bits.comm_error ==1)
        {
#if DISPLAY_HISTORY
            DispControler.ucPageNum=HISTORY_PAGE;
#else
            DispControler.ucPageNum=PARAMETER_PAGE;
#endif
        }
        else
        {
            if(STAer.ucAuthor==1)
            {
                 StartStopHighSpeed(0);
            }
        }
        break;
    }
    case KEY_9_DOWN:
    {
        DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
        break;
    }
    default:
        break;
    }
}

/*
*********************************************************************************************************
*	函 数 名: StatePageBtnProcess
*	功能说明: 状态界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void HistoryPage_BtnProcess(unsigned int keycode)
{
    switch(keycode)
    {
    case KEY_1_DOWN:
    {
        DispControler.ucPageNum=USERPASSWORDPAGE;
        break;
    }
    case KEY_1_LONG:
    {
        if( MachineFlag.Bits.low_Oload == 1
                || MachineFlag.Bits.high_Oload ==1
          )
        {
            Set_Clear_Overload_Switch(1);
        }
        break;
    }
    case KEY_2_DOWN:
    {
        DispControler.ucPageNum=STATE_PAGE;
        break;
    }
    case KEY_3_DOWN:
    {
        DispControler.ucPageNum=PARAMETER_PAGE;
        break;
    }
    case KEY_4_DOWN:
    {
        if(STAer.ucAuthor==1)
        {
            if(MachineFlag.Bits.auto_bit ==1 )
            {
                Set_Auto_Switch(0);
            }
            else
            {
                Set_Auto_Switch(1);
            }
        }
        break;
    }
    case KEY_5_DOWN:
    {

        if(STAer.ucAuthor==1)
        {

            if(getFactoryPara(MachineType_E) == 1) //双速风机
            {
                StartStopLowSpeed(1);

            }
            else  //单速风机
            {
                StartStopHighSpeed(1);
            }
        }
        break;
    }
    case KEY_6_DOWN:
    {
        if(STAer.ucAuthor==1)
        {
            if(getFactoryPara(MachineType_E)== 1) //双速风机
            {
                StartStopLowSpeed(0);
            }
        }
        break;
    }
    case KEY_7_DOWN:
    {
        if(STAer.ucAuthor==1)
        {
            if(getFactoryPara(MachineType_E) == 1) //双速风机
            {
                StartStopHighSpeed(1);
            }
        }
        break;
    }
    case KEY_8_DOWN:
    {
        if(STAer.ucAuthor==1)
        {
                StartStopHighSpeed(0);
        }
        break;
    }
    case KEY_9_DOWN:
    {
        DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
        break;
    }
    default:
        break;
    }
}

/*
*********************************************************************************************************
*	函 数 名: ParaPageBtnProcess
*	功能说明: 参数界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

unsigned char UserPasswordBtnProcess(unsigned int keycode)
{
    switch(keycode)
    {
    case KEY_1_DOWN:
    {
        if( UserInput.uiPwdNum == getUserPwder() )
        {
            STAer.ucAuthor = 1; //授权
            STAer.ucAuthor_time =0;//授权时间开始
            DispControler.ucPageNum = STATE_PAGE;
            ClearUserInput();
            UserInput.ucSelected = 0;
            return 1;
        }
        else
        {
            DispPwdError();
            ClearUserInput();
            UserInput.ucSelected = 0;
            STAer.ucAuthor=0;
            return 0;
        }
    }
    case KEY_1_LONG:
    {
        if( MachineFlag.Bits.low_Oload == 1
         || MachineFlag.Bits.high_Oload ==1
          )
        {
            Set_Clear_Overload_Switch(1);
        }
        break;
    }
    case KEY_2_DOWN:
    {
        ClearPwdError();
        UserInput.ucSelected++;
        if( UserInput.ucSelected>2 )
        {
            UserInput.ucSelected = 0;
        }
        break;
    }
    case KEY_3_DOWN:
    {
        ClearPwdError();
        if( ++UserInput.ucPwdBit[UserInput.ucSelected]>9 )
        {
            UserInput.ucPwdBit[UserInput.ucSelected] = 0;
        }
		PwderToMerge(&UserInput);
        break;
    }
    case KEY_4_DOWN:
    {
        ClearUserInput();
        DispControler.ucPageNum=STATE_PAGE;
        break;
    }
    case KEY_5_DOWN:
    {

        break;
    }
	case KEY_5_LONG:
    {
		RecoverInit();
        break;
    }
	
    case KEY_6_DOWN:
    {

        break;
    }
    case KEY_7_DOWN:
    {

        break;
    }
    case KEY_8_DOWN:
    {
        break;
    }
    case KEY_9_DOWN:
    {
        DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
        break;
    }
    default:
        break;
    }
    return 0;
}


/*
*********************************************************************************************************
*	函 数 名: FactoryPasswordBtnProcess
*	功能说明: 工厂模式密码输入界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

unsigned char FactoryPasswordBtnProcess(unsigned int keycode)
{


    switch(keycode)
    {
    case KEY_1_DOWN:
    {
//				DispControler.ucIndex = 0;
//				DispControler.ucPreSel = 0;
//				DispControler.ucSel = 0;
//				ClearUserInput();
//				DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
//				break;

        if(Compary_Pwder(&FactoryPageParaer.factory_PWD,&UserInput)  )
        {
            DispPwdError();
            ClearUserInput();
            UserInput.ucSelected = 0;
            return 0;
        }
        else
        {
            DispControler.ucPageNum = FACTORYPAGE_ONE;
            ClearUserInput();
            UserInput.ucSelected = 0;
            return 1;
        }

    }
    case KEY_1_LONG:
    {
        if( MachineFlag.Bits.low_Oload == 1
                || MachineFlag.Bits.high_Oload ==1
          )
        {
            Set_Clear_Overload_Switch(1);
        }
        break;
    }
    case KEY_2_DOWN:
    {
        ClearPwdError();
        UserInput.ucSelected++;
        if( UserInput.ucSelected>2 )
        {
            UserInput.ucSelected = 0;
        }
        break;
    }
    case KEY_3_DOWN:
    {
        ClearPwdError();
        if( ++UserInput.ucPwdBit[UserInput.ucSelected]>9 )
        {
            UserInput.ucPwdBit[UserInput.ucSelected] = 0;
        }
        PwderToMerge(&UserInput);
        break;
    }
    case KEY_4_DOWN:
    {
        ClearUserInput();
        DispControler.ucPageNum=STATE_PAGE;
    }
    case KEY_5_DOWN:
    {

        break;
    }
	case KEY_5_LONG:
    {
		RecoverInit();
        break;
    }
	
    case KEY_6_DOWN:
    {

        break;
    }
    case KEY_7_DOWN:
    {

        break;
    }
    case KEY_8_DOWN:
    {

        break;
    }
    case KEY_9_DOWN:
    {
        DispControler.ucPageNum=FACTORYPQSSWORDPAGE;
        break;
    }
    default:
        break;
    }
    return 0;
}
/*
*********************************************************************************************************
*	函 数 名: FactoryPage_BtnProcess
*	功能说明: 工厂设置1界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void FactoryPage_BtnProcess(unsigned int keycode)
{
	#define 	FACTORY_PAGE_MAX_NUM 16
	
    switch(keycode)
    {
    case KEY_1_DOWN:
    {
        if(DispControler.ucSel==1||DispControler.ucSel==2)
        {
            if( ++DispControler.ucIndex >= 3 )
            {
                DispControler.ucIndex = 0;
            }

        }
        else
        {

            if( DispControler.ucIndex == 0 )
            {
                DispControler.ucIndex = 1;
            }
            else
            {
                DispControler.ucIndex = 0;
            }
        }
        break;
    }
    case KEY_2_DOWN:
    {
        if( DispControler.ucIndex == 0 )
        {
            DispControler.ucSel++;
            if(DispControler.ucSel > FACTORY_PAGE_MAX_NUM)
            {
                DispControler.ucSel = 0;
            }
			 DispControler.ucPageNum = FACTORYPAGE_ONE + DispControler.ucSel/4;			
        }
        else
        {
            switch(DispControler.ucSel)
            {
            case 0:
            {
                if(getUserFactoryPara(MachineType_E))
                {
                    setUserFactoryPara( MachineType_E, 0);
                }
				else
				{
					setUserFactoryPara( MachineType_E, 1);
				}
                break;
            }
            case 1:	//高速调试
            {
                if(DispControler.ucIndex==1) //粗调
                {
                    if(getFactoryPara(MachineType_E)== 1)//双速风机 调高速联动调低速
                    {
						setUserFactoryPara(H_Power_Select_E, getUserFactoryPara(H_Power_Select_E)+1);
                        if(getUserFactoryPara(H_Power_Select_E)>=DOUBLE_H_POWER_LIST_NUM)
                        {
							setUserFactoryPara(H_Power_Select_E, 0);
                        }
						setPwderNum(&UserInputFactoryPageParaer.H_Power,DOUBLE_H_POWER_LIST[getUserFactoryPara(H_Power_Select_E)]);

                        UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select = getUserFactoryPara(H_Power_Select_E);
					
						setPwderNum(&UserInputFactoryPageParaer.L_Power,DOUBLE_L_POWER_LIST[UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select]);
                    }
                    else //单速风机
                    {
						setUserFactoryPara(H_Power_Select_E, getUserFactoryPara(H_Power_Select_E)+1);
                        if(getUserFactoryPara(H_Power_Select_E) >=SINGLE_POWER_LIST_NUM)
                        {
                            setUserFactoryPara(H_Power_Select_E, 0);
                        }

						setPwderNum(&UserInputFactoryPageParaer.H_Power,SINGLE_POWER_LIST[getUserFactoryPara(H_Power_Select_E)]);

                    }
                }
                else//微调
                {
                    DispControler.ucIndex++;
                    if( DispControler.ucIndex > 4 )
                    {
                        DispControler.ucIndex = 2;
                    }
                }

                break;
            }
            case 2: //低速调试
            {
                if(DispControler.ucIndex==1) //粗调
                {
                    if(++UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select >= DOUBLE_L_POWER_LIST_NUM)
                    {
                        UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select=0;
                    }					
					setPwderNum(&UserInputFactoryPageParaer.L_Power,DOUBLE_L_POWER_LIST[UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select]);

                }
                else//微调
                {
                    DispControler.ucIndex++;
                    if( DispControler.ucIndex > 4 )
                    {
                        DispControler.ucIndex = 2;
                    }
                }
                break;
            }
            case 3: {	UserInputFactoryPageParaer.FactoryParamember.usPower_Factor++; break;}
            case 4:
            {
                DispControler.ucIndex++;
                if( DispControler.ucIndex > 2 )
                {
                    DispControler.ucIndex = 1;
                }
                break;
            }
            case 5:	{	DispControler.ucIndex = 0;	break;	}
            case 6:	{	DispControler.ucIndex = 0;	break;	}
            case 7:
            {
                DispControler.ucIndex++;
                if( DispControler.ucIndex > 3 )
                {
                    DispControler.ucIndex = 1;
                }
                break;
            }
            case 8:
            {
				 if(getUserFactoryPara(OverLimit_Switch_E))
                {
                    setUserFactoryPara( OverLimit_Switch_E, 0);
                }
				else
				{
					setUserFactoryPara( OverLimit_Switch_E, 1);
				}
                break;
            }
            case 9:
            {						
                UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time++;
                if(UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time>=AD_SAMPLE_MAX_TIME)
                {
                    UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time=AD_SAMPLE_MAX_TIME;
                }
                break;
            }
            case 10:
            {
				if(getUserFactoryPara(usCheckout_Switch_E))
                {
                    setUserFactoryPara( usCheckout_Switch_E, 0);
                }
				else
				{
					setUserFactoryPara( usCheckout_Switch_E, 1);
				}
                break;
            }
			case 11:
            {
                DispControler.ucIndex++;
                if( DispControler.ucIndex > 3 )
                {
                    DispControler.ucIndex = 1;
                }
                break;
            }
			case 12:
            {
                DispControler.ucIndex++;
                if( DispControler.ucIndex > 3 )
                {
                    DispControler.ucIndex = 1;
                }
                break;
            }
			case 13:
            {
                DispControler.ucIndex++;
                if( DispControler.ucIndex > 3 )
                {
                    DispControler.ucIndex = 1;
                }
                break;
            }
			case 14:
            {		 
                UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG++;
				if( getUserFactoryPara(DS_FEEDBACK_FLAG_E) > RELAY_MACHINE_RUNNING)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG  =RELAY_AUTOBIT_E;
				}
                break;
            }
			case 15:
            {		 
				UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG++;
				if( getUserFactoryPara(DS_ERROR_FLAG_E)  > RELAY_MACHINE_RUNNING)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG  =RELAY_AUTOBIT_E;
				}
                break;
            }
			case 16:
            {		 
				UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG++;
				if( getUserFactoryPara(DJYX_FLAG_E) > RELAY_MACHINE_RUNNING)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG  =RELAY_AUTOBIT_E;
				}
                break;
            }
            default:
                break;
            }
        }
        break;
    }
    case KEY_3_DOWN:
    {
        if( DispControler.ucIndex == 0 )
        {
            DispControler.ucSel--;		 
			if(DispControler.ucSel < 0)
            {
                DispControler.ucSel = FACTORY_PAGE_MAX_NUM;                
            }			
			DispControler.ucPageNum = FACTORYPAGE_ONE + DispControler.ucSel/4;		
        }
        else
        {
            switch(DispControler.ucSel)
            {
            case 0:
            {
                if(getUserFactoryPara(MachineType_E))
                {
                    setUserFactoryPara( MachineType_E, 0);
                }
				else
				{
					 setUserFactoryPara( MachineType_E, 1);
				}
                break;
            }
            case 1:
            {
                if(DispControler.ucIndex==1) //粗调
                {
                    if(getFactoryPara(MachineType_E)== 1)//双速风机 调高速联动调低速
                    {

                        if( getUserFactoryPara(H_Power_Select_E)==0)
                        {
							setUserFactoryPara(H_Power_Select_E,DOUBLE_H_POWER_LIST_NUM);
                        }
						setUserFactoryPara(H_Power_Select_E,getUserFactoryPara(H_Power_Select_E) -1);

						setPwderNum(&UserInputFactoryPageParaer.H_Power,DOUBLE_H_POWER_LIST[getUserFactoryPara(H_Power_Select_E)]);

                        UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select = getUserFactoryPara(H_Power_Select_E);
						setPwderNum(&UserInputFactoryPageParaer.L_Power,DOUBLE_L_POWER_LIST[UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select]);
                    }
                    else //单速风机
                    {

                        if(getUserFactoryPara(H_Power_Select_E)==0)
                        {
							setUserFactoryPara(H_Power_Select_E,SINGLE_POWER_LIST_NUM);
                        }
						setUserFactoryPara(H_Power_Select_E,getUserFactoryPara(H_Power_Select_E) -1);

						setPwderNum(&UserInputFactoryPageParaer.H_Power,SINGLE_POWER_LIST[getUserFactoryPara(H_Power_Select_E)]);
                    }
                }
                else//微调
                {
                    UserInputFactoryPageParaer.H_Power.ucPwdBit[DispControler.ucIndex-2]++;
                    if( UserInputFactoryPageParaer.H_Power.ucPwdBit[DispControler.ucIndex-2] > 9 )
                    {
                        UserInputFactoryPageParaer.H_Power.ucPwdBit[DispControler.ucIndex-2] = 0;
                    }
                    PwderToMerge(&UserInputFactoryPageParaer.H_Power);
                }

                break;
            }
            case 2:
            {
                if(DispControler.ucIndex==1) //粗调
                {
                    if(UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select==0)
                    {
                        UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select=DOUBLE_L_POWER_LIST_NUM;
                    }
                    UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select--;
					
					setPwderNum(&UserInputFactoryPageParaer.L_Power,DOUBLE_L_POWER_LIST[UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select]);
                }
                else//微调
                {
                    UserInputFactoryPageParaer.L_Power.ucPwdBit[DispControler.ucIndex-2]++;
                    if( UserInputFactoryPageParaer.L_Power.ucPwdBit[DispControler.ucIndex-2] > 9 )
                    {
                        UserInputFactoryPageParaer.L_Power.ucPwdBit[DispControler.ucIndex-2] = 0;
                    }
                    PwderToMerge(&UserInputFactoryPageParaer.L_Power);
                }
                break;
            }
            case 3:
            {
                UserInputFactoryPageParaer.FactoryParamember.usPower_Factor--;
                break;
            }
            case 4:
            {
                if(DispControler.ucIndex ==1 )
                {
					setUserFactoryPara( FaultSwitch_E, getUserFactoryPara(FaultSwitch_E)+1);
                    if(getUserFactoryPara(FaultSwitch_E)>2)
                    {
						setUserFactoryPara( FaultSwitch_E, 0);
                    }
                }
                else if(DispControler.ucIndex ==2)
                {	
                    if(getUserFactoryPara(Lcd_light_E))
						setUserFactoryPara( Lcd_light_E, 0);
                    else setUserFactoryPara( Lcd_light_E, 1);
                }
                break;
            }
            case 5:
            {
                DispControler.ucIndex = 0;
                break;
            }
            case 6:
            {
                DispControler.ucIndex = 0;
                break;
            }
            case 7:
            {
                UserInputFactoryPageParaer.factory_PWD.ucPwdBit[DispControler.ucIndex-1]++;
                if( UserInputFactoryPageParaer.factory_PWD.ucPwdBit[DispControler.ucIndex-1] > 9 )
                {
                    UserInputFactoryPageParaer.factory_PWD.ucPwdBit[DispControler.ucIndex-1] = 0;
                }
                PwderToMerge(&UserInputFactoryPageParaer.factory_PWD);
                break;
            }

            case 8:
            {
				if(getUserFactoryPara(OverLimit_Switch_E))
                {
                    setUserFactoryPara( OverLimit_Switch_E, 0);
                }
				else
				{
					setUserFactoryPara( OverLimit_Switch_E, 1);
				}
                break;
            }
            case 9:
            {
                UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time--;
                if( getUserFactoryPara(AD_Sample_Time_E)<=AD_SAMPLE_MIN_TIME)
                {
                    UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time=AD_SAMPLE_MIN_TIME;
                }
                break;
            }
            case 10:
            {						 
				 if(getUserFactoryPara(usCheckout_Switch_E))
                {
                    setUserFactoryPara( usCheckout_Switch_E, 0);
                }
				else
				{
					setUserFactoryPara( usCheckout_Switch_E, 1);
				}
                break;
            }
			case 11:
            {		 
                UserInputFactoryPageParaer.user_PWD.ucPwdBit[DispControler.ucIndex-1]++;
                if( UserInputFactoryPageParaer.user_PWD.ucPwdBit[DispControler.ucIndex-1] > 9 )
                {
                    UserInputFactoryPageParaer.user_PWD.ucPwdBit[DispControler.ucIndex-1] = 0;
                }
                PwderToMerge(&UserInputFactoryPageParaer.user_PWD);
                break;
            }
			case 12:
            {		 
                UserInputFactoryPageParaer.VoltageMax.ucPwdBit[DispControler.ucIndex-1]++;
                if( UserInputFactoryPageParaer.VoltageMax.ucPwdBit[DispControler.ucIndex-1] > 9 )
                {
                    UserInputFactoryPageParaer.VoltageMax.ucPwdBit[DispControler.ucIndex-1] = 0;
                }
                PwderToMerge(&UserInputFactoryPageParaer.VoltageMax);
                break;
            }
			case 13:
            {		 
                UserInputFactoryPageParaer.VoltageMin.ucPwdBit[DispControler.ucIndex-1]++;
                if( UserInputFactoryPageParaer.VoltageMin.ucPwdBit[DispControler.ucIndex-1] > 9 )
                {
                    UserInputFactoryPageParaer.VoltageMin.ucPwdBit[DispControler.ucIndex-1] = 0;
                }
                PwderToMerge(&UserInputFactoryPageParaer.VoltageMin);
                break;
            }			
			case 14:
            {	
				if( getUserFactoryPara(DS_FEEDBACK_FLAG_E) <= RELAY_AUTOBIT_E)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG  =RELAY_MACHINE_RUNNING + 1;
				}						
                UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG--;
                break;
            }
			case 15:
            {	
				if(getUserFactoryPara(DS_ERROR_FLAG_E)<= RELAY_AUTOBIT_E)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG  =RELAY_MACHINE_RUNNING + 1;
				}					
				UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG--;
                break;
            }
			case 16:
            {		
				if(getUserFactoryPara(DJYX_FLAG_E)<= RELAY_AUTOBIT_E)
				{
					UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG  =RELAY_MACHINE_RUNNING + 1;
				}				
				UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG--;
				
                break;
            }
            default:
                break;
            }

        }
        break;
    }
    case KEY_4_DOWN:
    {

        if( DispControler.ucIndex == 0)
        {
            DispControler.ucPrePageNum = DispControler.ucPageNum;
            DispControler.ucPreSel = 0;
            DispControler.ucSel = 0;

            /* 参数是否修改 */
            if( 		Compary_Pwder(&FactoryPageParaer.factory_PWD,&UserInputFactoryPageParaer.factory_PWD)
					||  Compary_Pwder(&FactoryPageParaer.user_PWD,&UserInputFactoryPageParaer.user_PWD)			
                    ||  Compary_Pwder(&FactoryPageParaer.H_Power,&UserInputFactoryPageParaer.H_Power)
                    ||  Compary_Pwder(&FactoryPageParaer.L_Power,&UserInputFactoryPageParaer.L_Power)			
			        ||  Compary_Pwder(&FactoryPageParaer.VoltageMax,&UserInputFactoryPageParaer.VoltageMax)
					||  Compary_Pwder(&FactoryPageParaer.VoltageMin,&UserInputFactoryPageParaer.VoltageMin)
					||	Compary_FactoryMember((uint8_t *)&FactoryPageParaer.FactoryParamember,(uint8_t *)&UserInputFactoryPageParaer.FactoryParamember)
			)
			{
                DispControler.ucPageNum = SAVEPAGE ;
            }
            else
            {
                ClearFactoryUserInput();
                DispControler.ucPageNum = STATE_PAGE;
            }
        }
        else
        {
            if(DispControler.ucSel==1||DispControler.ucSel==2)
            {
                if( DispControler.ucIndex >= 1)
                {
                    DispControler.ucIndex = 0;
                }
            }
        }
        break;
    }
    case KEY_5_DOWN:
    {

        break;
    }
    case KEY_6_DOWN:
    {

        break;
    }
    case KEY_7_DOWN:
    {

        break;
    }
    case KEY_8_DOWN:
    {

        break;
    }
    case KEY_9_DOWN:
    {

        break;
    }
    default:
        break;
    }
}

/*
*********************************************************************************************************
*	函 数 名: SaveBtnProcess
*	功能说明: 保存界面按键控制。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void SaveBtnProcess(unsigned int keycode)
{
    switch(keycode)
    {
    case KEY_1_DOWN:
    {
        uint8_t s_Save=0;
        DispControler.ucPageNum = STATE_PAGE;

        if( Compary_Pwder(&FactoryPageParaer.factory_PWD,&UserInputFactoryPageParaer.factory_PWD) )
        {
            CopyPwderToInput(&UserInputFactoryPageParaer.factory_PWD,&FactoryPageParaer.factory_PWD);
            s_Save=1;
        }
		
		if( Compary_Pwder(&FactoryPageParaer.user_PWD,&UserInputFactoryPageParaer.user_PWD) )
        {
            CopyPwderToInput(&UserInputFactoryPageParaer.user_PWD,&FactoryPageParaer.user_PWD);
            s_Save=1;
        }

        if(Compary_Pwder(&FactoryPageParaer.H_Power,&UserInputFactoryPageParaer.H_Power) ) //修改功率
        {
            if(UserInputFactoryPageParaer.H_Power.uiPwdNum <= HIGH_POWER_MAX)			//防止超限
            {
                Set_High_Power_Switch(get_PwdNum(UserInput_H_Power_E));
            }
        }
        if(Compary_Pwder(&FactoryPageParaer.L_Power,&UserInputFactoryPageParaer.L_Power) )
        {
            if(get_PwdNum(UserInput_L_Power_E) <= LOW_POWER_MAX)	 //防止超限
            {
                Set_Low_Power_Switch(get_PwdNum(UserInput_L_Power_E) );
            }
        }
		
		if(Compary_Pwder(&FactoryPageParaer.VoltageMax,&UserInputFactoryPageParaer.VoltageMax) )
        {
            if(get_PwdNum(UserInput_VoltageMax_E) <= VOLTAGE_MAX)	 //防止超限
            {
                Set_VoltageMax_Switch(get_PwdNum(UserInput_VoltageMax_E));
            }
        }
		
		if(Compary_Pwder(&FactoryPageParaer.VoltageMin,&UserInputFactoryPageParaer.VoltageMin) )
        {
            if(get_PwdNum(UserInput_VoltageMin_E) >= VOLTAGE_MIN)	 //防止超限
            {
                Set_VoltageMin_Switch(get_PwdNum(UserInput_VoltageMin_E));
            }
        }		

        if( getUserFactoryPara(Power_Factor_E) !=  getFactoryPara(Power_Factor_E))
        {
            Set_Power_Factor_Switch( getUserFactoryPara(Power_Factor_E));
        }

        if(getUserFactoryPara(MachineType_E) != getFactoryPara(MachineType_E))
        {
            Set_Machine_Type_Switch(getUserFactoryPara(MachineType_E));
        }

        if( getUserFactoryPara(OverLimit_Switch_E) !=  getFactoryPara(OverLimit_Switch_E))
        {
            Set_Display_AD_Switch(getUserFactoryPara(OverLimit_Switch_E));
        }
        //修改采样时间
        if( getUserFactoryPara(AD_Sample_Time_E) !=  getFactoryPara(AD_Sample_Time_E))
        {
            Set_Sample_Time_Switch(getUserFactoryPara(AD_Sample_Time_E));
        }

        if(getUserFactoryPara(usOverLoad_multiply_E) != getFactoryPara(usOverLoad_multiply_E))
        {
            Set_OloadMultply_Switch(getUserFactoryPara(usOverLoad_multiply_E) );
        }

	    if(getUserFactoryPara(usCheckout_Switch_E) != getFactoryPara(usCheckout_Switch_E))
        {
            Set_CheckOut_Switch(getUserFactoryPara(usCheckout_Switch_E) );
        }
		//继电器设置
        if(getUserFactoryPara(DS_FEEDBACK_FLAG_E)!= getFactoryPara(DS_FEEDBACK_FLAG_E))
        {
            Set_DS_FEEDBACK_Switch(getUserFactoryPara(DS_FEEDBACK_FLAG_E));
        }

        if(getUserFactoryPara(DS_ERROR_FLAG_E)!= getFactoryPara(DS_ERROR_FLAG_E))
        {
            Set_DS_ERROR_Switch(getUserFactoryPara(DS_ERROR_FLAG_E));
        }

	    if(getUserFactoryPara(DJYX_FLAG_E)!=  getFactoryPara(DJYX_FLAG_E))
        {
            Set_DJYX_Switch( getUserFactoryPara(DJYX_FLAG_E));
        }
		//继电器设置
		
        if(getUserFactoryPara(FaultSwitch_E) != getFactoryPara(FaultSwitch_E))
        {
			setFactoryPara(FaultSwitch_E,getFactoryPara(FaultSwitch_E));
            s_Save=1;
        }
        if(getUserFactoryPara(Lcd_light_E) != getFactoryPara(Lcd_light_E))
        {
			setFactoryPara(Lcd_light_E,getFactoryPara(Lcd_light_E));
            s_Save=1;
        }
        if(s_Save)
        {
            Flash_Write_Update();
        }
        ClearFactoryUserInput();
        break;
    }
    case KEY_2_DOWN:
    {

        break;
    }
    case KEY_3_DOWN:
    {

        break;
    }
    case KEY_4_DOWN:
    {
		ClearFactoryUserInput();
        DispControler.ucPageNum = STATE_PAGE;
        break;
    }
    case KEY_5_DOWN:
    {

        break;
    }
    case KEY_6_DOWN:
    {

        break;
    }
    case KEY_7_DOWN:
    {

        break;
    }
    case KEY_8_DOWN:
    {


        break;
    }
    case KEY_9_DOWN:
    {

        break;
    }
    default:
        break;
    }
}

