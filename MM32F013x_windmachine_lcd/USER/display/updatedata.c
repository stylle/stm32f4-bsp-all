#include "updatedata.h"

#include "bsp.h"




/*********************************************页面更新显示函数*****************************************************/
void UpdateFactoryPage(uint8_t _page);



void UpdateParaPage(void);

void UpdateStatePage(void);

void UpdateHistoryPage(void);

void UpdateUserInput(void);

/*
*********************************************************************************************************
*	函 数 名: UpdateFactoryPage
*	功能说明: 更新工厂设置页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void UpdateFactoryPage(uint8_t _page)
{
#define FACTORY_DATA_DISPLAY_ADDR 66


    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
    if( DispControler.ucIndex == 0 )
    {
        UserInputFactoryPageParaer.factory_PWD.ucSelected = 4; /*  */
		UserInputFactoryPageParaer.user_PWD.ucSelected = 4; /*  */
        UserInputFactoryPageParaer.H_Power.ucSelected = 4; /*  */
        UserInputFactoryPageParaer.L_Power.ucSelected = 4; /*  */
		UserInputFactoryPageParaer.VoltageMax.ucSelected = 4; /*  */
        UserInputFactoryPageParaer.VoltageMin.ucSelected = 4; /*  */
		
        Parameter_Set_choose(0,(DispControler.ucSel%4)*16+2);

        switch(_page)
        {
			case 0:
			{
				DispMachine_Switch(FACTORY_DATA_DISPLAY_ADDR, 2);
				DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 18, &UserInputFactoryPageParaer.H_Power);
				DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 34, &UserInputFactoryPageParaer.L_Power);
				DispPower_Factor(FACTORY_DATA_DISPLAY_ADDR,50); //功率因数显示
				break;
			}
			case 1:
			{
				DispFault_Switch(FACTORY_DATA_DISPLAY_ADDR, 2); //故障开关数值
				DispLCDLight_Switch(112,2);

				DispMachine_Version(FACTORY_DATA_DISPLAY_ADDR, 18);
				DispLCD_Version(FACTORY_DATA_DISPLAY_ADDR,34);
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 50, &UserInputFactoryPageParaer.factory_PWD);//显示工厂密码输入

				break;
			}
			case 2:
			{
				Urgent_Set(FACTORY_DATA_DISPLAY_ADDR,2, getUserFactoryPara(OverLimit_Switch_E));
				DispAD_Sample_Time(FACTORY_DATA_DISPLAY_ADDR,18); //功率因数显示
	//            DispOverLoad_multiply(FACTORY_DATA_DISPLAY_ADDR,34);
				DispCheckout_Switch(FACTORY_DATA_DISPLAY_ADDR,34);
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 50, &UserInputFactoryPageParaer.user_PWD);//显示用户密码输入
				break;
			}
			 case 3:
			{				
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 2, &UserInputFactoryPageParaer.VoltageMax);
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 18, &UserInputFactoryPageParaer.VoltageMin);
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 34,  getUserFactoryPara(DS_FEEDBACK_FLAG_E));
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 50,  getUserFactoryPara(DS_ERROR_FLAG_E));				
				break;
			}
			case 4:
			{
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 2,  getUserFactoryPara(DJYX_FLAG_E));
				break;
			}
			default:
				break;
        }
    }
    else
    {
        Parameter_Set_choose(0,(DispControler.ucSel%4)*16+2);
        switch(DispControler.ucSel)
        {
			case 0:
			{
				DispMachine_Switch(FACTORY_DATA_DISPLAY_ADDR, 2);
				break;
			}
			case 1:
			{
				if(DispControler.ucIndex==1) //粗调
				{
					UserInputFactoryPageParaer.H_Power.ucSelected = 3;
					DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 18, &UserInputFactoryPageParaer.H_Power);
					UserInputFactoryPageParaer.H_Power.ucSelected = 4;
					if(getFactoryPara(MachineType_E)== 1)
					{
						UserInputFactoryPageParaer.L_Power.ucSelected = 3;
						DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 34, &UserInputFactoryPageParaer.L_Power);
						UserInputFactoryPageParaer.L_Power.ucSelected = 4;
					}
				}
				else//微调
				{
					UserInputFactoryPageParaer.H_Power.ucSelected = DispControler.ucIndex - 2;
					DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 18, &UserInputFactoryPageParaer.H_Power);
					UserInputFactoryPageParaer.H_Power.ucSelected = 4;

					UserInputFactoryPageParaer.L_Power.ucSelected = 4;
					DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 34, &UserInputFactoryPageParaer.L_Power);
				}
				break;
			}
			case 2:
			{
				if(DispControler.ucIndex==1) //粗调
				{
					UserInputFactoryPageParaer.L_Power.ucSelected = 3;
					DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 34, &UserInputFactoryPageParaer.L_Power);
					UserInputFactoryPageParaer.L_Power.ucSelected = 4;
				}
				else//微调
				{
					UserInputFactoryPageParaer.L_Power.ucSelected = DispControler.ucIndex - 2;
					DispPowerInput(FACTORY_DATA_DISPLAY_ADDR, 34, &UserInputFactoryPageParaer.L_Power);
					UserInputFactoryPageParaer.L_Power.ucSelected = 4;
				}
				break;
			}
			case 3:
			{
				DispPower_Factor(FACTORY_DATA_DISPLAY_ADDR,50); //功率因数显示
				break;
			}
			case 4:
			{
				DispFault_Switch(FACTORY_DATA_DISPLAY_ADDR, 2); //故障开关数值
				DispLCDLight_Switch(112,2);
				break;
			}
			case 5:{break;}
			case 6:{break;}
			case 7:
			{
				UserInputFactoryPageParaer.factory_PWD.ucSelected = DispControler.ucIndex - 1;
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 50, &UserInputFactoryPageParaer.factory_PWD);
				UserInputFactoryPageParaer.factory_PWD.ucSelected = 4;
				break;
			}
			case 8:
			{
				Urgent_Set(FACTORY_DATA_DISPLAY_ADDR,2, getUserFactoryPara(OverLimit_Switch_E        ));
				break;
			}
			case 9:
			{
				DispAD_Sample_Time(FACTORY_DATA_DISPLAY_ADDR,18);
				break;
			}
			case 10:
			{
	//            DispOverLoad_multiply(FACTORY_DATA_DISPLAY_ADDR,34);
				DispCheckout_Switch(FACTORY_DATA_DISPLAY_ADDR,34);
				break;
			}
			case 11:
			{
				UserInputFactoryPageParaer.user_PWD.ucSelected = DispControler.ucIndex - 1;
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 50, &UserInputFactoryPageParaer.user_PWD);//显示用户密码输入
				UserInputFactoryPageParaer.user_PWD.ucSelected = 4;
				break;
			}
			case 12:
			{
				UserInputFactoryPageParaer.VoltageMax.ucSelected = DispControler.ucIndex - 1;
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 2, &UserInputFactoryPageParaer.VoltageMax);//显示用户密码输入
				UserInputFactoryPageParaer.VoltageMax.ucSelected = 4;
				break;
			}
			case 13:
			{
				UserInputFactoryPageParaer.VoltageMin.ucSelected = DispControler.ucIndex - 1;
				DispUserInput(FACTORY_DATA_DISPLAY_ADDR, 18, &UserInputFactoryPageParaer.VoltageMin);//显示用户密码输入
				UserInputFactoryPageParaer.VoltageMin.ucSelected = 4;
				break;
			}
			case 14:
			{
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 34,  getUserFactoryPara(DS_FEEDBACK_FLAG_E));
				break;
			}
			case 15:
			{
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 50, getUserFactoryPara(DS_ERROR_FLAG_E));				
				break;
			}
			case 16:
			{
				DispRelaySet(FACTORY_DATA_DISPLAY_ADDR, 2, getUserFactoryPara(DJYX_FLAG_E));
				break;
			}
        default:
            break;
        }

    }
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}




/*
*********************************************************************************************************
*	函 数 名: UpdateParaPage
*	功能说明: 更新参数页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void UpdateParaPage(void)
{
#define PARAPAGE_DISPLAY_ADDR 66

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
    DispWorkState(PARAPAGE_DISPLAY_ADDR, 2);
    DispIntNumWithChar(PARAPAGE_DISPLAY_ADDR, 18, Main_Power_Data[0].data, 0,"V     ");
//			DispIntNum(70, 2, Main_Power_Data[1].data, 0);
//			DispIntNum(70, 4, Main_Power_Data[2].data, 0);

    if(getFactoryPara(MachineType_E)== 1)
    {
        DispFloatNumWithDotChar(PARAPAGE_DISPLAY_ADDR, 34, Current_Data[0].data, 0,"A   ");

        DispFloatNumWithDotChar(PARAPAGE_DISPLAY_ADDR, 50, Current_Data[1].data, 0,"A   ");
    }
    else
    {
        DispFloatNumWithDotChar(PARAPAGE_DISPLAY_ADDR, 34, Current_Data[0].data, 0,"A   ");
    }
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示

}


/*
*********************************************************************************************************
*	函 数 名: UpdateParaPage
*	功能说明: 更新参数页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/


/*********************************************状态页面功能函数*****************************************************/


void UpdateStatePage(void)
{
#define STATEPAGE_DISPLAY_ADDR 66

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    DispAuthorState(STATEPAGE_DISPLAY_ADDR, 2);

    DispError(STATEPAGE_DISPLAY_ADDR, 18);

    DispWorkState(STATEPAGE_DISPLAY_ADDR, 34);

    DispControlState(STATEPAGE_DISPLAY_ADDR, 50);

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示

}


/*
*********************************************************************************************************
*	函 数 名: UpdateHistoryPage
*	功能说明: 更新巡检页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void UpdateHistoryPage(void)
{
    uint32_t temp=0;
    unsigned char i=0;
    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
    temp = STAer.Error.uiHistory;
    for(i = 0; i < 8; i ++)
    {
        if(i&0x01)
        {
            DispHistoryError(73, (i-1)*8+2, i+1, temp&0x0000000F);
        }
        else
        {
            DispHistoryError(6, i*8+2, i+1, temp&0x0000000F);
        }
        temp >>= 4;
    }
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示


}
/*
*********************************************************************************************************
*	函 数 名: UpdateUserInput
*	功能说明: 更新用户输入密码  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void UpdateUserInput(void)
{
    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
    DispUserInput(70, 26, &UserInput);
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}

