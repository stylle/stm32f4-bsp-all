#include "bsp.h"
#include "displaypage.h"
#include "displaypacket.h"

#include "data.h"





/*********************************************页面固定显示函数*****************************************************/
void DispChuangHong(void);
void DisplayWelcomePage(void);
void DisplayParameterPage(void);
void DisplayStatePage(void);
void DisplayHistoryPage(void);

void DisplayUserPasswordPage(void);
void DispFactoryPasswordPage(void);

void DispFactoryPage(uint8_t nIndex);
void DispSavePage(void);

/*
*********************************************************************************************************
*	函 数 名: DispChuangHong
*	功能说明:公司LOGO页面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispChuangHong(void)
{
    OLED_ClrScr(0);			 /* 清屏，黑底 */
//    PhotoXLCD(chuanghong);
}
/*
*********************************************************************************************************
*	函 数 名: DisplayWelcomePage
*	功能说明:欢迎页面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DisplayWelcomePage(void)
{
    OLED_ClrScr(0);			 /* 清屏，黑底 */

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    OLED_DispStr(20, 15,  "感 谢 使 用", &tFont16);
    OLED_DispStr(8, 33, "智能风机控制器", &tFont16);

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}


/*
*********************************************************************************************************
*	函 数 名: DisplayParameterPage
*	功能说明:参数显示
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DisplayParameterPage(void)
{

    OLED_ClrScr(0);			 /* 清屏，黑底 */

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    OLED_DispStr(10, 2,  "输出状态:", &tFont12);
    OLED_DispStr(10, 18, "输出电压:", &tFont12);

    if(getFactoryPara(MachineType_E)== 1)//双速风机
    {
        OLED_DispStr(10, 34, "高速电流:", &tFont12);
        OLED_DispStr(10, 50, "低速电流:", &tFont12);
    }
    else
    {
        OLED_DispStr(10, 34, "输出电流:", &tFont12);
        OLED_DispStr(10, 50, "             ", &tFont12);

    }

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示






}

/*
*********************************************************************************************************
*	函 数 名: DisplayStatePage
*	功能说明:状态显示页面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void DisplayStatePage(void)
{


    OLED_ClrScr(0);			 /* 清屏，黑底 */

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    OLED_DispStr(10, 2,  "权限状态:", &tFont12);
    OLED_DispStr(10, 18, "故障类型:", &tFont12);
    OLED_DispStr(10, 34, "输出状态:", &tFont12);
    OLED_DispStr(10, 50, "控制状态:", &tFont12);

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}

/*
*********************************************************************************************************
*	函 数 名: DisplayHistoryPage
*	功能说明:状态显示页面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void DisplayHistoryPage(void)
{
    uint32_t i;
    char buf[20];
    OLED_ClrScr(0);			 /* 清屏，黑底 */
    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
    for(i = 0; i < 8; i ++)
    {
        sprintf(buf,"%d",i+1);
        if(i&0x01)
        {
            OLED_DispStr(0, (i-1)*8+2, buf, &tFont12);
        }
        else
        {
            OLED_DispStr(0, i*8+2, buf, &tFont12);

        }
    }
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}

/*
*********************************************************************************************************
*	函 数 名: DisplayUserPasswordPage
*	功能说明:用户设置输入密码界面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DisplayUserPasswordPage(void)
{
    OLED_ClrScr(0);			 /* 清屏，黑底 */
    DispMiMa(0);
}


/*
*********************************************************************************************************
*	函 数 名: DispFactoryPasswordPage
*	功能说明:工厂设置输入密码界面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFactoryPasswordPage(void)
{
    OLED_ClrScr(0);			 /* 清屏，黑底 */
    DispMiMa(1);
}


/*
*********************************************************************************************************
*	函 数 名: DispFactoryPage
*	功能说明:工厂设置界面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFactoryPage(uint8_t nIndex)
{

    OLED_ClrScr(0);			 /* 清屏，黑底 */
    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示
	switch(nIndex)
	{
		case 0:
		{
			OLED_DispStr(13,  2,  "设备类型:", &tFont12);
			OLED_DispStr(13, 18,  "消防功率:", &tFont12);
			OLED_DispStr(13, 34,  "普通功率:", &tFont12);
			OLED_DispStr(13, 50,  "功率因数:", &tFont12);
			break;
		}
		case 1:
		{
			OLED_DispStr(13,  2,  "故障开关:  背光: ", &tFont12);
			OLED_DispStr(13, 18,  "主机版本:", &tFont12);
			OLED_DispStr(13, 34,  "液晶版本:", &tFont12);
			OLED_DispStr(13, 50,  "工厂密码:", &tFont12);
			break;
		}	
		case 2:
		{
			OLED_DispStr(13,  2,  "辐射开关:", &tFont12);
			OLED_DispStr(13, 18,  "过载时间:", &tFont12);
	//        OLED_DispStr(13, 34,  "电流倍率:", &tFont12);
	//		  	OLED_DispStr(13, 34,  "      ", &tFont12);
			OLED_DispStr(13, 34,  "检修控制:", &tFont12);
			OLED_DispStr(13, 50,  "用户密码:", &tFont12);
			break;
		}
		case 3:
		{
			OLED_DispStr(13,  2,  "电压上限:   V", &tFont12);
			OLED_DispStr(13, 18,  "电压下限:   V", &tFont12);
			OLED_DispStr(13, 34,  "低速反馈:", &tFont12);
			OLED_DispStr(13, 50,  "低速故障:", &tFont12);
			break;
		}
		case 4:
		{
			OLED_DispStr(13,  2,  "电机运行:", &tFont12);
			OLED_DispStr(13, 18,  "     ", &tFont12);
			OLED_DispStr(13, 34,  "     ", &tFont12);
			OLED_DispStr(13, 50,  "     ", &tFont12);
			break;
		}
	}
	
    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示


}

/*
*********************************************************************************************************
*	函 数 名: DispSavePage
*	功能说明:参数保存页面
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispSavePage(void)
{
    OLED_ClrScr(0);			 /* 清屏，黑底 */

    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    OLED_DispStr(13,  2,  "是否保存参数修改", &tFont12);
    OLED_DispStr(40, 18,  "保存>Enter!", &tFont12);
    OLED_DispStr(40, 34,  "取消>Ese", &tFont12);

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示

}












