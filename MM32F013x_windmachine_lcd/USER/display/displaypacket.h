/* 
 * File:   displaypacket.h
 * Author: huangzhiming
 *
 * Created on April 17, 2018, 3:40 PM
 */

#ifndef DISPLAYPACKET_H
#define	DISPLAYPACKET_H



#ifdef	__cplusplus
extern "C" {
#endif
#include <stdint.h>


typedef struct
{
    uint8_t  ucPwdBit[3];
	uint16_t uiPwdNum;
    uint8_t ucSelected; 
}PWD_T;
//初始化







/*********************************************数值显示功能函数*****************************************************/
void DispOneBitIntNum(unsigned char x, unsigned char y, unsigned char s_num, unsigned char inv);
void DispTwoBitIntNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv);
void DispThreeBitIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);
void DispFloatNumWithDot(unsigned char x, unsigned char y, unsigned int num,unsigned char inv);
void DispFloatNumWithDotChar(unsigned char x, unsigned char y, unsigned int num,unsigned char inv,char *ptr);
void DispIntNumWithChar(unsigned char x, unsigned char y, unsigned int num, unsigned char inv,char *ptr);
void DispIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);
void DispFloatNumWithDot_Two(unsigned char x, unsigned char y, unsigned int num,unsigned char inv);

void DispPercentNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv);
void DispNumWith_KW(unsigned char x, unsigned char y, unsigned int num, unsigned char inv); 
void DispFourBitIntNum(unsigned char x, unsigned char y, unsigned int s_num, unsigned char inv);
void DispNumWith_V_ONEDOT(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);

/*********************************************字符显示功能函数*****************************************************/
void DisplayV(unsigned char x, unsigned char y);
void DisplayColon(unsigned char x, unsigned char y);
void Parameter_Set_choose(unsigned char x,unsigned char line);//手指图
void DispOpenOrClose(unsigned char x, unsigned char y, unsigned char sel);//显示开关



/*********************************************密码输入页面功能函数*****************************************************/
void DispUserPwd(unsigned char x, unsigned char y);
void DispUserPwdData(unsigned char x, unsigned char y);
void DispFactoryPwd(unsigned char x, unsigned char y);
void DispFactoryPwdData(unsigned char x, unsigned char y);
void DispPwdInput(unsigned char x, unsigned char y);
void DispPwdError(void);
void ClearPwdError(void);
void DispUserInput(unsigned char x, unsigned char y, PWD_T* pwd);
void DispPowerInput(unsigned char x, unsigned char y, PWD_T* pwd);
void DispMiMa(unsigned char i);
/*********************************************工厂设置页面功能函数*****************************************************/

void DispFault_Switch(unsigned char x, unsigned char y);
void DispLCDLight_Switch(unsigned char x, unsigned char y);
void DispMachine_Switch(unsigned char x, unsigned char y);

void DispH_Power_Select(unsigned char x, unsigned char y);
void DispL_Power_Select(unsigned char x, unsigned char y);

void DispLCD_Version(unsigned char x, unsigned char y);
void DispMachine_Version(unsigned char x, unsigned char y);

void Urgent_Set(unsigned char x, unsigned char y,unsigned char dat);
void DispPower_Factor(unsigned char x, unsigned char y);
void DispOverLoad_multiply(unsigned char x, unsigned char y);
void DispAD_Sample_Time(unsigned char x, unsigned char y);
void DispCheckout_Switch(unsigned char x, unsigned char y);
void DispRelaySet(unsigned char x, unsigned char y, unsigned char type);
/*********************************************状态页面功能函数*****************************************************/

void DispWorkState(unsigned char x, unsigned char y);//工作状态显示
void DispControlState(unsigned char x, unsigned char y);
void DispError(unsigned char x, unsigned char y);
void DispAuthorState(unsigned char x, unsigned char y);


void DispHistoryError(unsigned char x, unsigned char y, unsigned char data1, unsigned char data);


#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAYPACKET_H */

