#include "bsp.h"
#include "display.h"
#include "pagebtnprocess.h"
#include "updatedata.h"


DispControl_T DispControler;

extern volatile unsigned char update;

/*
*********************************************************************************************************
*	函 数 名: displayInit
*	功能说明: 显示结构体初始化。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void displayInit(void)
{
    DispControler.ucPageNum = START_LCD_PAGE;
    DispControler.ucHold = 255;
    DispControler.ucIndex = 0;
    DispControler.ucPreIndex = 0;
    DispControler.ucSel = 0;
    DispControler.ucPreSel = 0;
	DispControler.ucLCD_Light_tim=0;
	

}


/*
*********************************************************************************************************
*	函 数 名: display
*	功能说明: 界面显示状态机。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void display(void)
{    
    static volatile unsigned long cnt=0;
    unsigned int keycode = bsp_GetKey();
    unsigned char ret;

	if(keycode)  
	{
		STAer.ucAuthor_time =0; //授权计时
		DispControler.ucLCD_Light_tim=0;	 //背光控制					
		Key_SoundBtnProcess(keycode);   //按键声控制
	}
		
    switch(DispControler.ucPageNum)
    {
        case START_LCD_PAGE:
			if(DispControler.ucHold  == LIGHTOFF_PAGE) //熄屏后开屏
			{
				OpenXLCD();						
			}
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
				DisplayWelcomePage();
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {
                cnt++;
                if( cnt == 20000 )
                {
                    cnt = 0;
                    DispControler.ucPageNum = STATE_PAGE;
                }
            }
            break;
        case WELCOM_PAGE:
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DisplayWelcomePage();
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {
                cnt++;
                if( cnt == 10000 )
                {
                    cnt = 0;
                    DispControler.ucPageNum = STATE_PAGE;
                }
            }
            break;
						
        case PARAMETER_PAGE:
            if( DispControler.ucHold != PARAMETER_PAGE )
            {
                DisplayParameterPage();
                DispControler.ucHold = PARAMETER_PAGE;
            }
            else
            {
                if( update == 0xff )
                {              
                    UpdateParaPage();
                    update = 0x00;
                }               
            }
						 ParaPageBtnProcess(keycode);
            break;

        case STATE_PAGE:
            if( DispControler.ucHold != STATE_PAGE )
            {
                DisplayStatePage();
                DispControler.ucHold = STATE_PAGE;
            }
            else
            {
				if( update == 0xff )
				{
						UpdateStatePage();
						update = 0x00;
				}               
            }
			StatePageBtnProcess(keycode);
            break;
		case HISTORY_PAGE:
		{
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DisplayHistoryPage();
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {
                if( update == 0xff )
                {
                    UpdateHistoryPage();
                    update = 0x00;
                }              
            }
						HistoryPage_BtnProcess(keycode);
            break;		
		}
       case USERPASSWORDPAGE:
            if( DispControler.ucHold != USERPASSWORDPAGE )
            {
                DisplayUserPasswordPage();
                DispControler.ucHold = USERPASSWORDPAGE;
            }
            else
            {
                ret = UserPasswordBtnProcess(keycode);
                if( (ret == 0) && (update == 0xff) )
                {
                    UpdateUserInput();
                    update = 0x00;
                }
            }
            break;


        case FACTORYPQSSWORDPAGE:
            if( DispControler.ucHold != FACTORYPQSSWORDPAGE )
            {
                DispFactoryPasswordPage();
                DispControler.ucHold = FACTORYPQSSWORDPAGE;
            }
            else
            {
                ret = FactoryPasswordBtnProcess(keycode);
                if( (ret == 0) && (update == 0xff) )
                {
                    UpdateUserInput();
                    update = 0x00;
                }
            }
            break;
        case FACTORYPAGE_ONE:
			if(DispControler.ucHold ==FACTORYPAGE_TWO 
				||DispControler.ucHold ==FACTORYPAGE_THREE
				||DispControler.ucHold ==FACTORYPAGE_FOUR
			)
			{
				DispFactoryPage(0);
				DispControler.ucHold = FACTORYPAGE_ONE;
			}
            else if( DispControler.ucHold != FACTORYPAGE_ONE )
            {
                CopyFactoryData();								
                DispFactoryPage(0);
                DispControler.ucHold = FACTORYPAGE_ONE;
            }
            else
            {                
                if( update == 0xff )
                {
                    UpdateFactoryPage(0);
                    update = 0x00;
                }
            }
						FactoryPage_BtnProcess(keycode);
            break;
		case FACTORYPAGE_TWO:
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DispFactoryPage(1);
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {                
                if( update == 0xff )
                {
                    UpdateFactoryPage(1);
                    update = 0x00;
                }
            }
						FactoryPage_BtnProcess(keycode);
            break;	
		case FACTORYPAGE_THREE:
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DispFactoryPage(2);
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {              
                if( update == 0xff )
                {
                    UpdateFactoryPage(2);
                    update = 0x00;
                }
            }
			FactoryPage_BtnProcess(keycode);
            break;	

		case FACTORYPAGE_FOUR:
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DispFactoryPage(3);
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {              
                if( update == 0xff )
                {
                    UpdateFactoryPage(3);
                    update = 0x00;
                }
            }
			FactoryPage_BtnProcess(keycode);
            break;		

		case FACTORYPAGE_FIVE:
            if( DispControler.ucHold != DispControler.ucPageNum )
            {
                DispFactoryPage(4);
                DispControler.ucHold = DispControler.ucPageNum;
            }
            else
            {              
                if( update == 0xff )
                {
                    UpdateFactoryPage(4);
                    update = 0x00;
                }
            }
			FactoryPage_BtnProcess(keycode);
            break;	
						
						
        case SAVEPAGE:
            if( DispControler.ucHold != SAVEPAGE )
            {
                DispSavePage();
                DispControler.ucHold = SAVEPAGE;
            }
            else
            {
                SaveBtnProcess(keycode);
            }
            break;
					 
		case LIGHTOFF_PAGE:
            if( DispControler.ucHold != LIGHTOFF_PAGE )
            {
				DisplayWelcomePage();
                DispControler.ucHold = LIGHTOFF_PAGE;
            }            
            break;
		default:
			break;

    }
}

/*
*********************************************************************************************************
*	函 数 名: LCD_Light_Auto
*	功能说明: 配置LED指示灯相关的GPIO,  该函数被 bsp_Init() 调用。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void LCD_Light_Auto(void)
{
    uint16_t _usLight_TIM=800; //40s
    if (++DispControler.ucLCD_Light_tim >= _usLight_TIM)
    {
        if( DispControler.ucPageNum == SAVEPAGE||DispControler.ucPageNum == FACTORYPAGE_ONE||DispControler.ucPageNum == FACTORYPAGE_TWO)
        {
            DispControler.ucLCD_Light_tim = 0;
        }
        else
        {
            LIGHT_OFF;
            DispControler.ucPageNum = LIGHTOFF_PAGE;//当背光熄灭的时候让屏幕上显示创宏的标志
            DispControler.ucLCD_Light_tim = 2*_usLight_TIM;
        }
    }
    else
    {
        LIGHT_ON;
        if(DispControler.ucPageNum  == LIGHTOFF_PAGE )
        {                        
           DispControler.ucPageNum = START_LCD_PAGE;
        }
    }
}
/*
*********************************************************************************************************
*	函 数 名: LCD_Light_Pro
*	功能说明:  每50ms?用一次
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_Light_Pro(void)
{
	if(getFactoryPara(Lcd_light_E) == 0 )
	{
			LCD_Light_Auto();	
	}
	else
	{
		LIGHT_ON;	
		DispControler.ucLCD_Light_tim=0;
	}


}

