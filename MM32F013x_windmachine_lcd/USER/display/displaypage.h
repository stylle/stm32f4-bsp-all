/* 
 * File:   displaypage.h
 * Author: huangzhiming
 *
 * Created on April 17, 2018, 3:34 PM
 */

#ifndef DISPLAYPAGE_H
#define	DISPLAYPAGE_H


#include "font.h"
#include "bsp_lcd12864.h"

#ifdef	__cplusplus
extern "C" {
#endif


/*********************************************ҳ��̶���ʾ����*****************************************************/
void DispChuangHong(void);
void DisplayWelcomePage(void);
void DisplayParameterPage(void);
void DisplayStatePage(void);
void DisplayHistoryPage(void);

void DisplayUserPasswordPage(void);
void DispFactoryPasswordPage(void);

void DispFactoryPage(uint8_t nIndex);
void DispSavePage(void);



#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAYPAGE_H */

