#include "displaypacket.h"
#include "displaypage.h"
#include "display.h"
#include  "data.h"
#include "bsp.h"





/*********************************************数值显示功能函数*****************************************************/
void DispOneBitIntNum(unsigned char x, unsigned char y, unsigned char s_num, unsigned char inv);
void DispTwoBitIntNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv);
void DispThreeBitIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);



void DispPercentNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv);
void DispNumWith_KW(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);


void DispFloatNumWithDot(unsigned char x, unsigned char y, unsigned int num,unsigned char inv);
void DispFloatNumWithDotChar(unsigned char x, unsigned char y, unsigned int num,unsigned char inv,char *ptr);
void DispIntNumWithChar(unsigned char x, unsigned char y, unsigned int num, unsigned char inv,char *ptr);
void DispFloatNumWithDot_Two(unsigned char x, unsigned char y, unsigned int num,unsigned char inv);
void DispIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);

void DispFourBitIntNum(unsigned char x, unsigned char y, unsigned int s_num, unsigned char inv);
void DispNumWith_V_ONEDOT(unsigned char x, unsigned char y, unsigned int num, unsigned char inv);





/*
*********************************************************************************************************
*	函 数 名: DispOneBitIntNum
*	功能说明: 显示2位数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispOneBitIntNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	sprintf(buf,"%d",num%10);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

}


/*
*********************************************************************************************************
*	函 数 名: DispTwoBitIntNum
*	功能说明: 显示2位数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispTwoBitIntNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
        sprintf(buf,"%d",num%100);
        OLED_DispStr(x, y, buf, &tFont12);

        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */  
}
/*
*********************************************************************************************************
*	函 数 名: DispThreeBitFloatNumWithDot
*	功能说明: 显示3位数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispThreeBitIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	sprintf(buf,"%d",num%1000);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */   
}
/*
*********************************************************************************************************
*	函 数 名: DispFourBitIntNum
*	功能说明: 显示4位数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFourBitIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
        sprintf(buf,"%d",num%10000);
        OLED_DispStr(x, y, buf, &tFont12);

        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
   
}


/*
*********************************************************************************************************
*	函 数 名: DispIntNum
*	功能说明: 显示数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispIntNum(unsigned char x, unsigned char y, unsigned int num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	sprintf(buf,"%d",num);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
  
}

/*
*********************************************************************************************************
*	函 数 名: DispIntNumWithChar
*	功能说明: 显示数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispIntNumWithChar(unsigned char x, unsigned char y, unsigned int num, unsigned char inv,char *ptr)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	sprintf(buf,"%d%s  ",num,ptr);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
  
}
/*
*********************************************************************************************************
*	函 数 名: DispFloatNumWithDotChar
*	功能说明: 显示数据带小数点
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFloatNumWithDotChar(unsigned char x, unsigned char y, unsigned int num,unsigned char inv,char *ptr)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	sprintf(buf,"%d.%d%s ",num/10,num%10,ptr);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */  
}

/*
*********************************************************************************************************
*	函 数 名: DispFloatNumWithDot
*	功能说明: 显示数据带小数点
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFloatNumWithDot(unsigned char x, unsigned char y, unsigned int num,unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}

	sprintf(buf,"%d.%d",num/10,num%10);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
}


/*
*********************************************************************************************************
*	函 数 名: DispFloatNumWithDot_Two
*	功能说明: 显示数据带小数点两位小数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFloatNumWithDot_Two(unsigned char x, unsigned char y, unsigned int num,unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	
	sprintf(buf,"%d.%d%d",num/100,(num%100)/10,num%10);
	OLED_DispStr(x, y, buf, &tFont12);

		/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

}


/*
*********************************************************************************************************
*	函 数 名: DispPercentNum
*	功能说明: 显示百分比数据
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispPercentNum(unsigned char x, unsigned char y, unsigned char num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}	
	sprintf(buf,"%d%%",num/100);
	OLED_DispStr(x, y, buf, &tFont12);

	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
}


/*
*********************************************************************************************************
*	函 数 名: DispNumWith_KW
*	功能说明: 显示 数字带 KW字符。用于工厂模式功率选择
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispNumWith_KW(unsigned char x, unsigned char y, unsigned int num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}  
        sprintf(buf,"%d.%dKW ",num%1000/10,num%10);
        OLED_DispStr(x, y, buf, &tFont12);

        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
   
}
/*
*********************************************************************************************************
*	函 数 名: DispNumWith_V_ONEDOT
*	功能说明: 显示V 加 数字带 KW字符。用于显示液晶和接口板版本
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispNumWith_V_ONEDOT(unsigned char x, unsigned char y, unsigned int num, unsigned char inv)
{
    char buf[20];
    if(inv)
    {
        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
        sprintf(buf,"V%d.%d",num%100/10,num%10);
        OLED_DispStr(x, y, buf, &tFont12);

        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
}


/*********************************************字符显示功能函数*****************************************************/
void DisplayV(unsigned char x, unsigned char y);
void DisplayColon(unsigned char x, unsigned char y);
void Parameter_Set_choose(unsigned char x,unsigned char line);//手指图
void DispOpenOrClose(unsigned char x, unsigned char y, unsigned char sel);//显示开关



/*
*********************************************************************************************************
*	函 数 名: DisplayV
*	功能说明: 显示 V字符。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DisplayV(unsigned char x, unsigned char y)
{
    OLED_DispStr(x, y, "V", &tFont12);
}
/*
*********************************************************************************************************
*	函 数 名: DisplayColon
*	功能说明: 显示冒号。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DisplayColon(unsigned char x, unsigned char y)
{
    OLED_DispStr(x, y, ":", &tFont12);
}



/*
*********************************************************************************************************
*	函 数 名: Parameter_Set_choose
*	功能说明: 显示手指图  用于工厂模式。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void Parameter_Set_choose(unsigned char x,unsigned char line)//手指图
{
    OLED_DispStr(0, 2, "  ", &tFont12);
    OLED_DispStr(0,18, "  ", &tFont12);
    OLED_DispStr(0,34, "  ", &tFont12);
    OLED_DispStr(0,50, "  ", &tFont12);
    OLED_DispStr(x,line, "》", &tFont12);

}


/*
*********************************************************************************************************
*	函 数 名: DispOpenOrClose
*	功能说明: 显示开关 0表示关 其他表示开。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispOpenOrClose(unsigned char x, unsigned char y, unsigned char sel)
{
    if( sel == 0 )
    {
        OLED_DispStr(x,y, "关", &tFont12);
    }
    else
    {
        OLED_DispStr(x,y, "开", &tFont12);
    }
}
/*********************************************密码输入页面功能函数*****************************************************/
void DispUserPwd(unsigned char x, unsigned char y);
void DispUserPwdData(unsigned char x, unsigned char y);
void DispFactoryPwd(unsigned char x, unsigned char y);
void DispFactoryPwdData(unsigned char x, unsigned char y);
void DispPwdInput(unsigned char x, unsigned char y);
void DispPwdError(void);
void ClearPwdError(void);
void DispUserInput(unsigned char x, unsigned char y, PWD_T* pwd);
void DispPowerInput(unsigned char x, unsigned char y, PWD_T* pwd);
void UpdateUserInput(void);
void DispMiMa(unsigned char i);


/*
*********************************************************************************************************
*	函 数 名: DispUserPwd
*	功能说明: 显示用户密码 字符 用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispUserPwd(unsigned char x, unsigned char y)
{
    OLED_DispStr(x,y, "用户密码", &tFont12);
}

/*
*********************************************************************************************************
*	函 数 名: DispPwdData
*	功能说明: 显示密码数据。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void DispPwdData(unsigned char x, unsigned char y,PWD_T *ptpwd)
{
    char buf[12]= {'\0'};

    sprintf(buf,"%d%d%d",ptpwd->ucPwdBit[0],ptpwd->ucPwdBit[1],ptpwd->ucPwdBit[2]);
    OLED_DispStr(x, y, buf, &tFont12);
}
/*
*********************************************************************************************************
*	函 数 名: DispFactoryPwd
*	功能说明: 显示工厂密码字符 用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFactoryPwd(unsigned char x, unsigned char y)
{
    OLED_DispStr(x,y, "工厂密码", &tFont12);
}


/*
*********************************************************************************************************
*	函 数 名: DispPwdInput
*	功能说明: 显示用户输入密码字符 用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispPwdInput(unsigned char x, unsigned char y)
{
    OLED_DispStr(x,y, "密码输入:", &tFont12);
}
/*
*********************************************************************************************************
*	函 数 名: DispPwdError
*	功能说明: 显示密码错误显示字符 用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispPwdError(void)
{
    OLED_DispStr(60, 42,  "密码错误", &tFont12);
}
/*
*********************************************************************************************************
*	函 数 名: ClearPwdError
*	功能说明: 清除密码错误显示字符 用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void ClearPwdError(void)
{
    OLED_DispStr(60, 42,  "        ", &tFont12);
}


/*
*********************************************************************************************************
*	函 数 名: DispUserInput
*	功能说明: 显示用户输入密码  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispUserInput(unsigned char x, unsigned char y, PWD_T* pwd)
{
    char buf[12]= {'\0'};
    if( pwd->ucSelected == 0 )
    {
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[0]);
        OLED_DispStr(x, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

        sprintf(buf,"%d%d",pwd->ucPwdBit[1],pwd->ucPwdBit[2]);
        OLED_DispStr(x+6, y, buf, &tFont12);

    }
    else if( pwd->ucSelected == 1 )
    {
        sprintf(buf,"%d",pwd->ucPwdBit[0]);
        OLED_DispStr(x, y, buf, &tFont12);

        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[1]);
        OLED_DispStr(x+6, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

        sprintf(buf,"%d",pwd->ucPwdBit[2]);
        OLED_DispStr(x+12, y, buf, &tFont12);
    }
    else if(  pwd->ucSelected == 2 )
    {
        sprintf(buf,"%d%d",pwd->ucPwdBit[0],pwd->ucPwdBit[1]);
        OLED_DispStr(x, y, buf, &tFont12);

        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[2]);
        OLED_DispStr(x+12, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

    }
    else
    {
        sprintf(buf,"%d%d%d",pwd->ucPwdBit[0],pwd->ucPwdBit[1],pwd->ucPwdBit[2]);
        OLED_DispStr(x, y, buf, &tFont12);

    }
}

/*
*********************************************************************************************************
*	函 数 名: DispUserInput
*	功能说明: 显示用户输入密码  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispPowerInput(unsigned char x, unsigned char y, PWD_T* pwd) //显示功率输出
{

    char buf[12]= {'\0'};
    if( pwd->ucSelected == 0 )
    {
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[0]);
        OLED_DispStr(x, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

        sprintf(buf,"%d.%dKW",pwd->ucPwdBit[1],pwd->ucPwdBit[2]);
        OLED_DispStr(x+6, y, buf, &tFont12);

    }
    else if( pwd->ucSelected == 1 )
    {
        sprintf(buf,"%d",pwd->ucPwdBit[0]);
        OLED_DispStr(x, y, buf, &tFont12);

        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[1]);
        OLED_DispStr(x+6, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

        sprintf(buf,".%dKW",pwd->ucPwdBit[2]);
        OLED_DispStr(x+12, y, buf, &tFont12);
    }
    else if(  pwd->ucSelected == 2 )
    {
        sprintf(buf,"%d%d. KW",pwd->ucPwdBit[0],pwd->ucPwdBit[1]);
        OLED_DispStr(x, y, buf, &tFont12);

        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d",pwd->ucPwdBit[2]);
        OLED_DispStr(x+18, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */

    }
    else if(  pwd->ucSelected == 3 )
    {
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
        sprintf(buf,"%d%d.%d",pwd->ucPwdBit[0],pwd->ucPwdBit[1],pwd->ucPwdBit[2]);
        OLED_DispStr(x, y, buf, &tFont12);
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
        OLED_DispStr(x+24, y, "KW", &tFont12);

    }
    else
    {
        sprintf(buf,"%d%d.%dKW",pwd->ucPwdBit[0],pwd->ucPwdBit[1],pwd->ucPwdBit[2]);
        OLED_DispStr(x, y, buf, &tFont12);

    }
}


/*
*********************************************************************************************************
*	函 数 名: DispMiMa
*	功能说明: 密码输入页  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispMiMa(unsigned char i)
{
    OLED_StartDraw();	  //调用改函数，只刷新缓冲区，不送显示

    if( i == 0 ) /* uesr pwd */
    {
        OLED_DispStr(40, 2,  "用户密码", &tFont12);
    }
    else if( i == 1 ) /* factory pwd */
    {
        OLED_DispStr(40, 2,  "工厂密码", &tFont12);
    }

    OLED_DispStr(0, 26,  "密码输入:", &tFont12);
    DispUserInput(70, 26, &UserInput);

    OLED_EndDraw();	  //调用改函数，将缓冲区中数据送显示
}







/*********************************************工厂设置页面功能函数*****************************************************/

void DispFault_Switch(unsigned char x, unsigned char y);
void DispLCDLight_Switch(unsigned char x, unsigned char y);
void DispMachine_Switch(unsigned char x, unsigned char y);

void DispH_Power_Select(unsigned char x, unsigned char y);
void DispL_Power_Select(unsigned char x, unsigned char y);

void DispLCD_Version(unsigned char x, unsigned char y);
void DispMachine_Version(unsigned char x, unsigned char y);

void Urgent_Set(unsigned char x, unsigned char y,unsigned char dat);
void DispPower_Factor(unsigned char x, unsigned char y);
/*
*********************************************************************************************************
*	函 数 名: DispFault_Switch
*	功能说明: 显示故障开关  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispFault_Switch(unsigned char x, unsigned char y)
{
    if( DispControler.ucIndex == 1 )
    {
        DispOneBitIntNum(x, y,getUserFactoryPara(FaultSwitch_E),1);
    }
    else
    {
        DispOneBitIntNum(x, y,getUserFactoryPara(FaultSwitch_E),0);
    }
}
/*
*********************************************************************************************************
*	函 数 名: DispLCDLight_Switch
*	功能说明: 显示背光开关  用于密码输入页。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispLCDLight_Switch(unsigned char x, unsigned char y)
{
    if( DispControler.ucIndex == 2 )
    {
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
        if( getUserFactoryPara(Lcd_light_E)== 1)
        {
            OLED_DispStr(x, y, "开", &tFont12);
        }
        else
        {
            OLED_DispStr(x, y, "关", &tFont12);
        }
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
   
}
/*
*********************************************************************************************************
*	函 数 名: DispMachine_Switch
*	功能说明: 显示风机类型选项  。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispMachine_Switch(unsigned char x, unsigned char y)
{

    if( DispControler.ucIndex == 1 )
    {
		/* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
        if(   getUserFactoryPara(MachineType_E)== 1)
        {
            OLED_DispStr(x, y,  "双速风机", &tFont12);
        }
        else
        {
            OLED_DispStr(x, y,  "单速风机", &tFont12);
        }
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
 }

/*
*********************************************************************************************************
*	函 数 名: DispH_Power_Select
*	功能说明: 显示风机类型选项  。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispH_Power_Select(unsigned char x, unsigned char y)
{
    DispNumWith_KW(x, y, get_PwdNum(UserInput_H_Power_E),  DispControler.ucIndex );
}

/*
*********************************************************************************************************
*	函 数 名: DispL_Power_Select
*	功能说明: 显示风机类型选项  。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispL_Power_Select(unsigned char x, unsigned char y)
{
    DispNumWith_KW(x, y, get_PwdNum(UserInput_L_Power_E), DispControler.ucIndex);
}


/*
*********************************************************************************************************
*	函 数 名: DispLCD_Version
*	功能说明: 液晶版本。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispLCD_Version(unsigned char x, unsigned char y)
{
    DispNumWith_V_ONEDOT(x, y, DispControler.LCD_Version, 0);
}


/*
*********************************************************************************************************
*	函 数 名: DispMachine_Version
*	功能说明: 设备版本 。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispMachine_Version(unsigned char x, unsigned char y)
{

    DispNumWith_V_ONEDOT(x, y, DispControler.Machine_version, 0);

}
/*
*********************************************************************************************************
*	函 数 名: DispPower_Factor
*	功能说明: 显示功率因数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispPower_Factor(unsigned char x, unsigned char y)
{
    DispFloatNumWithDot_Two(x,y, getUserFactoryPara(Power_Factor_E),DispControler.ucIndex ); 
}
/*
*********************************************************************************************************
*	函 数 名: DispOverLoad_multiply
*	功能说明: 显示功率因数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispOverLoad_multiply(unsigned char x, unsigned char y)
{
      DispFloatNumWithDot_Two(x,y, getUserFactoryPara(usOverLoad_multiply_E     )*5,DispControler.ucIndex );
}

/*
*********************************************************************************************************
*	函 数 名: DispComplay_logo
*	功能说明: 显示功率因数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispCheckout_Switch(unsigned char x, unsigned char y)
{		
		if( DispControler.ucIndex ==1 )
		{
			tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
			tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
		}
        if( getUserFactoryPara(usCheckout_Switch_E       ))
        {
            OLED_DispStr(x, y, "常开有效", &tFont12);
        }
        else
        {
            OLED_DispStr(x, y, "常闭有效", &tFont12);
        }
        /* 恢复黑底白字 */
        tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
}

/*******************************************************************************
 * 函 数 名: Urgent_Set
 * 函数功能: 显示开关
 * 入口参数: x:设置横坐标位置y:设置纵坐标位置
 * 返    回: 无
 *******************************************************************************/
void Urgent_Set(unsigned char x, unsigned char y,unsigned char dat)
{
    if( DispControler.ucIndex ==1 )
    {
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	if(dat)
	{
		OLED_DispStr(x, y, "开", &tFont12);
	}
	else
	{
		OLED_DispStr(x, y, "关", &tFont12);
	}
	/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
   
}




/*
*********************************************************************************************************
*	函 数 名: DispPower_Factor
*	功能说明: 显示功率因数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispAD_Sample_Time(unsigned char x, unsigned char y)
{
    if( DispControler.ucIndex == 0 )
    {
        DispIntNumWithChar(x,y, getUserFactoryPara(AD_Sample_Time_E          ),0,"s");
    }
    else
    {
        DispIntNumWithChar(x,y, getUserFactoryPara(AD_Sample_Time_E          ),1,"s");
    }

}

/*********************************************状态页面功能函数*****************************************************/

void DispWorkState(unsigned char x, unsigned char y);//工作状态显示
void DispControlState(unsigned char x, unsigned char y);
void DispError(unsigned char x, unsigned char y);
void DispAuthorState(unsigned char x, unsigned char y);
/*
*********************************************************************************************************
*	函 数 名: DispWorkState
*	功能说明: 显示输出状态 。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void DispWorkState(unsigned char x, unsigned char y)//工作状态显示
{
    switch(STAer.ucOutput)
    {
		case NONE_SPEED:  //停止输出
		{
			OLED_DispStr(x, y,  "输出停止", &tFont12);
			break;
		}
		case LOW_SPEED: //低速输出
		{
			if(MachineFlag.Bits.bas_running ==1)
			{
				OLED_DispStr(x, y,  "BAS输出 ", &tFont12);
			}
			else
			{
				OLED_DispStr(x, y,  "普通输出 ", &tFont12);
			}
			break;
		}
		case HIGH_SPEED: //高速输出
		{
			if(MachineFlag.Bits.fire_ptc ==1)
			{
				OLED_DispStr(x, y,  "消防联动", &tFont12);
				break;
			}
			else
			{
				if(getFactoryPara(MachineType_E) == 0) //单速风机
				{
					if(MachineFlag.Bits.bas_running ==1)
					{
						OLED_DispStr(x, y,  "BAS输出 ", &tFont12);
					}
					else
					{
						OLED_DispStr(x, y,  "输出运行", &tFont12);
					}
				}
				else
				{
					OLED_DispStr(x, y,  "消防输出", &tFont12);
				}
			}
			break;
		}
		default:
			break;

    }

}
/*
*********************************************************************************************************
*	函 数 名: DispError
*	功能说明: 显示故障状态 。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispError(unsigned char x, unsigned char y)
{
//    if( STAer.Error.ucErrorRef ) //500ms更新一次
    {
        STAer.Error.ucErrorRef = 0x00;

        if( STAer.Error.ulErrCode == 0 ||getFactoryPara(FaultSwitch_E) ==0  )
        {
            STAer.Error.ucCurrentErrNum = 0;
            STAer.Error.ucErrorCnt = 0;
            STAer.Error.ulErrCode = 0;
            STAer.Error.ulPreErrCode = 0;
            ClearErrBuf(STAer.Error.ucError);
            OLED_DispStr(x, y,  "无故障    ", &tFont12);
            return;
        }
        else
        {
            if( STAer.Error.ucCurrentErrNum < STAer.Error.ucErrorCnt )
            {
                switch((STAer.Error.ucError[STAer.Error.ucCurrentErrNum++]))
                {
                case MASK_Low_OverLoad:                     OLED_DispStr(x, y,  "普通过载 ", &tFont12);                    break;
                case MASK_High_OverLoad://高速过载
                {
                    if(getFactoryPara(MachineType_E) == 0) //单速风机
                    {
                        OLED_DispStr(x, y,  "输出过载  ", &tFont12);
                    }
                    else
                    {
                        OLED_DispStr(x, y,  "消防过载  ", &tFont12);
                    }
                    break;
                }
                case MASK_Low_Short:                    OLED_DispStr(x, y,  "普通短路  ", &tFont12);                    break;
                case MASK_High_Short:                   OLED_DispStr(x, y,  "消防短路  ", &tFont12);                    break;
                case MASK_WindValve_Error:              OLED_DispStr(x, y,  "防火阀关闭", &tFont12);                    break;
                case MASK_CheckOut:                     OLED_DispStr(x, y,  "检修状态  ", &tFont12);                    break;
                case MASK_Power_OverLimit:              OLED_DispStr(x, y,  "主电过压  ", &tFont12);                    break;
                case MASK_Power_UnderLimit:             OLED_DispStr(x, y,  "主电欠压  ", &tFont12);                    break;
                case MASK_Power_LackPhase:              OLED_DispStr(x, y,  "主电缺相  ", &tFont12);                    break;
                case MASK_Power_PhaseError:             OLED_DispStr(x, y,  "相位故障  ", &tFont12);                    break;
                case MASK_KM1_Error:                    OLED_DispStr(x, y,  "KM1故障   ", &tFont12);                    break;
                case MASK_KM2_Error:                    OLED_DispStr(x, y,  "KM2故障   ", &tFont12);                    break;
				case MASK_TOPPRESSURE:					OLED_DispStr(x, y,  "余压控制   ", &tFont12);                   break;
                case MASK_COM_Error:                    OLED_DispStr(x, y,  "通信故障  ", &tFont12);                    break;
                }
            }
            else
            {
                STAer.Error.ucCurrentErrNum = 0;


            }
        }
    }
}


/*
*********************************************************************************************************
*	函 数 名: DispAuthorState
*	功能说明: 授权状态 。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispAuthorState(unsigned char x, unsigned char y)
{
    if( STAer.ucAuthor == 0 )
    {
        OLED_DispStr(x, y,  "无权限  ", &tFont12);
    }
    else
    {
        OLED_DispStr(x, y,  " 授权 ", &tFont12);
    }
}
/*
*********************************************************************************************************
*	函 数 名: DispControlState
*	功能说明: 手自动状态 。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void DispControlState(unsigned char x, unsigned char y)
{
    if( MachineFlag.Bits.auto_bit == 1 )
    {
        OLED_DispStr(x, y,  "手动控制", &tFont12);
    }
    else
    {
        OLED_DispStr(x, y,  "自动控制", &tFont12);
    }
}


/*******************************************************************************
 * 函 数 名: History()
 * 函数功能: 用于显示历史故障
 * 入口参数: x,y表示坐标，data1表示第几个故障，data表示故障代码
 * 返    回: 无
*******************************************************************************/
void DispHistoryError(unsigned char x, unsigned char y, unsigned char data1, unsigned char data)
{
//    putrsXLCD(x, y, c_digits[data1], 6, 0);
    switch(data)
    {
    case 1:    {        OLED_DispStr(x, y,  "普通过载  ", &tFont12);        break;    }
    case 2:    {
        if(getFactoryPara(MachineType_E) == 0) //单速风机
        {
            OLED_DispStr(x, y,  "输出过载  ", &tFont12);
        }
        else
        {
            OLED_DispStr(x, y,  "消防过载  ", &tFont12);
        }
        break;
    }
    case 3:    {        OLED_DispStr(x, y,  "普通短路  ", &tFont12);        break;    } 
	case 4:    {        OLED_DispStr(x, y,  "消防短路  ", &tFont12);        break;    }  
	case 5:    {        OLED_DispStr(x, y,  "防火阀关闭", &tFont12);        break;    }
    case 6:    {        OLED_DispStr(x, y,  "检修状态  ", &tFont12);        break;    }
    case 7:    {        OLED_DispStr(x, y,  "主电过压  ", &tFont12);        break;    }
    case 8:    {        OLED_DispStr(x, y,  "主电欠压  ", &tFont12);        break;    }
    case 9:    {        OLED_DispStr(x, y,  "主电缺相  ", &tFont12);        break;    }
    case 10:    {        OLED_DispStr(x, y,  "相位故障  ", &tFont12);        break;    }
    case 11:    {        OLED_DispStr(x, y,  "通信故障  ", &tFont12);        break;    }
	case 12:    {        OLED_DispStr(x, y,  "余压控制  ", &tFont12);        break;    }
    default:    {        OLED_DispStr(x, y,  "        ", &tFont12);        break;    }

    }
}






/*******************************************************************************
 * 函 数 名: History()
 * 函数功能: 用于显示历史故障
 * 入口参数: x,y表示坐标，data1表示第几个故障，data表示故障代码
 * 返    回: 无
*******************************************************************************/
void DispRelaySet(unsigned char x, unsigned char y, unsigned char type)
{	
	if( DispControler.ucIndex)
	{
		        /* 白底黑字 */
        tFont12.FrontColor = 0;		/* 字体颜色 0 或 1 */
        tFont12.BackColor = 1;		/* 文字背景颜色 0 或 1 */
	}
	
    switch(type)
    {
		case 0: { OLED_DispStr(x, y,  "未定义  ", &tFont12);break;}
		case 1: { OLED_DispStr(x, y,  "自动反馈", &tFont12);break;}
		case 2: { OLED_DispStr(x, y,  "手动反馈", &tFont12);break;}
		case 3: { OLED_DispStr(x, y,  "高速反馈", &tFont12);break;}
		case 4: { OLED_DispStr(x, y,  "高速停止", &tFont12);break;}
		case 5: { OLED_DispStr(x, y,  "低速反馈", &tFont12);break;}
		case 6: { OLED_DispStr(x, y,  "低速停止", &tFont12);break;}
		case 7: { OLED_DispStr(x, y,  "高速故障", &tFont12);break;}
		case 8: { OLED_DispStr(x, y,  "低速故障", &tFont12);break;}
		case 9: { OLED_DispStr(x, y,  "电机运行", &tFont12);break;}
		default:{ OLED_DispStr(x, y,  "        ", &tFont12);break;}
    }
		/* 恢复黑底白字 */
	tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
	tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
}






