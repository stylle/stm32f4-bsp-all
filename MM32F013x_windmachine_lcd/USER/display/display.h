/* 
 * File:   display.h
 * Author: huangzhiming
 *
 * Created on 2018?3?14?, ??4:40
 */

#ifndef DISPLAY_H
#define	DISPLAY_H



#ifdef	__cplusplus
extern "C" {
#endif

typedef struct
{
   volatile unsigned char ucPageNum;
   unsigned char ucPrePageNum;
	 volatile unsigned char ucHold;//区分页面
	
   volatile signed int ucSel;//区分行
   volatile signed int ucPreSel;
   volatile signed int ucIndex;//区分页内确认键值后反显
   volatile signed int ucPreIndex;
  
	 volatile uint16_t ucLCD_Light_tim;
	
	 volatile unsigned char LCD_Version;
	
	volatile unsigned char Machine_version;

}DispControl_T;


extern DispControl_T DispControler;

typedef enum 
{
    START_LCD_PAGE = 0,
    WELCOM_PAGE,
	
    PARAMETER_PAGE,
	STATE_PAGE,
	HISTORY_PAGE,
    
    USERPASSWORDPAGE,
    USERPAGE,

    FACTORYPQSSWORDPAGE,
    FACTORYPAGE_ONE,
	FACTORYPAGE_TWO,
	FACTORYPAGE_THREE,
	FACTORYPAGE_FOUR,
	FACTORYPAGE_FIVE,
	
    SAVEPAGE,
    LIGHTOFF_PAGE,
}PAGE_E;



void displayInit(void);
void display(void);
void LCD_Light_Pro(void);

#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAY_H */

