#ifndef _USERPWD_H
#define	_USERPWD_H



#ifdef	__cplusplus
extern "C" {
#endif


extern PWD_T UserInput;
extern PWD_T UserPwder;

void ClearUserInput(void);
void setUserPwder(uint16_t _number);
uint16_t  getUserPwder(void);
void CopyUserPwderToUserInput(void);

#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAYPACKET_H */
