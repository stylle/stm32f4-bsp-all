/*
*********************************************************************************************************
*
*	模块名称 : 主程序入口
*	文件名称 : main.c
*	版    本 : V1.0
*	说    明 : CPU内部Flash读写例子
*	修改记录 :
*		版本号  日期       作者    说明
*		V1.0    2015-08-30 armfly  首发
*
*	Copyright (C), 2015-2016, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#include "bsp.h"				/* 底层硬件驱动 */

#include "display.h"	
#include "modbus_host.h"
#include "TSK_Thread.h"


void HardWare_Test(void)
{
	uint8_t i=0;
	
	for(i=0;i<LED_NUMS;i++)
	{
		LED_Start(i,50,0);		
	}	
	
	BEEP_Start(FireCtrlBEEP,1500,50,0,0); 
	bsp_DelayMS(1000);
	BEEP_Stop(FireCtrlBEEP);
	
	for(i=0;i<10;i++)
	{
		BEEP2_Troggle();
		bsp_DelayMS(100);		
	}	
	
}




FONT_T tFont12, tFont16;

#define DEMO_PAGE_COUNT		7	/* 演示页面的个数 */


/*
*********************************************************************************************************
*	函 数 名: main
*	功能说明: c程序入口
*	形    参：无
*	返 回 值: 错误代码(无需处理)
*********************************************************************************************************
*/
int main(void)
{
	
			/* 设置字体参数 */
	{
		tFont16.FontCode = FC_ST_16;	/* 字体代码 16点阵 */
		tFont16.FrontColor = 1;		/* 字体颜色 0 或 1 */
		tFont16.BackColor = 0;		/* 文字背景颜色 0 或 1 */
		tFont16.Space = 0;			/* 文字间距，单位 = 像素 */

		tFont12.FontCode = FC_ST_12;	/* 字体代码 12点阵 */
		tFont12.FrontColor = 1;		/* 字体颜色 0 或 1 */
		tFont12.BackColor = 0;		/* 文字背景颜色 0 或 1 */
		tFont12.Space = 0;			/* 文字间距，单位 = 像素 */
	}	
	
	OLED_InitSoft();
	
	DispControler.LCD_Version= 21; //V1.3 功率设置功能调整
	
	/*
		ST固件库中的启动文件已经执行了 SystemInit() 函数，该函数在 system_stm32f4xx.c 文件，主要功能是
	配置CPU系统的时钟，内部Flash访问时序，配置FSMC用于外部SRAM
	*/
	bsp_Init();		/* 硬件初始化 */

	displayInit();
	
	Flash_Init();
		
	OLED_ClrScr(0x00);	 /* 清屏，0x00表示黑底； 0xFF 表示白底 */

	DisplayWelcomePage();	

	HardWare_Test();

	/* 主程序大循环 */
	while (1)
	{
		bsp_Idle();		/* CPU空闲时执行的函数，在 bsp.c */		
		MODH_Poll();
		TSK_Thread_1ms_Funcs();
		TSK_Thread_10ms_Funcs();//继电器动作处理
		TSK_Thread_50ms_Funcs();// 电池电压数据处理，温度数据处理
		TSK_Thread_500ms_Funcs();//led灯控制
		TSK_Thread_1min_Funcs();//soh处理		
		display();	
		TskModbusMgt();

	
	}
}



