
#ifndef DATA_H
#define	DATA_H


#ifdef	__cplusplus
extern "C" {
#endif

#include "bsp.h"
	
	#ifndef DISPLAY_HISTORY 
	#define DISPLAY_HISTORY 0
	#endif

	#define HIGH_POWER_MAX 380
	#define LOW_POWER_MAX  220
	
	#define VOLTAGE_MAX 500
	#define VOLTAGE_MIN  220
	
	#define AD_SAMPLE_MIN_TIME 6	
	#define AD_SAMPLE_MAX_TIME 20
	
typedef enum
{
	RELAY_AUTOBIT_E=1,
	RELAY_HANDBIT_E,
	RELAY_HIGHSPEED_RUNNING_E,
	RELAY_HIGHSPEED_STOP_E,
	RELAY_LOWSPEED_RUNNING_E,
	RELAY_LOWSPEED_STOP_E,
	RELAY_HIGHSPEED_ERROR_E,
	RELAY_LOWSPEED_ERROR_E,
	RELAY_MACHINE_RUNNING,
}RELAY_TYPE_E;


typedef struct
{                                        /* bits  description */
    uint32_t GS_FEEDBACK_FLAG       : 4 ;  /* 0 开关机标志位,      1-开机，0-关机；*/
    uint32_t DS_FEEDBACK_FLAG       : 4 ;  /* 1 充电状态位,        1-打开，0-关闭；*/
    uint32_t GS_ERROR_FLAG     		: 4 ;  /* 2 设备自检标志位,    1-正常，0-异常；*/
    uint32_t DS_ERROR_FLAG      	: 4 ;  /* 3 充电设备故障标志位,1-故障，0-无故障；*/
	uint32_t AUTOBIT_FLAG       	: 4 ;  /* 0 开关机标志位,      1-开机，0-关机；*/
    uint32_t DJYX_FLAG       		: 4 ;  /* 1 充电状态位,        1-打开，0-关闭；*/
    uint32_t REV                    : 8 ;  /* 4 设备自检标志位,    1-正常，0-异常；*/
}Flag_RELAY_BITS;



typedef union 
{
    uint32_t all;
    Flag_RELAY_BITS Byte;
} Flag_RELAY_T;

	
	/*********************************************设备状态结构体*****************************************************/

typedef union
{
	uint32_t all;
	struct 
	{
		uint32_t auto_bit	    :1;         // 自动位
		uint32_t low_running	    :1;         // 低速运行
		uint32_t high_running		:1;         // 高速运行 
		uint32_t fire_ptc	    :1;         //消防联动
		
		uint32_t bas_running  	:1;         // 邻近电池包通信       
		uint32_t fan_runnig   :1;         // 风机运行
		uint32_t low_Oload		:1;         // 温度子板通信
		uint32_t high_Oload   :1;         // BMU通信 rs485
		
		uint32_t low_short		:1;         // 温度子板通信
		uint32_t high_short   :1;         // BMU通信 rs485
		uint32_t windvalve_err		:1;         // 温度子板通信
		uint32_t checkout   :1;         // BMU通信 rs485	
		
		uint32_t grid_overlimit		:1;         // 温度子板通信
		uint32_t gird_underlimit   :1;         // BMU通信 rs485
		uint32_t gird_lackphase		:1;         // 温度子板通信
		uint32_t grid_phaseerr   :1;         // BMU通信 rs485	
		
		uint32_t km1_error   :1;         // BMU通信 rs485
		uint32_t km2_error		:1;         // 温度子板通信
		uint32_t machine_type :1;
		uint16_t rev1 :1;
		
		uint32_t toppressure :1; //余压控制		
		uint32_t checkout_switch :1; //巡检控制开关		
			
		uint32_t rev  :9;
		uint32_t comm_error   :1;         //  rs485	
	}Bits;
}MachineFlagDef;

extern MachineFlagDef MachineFlag;





/*
*********************************************************************************************************
*输出状态
*********************************************************************************************************
*/
typedef enum 
{
	NONE_SPEED=0,
	LOW_SPEED,
	HIGH_SPEED,
}Speed_E;

typedef struct
{
    uint16_t data;          //数据
    uint16_t k;             //斜率
    int16_t b;               //常数
}CountParameter_T;


extern CountParameter_T Main_Power_Data[3];
extern CountParameter_T Current_Data[2];
//故障状态
typedef struct
{
    uint8_t ucError[16];  //buf
    uint32_t ulErrCode; // error code
    uint32_t ulPreErrCode; // pre error code
    uint8_t  ucErrorRef; // error refresh flag
    uint8_t  ucErrorCnt; // how much error occur
    
    uint8_t ucCurrentErrNum; // current error num
	
	uint32_t uiHistory; //错误历史记录
} Error_T ;

typedef struct
{	
	uint8_t ucAuthor;  	//授权
	
	uint8_t ucManOrAuto; //手自动状态 
	
	uint8_t ucOutput;	//输出模式	
	
	uint8_t ucFirePtc; //消防联动	
	
	uint8_t ucBAS; 
	
	Error_T  Error; //故障
	
	uint16_t ucAuthor_time;//授权时间
	
}STA_Para_T;


extern STA_Para_T STAer;



enum {
	MachineType_E,
	H_Power_Select_E,
	L_Power_Select_E,
	FaultSwitch_E,
	Lcd_light_E,
	OverLimit_Switch_E,
	Power_Factor_E,
	AD_Sample_Time_E,
	usOverLoad_multiply_E,
	usCheckout_Switch_E,
	usPower_OverLimit_Switch_E,	
	GS_FEEDBACK_FLAG_E,
	DS_FEEDBACK_FLAG_E, 
	GS_ERROR_FLAG_E,    
	DS_ERROR_FLAG_E,    
	AUTOBIT_FLAG_E,     
	DJYX_FLAG_E,  
};

enum
{
	Factory_H_Power_E,
	Factory_L_Power_E,	
	Factory_VoltageMax_E,
	Factory_VoltageMin_E,	
	Factory_factory_PWD_E,
	Factory_user_PWD_E,
	UserInput_H_Power_E,
	UserInput_L_Power_E,	
	UserInput_VoltageMax_E,
	UserInput_VoltageMin_E,	
	UserInput_factory_PWD_E,
	UserInput_user_PWD_E,		
};

typedef struct 
{		
	uint16_t ucFaultSwitch; //故障开关
	
	uint16_t ucLcd_light;	//背光开关
}
FactoryFlashPara_T;

typedef struct
{
	uint16_t ucMachineType; //风机类型
	
	uint8_t usH_Power_Select; //高速功率选择
	
	uint8_t usL_Power_Select; //低速功率选择

	FactoryFlashPara_T tFlashdata;
	
	uint16_t  usOverLimit_Switch; //显示辐射开关
	
	uint16_t usPower_Factor; //功率因数
	
	uint16_t AD_Sample_Time; //过流采样时间设置
	
	uint16_t usOverLoad_multiply;//过载倍率放大100倍
	
	uint16_t usCheckout_Switch;//检修开关控制 1: 常开有效 0：常闭有效
	
	uint16_t usPower_OverLimit_Switch;//过压开关
	
	Flag_RELAY_T tRelay_flg;

}FactoryParamember_T;



typedef struct
{
	PWD_T H_Power;
	PWD_T L_Power;
	
	PWD_T  factory_PWD;	
	PWD_T  user_PWD;	
	
	PWD_T VoltageMax;//电压上限
	PWD_T VoltageMin;//电压下限
	
	FactoryParamember_T FactoryParamember;

}FactoryPagePara_T;


extern FactoryPagePara_T FactoryPageParaer;
extern FactoryPagePara_T UserInputFactoryPageParaer;




extern const uint16_t HIGH_POWER_LIST[5];
extern const uint16_t LOW_POWER_LIST[5];

#define LOW_POWER_LIST_NUM 5
#define HIGH_POWER_LIST_NUM 5



#define SINGLE_POWER_LIST_NUM 14
#define DOUBLE_H_POWER_LIST_NUM 16
#define DOUBLE_L_POWER_LIST_NUM 16

extern const uint16_t SINGLE_POWER_LIST[SINGLE_POWER_LIST_NUM];
extern const uint16_t DOUBLE_H_POWER_LIST[DOUBLE_H_POWER_LIST_NUM];
extern const uint16_t DOUBLE_L_POWER_LIST[DOUBLE_L_POWER_LIST_NUM];
/*********************************************通信数据处理*****************************************************/

enum  //设备状态枚举状态
{
	MASK_Auto_Bit =0      ,
	MASK_Low_Running      ,
	MASK_High_Running     ,
	MASK_Fire_PTC         ,
	
	MASK_BAS              ,
	MASK_Running			    ,
	
	MASK_Low_OverLoad     ,
	MASK_High_OverLoad    ,
	
	
	MASK_Low_Short        ,
	MASK_High_Short       ,
	MASK_WindValve_Error  ,
	MASK_CheckOut         ,
	
	MASK_Power_OverLimit  ,
	MASK_Power_UnderLimit ,
	MASK_Power_LackPhase  ,
	MASK_Power_PhaseError ,	
	
	MASK_KM1_Error        ,
	MASK_KM2_Error 	      ,
	MASK_MACHINE_TYPE			,
	MASK_OVERLIMIT_SWITCH	,
	
	MASK_TOPPRESSURE			,
	MASK_COM_Error
};



void MODH_Data_BitDeal(uint32_t var);

void MODH_Data_Deal(void);

void LED_Error_Ctrl(void);

void Beep_Error_Ctrl(void);

void Error_Deal(uint32_t var);


void ClearErrBuf(unsigned char* buf);


/*********************************************Flash保存函数*****************************************************/
void Flash_Init(void);
void SaveOneByte(uint32_t _ulFlashAddr,uint8_t *_ucpDst);
void SavePwdToEEPROM(uint32_t _ulFlashAddr,uint8_t *_ucpDst);
void Flash_Write_Update(void);
void RecoverInit(void);
/*********************************************工厂模式数据操作函数*****************************************************/
void CopyFactoryPwderToUserInput(void);
void CopyFactoryPageParaToUserInput(FactoryPagePara_T* factory, FactoryPagePara_T* userinput);
uint8_t Compary_FactoryMember(uint8_t *pSrc,uint8_t *pDst);
void CopyFactoryData(void);
void ClearFactoryUserInput(void);
void CopyPwderToInput(PWD_T *pSrc,PWD_T *pDst);
void PwderToDeliver(PWD_T *pSrc);
void PwderToMerge(PWD_T *pSrc);
uint8_t Compary_Pwder(PWD_T *pSrc,PWD_T *pDst);
void Clear_Pwder(PWD_T *pSrc);

void setFactoryPwder(uint16_t _number);
uint16_t  getFactoryPwder(void);
void setPwderNum(PWD_T * _PWD_t,uint16_t _number);

/*********************************************用户密码输入操作函数*****************************************************/
void CopyUserPwderToUserInput(void);
void ClearUserInput(void);
void Author_Tim_Ctrl(void);

uint16_t getFactoryPara(uint8_t index);
uint16_t setFactoryPara(uint8_t index,uint16_t val);

uint16_t getUserFactoryPara(uint8_t index);
uint16_t setUserFactoryPara(uint8_t index,uint16_t val);
uint16_t get_PwdNum(uint8_t index);
#ifdef	__cplusplus
}
#endif

#endif	/* DATA_H */

