#include "bsp.h"

FactoryPagePara_T UserInputFactoryPageParaer ;
FactoryPagePara_T FactoryPageParaer ;

/*********************************************工厂模式数据操作函数*****************************************************/

void CopyFactoryPageParaToUserInput(FactoryPagePara_T* factory, FactoryPagePara_T* userinput);
void CopyFactoryData(void);
void ClearFactoryUserInput(void);
void CopyPwderToInput(PWD_T *pSrc,PWD_T *pDst);
void PwderToDeliver(PWD_T *pSrc);
void PwderToMerge(PWD_T *pSrc);
uint8_t Compary_Pwder(PWD_T *pSrc,PWD_T *pDst);
void Clear_Pwder(PWD_T *pSrc);

void setPwderNum(PWD_T * _PWD_t,uint16_t _number);

/*
*********************************************************************************************************
*	函 数 名: CopyFactoryPwderToUserInput 
*	功能说明:复制工厂密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void CopyPwderToInput(PWD_T *pSrc,PWD_T *pDst)
{
    pDst->ucPwdBit[0] = pSrc->ucPwdBit[0];
    pDst->ucPwdBit[1] = pSrc->ucPwdBit[1];
    pDst->ucPwdBit[2] = pSrc->ucPwdBit[2];
	pDst->uiPwdNum 		= pSrc->uiPwdNum;
    pDst->ucSelected = 4;
}

/*
*********************************************************************************************************
*	函 数 名: CopyFactoryPwderToUserInput 
*	功能说明:复制工厂密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void PwderToDeliver(PWD_T *pSrc)
{
    pSrc->ucPwdBit[0] = (pSrc->uiPwdNum%1000)/100;
    pSrc->ucPwdBit[1] = (pSrc->uiPwdNum%100)/10;
    pSrc->ucPwdBit[2] = pSrc->uiPwdNum%10;		
    pSrc->ucSelected = 0;
}

/*
*********************************************************************************************************
*	函 数 名: CopyFactoryPwderToUserInput 
*	功能说明:复制工厂密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void PwderToMerge(PWD_T *pSrc)
{
		pSrc->uiPwdNum = (pSrc->ucPwdBit[0]%10)*100+(pSrc->ucPwdBit[1]%10)*10+(pSrc->ucPwdBit[2]%10);
}
/*
*********************************************************************************************************
*	函 数 名: CopyFactoryPwderToUserInput 
*	功能说明:复制工厂密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
uint8_t Compary_Pwder(PWD_T *pSrc,PWD_T *pDst)
{
	if(pDst->uiPwdNum !=pSrc->uiPwdNum)
	{
		return 1;
	}
	else
	{
		return 0;
	}
   
}


uint8_t Compary_FactoryMember(uint8_t *pSrc,uint8_t *pDst)
{
	uint8_t len = sizeof(FactoryParamember_T);
	uint8_t i = 0;
	for(i = 0; i< len; i++)
	{
		if(*pSrc++ != *pDst++)
		{
			return 1;
		}	
	}
	return 0;
   
}

/*
*********************************************************************************************************
*	函 数 名: CopyFactoryPwderToUserInput 
*	功能说明:复制工厂密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void Clear_Pwder(PWD_T *pSrc)
{
	pSrc->ucPwdBit[0] =0;
    pSrc->ucPwdBit[1] =0;
    pSrc->ucPwdBit[2] =0;
	pSrc->uiPwdNum 	  =0;
    pSrc->ucSelected = 0;   
}


uint16_t get_PwdNum(uint8_t index)
{
	switch(index)
	{
		case  Factory_H_Power_E			:  return FactoryPageParaer.H_Power.uiPwdNum;
		case  Factory_L_Power_E         :  return FactoryPageParaer.L_Power.uiPwdNum;
		case  Factory_VoltageMax_E      :  return FactoryPageParaer.VoltageMax.uiPwdNum;
		case  Factory_VoltageMin_E      :  return FactoryPageParaer.VoltageMin.uiPwdNum;
		case  Factory_factory_PWD_E     :  return FactoryPageParaer.factory_PWD.uiPwdNum;
		case  Factory_user_PWD_E        :  return FactoryPageParaer.user_PWD.uiPwdNum;
		case  UserInput_H_Power_E       :  return UserInputFactoryPageParaer.H_Power.uiPwdNum;
		case  UserInput_L_Power_E       :  return UserInputFactoryPageParaer.L_Power.uiPwdNum;
		case  UserInput_VoltageMax_E    :  return UserInputFactoryPageParaer.VoltageMax.uiPwdNum;
		case  UserInput_VoltageMin_E    :  return UserInputFactoryPageParaer.VoltageMin.uiPwdNum;
		case  UserInput_factory_PWD_E   :  return UserInputFactoryPageParaer.factory_PWD.uiPwdNum;
		case  UserInput_user_PWD_E      :  return UserInputFactoryPageParaer.user_PWD.uiPwdNum;			
	}
	return 0;
}

uint16_t set_PwdNum(uint8_t index,uint16_t val)
{
	switch(index)
	{
		case  Factory_H_Power_E			:   FactoryPageParaer.H_Power.uiPwdNum  =val;               PwderToDeliver(&FactoryPageParaer.H_Power  );break;
		case  Factory_L_Power_E         :   FactoryPageParaer.L_Power.uiPwdNum  =val;               PwderToDeliver(&FactoryPageParaer.L_Power  );break;
		case  Factory_VoltageMax_E      :   FactoryPageParaer.VoltageMax.uiPwdNum =val;             PwderToDeliver(&FactoryPageParaer.VoltageMax );break;
		case  Factory_VoltageMin_E      :   FactoryPageParaer.VoltageMin.uiPwdNum=val;              PwderToDeliver(&FactoryPageParaer.VoltageMin);break;
		case  Factory_factory_PWD_E     :   FactoryPageParaer.factory_PWD.uiPwdNum=val;             PwderToDeliver(&FactoryPageParaer.factory_PWD);break;
		case  Factory_user_PWD_E        :   FactoryPageParaer.user_PWD.uiPwdNum=val;                PwderToDeliver(&FactoryPageParaer.user_PWD);break;
		case  UserInput_H_Power_E       :   UserInputFactoryPageParaer.H_Power.uiPwdNum=val;        PwderToDeliver(&UserInputFactoryPageParaer.H_Power);break;
		case  UserInput_L_Power_E       :   UserInputFactoryPageParaer.L_Power.uiPwdNum=val;        PwderToDeliver(&UserInputFactoryPageParaer.L_Power);break;
		case  UserInput_VoltageMax_E    :   UserInputFactoryPageParaer.VoltageMax.uiPwdNum=val;     PwderToDeliver(&UserInputFactoryPageParaer.VoltageMax);break;
		case  UserInput_VoltageMin_E    :   UserInputFactoryPageParaer.VoltageMin.uiPwdNum=val;     PwderToDeliver(&UserInputFactoryPageParaer.VoltageMin);break;
		case  UserInput_factory_PWD_E   :   UserInputFactoryPageParaer.factory_PWD.uiPwdNum=val;    PwderToDeliver(&UserInputFactoryPageParaer.factory_PWD);break;
		case  UserInput_user_PWD_E      :   UserInputFactoryPageParaer.user_PWD.uiPwdNum=val;		PwderToDeliver(&UserInputFactoryPageParaer.user_PWD);break;
	}
	return 0;
}



/*
*********************************************************************************************************
*	函 数 名: CopyFactoryData 
*	功能说明: 复制工厂模式参数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void CopyFactoryData(void)
{
	memcpy((uint8_t*)&UserInputFactoryPageParaer,(uint8_t*)&FactoryPageParaer,sizeof(FactoryPagePara_T));
}

/*
*********************************************************************************************************
*	函 数 名: ClearFactoryUserInput 
*	功能说明: 清除工厂模式用户输入参数
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void ClearFactoryUserInput(void)
{
		memset((uint8_t*)&UserInputFactoryPageParaer,0,sizeof(FactoryPagePara_T));	
		Clear_Pwder(&UserInput);
}
/*
*********************************************************************************************************
*	函 数 名: setPwderNum 
*	功能说明: 设置用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void setPwderNum(PWD_T * _PWD_t,uint16_t _number)
{
	_PWD_t->uiPwdNum = _number;	
	PwderToDeliver(_PWD_t);
}

/*
*********************************************************************************************************
*	函 数 名: SetUserPwder 
*	功能说明: 设置用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void setFactoryPwder(uint16_t _number)
{
	FactoryPageParaer.factory_PWD.uiPwdNum = _number;	
	PwderToDeliver(&FactoryPageParaer.factory_PWD);
}

/*
*********************************************************************************************************
*	函 数 名: SetUserPwder 
*	功能说明: 设置用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
uint16_t  getFactoryPwder(void)
{
	return FactoryPageParaer.factory_PWD.uiPwdNum;	
}


uint16_t getFactoryPara(uint8_t index)
{
	switch(index)
	{
		case  MachineType_E             :	return  FactoryPageParaer.FactoryParamember.ucMachineType  					;
		case  H_Power_Select_E          :   return  FactoryPageParaer.FactoryParamember.usH_Power_Select                ;
		case  L_Power_Select_E          :   return  FactoryPageParaer.FactoryParamember.usL_Power_Select                ;
		case  FaultSwitch_E             :   return  FactoryPageParaer.FactoryParamember.tFlashdata.ucFaultSwitch        ;
		case  Lcd_light_E               :   return  FactoryPageParaer.FactoryParamember.tFlashdata.ucLcd_light          ;
		case  OverLimit_Switch_E        :   return  FactoryPageParaer.FactoryParamember.usOverLimit_Switch              ;
		case  Power_Factor_E            :   return  FactoryPageParaer.FactoryParamember.usPower_Factor                	;
		case  AD_Sample_Time_E          :   return  FactoryPageParaer.FactoryParamember.AD_Sample_Time                	;
		case  usOverLoad_multiply_E     :   return  FactoryPageParaer.FactoryParamember.usOverLoad_multiply             ;
		case  usCheckout_Switch_E       :   return  FactoryPageParaer.FactoryParamember.usCheckout_Switch               ;
		case  usPower_OverLimit_Switch_E:   return  FactoryPageParaer.FactoryParamember.usPower_OverLimit_Switch        ;
		case  GS_FEEDBACK_FLAG_E        :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_FEEDBACK_FLAG;
		case  DS_FEEDBACK_FLAG_E        :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG;
		case  GS_ERROR_FLAG_E           :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_ERROR_FLAG   ;  
		case  DS_ERROR_FLAG_E           :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG   ;
		case  AUTOBIT_FLAG_E            :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.AUTOBIT_FLAG    ;
		case  DJYX_FLAG_E               :   return  FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG       ;
	}
	return 0;
}

uint16_t setFactoryPara(uint8_t index,uint16_t val)
{
	switch(index)
	{
		case  MachineType_E             :	  FactoryPageParaer.FactoryParamember.ucMachineType 				  =val			;	break;
		case  H_Power_Select_E          :     FactoryPageParaer.FactoryParamember.usH_Power_Select                =val			;   break;
		case  L_Power_Select_E          :     FactoryPageParaer.FactoryParamember.usL_Power_Select                =val			;   break;
		case  FaultSwitch_E             :     FactoryPageParaer.FactoryParamember.tFlashdata.ucFaultSwitch        =val			;   break;
		case  Lcd_light_E               :     FactoryPageParaer.FactoryParamember.tFlashdata.ucLcd_light          =val			;   break;
		case  OverLimit_Switch_E        :     FactoryPageParaer.FactoryParamember.usOverLimit_Switch              =val			;   break;
		case  Power_Factor_E            :     FactoryPageParaer.FactoryParamember.usPower_Factor                  =val			;   break;
		case  AD_Sample_Time_E          :     FactoryPageParaer.FactoryParamember.AD_Sample_Time                  =val			;   break;
		case  usOverLoad_multiply_E     :     FactoryPageParaer.FactoryParamember.usOverLoad_multiply             =val			;   break;
		case  usCheckout_Switch_E       :     FactoryPageParaer.FactoryParamember.usCheckout_Switch               =val			;   break;
		case  usPower_OverLimit_Switch_E:     FactoryPageParaer.FactoryParamember.usPower_OverLimit_Switch        =val        	;   break;
		case  GS_FEEDBACK_FLAG_E        :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_FEEDBACK_FLAG=val          ;   break;
		case  DS_FEEDBACK_FLAG_E        :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG=val          ;   break;
		case  GS_ERROR_FLAG_E           :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_ERROR_FLAG   =val          ;   break;
		case  DS_ERROR_FLAG_E           :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG   =val          ;   break;
		case  AUTOBIT_FLAG_E            :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.AUTOBIT_FLAG    =val          ;   break;
		case  DJYX_FLAG_E               :     FactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG       =val          ;   break;
	}
	return 1;
}

uint16_t getUserFactoryPara(uint8_t index)
{
	switch(index)
	{
		case  MachineType_E             :	return  UserInputFactoryPageParaer.FactoryParamember.ucMachineType  			     ;
		case  H_Power_Select_E          :   return  UserInputFactoryPageParaer.FactoryParamember.usH_Power_Select                ;
		case  L_Power_Select_E          :   return  UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select                ;
		case  FaultSwitch_E             :   return  UserInputFactoryPageParaer.FactoryParamember.tFlashdata.ucFaultSwitch        ;
		case  Lcd_light_E               :   return  UserInputFactoryPageParaer.FactoryParamember.tFlashdata.ucLcd_light          ;
		case  OverLimit_Switch_E        :   return  UserInputFactoryPageParaer.FactoryParamember.usOverLimit_Switch              ;
		case  Power_Factor_E            :   return  UserInputFactoryPageParaer.FactoryParamember.usPower_Factor                	 ;
		case  AD_Sample_Time_E          :   return  UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time                	 ;
		case  usOverLoad_multiply_E     :   return  UserInputFactoryPageParaer.FactoryParamember.usOverLoad_multiply             ;
		case  usCheckout_Switch_E       :   return  UserInputFactoryPageParaer.FactoryParamember.usCheckout_Switch               ;
		case  usPower_OverLimit_Switch_E:   return  UserInputFactoryPageParaer.FactoryParamember.usPower_OverLimit_Switch        ;
		case  GS_FEEDBACK_FLAG_E        :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_FEEDBACK_FLAG;
		case  DS_FEEDBACK_FLAG_E        :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG;
		case  GS_ERROR_FLAG_E           :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_ERROR_FLAG   ;  
		case  DS_ERROR_FLAG_E           :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG   ;
		case  AUTOBIT_FLAG_E            :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.AUTOBIT_FLAG    ;
		case  DJYX_FLAG_E               :   return  UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG       ;
	}
	return 0;
}

uint16_t setUserFactoryPara(uint8_t index,uint16_t val)
{
	switch(index)
	{
		case  MachineType_E             :	  UserInputFactoryPageParaer.FactoryParamember.ucMachineType 				   =val			;      break;
		case  H_Power_Select_E          :     UserInputFactoryPageParaer.FactoryParamember.usH_Power_Select                =val			;      break;
		case  L_Power_Select_E          :     UserInputFactoryPageParaer.FactoryParamember.usL_Power_Select                =val			;      break;
		case  FaultSwitch_E             :     UserInputFactoryPageParaer.FactoryParamember.tFlashdata.ucFaultSwitch        =val			;      break;
		case  Lcd_light_E               :     UserInputFactoryPageParaer.FactoryParamember.tFlashdata.ucLcd_light          =val			;      break;
		case  OverLimit_Switch_E        :     UserInputFactoryPageParaer.FactoryParamember.usOverLimit_Switch              =val			;      break;
		case  Power_Factor_E            :     UserInputFactoryPageParaer.FactoryParamember.usPower_Factor                  =val			;      break;
		case  AD_Sample_Time_E          :     UserInputFactoryPageParaer.FactoryParamember.AD_Sample_Time                  =val			;      break;
		case  usOverLoad_multiply_E     :     UserInputFactoryPageParaer.FactoryParamember.usOverLoad_multiply             =val			;      break;
		case  usCheckout_Switch_E       :     UserInputFactoryPageParaer.FactoryParamember.usCheckout_Switch               =val			;      break;
		case  usPower_OverLimit_Switch_E:     UserInputFactoryPageParaer.FactoryParamember.usPower_OverLimit_Switch        =val        	;      break;
		case  GS_FEEDBACK_FLAG_E        :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_FEEDBACK_FLAG=val         ;      break;
		case  DS_FEEDBACK_FLAG_E        :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_FEEDBACK_FLAG=val         ;      break;
		case  GS_ERROR_FLAG_E           :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.GS_ERROR_FLAG   =val         ;      break;
		case  DS_ERROR_FLAG_E           :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DS_ERROR_FLAG   =val         ;      break;
		case  AUTOBIT_FLAG_E            :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.AUTOBIT_FLAG    =val         ;      break;
		case  DJYX_FLAG_E               :     UserInputFactoryPageParaer.FactoryParamember.tRelay_flg.Byte.DJYX_FLAG       =val         ;      break;
	}
	return 1;
}

