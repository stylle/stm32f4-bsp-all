#include "bsp.h"

void CopyUserPwderToUserInput(void);
void ClearUserInput(void);

PWD_T UserInput = {{0, 0, 0}, 0, 0};

/*
*********************************************************************************************************
*	函 数 名: ClearUserInput 
*	功能说明:清除用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void ClearUserInput(void)
{
    unsigned char i;
    for(i = 0; i<3; i++)
    {
       UserInput.ucPwdBit[i] = 0;
    }
    UserInput.ucSelected = 0;
}


/*
*********************************************************************************************************
*	函 数 名: SetUserPwder 
*	功能说明: 设置用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void setUserPwder(uint16_t _number)
{
	FactoryPageParaer.user_PWD.uiPwdNum = _number;	
	PwderToDeliver(&FactoryPageParaer.user_PWD);
}

/*
*********************************************************************************************************
*	函 数 名: SetUserPwder 
*	功能说明: 设置用户输入密码
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
uint16_t  getUserPwder(void)
{
	return FactoryPageParaer.user_PWD.uiPwdNum;	
}

/*
*********************************************************************************************************
*	函 数 名: CopyUserPwderToUserInput 
*	功能说明:复制用户密码参数到用户输入参数中
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void CopyUserPwderToUserInput(void)
{
    UserInput.ucPwdBit[0] = FactoryPageParaer.user_PWD.ucPwdBit[0];
    UserInput.ucPwdBit[1] = FactoryPageParaer.user_PWD.ucPwdBit[1];
    UserInput.ucPwdBit[2] = FactoryPageParaer.user_PWD.ucPwdBit[2];
    UserInput.ucSelected = 4;
}
