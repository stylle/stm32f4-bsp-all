/**********************************************************************
* Copyright (C),2017-2030, SinceHuaFu Tech. Co., Ltd.
* File name:TSK_Thread.c
* Devices: STM32F030xx
* Description: 系统时间片段生成，分时任务调度
* Others: 
* Author: LiuDong
* Version:V1.0
* Date: 2017.04.07   
* History:
**********************************************************************/
#ifndef __TSK_THREAD_C__
#define __TSK_THREAD_C__

#include "bsp.h"			// Main include file
#include "TSK_Thread.h"
#include "Command.h"
/******************************************************************
* 函数名称：TSK_Thread_Init
* 功能描述：分时任务初始化
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
void TSK_Thread_Init(void)
{
    memset((void*)&TaskRegs,0,sizeof(TaskRegs));
    TaskRegs.SystemRunTimer = 0;
    TaskRegs.Relay_ON_Timer = 0;
    TaskRegs.Relay_OFF_Timer = 0;
}

/******************************************************************
* 函数名称：TSK_Thread_Timer0Isr_Func
* 功能描述：分时任务标志生成程序
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
//#pragma CODE_SECTION(TSK_Thread_Timer0Isr_Func,"ramfuncs");
void TSK_Thread_Timer0Isr_Func(void)
{
    TaskRegs.Tsk_1msFlg = TRUE;
	
    TaskRegs.Cnt_10ms++;           
    if(TaskRegs.Cnt_10ms>=10)
    {
        TaskRegs.Cnt_10ms=0;       											//10ms
    }
    switch(TaskRegs.Cnt_10ms)
    {
        case 0:
        {
            TaskRegs.Cnt_50ms++;    
            if(TaskRegs.Cnt_50ms>=5)
            {
                TaskRegs.Cnt_50ms=0;										//50ms
            }

            switch(TaskRegs.Cnt_50ms)
            {
                case 0:
                {
                    TaskRegs.Cnt_500ms++;    
                    if(TaskRegs.Cnt_500ms>=10)
                    {
                        TaskRegs.Cnt_500ms=0;          //500ms
                    }
                    switch(TaskRegs.Cnt_500ms)
                    {
                        case 0:
                        {
                            TaskRegs.Cnt_1min++; 
                            if(TaskRegs.Cnt_1min >= 120)
                            {
                                TaskRegs.Cnt_1min = 0;
                                TaskRegs.Tsk_1minFlg = TRUE;
                            }
                        }
                        break;
												
                        case 1:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_1_Flg = TRUE;
                        }
                        break;
												
                        case 2:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_2_Flg = TRUE;
                        }
                        break;
												
                        case 3:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_3_Flg = TRUE;
                        }
                        break;
												
                        case 4:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_4_Flg = TRUE;
                        }
                        break;
												
                        case 5:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_5_Flg = TRUE;
                        }
                        break;
												
                        case 6:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_6_Flg = TRUE;
                        }
                        break;
												
                        case 7:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_7_Flg = TRUE;
                        }
                        break;
							
                        case 8:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_8_Flg = TRUE;
                        }
                        break;
												
                        case 9:
                        {
                            TaskRegs.Tsk_500msFlg.Byte.Byte_H.Tsk_9_Flg = TRUE;
                        }
                        break;
												
                        default:
                        {
                            ;
                        }
                        break;
                    }
                }
                break;

                case 1:
                {
                    TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_1_Flg	= TRUE;
                }
                break;
								
                case 2:
                {
                    TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_2_Flg	= TRUE;
                }
                break;
								
                case 3:
                {
                    TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_3_Flg	= TRUE;
                }
                break;
								
                case 4:
                {
                    TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_4_Flg	= TRUE;
                }
                break;
								
                default:
                {
                    ;
                }
                break;
            }
        }
        break;
				
        case 1:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_1_Flg	= TRUE;
        }
        break;
				
        case 2:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_2_Flg	= TRUE;
        }
        break;
				
        case 3:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_3_Flg	= TRUE;
        }
        break;
				
        case 4:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_4_Flg	= TRUE;
        }
        break;
				
        case 5:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_5_Flg	= TRUE;
        }
        break;
				
        case 6:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_6_Flg	= TRUE;
        }
        break;
				
        case 7:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_7_Flg	= TRUE;
        }
        break;
				
        case 8:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_8_Flg	= TRUE;
        }
        break;
				
        case 9:
        {
            TaskRegs.Tsk_10msFlg.Byte.Byte_H.Tsk_9_Flg	= TRUE;
        }
        break;
				
        default:
        {
            ;
        }
        break;
    }
}   // end of TSK_Thread_Func()

/******************************************************************
* 函数名称：TSK_Thread_1min_Funcs
* 功能描述：调度周期为1分钟的任务管理
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
void TSK_Thread_1min_Funcs(void)
{
    if(TRUE	== TaskRegs.Tsk_1minFlg)
    {
			

        TaskRegs.Tsk_1minFlg = FALSE;
    }
}

/******************************************************************
* 函数名称：TSK_Thread_500ms_Funcs
* 功能描述：调度周期为500ms的任务管理
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
volatile unsigned char update;
void TSK_Thread_500ms_Funcs(void)
{
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_1_Flg)
    {
				update^=0xff;
				STAer.Error.ucErrorRef ^=0xff;
				TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_1_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_2_Flg)
    {
        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_2_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_3_Flg)
    {

				 
        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_3_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_4_Flg )
    {	

        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_4_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_5_Flg)
    {

        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_5_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_6_Flg)
    {

        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_6_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_7_Flg)
    {

        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_7_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_8_Flg)
    {

        TaskRegs.Tsk_500msFlg.Byte.Byte_L.Tsk_8_Flg =FALSE;
    }
    if(TRUE	== TaskRegs.Tsk_500msFlg.Byte.Byte_H.Tsk_9_Flg)
    {
        static uint16_t CounterTemp=0;
        CounterTemp++;
			  

        if(CounterTemp&0x0001)                               //	1/2 TIMER
        {
					/********* 1s timer (System)********/
            if(UINT16_MAX > TaskRegs.SystemRunTimer)
            {
                TaskRegs.SystemRunTimer++;
                if(TaskRegs.SystemRunTimer > TIME_BASE1S_2S)
                {
//                    ControlReg.FlagLogic.bits.PowerOn_Timer2s = BITSET;
//									 ControlReg.FlagLogic.bits.System_Init  =BITSET;
                }
            }
//						{
//							 RTC_TimeTypeDef RTC_TimeStruct;            
//							 RTC_DateTypeDef RTC_DateStruct;

//							 HAL_RTC_GetTime(&RTC_Handler,&RTC_TimeStruct,RTC_FORMAT_BIN);
//							printf("Time:%02d:%02d:%02d",RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds);

//							HAL_RTC_GetDate(&RTC_Handler,&RTC_DateStruct,RTC_FORMAT_BIN);
//							printf("Date:20%02d-%02d-%02d",RTC_DateStruct.Year,RTC_DateStruct.Month,RTC_DateStruct.Date);

//						
//						}

							Author_Tim_Ctrl();
							InputAuthorMonitor();
   
        }

        TaskRegs.Tsk_500msFlg.Byte.Byte_H.Tsk_9_Flg =FALSE;
    }

}//end of TSK_500ms

/******************************************************************
* 函数名称：TSK_Thread_50ms_Funcs
* 功能描述：调度周期为50ms的任务管理
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/

void TSK_Thread_50ms_Funcs(void)
{
    if(TRUE	== TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_1_Flg)
    {
				LCD_Light_Pro();
        TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_1_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_2_Flg)
    {
			LED_Error_Ctrl();
		    TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_2_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_3_Flg)
    {
			
				Beep_Error_Ctrl();
        TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_3_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_4_Flg)
    {
							
        TaskRegs.Tsk_50msFlg.Byte.Byte_L.Tsk_4_Flg	= FALSE;
    }
} 

/******************************************************************
* 函数名称：TSK_Thread_10ms_Funcs
* 功能描述：调度周期为10ms的任务管理
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
void TSK_Thread_10ms_Funcs(void)
{
    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_1_Flg)
    {	
				
        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_1_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_2_Flg)
    {
				bsp_InputScan();
        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_2_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_3_Flg)
    {
        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_3_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_4_Flg)
    {
				
        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_4_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_5_Flg)
    {				

        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_5_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_6_Flg)
    {
      

        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_6_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_7_Flg)
    {
				
        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_7_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_8_Flg)
    {

        TaskRegs.Tsk_10msFlg.Byte.Byte_L.Tsk_8_Flg	= FALSE;
    }

    if(TRUE	== TaskRegs.Tsk_10msFlg.Byte.Byte_H.Tsk_9_Flg)
    {

        TaskRegs.Tsk_10msFlg.Byte.Byte_H.Tsk_9_Flg	= FALSE;
    }
} 

/******************************************************************
* 函数名称：TSK_Thread_1ms_Funcs
* 功能描述：调度周期为1ms的任务管理
* 输入参数：无
* 输出参数：无
* 补充说明：无
*******************************************************************/
void TSK_Thread_1ms_Funcs(void)
{
    if(TRUE	== TaskRegs.Tsk_1msFlg)
    {

        TaskRegs.Tsk_1msFlg	= FALSE;
    }
} 

//--------------------------------------------------------------------

#endif  // end of __TSK_THREAD_C__ definition

//===========================================================================
// End of file.
//===========================================================================
