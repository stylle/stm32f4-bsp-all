/**********************************************************************
* Copyright (C),2017-2030, SinceHuaFu Tech. Co., Ltd.
* File name:TSK_Thread.h
* Devices: STM32F030xx
* Description: 
* Others: 
* Author: LiuDong
* Version:V1.0
* Date: 2017.04.07   
* History:
**********************************************************************/


#ifndef __TSK_THREAD_H__
#define __TSK_THREAD_H__

/*********************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------
// Constant Definitions
//
#ifndef TRUE
#define TRUE                                 (1)
#endif
#ifndef FALSE
#define FALSE                                (0)
#endif

//#define UINT16_MAX 		       0xFFFF
//#define UINT32_MAX 		       0xFFFFFFFF

#define TIME_BASE1MS_5MS       (5)      //5MS@1MS
#define TIME_BASE1MS_8MS       (8)	    //8MS@1MS
#define TIME_BASE1MS_10MS      (10)	    //10MS@1MS
#define TIME_BASE1MS_20MS      (20)	    //20MS@1MS
#define TIME_BASE1MS_40MS      (40)	    //20MS@1MS
#define TIME_BASE1MS_50MS      (50)	    //50MS@1MS
#define TIME_BASE1MS_100MS     (100)    //100MS@1MS
#define TIME_BASE1MS_200MS     (200)	//200MS@1MS
#define TIME_BASE1MS_1S        (1000)	//1S@1MS
#define TIME_BASE1MS_3S        (3000)	//3S@1MS
#define TIME_BASE1MS_5S        (5000)   //5S@1MS
#define TIME_BASE1MS_30S       (30000)  //30S@1MS

#define TIME_BASE10MS_30MS     (3)	    //30MS@10MS
#define TIME_BASE10MS_40MS     (4)	    //40MS@10MS
#define TIME_BASE10MS_60MS     (6)	    //60MS@10MS
#define TIME_BASE10MS_80MS     (8)	    //80MS@10MS
#define TIME_BASE10MS_100MS    (10)	    //100MS@10MS
#define TIME_BASE10MS_160MS    (16)	    //160MS@10MS
#define TIME_BASE10MS_200MS    (20)	    //200MS@10MS
#define TIME_BASE10MS_400MS    (40)	    //400MS@10MS
#define TIME_BASE10MS_1S       (100)	//1S@10MS
#define TIME_BASE10MS_2S       (200)	//2S@10MS
#define TIME_BASE10MS_3S	     (300)	//3S@10MS
#define TIME_BASE10MS_5S       (500)	//5S@10MS
#define TIME_BASE10MS_6S       (600)	//6S@10MS
#define TIME_BASE10MS_10S      (1000)	//10S@10MS
#define TIME_BASE10MS_11S      (1100)	//11S@10MS
#define TIME_BASE10MS_20S      (2000)	//20S@10MS
#define TIME_BASE10MS_1MIN     (6500)	//1MIN@10MS
#define TIME_BASE10MS_10MIN    (61000)  //10MIN@10MS
#define TIME_BASE10MS_30MIN    (181000) //30MIN@10MS
#define TIME_BASE10MS_60MIN    (361000) //60MIN@10MS

#define TIME_BASE50MS_100MS    (2)      //100MS@50MS
#define TIME_BASE50MS_200MS    (4)      //200MS@50MS
#define TIME_BASE50MS_400MS    (8)      //400MS@50MS
#define TIME_BASE50MS_500MS    (10)     //500MS@50MS
#define TIME_BASE50MS_600MS    (12)     //600MS@50MS
#define TIME_BASE50MS_800MS    (16)     //800MS@50MS
#define TIME_BASE50MS_1S       (20)     //1S@50MS
#define TIME_BASE50MS_1S5      (30)     //1.5S@50MS
#define TIME_BASE50MS_2S       (40)     //2S@50MS
#define TIME_BASE50MS_3S       (60)     //3S@50MS
#define TIME_BASE50MS_5S       (100)    //5S@50MS
#define TIME_BASE50MS_9S       (180)    //9S@50MS
#define TIME_BASE50MS_10S      (200)    //10S@50MS
#define TIME_BASE50MS_30S      (600)    //30S@50MS
#define TIME_BASE50MS_40S      (800)    //40S@50MS
#define TIME_BASE50MS_90S      (1800)   //75S@50MS

#define TIME_BASE500MS_1S      (2)      //1S@500MS
#define TIME_BASE500MS_2S      (4)      //2S@500MS
#define TIME_BASE500MS_3S      (6)      //3S@500MS
#define TIME_BASE500MS_5S      (5)      //3S@500MS
#define TIME_BASE500MS_10S     (20)     //10S@500MS
#define TIME_BASE500MS_30S     (60)     //6s@500ms
#define TIME_BASE500MS_1MIN    (120)    //1min@500MS
#define TIME_BASE500MS_3MIN    (360)    //3min@500MS
#define TIME_BASE500MS_5MIN    (600)    //5min@500MS
#define TIME_BASE500MS_60MIN   (7200)   //60min@500MS


#define TIME_BASE1S_2S         (2)      //3S@1S
#define TIME_BASE1S_3S         (3)      //3S@1S
#define TIME_BASE1S_10S        (10)     //10S@1S
#define TIME_BASE1S_20S        (20)     //20S@1S
#define TIME_BASE1S_30S        (30)     //30S@1S
#define TIME_BASE1S_40S        (40)     //40S@1S
#define TIME_BASE1S_50S        (50)     //50S@1S
#define TIME_BASE1S_1MIN       (60)     //1min@1S
#define TIME_BASE1S_5MIN       (300)    //5min@1S
#define TIME_BASE1S_10MIN      (600)    //10min@1S
#define TIME_BASE1S_20MIN      (1200)   //20min@1S
#define TIME_BASE1S_30MIN      (1800)   //30min@1S
#define TIME_BASE1S_40MIN      (2400)   //40min@1S
#define TIME_BASE1S_50MIN      (3000)   //50min@1S
#define TIME_BASE1S_60MIN      (3600)   //60min@1S

//---------------------------------------------------------------------------
// Macros
//

//---------------------------------------------------------------------------
// Global Variable References
//
typedef struct __TSK_FLAG_H_BITS
{
    uint8_t Tsk_9_Flg    :1;  
    uint8_t Tsk_RSD:7;
}TSK_FLAG_H_BITS;

typedef struct __TSK_FLAG_L_BITS
{
    uint8_t Tsk_1_Flg:1;
    uint8_t Tsk_2_Flg:1;
    uint8_t Tsk_3_Flg:1;
    uint8_t Tsk_4_Flg:1;
    uint8_t Tsk_5_Flg:1;
    uint8_t Tsk_6_Flg:1;
    uint8_t Tsk_7_Flg:1;
    uint8_t Tsk_8_Flg:1;
}TSK_FLAG_L_BITS;

typedef struct __TSK_10MS_BITS		  //TSK_10MS_BITS
{
    TSK_FLAG_L_BITS  Byte_L;
    TSK_FLAG_H_BITS Byte_H;
} TSK_10MS_BITS;

typedef union __TSK_10MS_FLG
{
    uint16_t all;
    TSK_10MS_BITS Byte;
} TSK_10MS_FLG;

typedef struct __TSK_50MS_BITS		 //TSK_50MS_BITS
{
   TSK_FLAG_L_BITS  Byte_L;
} TSK_50MS_BITS;

typedef union __TSK_50MS_FLG
{
  uint8_t all;
  TSK_50MS_BITS Byte;
} TSK_50MS_FLG;

typedef struct TSK_500MS_BITS		 //TSK_500MS_BITS
{
    TSK_FLAG_L_BITS  Byte_L;
    TSK_FLAG_H_BITS Byte_H;
} TSK_500MS_BITS;

typedef union __TSK_500MS_FLG
{
  uint16_t all;
  TSK_500MS_BITS Byte;
} TSK_500MS_FLG;


typedef volatile struct __TSK_REGS	//TSK_REGS
{
	uint8_t			     Tsk_1msFlg;
	uint8_t			     Tsk_1minFlg;
	TSK_10MS_FLG	 Tsk_10msFlg;
	TSK_50MS_FLG	 Tsk_50msFlg;
	TSK_500MS_FLG  Tsk_500msFlg;
	uint8_t			     Cnt_10ms;
	uint8_t			     Cnt_50ms;
	uint8_t		 	     Cnt_500ms;
	uint8_t			     Cnt_1s;
	uint8_t			     Cnt_1min;
	uint16_t  	     SystemRunTimer;			//(s)
	uint16_t         Relay_ON_Timer;
	uint16_t         Relay_OFF_Timer;
}TSK_REGS;


/***************************************************************************/

#ifndef __TSK_THREAD_C__
#define TSK_THREAD_EXTERN  extern
#else
#define TSK_THREAD_EXTERN
#endif

//---------------------------------------------------------------------------
// Global Variable References
//
TSK_THREAD_EXTERN TSK_REGS TaskRegs;

//---------------------------------------------------------------------------
// Function Prototypes
//
TSK_THREAD_EXTERN void TSK_Thread_1min_Funcs(void);
TSK_THREAD_EXTERN void TSK_Thread_500ms_Funcs(void);
TSK_THREAD_EXTERN void TSK_Thread_50ms_Funcs(void);
TSK_THREAD_EXTERN void TSK_Thread_10ms_Funcs(void);
TSK_THREAD_EXTERN void TSK_Thread_1ms_Funcs(void);

TSK_THREAD_EXTERN void TSK_Thread_Timer0Isr_Func(void);
TSK_THREAD_EXTERN void TSK_Thread_Init(void);

/****************************************************************************/

#ifdef __cplusplus
}
#endif /* extern "C" */

//---------------------------------------------------------------------------
#endif  // end of __SWI_THREAD_H__ definition
//===========================================================================
// End of file.
//===========================================================================
