#include "bsp.h"

/*********************************************Flash保存函数*****************************************************/
#define FLASH_INIT_DATA DispControler.LCD_Version
#define NUM_FLASH_STORE 16

#define FATORY_PWD_INIT 10
#define USER_PWD_INIT  10

__IO  static uint16_t s_usFlash_Init_Val=0;


void Flash_Init(void);
void SaveOneByte(uint32_t _ulFlashAddr,uint8_t *_ucpDst);
void SavePwdToEEPROM(uint32_t _ulFlashAddr,uint8_t *_ucpDst);
void Flash_Write_Update(void);
void RecoverInit(void);




/*
*********************************************************************************************************
*	函 数 名: Flash_Write_Update 
*	功能说明:FLASH 写入更新
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void setFlashInitVal(uint16_t _num)
{
	s_usFlash_Init_Val = _num;
}
/*
*********************************************************************************************************
*	函 数 名: Flash_Write_Update 
*	功能说明:FLASH 写入更新
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
uint16_t getFlashInitVal(void)
{
	return s_usFlash_Init_Val;
}


/*
*********************************************************************************************************
*	函 数 名: Flash_Write_Update 
*	功能说明:FLASH 写入更新
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void Flash_Write_Update(void)
{
	
		uint16_t buf[NUM_FLASH_STORE/2]={0};

		FLASH_SIM_EEPROM_Init();

		
		buf[0] = getFlashInitVal();
		buf[1] = getFactoryPwder();
		buf[2] = getFactoryPara(FaultSwitch_E);
		buf[3] = getFactoryPara(Lcd_light_E);
		buf[4] = getUserPwder();

		EEPROM_Write((uint8_t *)buf, NUM_FLASH_STORE);			

		
		bsp_DelayUS(500);
		
		EEPROM_Read((uint8_t *)buf,8);
		
		s_usFlash_Init_Val = buf[0];
		setFactoryPwder(buf[1]) ;
		setFactoryPara(FaultSwitch_E,buf[2]);
		setFactoryPara(Lcd_light_E,buf[3]);

		setUserPwder(buf[4]);
}



/*
*********************************************************************************************************
*	函 数 名: Flash_Init 
*	功能说明:FLASH 初始化
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void Flash_Init(void)
{
	
		uint16_t buf[NUM_FLASH_STORE/2]={0};

		EEPROM_Read((uint8_t *)buf,NUM_FLASH_STORE);
		
		setFlashInitVal(buf[0]) ;
		
	if(getFlashInitVal() == FLASH_INIT_DATA)
	{
		setFactoryPwder(buf[1]) ;
		setFactoryPara(FaultSwitch_E,buf[2]);
		setFactoryPara(Lcd_light_E,buf[3]);
		setUserPwder(buf[4]);


		if(  getFactoryPara(FaultSwitch_E) == 0xffff 
			|| getFactoryPara(Lcd_light_E) == 0xffff 
			|| getFactoryPwder() > 999
			|| getUserPwder() > 999
			)			 
		{
			setFlashInitVal(FLASH_INIT_DATA);
			setFactoryPwder(FATORY_PWD_INIT);				

			setFactoryPara(FaultSwitch_E,1);
			setFactoryPara(Lcd_light_E,0);
			setUserPwder(USER_PWD_INIT);
			
			Flash_Write_Update();
		}
	}
	else
	{
		setFlashInitVal(FLASH_INIT_DATA);
		setFactoryPwder(FATORY_PWD_INIT);	
		setFactoryPara(FaultSwitch_E,1);
		setFactoryPara(Lcd_light_E,0);
		setUserPwder(USER_PWD_INIT);	
		
		Flash_Write_Update();
	
	}
	
}


void System_SoftReset(void)
{
//	 __set_FAULTMASK(1); //关闭所有中断 
	 __disable_irq();
	 NVIC_SystemReset(); //复位
}

/*
*********************************************************************************************************
*	函 数 名: RecoverInit 
*	功能说明: 恢复出厂设置 初始化
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void RecoverInit(void)
{
	setFlashInitVal(getFlashInitVal()+1);
	Flash_Write_Update();
	NVIC_SystemReset();
}




