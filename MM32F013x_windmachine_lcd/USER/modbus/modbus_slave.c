/*
*********************************************************************************************************
*
*	模块名称 : MODS通信模块. 从站模式
*	文件名称 : modbus_slave.c
*	版    本 : V1.4
*	说    明 : 头文件
*
*	Copyright (C), 2015-2016, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/
#include "bsp.h"
#include "modbus_slave.h"
#include "main.h"

VAR_T g_tVar;
/*
*********************************************************************************************************
*	函 数 名: MODS_ReadRegValue
*	功能说明: 读取保持寄存器的值
*	形    参: reg_addr 寄存器地址
*			  reg_value 存放寄存器结果
*	返 回 值: 1表示OK 0表示错误
*********************************************************************************************************
*/
static uint8_t MODS_ReadRegValue(uint16_t reg_addr, uint8_t *reg_value)
{

	
	switch (reg_addr)									/* 判断寄存器地址 */
	{
		case REG_P01: { reg_value[0] =  g_tVar.P01;  reg_value[1] =  g_tVar.P02;}break;
    case REG_P02: { reg_value[0] =  g_tVar.P02;  reg_value[1] =  g_tVar.P03;}break;
    case REG_P03: { reg_value[0] =  g_tVar.P03;  reg_value[1] =  g_tVar.P04;}break;
    case REG_P04: { reg_value[0] =  g_tVar.P04;  reg_value[1] =  g_tVar.P05;}break;
    case REG_P05: { reg_value[0] =  g_tVar.P05;  reg_value[1] =  g_tVar.P06;}break;
    case REG_P06: { reg_value[0] =  g_tVar.P06;  reg_value[1] =  g_tVar.P07;}break;
		case REG_P07: { reg_value[0] =  g_tVar.P07;  reg_value[1] =  g_tVar.P08;}break;
		case REG_P08: { reg_value[0] =	g_tVar.P08;  reg_value[1] =  g_tVar.P09;}break;
		case REG_P09: { reg_value[0] =	g_tVar.P09;  reg_value[1] =  g_tVar.P0A;}break;
		case REG_P10: { reg_value[0] =	g_tVar.P0A;  reg_value[1] =  g_tVar.P0B;}break;
		case REG_P11: { reg_value[0] =	g_tVar.P0B;  reg_value[1] =   0;}break;	
		default:
			return 0;									/* 参数异常，返回 0 */
	}
	return 1;											/* 读取成功 */
}


/*
*********************************************************************************************************
*	函 数 名: MODS_WriteRegValue
*	功能说明: 读取保持寄存器的值
*	形    参: reg_addr 寄存器地址
*			  reg_value 寄存器值
*	返 回 值: 1表示OK 0表示错误
*********************************************************************************************************
*/
static uint8_t MODS_WriteRegValue(uint16_t reg_addr, uint16_t reg_value)
{
	switch (reg_addr)							/* 判断寄存器地址 */
	{	
		case REG_W01:
			g_tVar.W01 = reg_value;				/* 将值写入保存寄存器 */
			break;
		
		case REG_W02:
			g_tVar.W02 = reg_value;				/* 将值写入保存寄存器 */
			break;
			
		case REG_W03:
			g_tVar.W03 = reg_value;				/* 将值写入保存寄存器 */
			break;
			
		default:
			return 0;		/* 参数异常，返回 0 */
	}

	return 1;		/* 读取成功 */
}
/*
*********************************************************************************************************
*	函 数 名: MODS_SendAckErr
*	功能说明: 发送错误应答
*	形    参: _ucErrCode : 错误代码
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_SendAckErr(uint8_t _ucErrCode)
{
	uint8_t txbuf[3];

	txbuf[0] = g_tModbus.RxBuf[0];					/* 485地址 */
	txbuf[1] = g_tModbus.RxBuf[1] | 0x80;				/* 异常的功能码 */
	txbuf[2] = _ucErrCode;							/* 错误代码(01,02,03,04) */

	MODBUS_SendWithCRC(txbuf, 3);
}

/*
*********************************************************************************************************
*	函 数 名: MODS_SendAckOk
*	功能说明: 发送正确的应答. 主要是写操作
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_SendAckOk(void)
{
	uint8_t txbuf[6];
	uint8_t i;

	for (i = 0; i < 6; i++)
	{
		txbuf[i] = g_tModbus.RxBuf[i];
	}
	MODBUS_SendWithCRC(txbuf, 6);
}



/* Modbus 应用层解码示范。下面的代码请放在主程序做 */

/*
*********************************************************************************************************
*	函 数 名: MODBUS_01H
*	功能说明: 读取线圈状态（对应远程开关D01/D02/D03）
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_01H(void)
{
}

/*
*********************************************************************************************************
*	函 数 名: MODBUS_02H
*	功能说明: 读取输入状态（对应T01～T18）
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_02H(void)
{

}

/*
*********************************************************************************************************
*	函 数 名: MODBUS_03H
*	功能说明: 读取保持寄存器 在一个或多个保持寄存器中取得当前的二进制值
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_03H(void)
{
		/*
		从机地址为11H。保持寄存器的起始地址为006BH，结束地址为006DH。该次查询总共访问3个保持寄存器。

		主机发送:
			11 从机地址
			03 功能码
			00 寄存器地址高字节
			6B 寄存器地址低字节
			00 寄存器数量高字节
			03 寄存器数量低字节
			76 CRC高字节
			87 CRC低字节

		从机应答: 	保持寄存器的长度为2个字节。对于单个保持寄存器而言，寄存器高字节数据先被传输，
					低字节数据后被传输。保持寄存器之间，低地址寄存器先被传输，高地址寄存器后被传输。
			11 从机地址
			03 功能码
			06 字节数
			00 数据1高字节(006BH)
			6B 数据1低字节(006BH)
			00 数据2高字节(006CH)
			13 数据2 低字节(006CH)
			00 数据3高字节(006DH)
			00 数据3低字节(006DH)
			38 CRC高字节
			B9 CRC低字节

		例子:
			01 03 30 06 00 01  6B0B      ---- 读 3006H, 触发电流
			01 03 4000 0010 51C6         ---- 读 4000H 倒数第1条浪涌记录 32字节
			01 03 4001 0010 0006         ---- 读 4001H 倒数第1条浪涌记录 32字节

			01 03 F000 0008 770C         ---- 读 F000H 倒数第1条告警记录 16字节
			01 03 F001 0008 26CC         ---- 读 F001H 倒数第2条告警记录 16字节

			01 03 7000 0020 5ED2         ---- 读 7000H 倒数第1条波形记录第1段 64字节
			01 03 7001 0020 0F12         ---- 读 7001H 倒数第1条波形记录第2段 64字节

			01 03 7040 0020 5F06         ---- 读 7040H 倒数第2条波形记录第1段 64字节
	*/
	uint16_t reg;
	uint16_t num;
	uint16_t i;
	uint8_t reg_value[64];

	g_tModbus.RspCode = RSP_OK;

	if (g_tModbus.RxCount != 8)								/* 03H命令必须是8个字节 */
	{
		g_tModbus.RspCode = RSP_ERR_VALUE;					/* 数据值域错误 */
		goto err_ret;
	}

	reg = BEBufToUint16(&g_tModbus.RxBuf[2]); 				/* 寄存器号 */
	num = BEBufToUint16(&g_tModbus.RxBuf[4]);					/* 寄存器个数 */
	if (num > sizeof(reg_value) / 2)
	{
		g_tModbus.RspCode = RSP_ERR_VALUE;					/* 数据值域错误 */
		goto err_ret;
	}

	for (i = 0; i < num; i++)
	{
		if (MODS_ReadRegValue(reg, &reg_value[2 * i]) == 0)	/* 读出寄存器值放入reg_value */
		{
			g_tModbus.RspCode = RSP_ERR_REG_ADDR;				/* 寄存器地址错误 */
			break;
		}
		reg+=2;//一个寄存器存放一个字节
	}

err_ret:
	if (g_tModbus.RspCode == RSP_OK)							/* 正确应答 */
	{
		g_tModbus.TxCount = 0;
		g_tModbus.TxBuf[g_tModbus.TxCount++] = g_tModbus.RxBuf[0];
		g_tModbus.TxBuf[g_tModbus.TxCount++] = g_tModbus.RxBuf[1];
		g_tModbus.TxBuf[g_tModbus.TxCount++] = num * 2;			/* 返回字节数 */

		for (i = 0; i < num; i++)
		{
			g_tModbus.TxBuf[g_tModbus.TxCount++] = reg_value[2*i];
			g_tModbus.TxBuf[g_tModbus.TxCount++] = reg_value[2*i+1];
		}
		MODBUS_SendWithCRC(g_tModbus.TxBuf, g_tModbus.TxCount);	/* 发送正确应答 */
	}
	else
	{
		MODS_SendAckErr(g_tModbus.RspCode);					/* 发送错误应答 */
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODBUS_04H
*	功能说明: 读取输入寄存器（对应A01/A02） SMA
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_04H(void)
{

}

/*
*********************************************************************************************************
*	函 数 名: MODBUS_05H
*	功能说明: 强制单线圈（对应D01/D02/D03）
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_05H(void)
{

}

/*
*********************************************************************************************************
*	函 数 名: MODBUS_06H
*	功能说明: 写单个寄存器
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/

static void MODBUS_06H(void)
{

}


/*
*********************************************************************************************************
*	函 数 名: MODBUS_10H
*	功能说明: 连续写多个寄存器.  进用于改写时钟
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODBUS_10H(void)
{

	/*
		从机地址为01H。保持寄存器的其实地址为0001H，寄存器的结束地址为0002H。总共访问2个寄存器。
		保持寄存器0001H的内容为000AH，保持寄存器0002H的内容为0102H。

		主机发送:
			11 从机地址
			10 功能码
			00 寄存器起始地址高字节
			01 寄存器起始地址低字节
			00 寄存器数量高字节
			02 寄存器数量低字节
			04 字节数
			00 数据1高字节
			0A 数据1低字节
			01 数据2高字节
			02 数据2低字节
			C6 CRC校验高字节
			F0 CRC校验低字节

		从机响应:
			11 从机地址
			06 功能码
			00 寄存器地址高字节
			01 寄存器地址低字节
			00 数据1高字节
			01 数据1低字节
			1B CRC校验高字节
			5A	CRC校验低字节

		例子:
			01 10 30 00 00 06 0C  07 DE  00 0A  00 01  00 08  00 0C  00 00     389A    ---- 写时钟 2014-10-01 08:12:00
			01 10 30 00 00 06 0C  07 DF  00 01  00 1F  00 17  00 3B  00 39     5549    ---- 写时钟 2015-01-31 23:59:57

	*/
	uint16_t reg_addr;
	uint16_t reg_num;
	uint8_t byte_num;
	uint8_t i;
	uint16_t value;
	
	g_tModbus.RspCode = RSP_OK;
		if (g_tModbus.RxCount < 11)
	{
		g_tModbus.RspCode = RSP_ERR_VALUE;			/* 数据值域错误 */
		goto err_ret;
	}

	reg_addr = BEBufToUint16(&g_tModbus.RxBuf[2]); 	/* 寄存器号 */
	reg_num = BEBufToUint16(&g_tModbus.RxBuf[4]);		/* 寄存器个数 */
	byte_num = g_tModbus.RxBuf[6];					/* 后面的数据体字节数 */

	if (byte_num != 2 * reg_num)
	{
		;
	}
	
	for (i = 0; i < reg_num; i++)
	{
		value = BEBufToUint16(&g_tModbus.RxBuf[7 + 2 * i]);	/* 寄存器值 */

		if (MODS_WriteRegValue(reg_addr + i, value) == 1)
		{
			;
		}
		else
		{
			g_tModbus.RspCode = RSP_ERR_REG_ADDR;		/* 寄存器地址错误 */
			break;
		}
	}

err_ret:
	if (g_tModbus.RspCode == RSP_OK)					/* 正确应答 */
	{
		MODS_SendAckOk();
	}
	else
	{
		MODS_SendAckErr(g_tModbus.RspCode);			/* 告诉主机命令错误 */
	}
}


/*
*********************************************************************************************************
*	函 数 名: MODBUS_AnalyzeApp
*	功能说明: 分析应用层协议
*	形    参:
*		     _DispBuf  存储解析到的显示数据ASCII字符串，0结束
*	返 回 值: 无
*********************************************************************************************************
*/
void MODS_AnalyzeApp(void)
{
	switch (g_tModbus.RxBuf[1])				/* 第2个字节 功能码 */
	{
		case 0x01:							/* 读取线圈状态（此例程用led代替）*/
			MODBUS_01H();
			bsp_PutMsg(MSG_MODS_01H, 0);	/* 发送消息,主程序处理 */
			break;

		case 0x02:							/* 读取输入状态（按键状态）*/
			MODBUS_02H();
			bsp_PutMsg(MSG_MODS_02H, 0);
			break;
		
		case 0x03:							/* 读取保持寄存器（此例程存在g_tVar中）*/
			MODBUS_03H();
			bsp_PutMsg(MSG_MODS_03H, 0);
			break;
		
		case 0x04:							/* 读取输入寄存器（ADC的值）*/
			MODBUS_04H();
			bsp_PutMsg(MSG_MODS_04H, 0);
			break;
		
		case 0x05:							/* 强制单线圈（设置led）*/
			MODBUS_05H();
			bsp_PutMsg(MSG_MODS_05H, 0);
			break;
		
		case 0x06:							/* 写单个保存寄存器（此例程改写g_tVar中的参数）*/
			MODBUS_06H();	
			bsp_PutMsg(MSG_MODS_06H, 0);
			break;
			
		case 0x10:							/* 写多个保存寄存器（此例程存在g_tVar中的参数）*/
			MODBUS_10H();
			bsp_PutMsg(MSG_MODS_10H, 0);
			break;
		
		default:
			g_tModbus.RspCode = RSP_ERR_CMD;
			MODS_SendAckErr(g_tModbus.RspCode);	/* 告诉主机命令错误 */
			break;
	}
}

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
































