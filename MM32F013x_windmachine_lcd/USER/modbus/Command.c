#include "bsp.h"
#include "modbus_host.h"
#include "data.h"


#define MODH10_AUTOMANUL   0x0000
#define MODH10_HIGHSPEED   0x0001
#define MODH10_LOWSPEED  0x0002
#define MODH10_HIGHPOWER   0x0003
#define MODH10_LOWPOWER   0x0004
#define MODH10_MACHINE_TYPE   0x0005
#define MODH10_DISPLAY_AD   0x0006
#define MODH10_POWER_FACTOR  0x0007
#define MODH10_OVERLOAD   0x0008
#define MODH10_SAMPLE_TIME   0x0009



/*******************************************************************************
 * 函 数 名: COM_Data_Timeout
 * 函数功能: 通信超时处理
 * 入口参数: 无
 * 返    回: 无
*******************************************************************************/

void COM_Data_Timeout(void)
{
	uint8_t i=0;
	for(i=0;i<MODBUS03H_NUM;i++)
	{
		g_tVar.usRegInputBuf[i]=0;
	}
	MODH_Data_Deal();
}


typedef enum
{
	MODH03H_SEND_SLAVE1=0,
    MODH03H_READ_SLAVE1=1,  
    MODH10H_SEND_SLAVE=2,
    MODH10H_READ_SLAVE=3, 
}MODHStateTypedef;

#define MODBUSH_LOOPTIMES 7

#define MODBUSH_TIMEOUT 800

//============================================================================
// Function    : TskModbusMgt 模拟前端 analog front end
// Description : 该函数实现modbus从机轮训的启动转换、读取数据、数据计算、均衡等功能
//               实现和状态切换
// Parameters  : none
// Returns     : none
//============================================================================
void TskModbusMgt(void)
{
	static uint32_t s_uitimeStamp;
	static uint8_t s_ucomErrCnt = MODBUSH_LOOPTIMES;
	static MODHStateTypedef s_tModHState = MODH03H_SEND_SLAVE1;
	static MSG10H_T s_tMsg10H = {0};					/* 消息代码 */
//	uint8_t i;
        
	switch (s_tModHState)
	{
	case MODH03H_SEND_SLAVE1:
		 if(DispControler.Machine_version >= 21) //风机控制器版本 增加过欠压门限
		 {
				MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 29);  //启动03指令
		 }
		 else if(DispControler.Machine_version >= 19) //风机控制器版本 增加过欠压门限
		 {
				MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 25);  //启动03指令
		 }
		 else if(DispControler.Machine_version >= 17) //风机控制器版本 增加过载倍率设置 默认1.0
		 {
				MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 21);  //启动03指令
		 }
		 else if(DispControler.Machine_version >=16) //风机控制器版本 增加过载时间设置
		 {
				MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 20);  //启动03指令
		 }
		  else if(DispControler.Machine_version>=15)
		 {
			 MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 19);  //启动03指令			 		 
		 } 
		 else
		 {
			  MODH_Send03H(BMU1_ADDR, REG_INPUT_START, 19);  //启动03指令
		 }
		
		s_uitimeStamp = bsp_GetRunTime();  //记录转换开始时间
		s_tModHState = MODH03H_READ_SLAVE1;  //状态切换
		break;

	case MODH03H_READ_SLAVE1:
		if (bsp_CheckRunTime(s_uitimeStamp) >= MODBUSH_TIMEOUT)  // 超时需要200ms
		{
			if (g_tModH.fAck03H>0)
			{
				s_tModHState = MODH10H_SEND_SLAVE;
				Modbus_Slave[0].COM_Error = 0;
				MachineFlag.Bits.comm_error =0;
				s_ucomErrCnt = MODBUSH_LOOPTIMES;
			}
			else
			{
				s_tModHState = MODH03H_SEND_SLAVE1;
				if (s_ucomErrCnt)
				{
					s_ucomErrCnt--;
				}
				else 
				{
					Modbus_Slave[0].COM_Error = 1;
					MachineFlag.Bits.comm_error =1;
					COM_Data_Timeout();
					s_tModHState = MODH10H_SEND_SLAVE;
					s_ucomErrCnt = MODBUSH_LOOPTIMES;
				}
			}
		}
		break;
	case MODH10H_SEND_SLAVE:
		if(bsp_GetMsg10H(&s_tMsg10H))
		{
			MODH_Send10H(s_tMsg10H.slaveaddr, s_tMsg10H.regaddr, s_tMsg10H.regnum, s_tMsg10H.MsgBuff);  //启动10指令
			s_tModHState = MODH10H_READ_SLAVE;//MODH03H_SEND_SLAVE1
			s_uitimeStamp = bsp_GetRunTime();  //记录转换开始时间
		}
		else
		{
			s_tModHState = MODH03H_SEND_SLAVE1;//MODH03H_SEND_SLAVE1
		}
		break;
	case MODH10H_READ_SLAVE:		
		 if ( bsp_CheckRunTime(s_uitimeStamp) >= MODBUSH_TIMEOUT)  // 超时需要200ms
		{
			if (g_tModH.fAck10H>0)
			{
				s_tModHState = MODH03H_SEND_SLAVE1;
				s_ucomErrCnt = MODBUSH_LOOPTIMES;
				memset(&s_tMsg10H,0, sizeof(s_tMsg10H));
			}
			else
			{
				s_tModHState = MODH10H_SEND_SLAVE;
				/* mcu与ltc6803 modbus通信错误检测 */
				if (s_ucomErrCnt)
				{
					s_ucomErrCnt--;					
					bsp_PutMsg10H(s_tMsg10H.slaveaddr,s_tMsg10H.regaddr, s_tMsg10H.regnum, s_tMsg10H.MsgBuff);
				}
				else 
				{
					s_tModHState = MODH03H_SEND_SLAVE1;
					s_ucomErrCnt = MODBUSH_LOOPTIMES;
				}
			}
		}
		break;

	default:
		s_tModHState = MODH03H_SEND_SLAVE1;
		break;
   }
}


void Modbus10HSetVal(uint16_t reg,uint16_t val)
{
	uint8_t buff[2]={0};
	
	buff[0]=val>>8;
	buff[1]=val;
	bsp_PutMsg10H(0x01, reg+REG_HOLDING_START, 0x01,buff);
}




