#ifndef _COMMAND_H_
#define _COMMAND_H_

#define MODH10_AUTOMANUL_REG   0x0000
#define MODH10_HIGHSPEED_REG   0x0001
#define MODH10_LOWSPEED_REG  	0x0002
#define MODH10_HIGHPOWER_REG   0x0003
#define MODH10_LOWPOWER_REG   0x0004
#define MODH10_MACHINE_TYPE_REG   0x0005
#define MODH10_DISPLAY_AD_REG   0x0006
#define MODH10_POWER_FACTOR_REG  0x0007
#define MODH10_OVERLOAD_REG   0x0008
#define MODH10_SAMPLE_TIME_REG   0x0009
#define MODH10_OLOAD_MULTPLY_REG   0x000A
#define MODH10_CHECKOUT_SW_REG   0x000B
#define MODH10_VOLTAGE_MAX_REG   0x000C
#define MODH10_VOLTAGE_MIN_REG   0x000D 

#define MODH10_DS_FEEDBACK_REG   0x000E
#define MODH10_DS_ERROR_REG   0x000F
#define MODH10_DJYX_REG   0x0010 

void Modbus10HSetVal(uint16_t reg,uint16_t val);


/*****************************ָ��ͺ���************************************/

#define Set_Auto_Switch(val)				Modbus10HSetVal( MODH10_AUTOMANUL_REG, val)
#define StartStopHighSpeed(val)	            Modbus10HSetVal( MODH10_HIGHSPEED_REG, val)
#define StartStopLowSpeed(val)              Modbus10HSetVal( MODH10_LOWSPEED_REG, val)
#define Set_High_Power_Switch(val)          Modbus10HSetVal( MODH10_HIGHPOWER_REG, val)
#define Set_Low_Power_Switch(val)           Modbus10HSetVal( MODH10_LOWPOWER_REG, val)
#define Set_Machine_Type_Switch( val)       Modbus10HSetVal( MODH10_MACHINE_TYPE_REG, val)
#define Set_Display_AD_Switch( val)         Modbus10HSetVal( MODH10_DISPLAY_AD_REG, val)
#define Set_Power_Factor_Switch( val)       Modbus10HSetVal( MODH10_POWER_FACTOR_REG, val)
#define Set_Clear_Overload_Switch( val)     Modbus10HSetVal( MODH10_OVERLOAD_REG, val)
#define Set_Sample_Time_Switch( val)        Modbus10HSetVal( MODH10_SAMPLE_TIME_REG, val)
#define Set_OloadMultply_Switch( val)       Modbus10HSetVal( MODH10_OLOAD_MULTPLY_REG, val)
#define Set_CheckOut_Switch( val)           Modbus10HSetVal( MODH10_CHECKOUT_SW_REG, val)
#define Set_VoltageMax_Switch( val)         Modbus10HSetVal( MODH10_VOLTAGE_MAX_REG, val)
#define Set_VoltageMin_Switch( val)         Modbus10HSetVal( MODH10_VOLTAGE_MIN_REG, val)

#define Set_DS_FEEDBACK_Switch( val)        Modbus10HSetVal( MODH10_DS_FEEDBACK_REG, val)
#define Set_DS_ERROR_Switch( val)           Modbus10HSetVal( MODH10_DS_ERROR_REG, val)
#define Set_DJYX_Switch( val)         		Modbus10HSetVal( MODH10_DJYX_REG, val)

void All_Switch(void);
void COM_Data_Timeout(void);
void TskModbusMgt(void);

#endif

