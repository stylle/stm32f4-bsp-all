/*
*********************************************************************************************************
*
*	模块名称 : 消息处理模块
*	文件名称 : bsp_msg.c
*	版    本 : V1.0
*	说    明 : 消息处理机制。
*
*	修改记录 :
*		版本号  日期        作者     说明
*		V1.0    2015-03-27 armfly  正式发布
*
*	Copyright (C), 2014-2015, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#include "bsp.h"
#include "ModH_msg.h"
#include "modbus_host.h"
//#include "include.h"

//#define REG_HOLDING_START 0

MSG_10HFIFO_T g_tMsg10H;


/*********************************************************************************************************
*	函 数 名: Modbus10HBattMode
*	功能说明: Modbus发送电池模式函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/

void Modbus10HBattMode(uint8_t _addr)
{
//	uint8_t buff[2]={0};
	
//	buff[0]=g_BatteryMode>>8;
//	buff[1]=g_BatteryMode;
//	bsp_PutMsg10H(_addr, BAT_MODE_ADDR+REG_HOLDING_START, 0x01,buff);
}


/*********************************************************************************************************
*	函 数 名: Modbus10HBattBalance
*	功能说明: Modbus发送电池模式函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/

void Modbus10HBattBalance(uint8_t _addr,uint8_t regaddr,uint16_t balance)
{
	uint8_t buff[2]={0};
	
	buff[0]=balance>>8;
	buff[1]=balance;
	bsp_PutMsg10H(_addr, regaddr+REG_HOLDING_START, 0x01,buff);
}









/*********************************************************************************************************
*	函 数 名: bsp_InitMsg10H
*	功能说明: 初始化消息缓冲区
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitMsg10H(void)
{
	bsp_ClearMsg10H();
}

/*********************************************************************************************************
*	函 数 名: Modbus10HMsgTest
*	功能说明: 测试10H功能的函数
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void Modbus10HMsgTest(void)
{
	uint8_t buff[20]={0};
	bsp_PutMsg10H(0x01, 0x0002, 0x01,buff);

}

/*
*********************************************************************************************************
*	函 数 名: bsp_PutMsg10H
*	功能说明: 将1个消息压入消息FIFO缓冲区。
*	形    参:  _MsgCode : 消息代码
*			  _pMsgParam : 消息参数，一般指向某个特定的结构体. 或者是0
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_PutMsg10H(uint8_t _addr, uint16_t _reg, uint8_t _num, uint8_t *_buf)
{
	uint8_t i=0;
	g_tMsg10H.Buf[g_tMsg10H.Write].slaveaddr = _addr;
	g_tMsg10H.Buf[g_tMsg10H.Write].command = 0x10;
	g_tMsg10H.Buf[g_tMsg10H.Write].regaddr = _reg;
	g_tMsg10H.Buf[g_tMsg10H.Write].regnum = _num;
	for (i = 0; i < 2 * _num; i++)
	{
		if (_num*2 > MSG_10BUFF_LEN - 3)
		{
			return;		/* 数据超过缓冲区超度，直接丢弃不发送 */
		}
		g_tMsg10H.Buf[g_tMsg10H.Write].MsgBuff[i] = _buf[i];		/* 后面的数据长度 */
	}
	if (++g_tMsg10H.Write  >= MSG_10HFIFO_SIZE)
	{
		g_tMsg10H.Write = 0;
	}
}


/*
*********************************************************************************************************
*	函 数 名: bsp_PutMsg10H
*	功能说明: 将1个消息压入消息FIFO缓冲区。
*	形    参:  _MsgCode : 消息代码
*			  _pMsgParam : 消息参数，一般指向某个特定的结构体. 或者是0
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InsertMsg10H(uint8_t _addr, uint16_t _reg, uint8_t _num, uint8_t *_buf)
{
	uint8_t i=0;
	g_tMsg10H.Buf[g_tMsg10H.Read].slaveaddr = _addr;
	g_tMsg10H.Buf[g_tMsg10H.Read].command = 0x10;
	g_tMsg10H.Buf[g_tMsg10H.Read].regaddr = _reg;
	g_tMsg10H.Buf[g_tMsg10H.Read].regnum = _num;
	for (i = 0; i < 2 * _num; i++)
	{
		if (_num*2 > MSG_10BUFF_LEN - 3)
		{
			return;		/* 数据超过缓冲区超度，直接丢弃不发送 */
		}
		g_tMsg10H.Buf[g_tMsg10H.Read].MsgBuff[i] = _buf[i];		/* 后面的数据长度 */
	}
	
	if (g_tMsg10H.Read == 0)
	{
		g_tMsg10H.Read = MSG_10HFIFO_SIZE ;
		
	}
	g_tMsg10H.Read --;

}
/*
*********************************************************************************************************
*	函 数 名: bsp_GetMsg
*	功能说明: 从消息FIFO缓冲区读取一个键值。
*	形    参:  无
*	返 回 值: 0 表示无消息； 1表示有消息
*********************************************************************************************************
*/
uint8_t bsp_GetMsg10H(MSG10H_T *_pMsg)
{
	MSG10H_T *p;
	uint8_t i=0;
	if (g_tMsg10H.Read == g_tMsg10H.Write)
	{
		return 0;
	}
	else
	{
		p = &g_tMsg10H.Buf[g_tMsg10H.Read];

		if (++g_tMsg10H.Read >= MSG_10HFIFO_SIZE)
		{
			g_tMsg10H.Read = 0;
		}
		
		_pMsg->slaveaddr = p->slaveaddr;
		_pMsg->command = p->command;
		_pMsg->regaddr = p->regaddr;		
		_pMsg->regnum = p->regnum;
		for (i = 0; i < 2 * p->regnum; i++)
		{
			_pMsg->MsgBuff[i] = p->MsgBuff[i];
		}
		
		return 1;
	}
}


/*
*********************************************************************************************************
*	函 数 名: bsp_GetMsg2
*	功能说明: 从消息FIFO缓冲区读取一个键值。使用第2个读指针。可以2个进程同时访问消息区。
*	形    参:  无
*	返 回 值: 0 表示无消息； 1表示有消息
*********************************************************************************************************
*/
uint8_t bsp_GetMsg10H2(MSG10H_T *_pMsg)
{
	MSG10H_T *p;
	uint8_t i=0;
	if (g_tMsg10H.Read2 == g_tMsg10H.Write)
	{
		return 0;
	}
	else
	{
		p = &g_tMsg10H.Buf[g_tMsg10H.Read2];

		if (++g_tMsg10H.Read2 >= MSG_10HFIFO_SIZE)
		{
			g_tMsg10H.Read2 = 0;
		}
		
		_pMsg->slaveaddr = p->slaveaddr;
		_pMsg->command = p->command;
		_pMsg->regaddr = p->regaddr;
		_pMsg->regnum = p->regnum;
		for (i = 0; i < 2 * p->regnum; i++)
		{
			_pMsg->MsgBuff[i] = p->MsgBuff[i];
		}
		
		return 1;
	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_ClearMsg
*	功能说明: 清空消息FIFO缓冲区
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_ClearMsg10H(void)
{
	g_tMsg10H.Read = g_tMsg10H.Write;
}


/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
