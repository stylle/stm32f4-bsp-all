/*
*********************************************************************************************************
*
*	模块名称 : 消息处理模块
*	文件名称 : bsp_msg.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C), 2013-2014, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/

#ifndef __MODH_MSG_H
#define __MODH_MSG_H


#define BAT_MODE_ADDR 0
#define AFE1_BALANCE_ADDR 1
#define AFE2_BALANCE_ADDR 2

#define MSG_10BUFF_LEN 50
#define MSG_10HFIFO_SIZE 10
/* 按键FIFO用到变量 */
typedef struct
{
  uint8_t slaveaddr;
  uint8_t command;
  uint16_t regaddr;
  uint8_t regnum;
  uint8_t MsgBuff[MSG_10BUFF_LEN];
}MSG10H_T;
/* 按键FIFO用到变量 */
typedef struct
{
	MSG10H_T Buf[MSG_10HFIFO_SIZE];	/* 消息缓冲区 */
	uint8_t Read;					/* 缓冲区读指针1 */
	uint8_t Write;					/* 缓冲区写指针 */
	uint8_t Read2;					/* 缓冲区读指针2 */
}MSG_10HFIFO_T;

/* 供外部调用的函数声明 */
void bsp_InitMsg10H(void);
void Modbus10HMsgTest(void);
void bsp_PutMsg10H(uint8_t _addr, uint16_t _reg, uint8_t _num, uint8_t *_buf);
uint8_t bsp_GetMsg10H(MSG10H_T *_pMsg);
uint8_t bsp_GetMsg10H2(MSG10H_T *_pMsg);
void bsp_ClearMsg10H(void);

//应用层
void Modbus10HBattMode(uint8_t _addr);
void Modbus10HBattBalance(uint8_t _addr,uint8_t regaddr,uint16_t balance);

#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
