#include "bsp.h"				/* 底层硬件驱动 */
#include "modbus_host.h"

#define TWO_120KW 0
#define TWO_185KW 1
#define TWO_240KW 2
#define TWO_380KW 3

#define ONE_55KW	0
#define ONE_110KW 1
#define ONE_220KW 2
#define ONE_370KW 3


const uint16_t HIGH_POWER_LIST[]={55,120,185,240,380};
//const uint16_t HIGH_POWER_LIST[]={55,110,120,185,220,240,370,380};
const uint16_t LOW_POWER_LIST[]={40,62,85,130,220};
//UserPagePara_T UserPageParaer;
//const uint16_t SINGLE_POWER_LIST[]={11,15,22,30,40,55,75,110,150,185,220,370};
const uint16_t SINGLE_POWER_LIST[]={11,15,22,30,37,40,55,75,110,150,185,220,300,370};
const uint16_t DOUBLE_H_POWER_LIST[]={30,	37,	47,	40,	55,	75,	80,	110,	120,	155,	185,	220,	240,	250,	300,	380};
const uint16_t DOUBLE_L_POWER_LIST[]={22,	30,	15,	20,	22,	55,	28,	37,		40,		51,		62,		150,	85,		110,	110,	130};



STA_Para_T STAer;


CountParameter_T Main_Power_Data[3];
CountParameter_T Current_Data[2];


MachineFlagDef MachineFlag;



/*********************************************通信数据处理*****************************************************/

//enum  //设备状态枚举状态
//{
//	MASK_Auto_Bit =0      ,
//	MASK_Low_Running      ,
//	MASK_High_Running     ,
//	MASK_Fire_PTC         ,
//	MASK_BAS              ,
//	MASK_Low_OverLoad     ,
//	MASK_High_OverLoad    ,
//	MASK_Low_Short        ,
//	MASK_High_Short       ,
//	MASK_WindValve_Error  ,
//	MASK_CheckOut         ,
//	MASK_Power_OverLimit  ,
//	MASK_Power_UnderLimit ,
//	MASK_Power_LackPhase  ,
//	MASK_Power_PhaseError ,
//	MASK_Running_Error    ,
//	MASK_KM1_Error        ,
//	MASK_KM2_Error 	      ,
//};




void MODH_Data_BitDeal(uint32_t var);

void MODH_Data_Deal(void);

void LED_Error_Ctrl(void);

void Beep_Error_Ctrl(void);

void Error_Deal(uint32_t var);


void ClearErrBuf(unsigned char* buf);

void Author_Tim_Ctrl(void);
/*
*********************************************************************************************************
*	函 数 名: LED_Error_Ctrl 
*	功能说明:控制LED输出
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void ClearErrBuf(unsigned char* buf)
{
    int i = 0;
    for(i=0; i<8; i++)
    {
        buf[i] = 0x00;
    }
}




/*
*********************************************************************************************************
*	函 数 名: LED_Error_Ctrl 
*	功能说明:控制LED输出
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

void Error_Deal(uint32_t var)
{
		unsigned char nCnt= 0;
//	
//		STAer.Error.ulErrCode = var & 0x0003ffc0;
	 STAer.Error.ulErrCode = var & 0x0013ffc0; //增加余压控制
	
	/****通信异常处理***/	
		if( MachineFlag.Bits.comm_error ==1 ) 
		{
			STAer.Error.ulErrCode |= ( 1 << MASK_COM_Error);		
		}
		else
		{
			STAer.Error.ulErrCode &= ~( 1 << MASK_COM_Error);
		
		}
	
		
		
		if( STAer.Error.ulErrCode == 0 )
    {
        STAer.Error.ucCurrentErrNum = 0;
        STAer.Error.ucErrorCnt = 0;
        STAer.Error.ulErrCode = 0;
        STAer.Error.ulPreErrCode = 0;
        ClearErrBuf(STAer.Error.ucError);
        return;
    }
		  if( STAer.Error.ulErrCode != STAer.Error.ulPreErrCode )
    {
       if( STAer.Error.ulErrCode & (1 << MASK_Low_OverLoad) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Low_OverLoad;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_High_OverLoad) )
        {
            STAer.Error.ucError[nCnt++] = MASK_High_OverLoad;
        }
        if( STAer.Error.ulErrCode & (1 << MASK_Low_Short) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Low_Short;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_High_Short))
        {
            STAer.Error.ucError[nCnt++] = MASK_High_Short;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_WindValve_Error))
        {
            STAer.Error.ucError[nCnt++] = MASK_WindValve_Error;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_CheckOut) )
        {
            STAer.Error.ucError[nCnt++] = MASK_CheckOut;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_Power_OverLimit) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Power_OverLimit;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_Power_UnderLimit) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Power_UnderLimit;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_Power_LackPhase) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Power_LackPhase;
        }

        if( STAer.Error.ulErrCode & (1 << MASK_Power_PhaseError) )
        {
            STAer.Error.ucError[nCnt++] = MASK_Power_PhaseError;
        }

				if( STAer.Error.ulErrCode & (1 << MASK_KM1_Error))
        {
            STAer.Error.ucError[nCnt++] = MASK_KM1_Error;
        }
       if( STAer.Error.ulErrCode & (1 << MASK_KM2_Error))
        {
            STAer.Error.ucError[nCnt++] = MASK_KM2_Error;
        }   
				if( STAer.Error.ulErrCode & (1 << MASK_TOPPRESSURE))
        {
            STAer.Error.ucError[nCnt++] = MASK_TOPPRESSURE;
        }   
				
			 if( STAer.Error.ulErrCode & (1 << MASK_COM_Error))
        {
            STAer.Error.ucError[nCnt++] = MASK_COM_Error;
        } 
				
				//历史故障处理
			if(STAer.Error.ucErrorCnt<nCnt) //故障增加才加入记录
			{
						unsigned char i;
            unsigned int x;
            x = STAer.Error.ulPreErrCode ^ STAer.Error.ulErrCode;//判断变化的故障
            for(i = 0; i < 32; i ++)//共16位，一位一位的判断
            {
                if(x & (1<<i))//如果找到变化的位
                {
                    STAer.Error.uiHistory <<= 4;
                    STAer.Error.uiHistory &= 0xFFFFFFF0;                    
                    switch(i)
                    {
                        case MASK_Low_OverLoad: 			STAer.Error.uiHistory |= 1; break;
                        case MASK_High_OverLoad: 			STAer.Error.uiHistory |= 2; break;
                        case MASK_Low_Short: 					STAer.Error.uiHistory |= 3; break;
                        case MASK_High_Short: 				STAer.Error.uiHistory |= 4; break;
                        case MASK_WindValve_Error: 		STAer.Error.uiHistory |= 5; break;
                        case MASK_CheckOut:						STAer.Error.uiHistory |= 6; break;
                        case MASK_Power_OverLimit: 		STAer.Error.uiHistory |= 7; break;
                        case MASK_Power_UnderLimit: 	STAer.Error.uiHistory |= 8; break;
                        case MASK_Power_LackPhase: 		STAer.Error.uiHistory |= 9; break;
                        case MASK_Power_PhaseError: 	STAer.Error.uiHistory |= 10; break;
                        case MASK_COM_Error: 					STAer.Error.uiHistory |= 11; break;
											  case MASK_TOPPRESSURE: 					STAer.Error.uiHistory |= 12; break;
                        default: STAer.Error.uiHistory >>= 4;
                            break;
                    }
                }
            }
				
				}
				
				
				STAer.Error.ulPreErrCode =STAer.Error.ulErrCode;
        STAer.Error.ucErrorCnt = nCnt;
    }

}





/*
*********************************************************************************************************
*	函 数 名: LED_Error_Ctrl 
*	功能说明:控制LED输出
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
//enum LED_NAME
//{
//	LED_POWER=0, //电源指示
//	LED_FIRE_PTR, //消防联动
//	LED_LOW_SPEED,//低速运行
//	LED_HIGH_SPEED,//高速运行
//	LED_RUNNING, 	//运行反馈
//	LED_FAULT	,		//故障指示
//	LED_STATE_AUTO, //自动状态
//};

//void LED_Start(uint8_t nIndex, uint16_t _usLedTime, uint16_t _usStopTime );


void LED_Error_Ctrl(void)
{
	//电源指示灯
	{
			if(MachineFlag.Bits.grid_overlimit ==1
		||MachineFlag.Bits.gird_underlimit ==1
		||MachineFlag.Bits.gird_lackphase==1
		||MachineFlag.Bits.grid_phaseerr ==1)
			{
				led_HardOff(LED_POWER);
					
			}
			else
			{

				led_HardOn(LED_POWER);
			}
	}
	
	//消防联动
	 if(MachineFlag.Bits.fire_ptc ==1 ) 
	{
		led_HardOn(LED_FIRE_PTR);
	}
 else 
	{ 
		led_HardOff(LED_FIRE_PTR);
	}
	
		//低速运行
//	 if(Machine_State.Machine_Low_Running ==1 ) 
		if( MachineFlag.Bits.low_running == 1)
			{
				led_HardOn(LED_LOW_SPEED);
			}
		 else 
			{ 
				led_HardOff(LED_LOW_SPEED);
			}

	//高速运行
//	 if(Machine_State.Machine_High_Running ==1 ) 
		if( MachineFlag.Bits.high_running ==1)
			{
				led_HardOn(LED_HIGH_SPEED);
			}
		 else 
			{ 
				led_HardOff(LED_HIGH_SPEED);
			}
	

		//运行反馈
	 if(MachineFlag.Bits.fan_runnig ==1 ) 
	 {
			led_HardOn(LED_RUNNING);
		}
	 else 
		{ 
			led_HardOff(LED_RUNNING);
		}
		 

	
	//故障指示
	if(getFactoryPara(FaultSwitch_E) ==1||getFactoryPara(FaultSwitch_E) ==2)
	{
		static uint8_t s_ucledcnt=0;
		
		if( MachineFlag.Bits.low_Oload ==1
			||MachineFlag.Bits.high_Oload ==1
		
			||MachineFlag.Bits.low_short ==1
		||MachineFlag.Bits.high_short ==1
		
//		||Machine_State.Machine_WindValve_Error ==1
//		||MachineFlag.Bits.checkout ==1
		
		||MachineFlag.Bits.grid_overlimit ==1
		||MachineFlag.Bits.gird_underlimit ==1
		||MachineFlag.Bits.gird_lackphase ==1
		||MachineFlag.Bits.grid_phaseerr ==1
			) 
		{
			if(++s_ucledcnt>=20)
			{
				s_ucledcnt=0;
				bsp_LedToggle(LED_FAULT);				
			}
			
		}
	 else 
		{ 
			led_HardOff(LED_FAULT);
			s_ucledcnt=0;			
		}
	}
	else
	{
		led_HardOff(LED_FAULT);
	}
		//手动位
	 if(MachineFlag.Bits.auto_bit== 0 ) 
		 {
			led_HardOn(LED_STATE_AUTO);
		}
	 else 
		{ 
			led_HardOff(LED_STATE_AUTO);
		}
			
		
}

/*
*********************************************************************************************************
*	函 数 名: Beep_Error_Ctrl 
*	功能说明:蜂鸣器故障控制
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
//enum BEEP_E
//{
//    FireCtrlBEEP=0,
//    FaultBEEP,
//};


	
void Beep_Error_Ctrl(void)
{
	
	if(getFactoryPara(FaultSwitch_E) ==1)
	{
		//消防联动
		 if(MachineFlag.Bits.fire_ptc ==1 ) 
		{
//			BEEP_Start(FireCtrlBEEP,1500,50,0,0); 
			BEEP1_ENABLE();
		}
	 else 
		{ 
//			BEEP_Stop(FireCtrlBEEP ); 
			BEEP1_DISABLE();
		}
		
		{ //故障蜂鸣器控制
			 static uint8_t s_ucbeepcnt=0;
				
				if(	
					MachineFlag.Bits.grid_overlimit ==1
					||MachineFlag.Bits.gird_underlimit ==1
					||MachineFlag.Bits.gird_lackphase ==1
					||MachineFlag.Bits.grid_phaseerr ==1
				
	//				||Machine_State.Machine_WindValve_Error ==1
	//				||MachineFlag.Bits.checkout ==1
				)				
				
				{		
//				50ms调用一次  1.5S一次	

					if(++s_ucbeepcnt >= 30)
					{
						s_ucbeepcnt =0;
						BEEP2_Troggle();
					}
				}
			 else if ( MachineFlag.Bits.low_Oload ==1
					||MachineFlag.Bits.high_Oload ==1				
					||MachineFlag.Bits.low_short ==1
					||MachineFlag.Bits.high_short ==1
						) 
				{ //50ms调用一次  1S一次
					if(++s_ucbeepcnt >= 20)
					{
						s_ucbeepcnt =0;
						BEEP2_Troggle();
					}
					
				}
				else 
				{
					BEEP2_DISABLE();
				}
				
		}
	}
	else
	{
		BEEP1_DISABLE();
		BEEP2_DISABLE();

	}
}



/*
*********************************************************************************************************
*	函 数 名: Search_Index 
*	功能说明:搜索列表索引
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

uint8_t Search_Index(uint16_t Value,uint16_t * Buf,uint8_t MaxIndex)
{
	uint8_t i=0;
	for(i=0;i<MaxIndex;i++)
	{
		if(Value <= Buf[i])
		{
			break;				
		}			
	}
	if(i<MaxIndex)
	{
		return i;
	}
	else return 0xff;		
}



/*
*********************************************************************************************************
*	函 数 名: MODH_Data_BitDeal 
*	功能说明:状态位分配
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/

//enum  //设备状态枚举状态
//{
//	MASK_Auto_Bit =0      ,
//	MASK_Low_Running      ,
//	MASK_High_Running     ,
//	MASK_Fire_PTC         ,
//	
//	MASK_BAS              ,
//	MASK_Running			    ,
//	
//	MASK_Low_OverLoad     ,
//	MASK_High_OverLoad    ,
//	MASK_Low_Short        ,
//	MASK_High_Short       ,
//	MASK_WindValve_Error  ,
//	MASK_CheckOut         ,
//	MASK_Power_OverLimit  ,
//	MASK_Power_UnderLimit ,
//	MASK_Power_LackPhase  ,
//	MASK_Power_PhaseError ,	
//	MASK_KM1_Error        ,
//	MASK_KM2_Error 	      ,
//	MASK_MACHINE_TYPE			,
//	
//	MASK_COM_Error
//};


/*
*********************************************************************************************************
*	函 数 名: InputAuthorMonitor
*	功能说明: 授权短接端口。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void InputAuthorMonitor(void)
{
	static uint8_t  s_Lock_ST=0; 
	
	unsigned char L_code =GetInput();

	if(L_code ==0 )
	{
		STAer.ucAuthor = 1; //授权
		STAer.ucAuthor_time =0; //授权计时
		s_Lock_ST=1;
	}
	else if(s_Lock_ST ==1) //授权过才关闭授权
	{
		STAer.ucAuthor = 0; //非授权				
		s_Lock_ST=0;
	}	
}

void MODH_Data_BitDeal(uint32_t var)
{
	MachineFlag.all = var;
	
	
	/********************改版 风机类型由接口板上传*****************************************/
	setFactoryPara( MachineType_E			 , (var & 1<<MASK_MACHINE_TYPE)?1:0);
	setFactoryPara(OverLimit_Switch_E        , (var & 1<<MASK_OVERLIMIT_SWITCH)?1:0);
	setFactoryPara(usCheckout_Switch_E        , MachineFlag.Bits.checkout_switch);
	
	if(MachineFlag.Bits.low_running ==0 && MachineFlag.Bits.high_running ==0 )
	{
		STAer.ucOutput=NONE_SPEED;
	}
	else if(MachineFlag.Bits.low_running ==1 && MachineFlag.Bits.high_running ==0 )
	{
		STAer.ucOutput=LOW_SPEED;
	}
	else if(MachineFlag.Bits.low_running ==0 && MachineFlag.Bits.high_running ==1 )
	{
		STAer.ucOutput=HIGH_SPEED;
	}

	
  Error_Deal( var);
	
}



/*
*********************************************************************************************************
*	函 数 名: MODH_Data_Deal 
*	功能说明:通信数据处理
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Data_Deal(void)
{
	
	uint32_t state=0;
	state=g_tVar.usRegInputBuf[0]>>8;
	state  = state<<8;
	state |= g_tVar.usRegInputBuf[0]&0XFF;
	state  = state<<8;
	state |= g_tVar.usRegInputBuf[1]>>8;

	MODH_Data_BitDeal(state);
	
	Main_Power_Data[0].data =g_tVar.usRegInputBuf[1]&0XFF;
	Main_Power_Data[0].data =Main_Power_Data[0].data<<8;
	Main_Power_Data[0].data |= g_tVar.usRegInputBuf[2]>>8;
	
	Main_Power_Data[1].data =g_tVar.usRegInputBuf[2]&0XFF;
	Main_Power_Data[1].data =Main_Power_Data[1].data<<8;
	Main_Power_Data[1].data |= g_tVar.usRegInputBuf[3]>>8;
	
	Main_Power_Data[2].data =g_tVar.usRegInputBuf[3]&0XFF;
	Main_Power_Data[2].data =Main_Power_Data[2].data<<8;
	Main_Power_Data[2].data |= g_tVar.usRegInputBuf[4]>>8;
	
	
	Current_Data[0].data =g_tVar.usRegInputBuf[4]&0XFF;
	Current_Data[0].data =Current_Data[0].data<<8;
	Current_Data[0].data |= g_tVar.usRegInputBuf[5]>>8;
	
	Current_Data[1].data =g_tVar.usRegInputBuf[5]&0XFF;
	Current_Data[1].data =Current_Data[1].data<<8;
	Current_Data[1].data |= g_tVar.usRegInputBuf[6]>>8;	
	
	DispControler.Machine_version=g_tVar.usRegInputBuf[6]&0XFF;
	
	/*************************改版**************************************/
	//功率模块
	{
		//防错处理
		if( g_tVar.usRegInputBuf[7] == 0xffff)
		{
			setPwderNum(&FactoryPageParaer.H_Power,0);				
		}
		else
		{
			setPwderNum(&FactoryPageParaer.H_Power,g_tVar.usRegInputBuf[7]);	
		}
		if( g_tVar.usRegInputBuf[8] == 0xffff)
		{
			setPwderNum(&FactoryPageParaer.L_Power,0);
		}
		else
		{
			setPwderNum(&FactoryPageParaer.L_Power, g_tVar.usRegInputBuf[8]);
		}
		
		if(getFactoryPara(MachineType_E)== 1)//双速风机 调高速联动调低速
		 {
			 setFactoryPara(H_Power_Select_E          ,Search_Index(get_PwdNum(Factory_H_Power_E) ,(uint16_t *)DOUBLE_H_POWER_LIST,DOUBLE_H_POWER_LIST_NUM));
		 }
		 else
		 {
			 setFactoryPara(H_Power_Select_E          ,Search_Index(get_PwdNum(Factory_H_Power_E) ,(uint16_t *)SINGLE_POWER_LIST,SINGLE_POWER_LIST_NUM));
		 }
		FactoryPageParaer.FactoryParamember.usL_Power_Select =Search_Index(FactoryPageParaer.L_Power.uiPwdNum ,(uint16_t *)DOUBLE_L_POWER_LIST,DOUBLE_L_POWER_LIST_NUM);
		 //防错处理
		if( getFactoryPara(H_Power_Select_E          )==0xff)
		{
			setFactoryPara(H_Power_Select_E          ,0);
		}
		if(FactoryPageParaer.FactoryParamember.usL_Power_Select ==0xff)
		{
			FactoryPageParaer.FactoryParamember.usL_Power_Select=0;
		}
	}
	 
	
	//功率因数	
	 setFactoryPara(Power_Factor_E            ,g_tVar.usRegInputBuf[9]>>8);
	 setFactoryPara(AD_Sample_Time_E            ,g_tVar.usRegInputBuf[9]&0xff);
	//过载倍率设置
	 setFactoryPara(usOverLoad_multiply_E            ,g_tVar.usRegInputBuf[10]>>8);
	 
	//市电过压模块
	 if(DispControler.Machine_version >= 19)
	 {
		 static uint8_t cnt =0;
		 uint16_t temp = 0;
		 if( ++cnt < 2) 
		 {
		   return ;
		 }
		cnt = 10;
    	temp =  ((g_tVar.usRegInputBuf[10]&0xff)<<8) + (g_tVar.usRegInputBuf[11]>>8);
		setPwderNum(&FactoryPageParaer.VoltageMax,temp);	


		temp =  ((g_tVar.usRegInputBuf[11]&0xff)<<8) + (g_tVar.usRegInputBuf[12]>>8);
		setPwderNum(&FactoryPageParaer.VoltageMin,temp);	
		 
		 {
			uint32_t state=0;
			 
			state |= g_tVar.usRegInputBuf[12]&0XFF;
			state  = state<<8;
			state=g_tVar.usRegInputBuf[13]>>8;
			state  = state<<8;
			state |= g_tVar.usRegInputBuf[13]&0XFF;
			state  = state<<8;
			state |= g_tVar.usRegInputBuf[14]>>8;
			FactoryPageParaer.FactoryParamember.tRelay_flg.all = state;			 
		 }


	 }

}





/*
*********************************************************************************************************
*	函 数 名: Author_Tim_Ctrl 
*	功能说明:授权时间未操作处理 1s执行一次
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void Author_Tim_Ctrl(void)
{
	if(++STAer.ucAuthor_time>1800)//30min
	{
		STAer.ucAuthor = 0;	
		STAer.ucAuthor_time=1800+200;
	}
	
}






