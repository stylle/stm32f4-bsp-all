/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LEFT_Pin GPIO_PIN_1
#define LEFT_GPIO_Port GPIOA
#define RIGHT_Pin GPIO_PIN_2
#define RIGHT_GPIO_Port GPIOA
#define CENTER_Pin GPIO_PIN_3
#define CENTER_GPIO_Port GPIOA
#define YOUHAI_Pin GPIO_PIN_10
#define YOUHAI_GPIO_Port GPIOB
#define YOUHAI_EXTI_IRQn EXTI15_10_IRQn
#define CHUYU_Pin GPIO_PIN_11
#define CHUYU_GPIO_Port GPIOB
#define CHUYU_EXTI_IRQn EXTI15_10_IRQn
#define QITA_Pin GPIO_PIN_12
#define QITA_GPIO_Port GPIOB
#define QITA_EXTI_IRQn EXTI15_10_IRQn
#define KEHUISHOU_Pin GPIO_PIN_13
#define KEHUISHOU_GPIO_Port GPIOB
#define KEHUISHOU_EXTI_IRQn EXTI15_10_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
