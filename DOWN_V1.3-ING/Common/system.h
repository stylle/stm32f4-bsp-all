
#ifndef	 SYSTEM_H
#define	 SYSTEM_H

#define	LAMP_AOFT		1024	//灯具地址偏移
#define	ASIG_AOFT		128		//分配电地址偏移

#define	POWER_BUF_LEN		10	//电源端数据长度
#define	ASSIG_BUF_LEN		17	//分配电数据长度

#define	BUZ_TONE1	0x00	//蜂鸣器频率1
#define	BUZ_TONE2 0x01	//蜂鸣器频率2

#define	MAX_CNT_LAMP		128	//总计灯具最大数量
#define	MAX_NUM_LAMP		30	//支路灯具最大数量
#define	MAX_NUM_TRACK		8		//支路最大数量
#define	MAX_NUM_ASSIGN	8		//分配电最大数量
#define	MAX_NUM_POWER		8		//电源最大数量

//EPS设备状态
#define	EPS_ST_FAUT		0x01	//故障
#define	EPS_ST_EMGY		0x02	//应急
#define EPS_ST_MAIN		0x04	//主电
#define	EPS_ST_CHGE		0x08	//充电
#define	EPS_ST_BACK		0x10	//备电
#define	EPS_ST_YCHK		0x80	//年检

//应急类型
#define	AUTO_EMGY		0x00		//自动应急
#define	MANU_EMGY		0x01		//手动应急
#define	FORC_EMGY		0x02		//强制应急
#define	AMCK_EMGY		0X03		//自动月检应急
#define	AYCK_EMGY		0X04		//自动年检应急
#define	MMCK_EMGY		0x05		//手动月检应急
#define	MYCK_EMGY   0x06		//手动年检应急
#define	EMGY_IDLE		0x07		//设备空闲

#define	TEST_EMGY		0x08		//实验应急

//设备类别
#define	DEV_CTRL		0X00	//控制器
#define	DEV_POWER		0X01	//电源
#define	DEV_ASSIGN	0X02	//分配电
#define	DEV_LAMP		0x03	//灯具总类
#define	DEV_LIGHT		0X04	//照明灯
#define	DEV_FLAG		0X05	//指示灯

//应答码
#define	ACD_FAILED	0x01	//失败应答码
#define	ACD_SUCCESS 0x02	//成功应答码

#define	ACD_EMPTY		0x00	//帧正常
#define	ACD_CMD_ERR	0x01	//帧命令错误
#define	ACD_PAR_ERR	0x02	//帧参数错误

#define EMGY_ON		0xFF
#define EMGY_OFF	0xAA

//事件码(调试用)
#define	EVENT_CHECK		0x00	//控制器自检
#define	EVENT_MCHK		0x01	//手动月检
#define	EVENT_YCHK		0x02	//手动年检
#define	EVENT_MANUAL	0x03	//手动应急
#define	EVENT_MUTE		0x04	//消音
#define	EVENT_RESET		0x05	//系统复位
#define	EVENT_FEMGY		0x06	//强制启动
//#define	EVENT_LIGHT		0x07　//照明灯处理

//事件码(显示代码)
#define	EVT_EMGY_BGN		0x7D	//应急开始
#define	EVT_AUTO_MCHK		0x7C	//自动月检
#define	EVT_AUTO_YCHK		0x7B	//自动年检
#define	EVT_MANU_MCHK		0x7A	//手动月检
#define	EVT_MANU_YCHK		0x79	//手动年检
#define	EVT_CHK_SEL			0x77	//自检开始
#define	EVT_FOR_SET			0x76	//开启强制
#define	EVT_FOR_CLR			0x75	//关闭强制
#define	EVT_OPR_ERR			0x74	//无法进行当前操作
#define	EVT_EMGY_END		0x73	//应急结束
#define	EVT_MCHK_OK			0x78	//月检成功
#define	EVT_MCHK_NO			0x72	//月检失败
#define	EVT_YCHK_OK			0x71	//年检成功
#define	EVT_YCHK_NO			0x70	//年检失败
#define	EVT_CTL_EMGY		0x6F	//控制器应急

//命令码
#define	CMD_POW_EVENT		0x18 //电源事件
#define	CMD_CTRL_EVENT	0x17
#define	CMD_SET_STAE		0x15
#define	CMD_SET_TURN		0x14
#define	CMD_SET_DBG			0x13
#define	CMD_DEV_DEAL		0x12
#define	EPS_DEV_SET	   	0x01
#define	EPS_DEV_SAVE	  0x02
#define	EPS_DEV_PRF		  0x03
#define	CMD_QUERY_LABLE 0x11
#define	CMD_WRITE_LABLE	0x10
#define	CMD_QUERY_POWER	0x19 //电源查询
#define	CMD_QUERY_ASSIG	0x1A
#define	CMD_CTRL_TEST		0x1E
#define	CMD_TIME_BASE		0x23 //年月检时间基数
#define	CMD_SYS_RESET		0x26
#define	CMD_PRF_SYS			0x29
#define	CMD_CLR_FLAG		0x07 //0111
#define	CMD_SET_ADDR		0x0F //1111
#define	CMD_QUERY_ADDR	0x0E //1110
#define	CMD_SET_LIGHT		0x0D //1101
#define	CMD_QUERY_LAMP	0x0B //1011
#define	CMD_DBGRD_LAMP	0x0A //1010

//设置和读取当前状态
#define	SET_ST_MAIN(n,f) n = (f==1)?(n|EPS_ST_MAIN):(n&(~EPS_ST_MAIN))//主电状态
#define	SET_ST_CHGE(n,f) n = (f==1)?(n|EPS_ST_CHGE):(n&(~EPS_ST_CHGE))//充电状态
#define	SET_ST_FAUT(n,f) n = (f==1)?(n|EPS_ST_FAUT):(n&(~EPS_ST_FAUT))//故障
#define	SET_ST_EMGY(n,f) n = (f==1)?(n|EPS_ST_EMGY):(n&(~EPS_ST_EMGY))//应急
#define	SET_ST_HEMGY(n,f) n = (f==1)?(n|EPS_ST_EMGY):(n&(~EPS_ST_EMGY))//按键应急

#define	SET_ST_BACK(n,f) n = (f==1)?(n|EPS_ST_BACK):(n&(~EPS_ST_BACK))//备电
#define	SET_ST_YCHK(n,f) n = (f==1)?(n|EPS_ST_YCHK):(n&(~EPS_ST_YCHK))//年检

#define	IS_ST_MAIN(n) ((n&EPS_ST_MAIN)==EPS_ST_MAIN)
#define	IS_ST_CHGE(n) ((n&EPS_ST_CHGE)==EPS_ST_CHGE)
#define	IS_ST_FAUT(n) ((n&EPS_ST_FAUT)==EPS_ST_FAUT)
#define	IS_ST_EMGY(n)	((n&EPS_ST_EMGY)==EPS_ST_EMGY)
#define	IS_ST_BACK(n)	((n&EPS_ST_BACK)==EPS_ST_BACK)
#define	IS_ST_YCHK(n)	((n&EPS_ST_YCHK)==EPS_ST_YCHK)

//故障操作宏
#define	IS_ERR(e,n) (e&(1<<n))
#define	SET_ERR(e,n) (e|=(1<<n))
#define	CLR_ERR(e,n) (e&=~(1<<n))

//控制器故障
#define FUC_BAT_SHORT		0 //电池短路
#define FUC_BAT_OPEN		1	//电池开路
#define FUC_CHR_SHORT		2 //充电器短路
#define FUC_CHR_OPEN		3 //充电器开路
#define	FUC_CHR_VHIGH		10//充电器过压
#define FUC_BAT_VLOW		5 //电池欠压
#define	FUC_BAT_VHIGH		9 //电池过压
#define FUC_MARK_VLOW		6 //市电欠压
#define FUC_MARK_VHIGH	7	//市电过压
#define	FUC_MAIN_ERR		8	//市电异常
#define	FUC_POW_ERR			11//开关电源故障

//电源故障

#define FUP_BAT_SHORT		0		//充电短路
#define FUP_BAT_OPEN		1		//充电开路
#define	FUP_BAT_VLOW	  2   //电池欠压
#define	FUP_BAT1_VLOW		3	  //电池1欠压
#define	FUP_BAT2_VLOW	  4	  //电池2欠压
#define	FUP_BAT3_VLOW		5	  //电池3欠压
#define FUP_MAIN_VLOW		6		//主电欠压
#define FUP_MAIN_VHIGH	7		//主电过压
#define	FUP_MAIN_ERR		8  	//主电异常
#define	FUP_OPUT_OPEN		9		//输出开路
#define	FUP_OPUT_SHORT	10	//输出短路
#define	FUP_SLAVE_NACK	11	//从机无应答
#define	FUP_BAT_VHIGH		12	//电池过压
#define	FUP_CHR_VHIGH		13	//充电器过压
#define	FUP_MCK_VLOW		14	//月检失败
#define	FUP_YCK_VLOW		15	//年检失败

#define FUP_CHR_ERR     16  //充电器故障


//分配电故障
#define FUA_TRACK1_OPEN	0	//支路开路
#define FUA_TRACK2_OPEN	1
#define FUA_TRACK3_OPEN	2
#define FUA_TRACK4_OPEN	3
#define FUA_TRACK5_OPEN	4
#define FUA_TRACK6_OPEN	5
#define FUA_TRACK7_OPEN	6
#define FUA_TRACK8_OPEN	7

#define FUA_TRACK1_SHORT	8	//支路短路
#define FUA_TRACK2_SHORT	9
#define FUA_TRACK3_SHORT	10
#define FUA_TRACK4_SHORT	11
#define FUA_TRACK5_SHORT	12
#define FUA_TRACK6_SHORT	13
#define FUA_TRACK7_SHORT	14
#define FUA_TRACK8_SHORT	15

#define	FUA_SLAVE_NACK	  16	//从机无应答

//灯具故障
#define	FUL_SLAVE_NACK	0	//从机无应答
#define FUL_LAMP_OPEN   1	//灯具开路
#define FUL_LAMP_SHORT  2	//灯具短路
#define FUL_LAMP_POWER  3	//灯具电源故障
#define FUL_MASTE_NACK  4	//主机无应答



#endif

