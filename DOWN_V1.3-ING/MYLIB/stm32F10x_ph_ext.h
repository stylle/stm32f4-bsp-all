
#ifndef	STM32F10x_PH_EXT_H
#define	STM32F10x_PH_EXT_H

#include "stdafx.h"

//=========================================================================
//define
//=========================================================================
#define CHIP_FLASH_SIZE		64
#define	CHIP_FLASH_BASE		0x08000000

#if CHIP_FLASH_SIZE < 256
#define	CHIP_SECTOR_SIZE	1024
#else
#define	CHIP_SECTOR_SIZE	2048
#endif

//=========================================================================
//function
//=========================================================================
void UartSendBuffer(USART_TypeDef* USARTx, unsigned char*pBuf, int  nLen);
void UartSendByte(USART_TypeDef* USARTx, unsigned char nByte);


void FlashWriteMoreData(uint32_t nAddr, uint16_t *pData, uint16_t nCount);
void FlashReadMoreData(uint32_t nAddr, uint16_t *pData, uint16_t nCount);
uint16_t FlashReadHalfWord(uint32_t nAddr);

void IWDG_Feed(void);
void IWDG_Init(uint8_t nPre,uint16_t nRlr);

void SysTickInit(void);
void ConfigSystick(unsigned int nDiv);
void SysTickDelayUs(uint32_t nUs);
void SysTickDelayMs(uint32_t nMs);

void SendFrameByUR1(unsigned char*pBuf, unsigned int bLen);
void SendFrameByUR2(unsigned char*pBuf, unsigned char bLen);
void SendFrameByUR5(unsigned char*pBuf, unsigned char bLen);
void SendFrameByUR(void);
void SendFrameByURA(void);
#endif
