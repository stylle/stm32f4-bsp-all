
#include "stdafx.h"

void NVIC_Configuration(void)
{
  //NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Configure one bit for preemption priority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* 配置中断源 */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
}

//=======================================================================
//timer1
//=======================================================================
void InitTm1ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;//定义TIM结构变量
	NVIC_InitTypeDef NVIC_InitStructure;//定义EXTI结构变量
	// 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);//开启定时器1时钟
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM1 , TIM_IT_Update);//清空定时器1中断标志
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;  //计数值
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;//分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//预分频值（1为不分频）
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//向上计数模式
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);//初始化TIM1
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;//分频值
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM1,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM1, DISABLE);
	 
}

void StartTimer1(void)
{
	TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM1, ENABLE);
}

void StopTimer1(void)
{
	TIM_Cmd(TIM1, DISABLE);
	TIM_ITConfig(TIM1,TIM_IT_Update,DISABLE);
}

//======================================================
//frame timer
//======================================================
void StartPTimer(void)
{
	StartTimer1();
}

void StopPTimer(void)
{
	StopTimer1();
}

//=======================================================================
//timer2
//=======================================================================

void InitTm2ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
	
	//EX: nArr = 1400, freq of pwm is 72000/1400 = 50k(100us)
	//nArr = 10, nCcr = 5, wave out is square wave(1k)
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
		
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM2,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM2, DISABLE);
		//
}

void StartTimer2(void)
{
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM2, ENABLE);
}

void StopTimer2(void)
{
	TIM_Cmd(TIM2, DISABLE);
	TIM_ITConfig(TIM2,TIM_IT_Update,DISABLE);
}


//=======================================================================
//timer3
//=======================================================================
void InitTm3ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM3,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM3, DISABLE);
}

void StartTimer3(void)
{
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM3, ENABLE);
}

void StopTimer3(void)
{
	TIM_Cmd(TIM3, DISABLE);
	TIM_ITConfig(TIM3,TIM_IT_Update,DISABLE);
}


//=======================================================================
//timer4
//=======================================================================
void InitTm4ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_ICInitTypeDef  TIM_ICInitStructure; 
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	//
	
	TIM_ICInitStructure.TIM_Channel =  TIM_Channel_2;  // 选择输入端 IC4映射到TI4上
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	//上升沿捕获
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	 //配置输入分频,不分频 
  TIM_ICInitStructure.TIM_ICFilter = 0x03;//IC4F=0011 配置输入滤波器 8个定时器时钟周期滤波
  TIM_ICInit(TIM4, &TIM_ICInitStructure);//初始化定时器输入捕获通道
  
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM4, DISABLE);
}


//=======================================================================
//timer4用于PWM
//=======================================================================
//PWM3通道
void InitTm4Ch3ToPwmMode(unsigned short nCcr)
{
	//EX: nArr = 7199, freq of pwm is 72000/7200 = 10k(100us)
	//nArr = 10, nCcr = 5, wave out is square wave(1k)
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //mode select
 	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(TIM4, &TIM_OCInitStructure);

	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable); //set channel2
	
	//TIM_ARRPreloadConfig(TIM2, ENABLE);
	
  TIM_SetCompare3(TIM1, nCcr); //set ch1' ccr值
	
	TIM_CtrlPWMOutputs(TIM4, ENABLE);
	TIM_Cmd(TIM4, ENABLE); //enable
}

void StartTimer4(void)
{
	TIM_Cmd(TIM4, ENABLE);
	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	TIM_ITConfig(TIM4,TIM_IT_Update|TIM_IT_CC2,ENABLE);
	
}

void StopTimer4(void)
{
	TIM_Cmd(TIM4, DISABLE);
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE);
}

//=======================================================================
//timer5
//=======================================================================
	
void InitTm5ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
	//clear flag first
	//otherwise int will happen after enable 0timer
	TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM5,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM5, DISABLE);
}

void StartTimer5(void)
{
	TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
	TIM_ITConfig(TIM5,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM5, ENABLE);
}

void StopTimer5(void)
{
	TIM_Cmd(TIM5, DISABLE);
	TIM_ITConfig(TIM5,TIM_IT_Update,DISABLE);
}

//=======================================================================
//timer6
//=======================================================================
void InitTm6ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM6,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM6, DISABLE);
}

void StartTimer6(void)
{
	TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
	TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM6, ENABLE);
}

void StopTimer6(void)
{
	TIM_Cmd(TIM6, DISABLE);
	TIM_ITConfig(TIM6,TIM_IT_Update,DISABLE);
}

//=======================================================================
//timer7
//=======================================================================
void InitTm7ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM7,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM7, DISABLE);
}

void StartTimer7(void)
{
	TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
	TIM_ITConfig(TIM7,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM7, ENABLE);
}

void StopTimer7(void)
{
	TIM_Cmd(TIM7, DISABLE);
	TIM_ITConfig(TIM7,TIM_IT_Update,DISABLE);
}

//=======================================================================
//timer8
//=======================================================================
void InitTm8ToBaseMode(unsigned short nArr,unsigned short nPsc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;//定义TIM结构变量
	NVIC_InitTypeDef NVIC_InitStructure;//定义EXTI结构变量
	// 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);//开启定时器1时钟
	//clear flag first
	//otherwise int will happen after enable timer
	TIM_ClearITPendingBit(TIM8 , TIM_IT_Update);//清空定时器1中断标志
	//
	TIM_TimeBaseStructure.TIM_Period = nArr;  //计数值
	TIM_TimeBaseStructure.TIM_Prescaler = nPsc;//分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//预分频值（1为不分频）
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//向上计数模式
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);//初始化TIM1
	//
	NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_IRQn;//分频值
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	TIM_ITConfig(TIM8,TIM_IT_Update,DISABLE);
	TIM_Cmd(TIM8, DISABLE);
}

void StartTimer8(void)
{
	TIM_ClearITPendingBit(TIM8, TIM_IT_Update);
	TIM_ITConfig(TIM8,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM8, ENABLE);
}

void StopTimer8(void)
{
	TIM_Cmd(TIM8, DISABLE);
	TIM_ITConfig(TIM8,TIM_IT_Update,DISABLE);
}


//=======================================================================
//uart1
//=======================================================================
void ConfigUART1(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	USART_DeInit(USART1);
	//
	USART_InitStructure.USART_BaudRate = 9600;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	//
  USART_Init(USART1, &USART_InitStructure); //
	//
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//
  USART_ITConfig(USART1, USART_IT_ORE, ENABLE);//
	//
  USART_Cmd(USART1, ENABLE);
}

//======================================================
//发送串口初始化
//======================================================
void SetUartToTxMode(void)
{
	USART_InitTypeDef USART_InitStructure;
	//
	GPIO_InitTypeDef GPIO_InitStructure;
		//set tx to uart mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //PA.2
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//
	GPIO_Init(GPIOA, &GPIO_InitStructure); //
		
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	USART_DeInit(USART2);
	//
	USART_InitStructure.USART_BaudRate = 19200;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode =  USART_Mode_Tx;	
	
	//
  USART_Init(USART2, &USART_InitStructure); //
	//
  USART_Cmd(USART2, ENABLE);
	
//	USART_ClearFlag(USART2,USART_FLAG_TC);//清除TC标志位，防止出现第一字节发送失败
}



//======================================================
//接受串口初始化
//======================================================
void SetUartToRxMode(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//PA.3
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	USART_DeInit(USART2);
	//
	USART_InitStructure.USART_BaudRate = 9600;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode =  USART_Mode_Rx;	
	//
  USART_Init(USART2, &USART_InitStructure); //
	//
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);//
	USART_ITConfig(USART2, USART_IT_ORE, ENABLE);//
	//
  USART_Cmd(USART2, ENABLE);
}

void SetTxToPPMode()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	//set tx to output mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	//
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void SetTxToAFMode()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	//set tx to output mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}
void SetRxToIpuMode()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	//set rx to input mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //
}

void SetTxValue(unsigned char bValue)
{
	if(bValue == 1)
		GPIO_SetBits(GPIOA, GPIO_Pin_2); //GPIOx->BSRR = GPIO_Pin; set high
	else
		GPIO_ResetBits(GPIOA, GPIO_Pin_2); //GPIOx->BRR = GPIO_Pin; clear high
}


//======================================================
//串口开关
//======================================================
void CloseUart()
{
	USART_Cmd(USART1, DISABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);//
}

void OpenUart()
{
	USART_Cmd(USART1, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//
}

//=======================================================================
//uart3
//=======================================================================
void ConfigUART3(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	USART_DeInit(USART3);
	//
	USART_InitStructure.USART_BaudRate = 9600;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	//
  USART_Init(USART3, &USART_InitStructure); //
	//
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//
	//
  USART_Cmd(USART3, ENABLE);
}

//=======================================================================
//uart4
//=======================================================================
void ConfigUART4(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
	USART_DeInit(UART4);
	//
	USART_InitStructure.USART_BaudRate = 9600;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	//
  USART_Init(UART4, &USART_InitStructure); //
	//
  NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
  USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);//
	//
  USART_Cmd(UART4, ENABLE);
}

//=======================================================================
//uart5
//=======================================================================
void ConfigUART5(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
	USART_DeInit(UART5);
	//
	USART_InitStructure.USART_BaudRate = 38400;//
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	//
  USART_Init(UART5, &USART_InitStructure); //
	//
  NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
  USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);//
	//
	
  USART_Cmd(UART5, ENABLE);
}

//=======================================================================
//can
//=======================================================================
void CanModeInit(uint8_t nTsjw,uint8_t nTbs2,uint8_t nTbs1,uint16_t nBrp,uint8_t nMode)
{
	CAN_InitTypeDef	CAN_InitStructure;
//CAN_FilterInitTypeDef	CAN_FilterInitStructure;
	NVIC_InitTypeDef	NVIC_InitStructure;
	//
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	//CAN set
	CAN_InitStructure.CAN_TTCM=DISABLE;
	CAN_InitStructure.CAN_ABOM=ENABLE;
	CAN_InitStructure.CAN_AWUM=DISABLE;
	CAN_InitStructure.CAN_NART=ENABLE;
	CAN_InitStructure.CAN_RFLM=DISABLE;
	CAN_InitStructure.CAN_TXFP=DISABLE;
	CAN_InitStructure.CAN_Mode= nMode;

	//baud
	CAN_InitStructure.CAN_SJW=nTsjw;
	CAN_InitStructure.CAN_BS1=nTbs1;
	CAN_InitStructure.CAN_BS2=nTbs2;
	CAN_InitStructure.CAN_Prescaler=nBrp;
	CAN_Init(CAN1, &CAN_InitStructure);

//	CAN_FilterInitStructure.CAN_FilterNumber=0;	
//	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 
//	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
//	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;//ID
//	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
//	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;//MASK
//	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
//	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO0;//filter0
//	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE; //active

//	CAN_FilterInit(&CAN_FilterInitStructure);//

	CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);//   

	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


uint8_t CanSendMsg(uint8_t* pMsg,uint8_t nLen)
{	
  uint8_t mbox;
  uint16_t i=0;
  CanTxMsg TxMessage;
	//
  TxMessage.StdId=0x12;
  TxMessage.ExtId=0x12;
  TxMessage.IDE=0;
  TxMessage.RTR=0;
  TxMessage.DLC=nLen;
	//
  for(i=0;i<nLen;i++)
	{
		TxMessage.Data[i]=pMsg[i];
	}
  mbox= CAN_Transmit(CAN1, &TxMessage);
  i=0;
  while((CAN_TransmitStatus(CAN1, mbox)==CAN_TxStatus_Failed)&&(i<0XFFF))i++;
  if(i >= 0XFFF)return 1;
  return 0;		
}

uint8_t CanReceiveMsg(uint8_t *pBuf)
{		   		   
 	uint32_t i;
	CanRxMsg RxMessage;
	//
	if( CAN_MessagePending(CAN1,CAN_FIFO0)==0)return 0;
	//
	CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
	//
	for(i=0;i<8;i++)
	{
		pBuf[i]=RxMessage.Data[i];
	}
	//
	return RxMessage.DLC;	
}

//=======================================================================
//GPIO_Exti
//=======================================================================
void Start_GPIO_EXTI(void)
{
 	EXTI_InitTypeDef EXTI_InitStructure;
 	NVIC_InitTypeDef NVIC_InitStructure;
	//
	
		//////////////////////EXTI4//////////////////////短路
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource4);//映射中断线
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	EXTI_InitStructure.EXTI_Line = EXTI_Line4;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;    //下降沿中断
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	///* Enable and set EXTI0 Interrupt to the lowest priority */

	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_InitStructure);

}

void Stop_GPIO_EXTI(void)
{
	
 	EXTI_InitTypeDef EXTI_InitStructure;
 	NVIC_InitTypeDef NVIC_InitStructure;
	//
	
		//////////////////////EXTI4//////////////////////短路
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource4);//映射中断线
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	EXTI_InitStructure.EXTI_Line = EXTI_Line4;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;    //下降沿中断
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	///* Enable and set EXTI0 Interrupt to the lowest priority */

	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	
	NVIC_Init(&NVIC_InitStructure);
	
}

//=======================================================================
//adc1扫描式
//=======================================================================
void ADC1Init()
{
	ADC_InitTypeDef ADC_InitStructure; 
	//
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE );
	RCC_ADCCLKConfig(RCC_PCLK2_Div6); //adc freq, 72/6=12 < 14M
	ADC_DeInit(ADC1);
	//
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; //mode
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;	//scan mode
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	//continus convert
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	//close external trig
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; //format
	ADC_InitStructure.ADC_NbrOfChannel = 1; //number of channel(modify)
	ADC_Init(ADC1, &ADC_InitStructure);
	//

	ADC_Cmd(ADC1, ENABLE); //enable adc
	//
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));
	//
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

//=======================================================================
//adc2中断式
//=======================================================================
void ADC2Init()
{
	ADC_InitTypeDef ADC_InitStructure; 
	NVIC_InitTypeDef NVIC_InitStructure;
	//
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE );
	RCC_ADCCLKConfig(RCC_PCLK2_Div6); //adc freq, 72/6=12 < 14M
	ADC_DeInit(ADC2);
	//
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; //mode
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;	//scan mode
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;	//continus convert
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	//close external trig
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; //format
	ADC_InitStructure.ADC_NbrOfChannel = 1; //number of channel(modify)
	ADC_Init(ADC2, &ADC_InitStructure);
	//
	ADC_RegularChannelConfig(ADC2, ADC_Channel_9, 1, ADC_SampleTime_239Cycles5);
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_239Cycles5);
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_239Cycles5);
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 4, ADC_SampleTime_239Cycles5);
	//
//	ADC_DMACmd(ADC1, ENABLE); //enable dma
	ADC_Cmd(ADC2, ENABLE); //enable adc
	//
	ADC_ResetCalibration(ADC2);
	while(ADC_GetResetCalibrationStatus(ADC2));
	ADC_StartCalibration(ADC2);
	while(ADC_GetCalibrationStatus(ADC2));
	//
	NVIC_InitStructure.NVIC_IRQChannel = ADC1_2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//
	ADC_ITConfig(ADC2, ADC_IT_EOC, ENABLE);
	ADC_SoftwareStartConvCmd(ADC2, ENABLE);
}

//=======================================================================
//dma
//=======================================================================
void MYDMA1_Config(DMA_Channel_TypeDef* DMA_CHx,uint32_t nDevAddr,uint32_t nBaseAddr,uint16_t nSize)
{
	DMA_InitTypeDef DMA_InitStructure;
	//
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  DMA_DeInit(DMA_CHx);
	//
	DMA_InitStructure.DMA_PeripheralBaseAddr = nDevAddr; //base addr for dma
	DMA_InitStructure.DMA_MemoryBaseAddr = nBaseAddr;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; //direction
	DMA_InitStructure.DMA_BufferSize = nSize; 	//buffer size
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; //memory addr increase
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA_CHx, &DMA_InitStructure);
	//
	DMA_Cmd(DMA_CHx, ENABLE);
} 

//=======================================================================
//SPI
//=======================================================================
void SPI2_Init(void)
{
  SPI_InitTypeDef  SPI_InitStructure;

	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_SPI2,  ENABLE );
 
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex; 
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI2, &SPI_InitStructure);
 
	SPI_Cmd(SPI2, ENABLE);
	
	SPI2_ReadWriteByte(0xff);
}

void SPI2_SetSpeed(uint8_t SPI_BaudRatePrescaler)
{
  assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));
	SPI2->CR1&=0XFFC7;
	SPI2->CR1|=SPI_BaudRatePrescaler;
	SPI_Cmd(SPI2,ENABLE);
} 

uint8_t SPI2_ReadWriteByte(uint8_t nData)
{
	uint8_t retry=0;		
	//
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET)
	{
		retry++;
		if(retry>200)return 0;
	}
	//
	SPI_I2S_SendData(SPI2, nData); 
	retry=0;
	//
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET) 
	{
		retry++;
		if(retry>200)return 0;
	}
	return SPI_I2S_ReceiveData(SPI2);		    
}

