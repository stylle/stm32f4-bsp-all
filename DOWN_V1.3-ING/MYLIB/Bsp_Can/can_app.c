#include "can_app.h"

// 结构体数据变量
static CAN_POWER_SUPPLY power_supply; 
static CAN_INPUT_VC input_vc;
static CAN_TIME canTime;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8;
static CAN_FLAG canFlag;
static CAN_LAMP_DATA lampData[LAMP_NUM]; // 512个灯具
uint8_t LampSendStatus = TRUE;

// 结构体历史数据变量
static CAN_POWER_SUPPLY power_supply_tmp;
static CAN_INPUT_VC input_vc_tmp;
static CAN_TIME canTime_tmp;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4_tmp;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8_tmp;
static CAN_FLAG canFlag_tmp;
CAN_LAMP_DATA lampData_tmp[LAMP_NUM];

// Can_extid
TYPEID power_supply_extid = {.id = CAN_POWER_SUPPLY_EXTID};
TYPEID input_vc_extid 		= {.id = CAN_INPUT_VC_EXTID};
TYPEID canTime_extid 			= {.id = CAN_TIME_EXTID};
TYPEID VolCur_1To4_extid 	= {.id = CAN_LOOP_VOL_CUR_14_EXTID}; 
TYPEID VolCur_5To8_extid 	= {.id = CAN_LOOP_VOL_CUR_58_EXTID};
TYPEID canFlag_extid 			= {.id = CAN_FLAG_EXTID};
TYPEID lampData_extid 		= {.id = CAN_LAMP_DATA_EXTID};

extern uint8_t FirstSend; //首次全部发送标志
extern unsigned short LampSet ; //控制灯具设置状态


// 模拟灯具数据列表
extern uint16_t lampAddr[250];
extern uint8_t lampState[250];

static int SEND_TYPE_CAN_POWER_SUPPLY_DATA(CAN_POWER_SUPPLY power_supply)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=power_supply_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_POWER_SUPPLY_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&power_supply,CAN_POWER_SUPPLY_DLC * sizeof(uint8_t));
	return Can_Send(&TxMessage);
}

static int SEND_TYPE_CAN_INPUT_VC_DATA(CAN_INPUT_VC input_vc)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=input_vc_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_INPUT_VC_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&input_vc,CAN_INPUT_VC_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_TIME_DATA(CAN_TIME canTime)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=canTime_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_TIME_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canTime,CAN_TIME_DLC * sizeof(uint8_t));
	
	return Can_Send(&TxMessage);	
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(CAN_LOOP_VOL_CUR_14 loop_vol_cur_14)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=VolCur_1To4_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LOOP_VOL_CUR_14_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_14,CAN_LOOP_VOL_CUR_14_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(CAN_LOOP_VOL_CUR_58 loop_vol_cur_58)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=VolCur_5To8_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LOOP_VOL_CUR_58_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_58,CAN_LOOP_VOL_CUR_58_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_FLAG_DATA(CAN_FLAG canflag)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=canFlag_extid.id;				// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_FLAG_DLC;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canflag,CAN_FLAG_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}

static int SEND_TYPE_CAN_LAMP_DATA_DATA(CAN_LAMP_DATA lamp_data)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=lampData_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LAMP_DATA_DLC ;							// 要发送的数据长度
	
	// 填充数据
	TxMessage.Data[0] = lamp_data.LAMP_ID_H;
	TxMessage.Data[1] = lamp_data.LAMP_ID_L;
	TxMessage.Data[2] = lamp_data.LAMP_DATA;
	
	return Can_Send(&TxMessage);
}

/*
* 装载灯具数据:传入第一个参数为id数组，第二个参数为灯具数据
 */
void upCanLampChar(uint16_t *id,uint8_t *data)
{
	int i;
	for(i = 0;i < LAMP_NUM;i++)
	{
		lampData[i].LAMP_ID_H = GETINT16_HIGH(data[i]);
		lampData[i].LAMP_ID_L = GETINT16_LOW(id[i]);
		lampData[i].LAMP_DATA = data[i];
	}
}

/**
	* 计算uint8_t 高低字节合成uint16_t
	* 判断数据还是太长暂时不使用，后续美化可能会采用
**/
uint16_t Uin8tToUint16(uint8_t H,uint8_t L)
{
	return MERGE_HL(H,L);
}

/*
 * can数据轮询发送任务
 */
void CanPoll(void)
{
	int byte_tmp[4]; 	// 用来保存高低字节合并的上一次数据
	int byte[4];		// 用于保存高低字节新的数据
	int i = 0;
	static RESET_SEND resetSend; //  重新发送标志
	static uint8_t sendFlag = 1;
	
	if(FirstSend) 
	{
		/* 全部发送标志，这里用到首次发送上面，首次发送过后会有一个标志位
			表示发送首次发送是否成功保证主机已经收到该数据	
		*/
		memcpy(&power_supply_tmp, &power_supply ,sizeof(power_supply));
		resetSend.ResetData.power = TRUE;
		
		memcpy(&input_vc_tmp, &input_vc ,sizeof(input_vc));
		resetSend.ResetData.input = TRUE;
		
		memcpy(&canFlag_tmp, &canFlag ,sizeof(canFlag));
		resetSend.ResetData.flag = TRUE;
		
		memcpy(&VolCur_1To4_tmp, &VolCur_1To4 ,sizeof(VolCur_1To4));
		resetSend.ResetData.vout14 = TRUE;
		
		memcpy(&VolCur_5To8_tmp, &VolCur_5To8 ,sizeof(VolCur_5To8));
		resetSend.ResetData.vout58 = TRUE;
		
		memcpy(&canTime_tmp, &canTime ,sizeof(canTime));
		resetSend.ResetData.time = TRUE;
		
		for(i= 0;i < LAMP_NUM; i++)
		{
			lampData_tmp[i].LAMP_ID_H = lampData[i].LAMP_ID_H;
			lampData_tmp[i].LAMP_ID_L = lampData[i].LAMP_ID_L;
//			lampData_tmp[i].LAMP_DATA = lampData[i].LAMP_DATA;
			lampData_tmp[i].LAMP_DATA = 0xff; // 重发标志，这里用来把全部数据依次轮询
		}
		FirstSend = FALSE;		// 首次发送标志关闭
		return;
	}
	
	//电源相关 
	if (TRUE == power_supply.sendStatus && sendFlag	== 1)
	{
		sendFlag++;
		byte_tmp[0] = MERGE_HL(power_supply_tmp.MAIN_VOL_H,power_supply_tmp.MAIN_VOL_L);
		byte[0] = MERGE_HL(power_supply.MAIN_VOL_H,power_supply.MAIN_VOL_L);
		
		byte_tmp[1] = MERGE_HL(power_supply_tmp.OUTPUT_VOL_H,power_supply_tmp.OUTPUT_VOL_L);
		byte[1] = MERGE_HL(power_supply.OUTPUT_VOL_H,power_supply.OUTPUT_VOL_L);
		
		byte_tmp[2] = MERGE_HL(power_supply_tmp.OUTPUT_CUR_H,power_supply_tmp.OUTPUT_CUR_L);
		byte[2] = MERGE_HL(power_supply.OUTPUT_CUR_H,power_supply.OUTPUT_CUR_L);
		
		byte_tmp[3] = MERGE_HL(power_supply_tmp.BATTERY_VOL_ALL_H,power_supply_tmp.BATTERY_VOL_ALL_L);
		byte[3] = MERGE_HL(power_supply.BATTERY_VOL_ALL_H,power_supply.BATTERY_VOL_ALL_L);
		
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) || 
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.power)
			{
				if(SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply)) //1为发送成功
				{
					// 发送数据成功过后、将标注位关闭并保存上次发送的数据
					memcpy(&power_supply_tmp, &power_supply ,sizeof(power_supply));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					power_supply.sendStatus = FALSE;
					resetSend.ResetData.power = FALSE;
				}else
				{
					resetSend.ResetData.power = TRUE; // 重发标志
					CAN_Init_All(BOUND20KB);
				}
			}
	}
	
	//充电电池相关
	if (TRUE == input_vc.sendStatus && sendFlag == 2)
	{
		sendFlag++;
		byte_tmp[0] = MERGE_HL(input_vc_tmp.BATTERY_VOL_1_H,input_vc_tmp.BATTERY_VOL_1_L);
		byte[0] = MERGE_HL(input_vc.BATTERY_VOL_1_H,input_vc.BATTERY_VOL_1_L);
		
		byte_tmp[1] = MERGE_HL(input_vc_tmp.BATTERY_VOL_2_H,input_vc_tmp.BATTERY_VOL_2_L);
		byte[1] = MERGE_HL(input_vc.BATTERY_VOL_2_H,input_vc.BATTERY_VOL_2_L);
		
		byte_tmp[2] = MERGE_HL(input_vc_tmp.INPUT_CUR_H,input_vc_tmp.INPUT_CUR_L);
		byte[2] = MERGE_HL(input_vc.INPUT_CUR_H,input_vc.INPUT_CUR_L);
		
		byte_tmp[3] = MERGE_HL(input_vc_tmp.INPUT_VOL_H,input_vc_tmp.INPUT_VOL_L);
		byte[3] = MERGE_HL(input_vc.INPUT_VOL_H,input_vc.INPUT_VOL_L);
		
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.input)
		  {
				if(SEND_TYPE_CAN_INPUT_VC_DATA(input_vc))
				{
					// 保存上次数据
					memcpy(&input_vc_tmp, &input_vc ,sizeof(input_vc));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					input_vc.sendStatus = FALSE;
					resetSend.ResetData.input = FALSE;
				}else{
					resetSend.ResetData.input = TRUE; // 重发标志
					CAN_Init_All(BOUND20KB);
				}
			}
	}
	//电池和应急相关 
	if (TRUE == canTime.sendStatus && sendFlag == 3)
	{
		sendFlag++;
		byte_tmp[0] = MERGE_HL(canTime_tmp.BATTERY_VOL_3_H,canTime_tmp.BATTERY_VOL_3_L);
		byte[0] = MERGE_HL(canTime.BATTERY_VOL_3_H,canTime.BATTERY_VOL_3_L);
		byte_tmp[1] = MERGE_HL(canTime_tmp.HEGTM_H,canTime_tmp.HEGTM_L);
		byte[1] = MERGE_HL(canTime.HEGTM_H,canTime.HEGTM_L);

			if(byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER) ||
					byte[1] > (byte_tmp[1] + HEGTM_SEND_TIME_S) || byte[1] < (byte_tmp[1]) || resetSend.ResetData.time) 
			// 定时更新一次应急时间，如果状态从新计数表示重新应急需要发送一次
			{
				if(SEND_TYPE_CAN_TIME_DATA(canTime)) 
				{
					// 保存上次数据
					memcpy(&canTime_tmp, &canTime ,sizeof(canTime));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					canTime.sendStatus = FALSE;
					resetSend.ResetData.time = FALSE;
				}else{
					resetSend.ResetData.time = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
	}
	//1-4路回路电压电流
	if (TRUE == VolCur_1To4.sendStatus && sendFlag == 4)
	{
			sendFlag++;
			byte_tmp[0] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_1_H,VolCur_1To4_tmp.LOOP_VOL_CUR_1_L);
			byte[0] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_1_H,VolCur_1To4.LOOP_VOL_CUR_1_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_2_H,VolCur_1To4_tmp.LOOP_VOL_CUR_2_L);
			byte[1] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_2_H,VolCur_1To4.LOOP_VOL_CUR_2_L);
			
			byte_tmp[2] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_3_H,VolCur_1To4_tmp.LOOP_VOL_CUR_3_L);
			byte[2] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_3_H,VolCur_1To4.LOOP_VOL_CUR_3_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_4_H,VolCur_1To4_tmp.LOOP_VOL_CUR_4_L);
			byte[3] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_4_H,VolCur_1To4.LOOP_VOL_CUR_4_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.vout14)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4))
				{
					// 保存上次数据
					memcpy(&VolCur_1To4_tmp, &VolCur_1To4 ,sizeof(VolCur_1To4));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					VolCur_1To4.sendStatus = FALSE;
					resetSend.ResetData.vout14 = FALSE;
				}else{
					resetSend.ResetData.vout14 = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
	}
	//5-8路回路电压电流
	if (TRUE == VolCur_5To8.sendStatus && sendFlag == 5)
	{
		sendFlag++;
			byte_tmp[0] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_5_H,VolCur_5To8_tmp.LOOP_VOL_CUR_5_L);
			byte[0] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_5_H,VolCur_5To8.LOOP_VOL_CUR_5_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_6_H,VolCur_5To8_tmp.LOOP_VOL_CUR_6_L);
			byte[1] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_6_H,VolCur_5To8.LOOP_VOL_CUR_6_L);
			
			byte_tmp[2] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_7_H,VolCur_5To8_tmp.LOOP_VOL_CUR_7_L);
			byte[2] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_7_H,VolCur_5To8.LOOP_VOL_CUR_7_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_8_H,VolCur_5To8_tmp.LOOP_VOL_CUR_8_L);
			byte[3] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_8_H,VolCur_5To8.LOOP_VOL_CUR_8_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.vout58)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8)) // 1为发送成功
				{
					// 保存上次数据
					memcpy(&VolCur_5To8_tmp, &VolCur_5To8 ,sizeof(VolCur_5To8));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					VolCur_5To8.sendStatus = FALSE;
					resetSend.ResetData.vout58 = FALSE;
				}else
				{
					resetSend.ResetData.vout58 = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
	}
	// 状态相关
	if (TRUE == canFlag.sendStatus && sendFlag == 6)
	{
		sendFlag++;
			if(canFlag_tmp.FAULT_FLAG_H == canFlag.FAULT_FLAG_H &&
					canFlag_tmp.FAULT_FLAG_L == canFlag.FAULT_FLAG_L &&
					canFlag_tmp.WORK_FLAG_H == canFlag.WORK_FLAG_H &&
					canFlag_tmp.WORK_FLAG_L == canFlag.WORK_FLAG_L && resetSend.ResetData.flag == FALSE)
			{
				// 数据相同不上传
			}
			else{
				if(SEND_TYPE_CAN_FLAG_DATA(canFlag)) 
				{
					// 发送上传数据
					memcpy(&canFlag_tmp, &canFlag ,sizeof(canFlag));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					canFlag.sendStatus = FALSE;
					resetSend.ResetData.flag = FALSE;
				}else{
					resetSend.ResetData.flag = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}	
	}
	// 灯具轮询开始
	if (LampSendStatus && (sendFlag >= 7))
	{
		sendFlag++;
		i = sendFlag - 7;
		if(i < LAMP_NUM)// 开始轮询灯具位
		{
			if((lampData_tmp[i].LAMP_DATA != lampData[i].LAMP_DATA) || (lampData_tmp[i].LAMP_DATA == 0xff))
			{
				SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[i]);// 单单发送在接受中断中去获取发送成功的数值
				#if !REVC_LAMP_SIGNAL  // 如果不开启接受确认就直接赋值到历史变量中
					lampData_tmp[i].LAMP_DATA = lampData[i].LAMP_DATA;
				#endif
				LampSendStatus = FALSE;
			}
		} 			
	}else{
		sendFlag = 1; // 重新开始下一次轮询
	}	
	
}

// 处理主机命令事件 广播帧处理
void Process_host_msg(CanRxMsg ISR_RxMessage)
{
	int indexAddr = 0,endAddr = 0; // 用来循环，j为临时变量
	
	// 查询信息指令
	if(ISR_RxMessage.Data[0] == CMD_CAN_QUERY) // 03查询指令
	{
		// 电源查询
		if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_POWER)
		{
			SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply);
		}
		
		// 工作状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_DEV_STATE) 
		{
			SEND_TYPE_CAN_FLAG_DATA(canFlag);
		}
		
		// 灯具状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_LAMP) 
		{
			// 该种方式会影响主循环的运行
			// while(SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[ISR_RxMessage.Data[2]])); // 等待发送成功
			// 采用下次轮询发送来实现重发
			if(!SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[ISR_RxMessage.Data[2]]))
			{
				lampData_tmp[ISR_RxMessage.Data[2]].LAMP_DATA = 0xff;// 重发标志
			}						
			SysTickDelayMs(CAN_SEND_DELAY_MS);
		}
		
		// 批量灯具查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_RANGE_LAMP) 
		{
			// REVC_LAMP_SIGNAL
			indexAddr = MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3]); // 查询首地址
			endAddr = MERGE_HL(ISR_RxMessage.Data[4],ISR_RxMessage.Data[5]); // 查询尾地址
			
			if (indexAddr > LAMP_NUM) return;// 查询长度超过灯具总数
			
			for(;indexAddr <= endAddr;indexAddr++)
			{
					if(!SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[indexAddr]))
					{
						lampData_tmp[indexAddr].LAMP_DATA = 0xff;// 重发标志
					}						
					SysTickDelayMs(CAN_SEND_DELAY_MS);	
			}
		}
		
		// 查询1-4路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL14) 
		{
			SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4);
		}
		
		// 查询5-8路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL58)
		{
			SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8);
		}
		
		// 查询应急时间
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_HEGTM)
		{
			SEND_TYPE_CAN_TIME_DATA(canTime);
		}
		
		// 查询版本信息
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_HEGTM)
		{
			// 发送的数据结构
			CanTxMsg TxMessage;
			TxMessage.StdId=0x12;						// 标准标识符 
			TxMessage.ExtId=0x12;						// 设置扩展标示符 
			TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
			TxMessage.RTR=CAN_RTR_Data;			// 数据帧
			TxMessage.DLC = 0x02 ;					// 要发送的数据长度
			
			// 填充数据
			TxMessage.Data[0] = 0x00;
			TxMessage.Data[1] = 0x01;
		
			Can_Send(&TxMessage);
		}
	} 
	
	// 修改指令
	else if(ISR_RxMessage.Data[0] == CMD_CAN_EDIT) // 06修改指令
	{
		// 改变灯具状态
		if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_LAMP) 				
		{

			lampState[MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3])] = ISR_RxMessage.Data[4];		// ISR_RxMessage.Data[2]; // 灯具ID高字节
			// ISR_RxMessage.Data[3]; // 灯具ID低字节
			// ISR_RxMessage.Data[4]; // 左亮，右亮，熄灭RxMessage.Data[3])] = ISR_RxMessage.Data[4];
		}
		
		// 重启复位：应该是全部重发
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_RESET) 
		{
			FirstSend = TRUE; // 重发标志
			EpsState.bReset = TRUE;	//复位
		}
		
		// 消音
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_SILENCING) 
		{
			SysInd.bMute = ISR_RxMessage.Data[2]; //消音标志
		}
	//#define	AUTO_EMGY		0x00		//自动应急
	//#define	MANU_EMGY		0x01		//手动应急
	//#define	FORC_EMGY		0x02		//强制应急
	//#define	AMCK_EMGY		0X03		//自动月检应急
	//#define	AYCK_EMGY		0X04		//自动年检应急
	//#define	AYCK_EMGY		0X05		//应急复位	
	//#define	EMGY_IDLE		0x06		//设备空闲
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MANUAL) 
		{
			EpsState.bCtrlSig &= ~(1<<ISR_RxMessage.Data[2]); 
		}
		
		// 月检
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MCHK) 
		{
			EpsState.bSelfSig &= ~(1<<ISR_RxMessage.Data[2]);
		}
		
		// 强制启动开关
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_FEMGY) 
		{
			EpsState.bSelfSig &= ~(1<<ISR_RxMessage.Data[2]);
		}
	}
}

void UpCanData(void)
{
	// 输出电压1-4
	VolCur_1To4.LOOP_VOL_CUR_1_H = GETINT16_HIGH(AdcAcquire.nVout[0]);
	VolCur_1To4.LOOP_VOL_CUR_1_L = GETINT16_LOW(AdcAcquire.nVout[0]);
	VolCur_1To4.LOOP_VOL_CUR_2_H = GETINT16_HIGH(AdcAcquire.nVout[1]);
	VolCur_1To4.LOOP_VOL_CUR_2_L = GETINT16_LOW(AdcAcquire.nVout[1]);
	VolCur_1To4.LOOP_VOL_CUR_3_H = GETINT16_HIGH(AdcAcquire.nVout[2]);
	VolCur_1To4.LOOP_VOL_CUR_3_L = GETINT16_LOW(AdcAcquire.nVout[2]);
	VolCur_1To4.LOOP_VOL_CUR_4_H = GETINT16_HIGH(AdcAcquire.nVout[3]);
	VolCur_1To4.LOOP_VOL_CUR_4_L = GETINT16_LOW(AdcAcquire.nVout[3]);

	// 输出电压5-8
	VolCur_5To8.LOOP_VOL_CUR_5_H = GETINT16_HIGH(AdcAcquire.nVout[4]);
	VolCur_5To8.LOOP_VOL_CUR_5_L = GETINT16_LOW(AdcAcquire.nVout[4]);
	VolCur_5To8.LOOP_VOL_CUR_6_H = GETINT16_HIGH(AdcAcquire.nVout[5]);
	VolCur_5To8.LOOP_VOL_CUR_6_L = GETINT16_LOW(AdcAcquire.nVout[5]);
	VolCur_5To8.LOOP_VOL_CUR_7_H = GETINT16_HIGH(AdcAcquire.nVout[6]);
	VolCur_5To8.LOOP_VOL_CUR_7_L = GETINT16_LOW(AdcAcquire.nVout[6]);
	VolCur_5To8.LOOP_VOL_CUR_8_H = GETINT16_HIGH(AdcAcquire.nVout[7]);
	VolCur_5To8.LOOP_VOL_CUR_8_L = GETINT16_LOW(AdcAcquire.nVout[7]);

	// 主电压、输出电压电流、总电压
	power_supply.MAIN_VOL_H = GETINT16_HIGH(VMAIN);
	power_supply.MAIN_VOL_L = GETINT16_LOW(VMAIN);
	power_supply.OUTPUT_VOL_H = GETINT16_HIGH(VOUT);
	power_supply.OUTPUT_VOL_L = GETINT16_LOW(VOUT);
	power_supply.OUTPUT_CUR_H = GETINT16_HIGH(IDIS);
	power_supply.OUTPUT_CUR_L = GETINT16_LOW(IDIS);
	power_supply.BATTERY_VOL_ALL_H = GETINT16_HIGH(LI_VBAT);
	power_supply.BATTERY_VOL_ALL_L = GETINT16_LOW(LI_VBAT);

	// 充电电压和电流
	input_vc.INPUT_VOL_H = GETINT16_HIGH(VCHAR);
	input_vc.INPUT_VOL_L = GETINT16_LOW(VCHAR);
	input_vc.INPUT_CUR_H = GETINT16_HIGH(ICHR);
	input_vc.INPUT_CUR_L = GETINT16_LOW(ICHR);
	// 电池1和2
	input_vc.BATTERY_VOL_1_H = GETINT16_HIGH(0);
	input_vc.BATTERY_VOL_1_L = GETINT16_LOW(0);
	input_vc.BATTERY_VOL_2_H = GETINT16_HIGH(LI_VBAT);
	input_vc.BATTERY_VOL_2_L = GETINT16_LOW(LI_VBAT);

	// 应急时间和电池3
	canTime.BATTERY_VOL_3_H = GETINT16_HIGH(0);
	canTime.BATTERY_VOL_3_L = GETINT16_LOW(0);
	canTime.HEGTM_H = GETINT16_HIGH(EpsState.hEgTm);
	canTime.HEGTM_L = GETINT16_LOW(EpsState.hEgTm);

	// 故障状态和工作状态
	canFlag.FAULT_FLAG_H = GETINT16_HIGH(gbMyError);
	canFlag.FAULT_FLAG_H = GETINT16_LOW(gbMyError);
	canFlag.WORK_FLAG_H = GETINT16_HIGH(EpsState.nState);
	canFlag.WORK_FLAG_L = GETINT16_LOW(EpsState.nState);

//	upCanLampChar(LampAddrBuf,LampState);
	upCanLampChar(lampAddr,lampState);

	UpDataSendStatus();// 更新发送状态
}

void UpDataSendStatus(void)
{
	canFlag.sendStatus = TRUE;
	canTime.sendStatus = TRUE;
	input_vc.sendStatus = TRUE;
	power_supply.sendStatus = TRUE;
	VolCur_5To8.sendStatus = TRUE;
	VolCur_1To4.sendStatus = TRUE;
}
