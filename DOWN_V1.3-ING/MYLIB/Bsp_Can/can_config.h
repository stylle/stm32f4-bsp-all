#ifndef __M_TYPE_H
#define __M_TYPE_H

#include <stm32f10x.h>
#include "stdafx.h"
#include "stm32F10x_ph_ext.h"
#include "string.h"
#include "can_app.h"
#include "can_drive.h"


// 本机地址
#define CAN_ADDR   203 // MyLable.nAddress

// 宏运算
#define MERGE_HL(H,L) 				((unsigned int)H<<8)+((unsigned int)L&0XFF)
#define GETINT16_HIGH(BYTE) 	((unsigned char)(BYTE>>8))
#define GETINT16_LOW(BYTE) 		((unsigned char)BYTE&0x00FF)
#define GETINT32_8(BYTE,n) 		(unsigned char)((BYTE&(0xff<<((4-n)*8)))>>((4-n)*8))

// 数据变化百分比范围
#define VC_PER 5
// 灯具数量
#define LAMP_NUM 40

// 应急时间发送间隔
#define HEGTM_SEND_TIME_S 60
// 接受收到回复消息等待时间
#define REVC_TIMEOUT_MS 100
#define LAMP_REVC_TIMEOUT_MS 100

// can发送间隔延时时间
#define CAN_SEND_DELAY_MS 20 // 20效果最佳
//心跳时间间隔基数
#define HEARTBEAT_DELAY 	1

#define CAN1_ENABLE 			1 	// CAN2开启标志位	
#define CAN2_ENABLE 			1 	// CAN2开启标志位	

#define HEARTBEAT_EN 			1 	// 心跳使能

#define REVC_SIGNAL 			0		// 电源等其他接受确认信号使能
#define REVC_LAMP_SIGNAL 	1		// 灯具数据发送确认信号使能

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

#endif


