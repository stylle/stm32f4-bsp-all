#include "can_drive.h"

#if REVC_SIGNAL
uint8_t revcSendOkSignal = FALSE;		// 主机回复标志位
#endif
#if REVC_LAMP_SIGNAL
uint8_t LampRevcSendOkSignal = FALSE;// 灯具回复标志位
#endif

CanRxMsg CanSendBuff; // 发送记录buff

// 该函数用于初始化FIFO1的过滤器，F103标准库的过滤器初始化函数没有这部分
void CAN_FilterInit1(CAN_FilterInitTypeDef* CAN_FilterInitStruct)
{
  uint32_t filter_number_bit_pos = 0;
  /* Check the parameters */
  assert_param(IS_CAN_FILTER_NUMBER(CAN_FilterInitStruct->CAN_FilterNumber));
  assert_param(IS_CAN_FILTER_MODE(CAN_FilterInitStruct->CAN_FilterMode));
  assert_param(IS_CAN_FILTER_SCALE(CAN_FilterInitStruct->CAN_FilterScale));
  assert_param(IS_CAN_FILTER_FIFO(CAN_FilterInitStruct->CAN_FilterFIFOAssignment));
  assert_param(IS_FUNCTIONAL_STATE(CAN_FilterInitStruct->CAN_FilterActivation));
  filter_number_bit_pos = ((uint32_t)1) << CAN_FilterInitStruct->CAN_FilterNumber;
  /* Initialisation mode for the filter */
  CAN2->FMR |= 0x00000001;
  /* Filter Deactivation */
  CAN2->FA1R &= ~(uint32_t)filter_number_bit_pos;
  /* Filter Scale */
  if (CAN_FilterInitStruct->CAN_FilterScale == CAN_FilterScale_16bit)
  {
    /* 16-bit scale for the filter */
    CAN2->FS1R &= ~(uint32_t)filter_number_bit_pos;
    /* First 16-bit identifier and First 16-bit mask */
    /* Or First 16-bit identifier and Second 16-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR1 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdLow) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdLow);
    /* Second 16-bit identifier and Second 16-bit mask */
    /* Or Third 16-bit identifier and Fourth 16-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR2 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdHigh);
  }
  if (CAN_FilterInitStruct->CAN_FilterScale == CAN_FilterScale_32bit)
  {
    /* 32-bit scale for the filter */
    CAN2->FS1R |= filter_number_bit_pos;
    /* 32-bit identifier or First 32-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR1 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdLow);
    /* 32-bit mask or Second 32-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR2 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdLow);
  }
  /* Filter Mode */
  if (CAN_FilterInitStruct->CAN_FilterMode == CAN_FilterMode_IdMask)
  {
    /*Id/Mask mode for the filter*/
    CAN2->FM1R &= ~(uint32_t)filter_number_bit_pos;
  }
  else /* CAN_FilterInitStruct->CAN_FilterMode == CAN_FilterMode_IdList */
  {
    /*Identifier list mode for the filter*/
    CAN2->FM1R |= (uint32_t)filter_number_bit_pos;
  }
  /* Filter FIFO assignment */
  if (CAN_FilterInitStruct->CAN_FilterFIFOAssignment == CAN_Filter_FIFO0)
  {
    /* FIFO 0 assignation for the filter */
    CAN2->FFA1R &= ~(uint32_t)filter_number_bit_pos;
  }
  if (CAN_FilterInitStruct->CAN_FilterFIFOAssignment == CAN_Filter_FIFO1)
  {
    /* FIFO 1 assignation for the filter */
    CAN2->FFA1R |= (uint32_t)filter_number_bit_pos;
  }
  /* Filter activation */
  if (CAN_FilterInitStruct->CAN_FilterActivation == ENABLE)
  {
    CAN2->FA1R |= filter_number_bit_pos;
  }
  /* Leave the initialisation mode for the filter */
  CAN2->FMR &= ~0x00000001;
}

#if CAN1_ENABLE
/*
	CAN_SJW:		重新同步跳跃时间单元.范围:CAN_SJW_1tq~ CAN_SJW_4tq
	CAN_BS2:		时间段2的时间单元.   范围:CAN_BS2_1tq~CAN_BS2_8tq;
	CAN_BS1:		时间段1的时间单元.   范围:CAN_BS1_1tq ~CAN_BS1_16tq
	Prescaler :	波特率分频器.范围:1~1024;  tq=(brp)*tpclk1
	CanMode：		CAN_Mode_LoopBack（回环模式） CAN_Mode_Normal（正常模式）
	波特率=Fpclk1 / ((BS1 + tBS2 + 1) * Pre); // 总线是时钟频率 / （时间段1 + 时间段2 + 1） * CAN分频

	CAN_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,CAN_Mode_LoopBack);
	波特率为:36M/((8+9+1)*4)=500Kbps 模式为回环模式
	返回值:0,初始化OK;   其他,初始化失败;    
*/
uint8_t CAN1_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode)
{
	GPIO_InitTypeDef 				GPIO_InitStructure; 
	CAN_InitTypeDef        	CAN_InitStructure;
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
	NVIC_InitTypeDef  			NVIC_InitStructure;

	// 时钟初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//使能PORTA时钟	                   											 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);	//使能CAN1时钟	

	// GPIO初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;				//复用推挽GPIO_Mode_AF_PP
	GPIO_Init(GPIOA, &GPIO_InitStructure);								//初始化IO

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;					//上拉输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);								//初始化IO

	// CAN单元设置
	CAN_InitStructure.CAN_TTCM=DISABLE;			//非时间触发通信模式  
	CAN_InitStructure.CAN_ABOM=ENABLE;			//软件自动离线管理	 
	CAN_InitStructure.CAN_AWUM=DISABLE;			//睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStructure.CAN_NART=ENABLE;			//禁止报文自动传送 
	CAN_InitStructure.CAN_RFLM=DISABLE;		 	//报文不锁定,新的覆盖旧的  
	CAN_InitStructure.CAN_TXFP=ENABLE;			//优先级由报文标识符决定 
	CAN_InitStructure.CAN_Mode= mode;	  		//模式设置： mode:0,普通模式;1,回环模式; 
	
	// 设置波特率
	CAN_InitStructure.CAN_SJW=tsjw;					//重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  CAN_SJW_1tq	 CAN_SJW_2tq CAN_SJW_3tq CAN_SJW_4tq
	CAN_InitStructure.CAN_BS1=tbs1; 				//Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2=tbs2;					//Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler=brp;		//分频系数(Fdiv)为brp+1	
	CAN_Init(CAN1, &CAN_InitStructure);     //初始化CAN1 
	
	// 配置过滤器
	CAN_FilterInitStructure.CAN_FilterNumber=0;													//过滤器0
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 			//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; 			//32位宽 
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;										//32位ID
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;								//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO0;	//过滤器0关联到FIFO0
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;								//激活过滤器1
	CAN_FilterInit(&CAN_FilterInitStructure);			//滤波器初始化
	
	// 配置FIFO1为CAN2接收器
	CAN_ITConfig(CAN1,CAN_IT_FMP0, ENABLE);				//FIFO1消息挂号中断允许.		    
	
	// 配置中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn; // USB_LP_CAN1_RX0_IRQn  CAN1_RX1_IRQn
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;     // 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;            // 次优先级为1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_SCE_IRQn;	   //CAN RX中断
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  /*CAN通信中断使能*/
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);		//接收信号量建好后再开启

	return 0;
}

uint8_t Can1_Send_Msg(CanTxMsg TxMessage)
{
	uint8_t res;
	uint16_t i=0;
	
	res= CAN_Transmit(CAN1, &TxMessage);	// 发送数据
	
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	
	if(i>=0XFFF)
			return 1;
	return 0;	 
}
#endif 
#if CAN2_ENABLE
////////////////////////////////////////////////////////////// can2
uint8_t CAN2_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode)
{
	GPIO_InitTypeDef 				GPIO_InitStructure; 
	CAN_InitTypeDef        	CAN_InitStructure;
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
	NVIC_InitTypeDef  			NVIC_InitStructure;

	// 时钟初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//使能PORTA时钟	                   											 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);	//使能CAN1时钟	

	// GPIO初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;				//复用推挽	GPIO_Mode_AF_PP
	GPIO_Init(GPIOB, &GPIO_InitStructure);					

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;					//上拉输入 GPIO_Mode_IPU
	GPIO_Init(GPIOB, &GPIO_InitStructure);						

	// CAN单元设置
	CAN_InitStructure.CAN_TTCM=DISABLE;			//非时间触发通信模式  
	CAN_InitStructure.CAN_ABOM=ENABLE;			//软件自动离线管理	 
	CAN_InitStructure.CAN_AWUM=DISABLE;			//睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStructure.CAN_NART=ENABLE;			//禁止报文自动传送 
	CAN_InitStructure.CAN_RFLM=DISABLE;		 	//报文不锁定,新的覆盖旧的  
	CAN_InitStructure.CAN_TXFP=ENABLE;			//优先级由报文标识符决定 
	CAN_InitStructure.CAN_Mode= mode;	  		//模式设置： mode:0,普通模式;1,回环模式; 
	
	// 设置波特率
	CAN_InitStructure.CAN_SJW=tsjw;					//重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  CAN_SJW_1tq	 CAN_SJW_2tq CAN_SJW_3tq CAN_SJW_4tq
	CAN_InitStructure.CAN_BS1=tbs1; 				//Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2=tbs2;					//Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler=brp;		//分频系数(Fdiv)为brp+1	
	
	CAN_Init(CAN2, &CAN_InitStructure);     //初始化CAN2
	
	// 配置过滤器
	CAN_FilterInitStructure.CAN_FilterNumber=1;												//过滤器1
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 			//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; 			//32位宽 
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;										//32位ID
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;								//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO1;	//过滤器1关联到FIFO1
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;								//激活过滤器1
	CAN_FilterInit1(&CAN_FilterInitStructure);			//滤波器初始化
	
	// 配置中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX1_IRQn; // CAN2_RX1_IRQn    USB_LP_CAN2_RX0_IRQn
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;     // 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;            // 次优先级为1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	// 配置FIFO1为CAN2接收器
	CAN_ITConfig(CAN2,CAN_IT_FMP1, ENABLE);				//FIFO1消息挂号中断允许.		 

	return 0;
}

/*
	can发送一组数据(固定格式:ID为0X12,标准帧,数据帧)	
	len:数据长度(最大为8)				     
	message:数据指针,最大为8个字节.
	返回值:0,成功; 其他,失败;
*/
uint8_t Can2_Send_Msg(CanTxMsg TxMessage)
{	
	uint8_t res;
	uint16_t i=0;
	 
	res= CAN_Transmit(CAN2, &TxMessage);	// 发送数据
	
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	
	if(i>=0XFFF)
			return 1;
	return 0;	 
}

#endif

/*
 * can心跳任务需要一直发送数据包到控制器保证通信是正常在线状态
 * 发送成功：0，发送失败：1
 */

static TYPEID heartbeat_extid = {.id = CAN_HEARTBEAT_EXTID};
uint8_t CanHeartBeatPoll(void)
{
#if HEARTBEAT_EN
	uint8_t res;
	int i=0;
	uint8_t heartbeatArrgy[] = HEARTBEATARRGY;

	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=heartbeat_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC=CAN_HEARTBEAT_DLC;							  // 要发送的数据长度
	
	// 填充数据
	for(i=8;i >= 0;i--)
		TxMessage.Data[i]=heartbeatArrgy[i];	 

#if CAN2_ENABLE
	res = CAN_Transmit(CAN2, &TxMessage);	// CAN2发送数据
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
	{
			i++;															//等待发送结束
	}
	i = 0;
	res = 0;
#endif

#if CAN1_ENABLE
	res = CAN_Transmit(CAN1, &TxMessage);	// CAN1发送数据
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
	{
			i++;															//等待发送结束
	}
#endif
	if(i>=0XFFF)
			return 1;
#endif
	return 0;	
}


#if CAN1_ENABLE|CAN2_ENABLE

//	BOUND10KB = 1,
//	BOUND20KB,
//	BOUND50KB,
//	BOUND100KB,
//	BOUND200KB,
//	BOUND500KB,
//	BOUND1M,

// 同时初始化两个can并重新初始化波特率
extern uint8_t FirstSend;
void CAN_Init_All(CAN_BOUND bound)
{
	uint16_t time = 100;
	FirstSend = 1;// 初始化完所有数据开始第一次数据发送标志
	// 默认为20k
	switch((uint8_t)bound)
	{
		case BOUND10KB:time = 200;
			break;
		case BOUND20KB:time = 100;
			break;
		case BOUND50KB:time = 40;
			break;
		case BOUND100KB:time = 20;
			break;
		case BOUND200KB:time = 10;
			break;
		case BOUND500KB:time = 4;
			break;
		case BOUND1M:time = 2;
			break;
		default: time = 200; // 默认20K
			break;
	}
#if CAN1_ENABLE
	CAN1_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,time,CAN_Mode_Normal);
#endif
#if CAN2_ENABLE
	CAN2_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,time,CAN_Mode_Normal);
#endif
}

// 0表示发送失败
// 1表示发送成功
uint8_t Can_Send(CanTxMsg *TxMessage)
{
	uint8_t res;
	uint16_t i = 0;	
	
	TxMessage->StdId=0x12;						// 标准标识符 
	TxMessage->IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage->RTR=CAN_RTR_Data;			// 数据帧
	
	// EXTID 使用宏定义数（出现过）未超过29位发送失败
#if CAN1_ENABLE
	res= CAN_Transmit(CAN1, TxMessage);		// 发送数据
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	if(i>=0XFFF)  return 0; //发送失败
	i = 0;
#endif
	
#if CAN2_ENABLE
	res= CAN_Transmit(CAN2, TxMessage);	// 发送数据
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	if(i>=0XFFF)  return 0; //发送失败
#endif
	return 1;
}

/**
  * @brief  This function handles can1
  * @param  None
  * @retval None
	* CAN1 中断服务函数
*/


extern TYPEID power_supply_exti;
extern TYPEID input_vc_extid 	;
extern TYPEID canTime_extid 		;
extern TYPEID VolCur_1To4_extid;
extern TYPEID VolCur_5To8_extid;
extern TYPEID canFlag_extid 		;
extern TYPEID lampData_extid 	;
extern CAN_LAMP_DATA lampData_tmp[LAMP_NUM];

//void CAN1_RX1_IRQHandler(void)
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  CanRxMsg ISR_RxMessage;
	CanTxMsg TxMessage;
  CAN_Receive(CAN1, 0, &ISR_RxMessage);
	CAN_ClearITPendingBit(CAN1,CAN_IT_FF0);
	
	if(ISR_RxMessage.IDE != CAN_Id_Extended)// 不是扩展帧直接退出
	{
		return;
	}
	
	memcpy(&TxMessage,&ISR_RxMessage,sizeof(CanTxMsg));
#if CAN2_ENABLE
//	if(extid.bit.addrBit != CAN_ADDR && (extid.bit.addrBit == 0)){ 
		// 广播帧或者不是自己的地址直接转发到CAN2
		Can2_Send_Msg(TxMessage);
#endif // 141d0103
	
#if REVC_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC))
	{
		revcSendOkSignal = TRUE;
		return;
	}
#endif
	
#if REVC_LAMP_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(&(lampData_extid.id),&(ISR_RxMessage.ExtId),CAN_LAMP_DATA_DLC))
	{
		lampData_tmp[MERGE_HL(ISR_RxMessage.Data[0],ISR_RxMessage.Data[1])].LAMP_DATA = ISR_RxMessage.Data[2];
	}
#endif
	
	Process_host_msg(ISR_RxMessage); // 处理主机消息
}
/**
  * @brief  This function handles can2
  * @param  None
  * @retval None
	* CAN2 中断服务函数
*/
void CAN2_RX1_IRQHandler(void)
// void USB_LP_CAN2_RX0_IRQHandler(void)
{
  CanRxMsg ISR_RxMessage;
	CanTxMsg TxMessage;
  CAN_Receive(CAN2, 1, &ISR_RxMessage);
	CAN_ClearITPendingBit(CAN2,CAN_IT_FF1);
	
	if(ISR_RxMessage.IDE != CAN_Id_Extended) // 不是扩展帧直接退出
	{
		return;
	}
	memcpy(&TxMessage,&ISR_RxMessage,sizeof(CanTxMsg));
#if CAN1_ENABLE
	if((ISR_RxMessage.ExtId&0xffff) != CAN_ADDR || ((ISR_RxMessage.ExtId&0xffff) == 0)){ 
		// 广播帧或者不是自己的地址直接转发到CAN1 
		Can1_Send_Msg(TxMessage);
	}
#endif	
	
#if REVC_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC) && !memcmp(&CanSendBuff.StdId,&ISR_RxMessage.StdId,sizeof(CanSendBuff.StdId)))
	{
		revcSendOkSignal = TRUE;
		return;
	}
#endif
	
#if REVC_LAMP_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(&(lampData_extid.id),&(ISR_RxMessage.ExtId),CAN_LAMP_DATA_DLC))
	{
		lampData_tmp[MERGE_HL(ISR_RxMessage.Data[0],ISR_RxMessage.Data[1])].LAMP_DATA = ISR_RxMessage.Data[2];
	}
#endif
	
	Process_host_msg(ISR_RxMessage); // 处理主机消息
}

void TIM1_UP_IRQHandler(void)
{
	static int i = 0;
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) 
		{			
			TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );
			if(i++ >= HEARTBEAT_DELAY )
			{
				CanHeartBeatPoll();		
				i = 0;
			}
		}
}

#endif
