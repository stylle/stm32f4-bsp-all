
#include "stdafx.h"
#include "SequeList.h"

unsigned int SequeLen;

SEQUE_ELEM SequeArray[SEQUE_LEN];

unsigned char SequeAddElem(SEQUE_ELEM* pElem)
{
	if(SequeLen == SEQUE_LEN) return 0;
	//
	SequeArray[SequeLen++] = *pElem;
	//
	return SequeLen;
}

unsigned char SequeDelElem(unsigned int nIndex)
{
	unsigned char i;
	//
	if(SequeLen == 0) return 0;
	//
	if(nIndex >= SequeLen) return 0;
	//
	for(i=nIndex; i<(SequeLen-1); i++)
	{
		SequeArray[i] = SequeArray[i+1];
	}
	//
	SequeLen--;
	return SequeLen;
}

unsigned int SeQueGetLen(void)
{
	return SequeLen;
}

void SeQueClearList(void)
{
	SequeLen = 0;
}

SEQUE_ELEM SequeGetElem(unsigned int nIndex)
{
	if(nIndex >= SequeLen) 
		return SequeArray[SequeLen-1];
	else
		return SequeArray[nIndex];
}

void SequeSetElem(unsigned int nIndex, SEQUE_ELEM* pElem)
{
	if(nIndex >= SequeLen) return;
	//
	SequeArray[nIndex] = *pElem;
}






