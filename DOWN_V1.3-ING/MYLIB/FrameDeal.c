
#include "stdafx.h"
//================================================
//master send frame
//================================================

stControlToPow ControlToPowInfo;
SearchArea_T tSerchLampArea[10];

//unsigned short Lampnum = 0xffff; //灯具控制的具体灯具编号
unsigned char  LampSet = 0xff; //控制灯具设置状态
unsigned short DengJu = 0;
unsigned short nSerchLampSectionNum; //灯具区间数量

unsigned char FMasterSendA(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen))
{
	if((pNode->bState != FRM_ST_READY)||(pNode->bSend == FALSE)) return FALSE;
	//
	pNode->bSend = FALSE;
	//
	pSend(pNode->bTxBuf, pNode->frmTx.bLen + 2);
	
	return TRUE;
}

//================================================
//slaver send frame
//================================================
unsigned char FSlaverSendA(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen))
{
	if((pNode->bSend == FALSE)||(pNode->bState != FRM_ST_READY)) return FALSE;
	//
	pNode->bSend = FALSE;
	//
	pSend(pNode->bTxBuf, pNode->frmTx.bLen+FRM_ID_LEN*2+5);
	//
	return TRUE;
}

unsigned char FSlaverSendB(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned int bLen))
{
	if((pNode->bSend == FALSE)||(pNode->bState != FRM_ST_READY)) return FALSE;
	//
	pNode->bSend = FALSE;
	//
	pSend(pNode->bTxBuf, pNode->frmTx.bLen + 2);
	//
	return TRUE;
}

unsigned char FSlaverSendC(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned int bLen))
{
	if((pNode->bSend == FALSE)||(pNode->bState != FRM_ST_READY)) return FALSE;
	//
	pNode->bSend = FALSE;
	//
	pSend(pNode->bTxBuf, pNode->frmTx.bLen + 2);
	//
	return TRUE;
}

//================================================
//fill frame
//================================================
void FFillTxBuf(FRM_NODE* pNode)
{
	unsigned char i = 0;
	unsigned char bIndex = 0;
	unsigned char bData = 0;
	unsigned short nCheck = 0;
	//
	pNode->bTxBuf[bIndex++] = (unsigned char)(FRM_HEAD&0xff);
	pNode->bTxBuf[bIndex++] = (unsigned char)((FRM_HEAD>>8)&0xff);
	//
	bData = (pNode->frmTx.bType)|(pNode->frmTx.bAck<<3)|(pNode->frmTx.bLen<<4);
	pNode->bTxBuf[bIndex++] = bData;
	//
	bData = (unsigned char)((pNode->frmTx.nMaster)&0xff);
	pNode->bTxBuf[bIndex++] = bData;
	//
	//bData = (unsigned char)(((pNode->nMaster)>>8)&0xff);
	//pNode->bTxBuf[bIndex++] = bData;
	//
	bData = (unsigned char)((pNode->frmTx.nSlaver)&0xff);
	pNode->bTxBuf[bIndex++] = bData;
	//
	//bData = (unsigned char)(((pNode->nSlaver)>>8)&0xff);
	//pNode->bTxBuf[bIndex++] = bData;
	//
	for(i=0; i<(pNode->frmTx.bLen); i++)
	{
		bData = pNode->frmTx.bDtBuf[i];
		pNode->bTxBuf[bIndex++] = bData;
	}
	//
	nCheck = CRC16_CCITT(pNode->bTxBuf+2, bIndex-2);
	//
	pNode->bTxBuf[bIndex++] = (unsigned char)(nCheck&0xff);
	pNode->bTxBuf[bIndex++] = (unsigned char)((nCheck>>8)&0xff);
	//
	pNode->bSend = TRUE;
	pNode->bState = FRM_ST_READY;
}

#define SEARCHAREADATA_LEN  (ControlToPowInfo.nSerchLampSectionNum * sizeof(tSerchLampArea[0]))
	
void FenpeiFFillTxBuf(FRM_NODE* pNode)
{
	stPowToSlaver ToSlaverInfo;
	
	unsigned short nCheck = 0;	
	
	ToSlaverInfo.nHead = 0x9B3A;
	ToSlaverInfo.nDataLengh = sizeof(stPowToSlaver)+ SEARCHAREADATA_LEN;
	ToSlaverInfo.nMaster = 0;//这里主从机暂时不填写编号
	ToSlaverInfo.nSlaver = 0;
	
	ToSlaverInfo.nEmgy =  Myec.LampZlSet;//这里发送持续非持续转换
	ToSlaverInfo.nOperaLampNo = 1;
	ToSlaverInfo.nLampSet = LampSet;
	ToSlaverInfo.nWholeLampNum = DengJu;
	ToSlaverInfo.nSerchLampSectionNum =	ControlToPowInfo.nSerchLampSectionNum ;
	
	memset(pNode->bTxBuf, 0, DATA_BUF_LEN);
	memcpy(pNode->bTxBuf , &ToSlaverInfo , sizeof(ToSlaverInfo)); 
	memcpy(pNode->bTxBuf+sizeof(ToSlaverInfo) , (uint8_t *)tSerchLampArea,SEARCHAREADATA_LEN );
	pNode->frmTx.bLen = (sizeof(ToSlaverInfo)+ SEARCHAREADATA_LEN);
	
	nCheck = CRC16_CCITT(pNode->bTxBuf, 	pNode->frmTx.bLen);
	pNode->bTxBuf[pNode->frmTx.bLen] = (unsigned char)(nCheck&0xff);
	pNode->bTxBuf[pNode->frmTx.bLen + 1] =(unsigned char)((nCheck>>8)&0xff);
	
	pNode->bSend =  TRUE;
	pNode->bState = FRM_ST_READY;
}

//与控制器通信(电源)数据填充
void CONFFillTxBufA(FRM_NODE* pNode)
{
	unsigned short nCheck = 0;
	//
	memcpy(pNode->bTxBuf , pNode->frmTx.bDtBuf , pNode->frmTx.bLen); 
	//
	nCheck = CRC16_CCITT(pNode->bTxBuf , pNode->frmTx.bLen);
	//
	pNode->bTxBuf[pNode->frmTx.bLen] = (unsigned char)(nCheck&0xff);
	pNode->bTxBuf[pNode->frmTx.bLen + 1] = (unsigned char)((nCheck>>8)&0xff);
	
	pNode->bSend = TRUE;
	pNode->bState = FRM_ST_READY;
}

//与控制器通信(灯具)数据填充
void CONLFFillTxBuf(FRM_NODE* pNode)
{
	unsigned short nCheck = 0;
		//
	memcpy(pNode->bTxBuf , pNode->frmTx.bDtBuf , pNode->frmTx.bLen); 
	//
	nCheck = CRC16_CCITT(pNode->bTxBuf , pNode->frmTx.bLen);
	//
	pNode->bTxBuf[pNode->frmTx.bLen] = (unsigned char)(nCheck&0xff);
	pNode->bTxBuf[pNode->frmTx.bLen + 1] = (unsigned char)((nCheck>>8)&0xff);
	
	pNode->bSend = TRUE;
	pNode->bState = FRM_ST_READY;
}

//================================================
//解析控制器数据
//================================================
unsigned short nPowerCheck = 0;
unsigned char FProtocolDeal(FRM_NODE* pNode, unsigned char bByte)
{	
	if(pNode->bAnaSte == 0)
	{
		if(bByte == (unsigned char)(FRM_HEAD&0xff))//两个字节帧头
		{
			pNode->bAnaSte = 1;
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
 			pNode->strAnaL[pNode->bAnaCnt++] = bByte;
		}
		else
		{
			pNode->bAnaSte = 0;
			pNode->bAnaCnt = 0;
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
		}
	}
	else if(pNode->bAnaSte == 1)
	{
		if(bByte == (unsigned char)((FRM_HEAD>>8)&0xff))
		{
			pNode->bAnaSte = 2;
			pNode->strAnaL[pNode->bAnaCnt++] = bByte;
		}
		else
		{
			pNode->bAnaSte = 0;
			pNode->bAnaCnt = 0;
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
		}
	}
	else if(pNode->bAnaSte == 2)
	{
		pNode->bAnaLen = bByte;
		pNode->strAnaL[pNode->bAnaCnt++] = bByte;
		pNode->bAnaSte = 3;
	}
	else if(pNode->bAnaSte == 3)
	{
		pNode->aAnaLen = bByte;
		pNode->LEN = ((pNode->bAnaLen)|((pNode->aAnaLen<<8)&0xffff));
		pNode->strAnaL[pNode->bAnaCnt++] = bByte;
		pNode->bAnaSte = 4;
	}//此后新加//此后之前起始4
	else if(pNode->bAnaSte == 4)
	{
		pNode->strAnaL[pNode->bAnaCnt++] = bByte;
		//
		if(pNode->bAnaCnt == pNode->LEN)
		{
			pNode->bAnaSte = 5;
			//
			pNode->nAnaChk = CRC16_CCITT(pNode->strAnaL, pNode->bAnaCnt);
		}
		else if((pNode->bAnaCnt > pNode->LEN) || (pNode->LEN > 200))
		{
			pNode->bAnaSte = 0;
			pNode->bAnaCnt = 0;
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
		}
	}
	else if(pNode->bAnaSte == 5)
	{
		if(bByte == (unsigned char)(pNode->nAnaChk&0xff))
		{
			pNode->bAnaSte = 6;
		}
		else
		{
			pNode->bAnaSte = 0;
			pNode->bAnaCnt = 0;
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
		}
	}
	else if(pNode->bAnaSte == 6)
	{
		pNode->bAnaSte = 0;
		pNode->bAnaCnt = 0;
		if(bByte == (unsigned char)((pNode->nAnaChk>>8)&0xff))
		{
			return TRUE;
		}
		else
		{
			memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
		}
	}
	else if(pNode->bAnaSte > 6) //bAnaSte可能值会跳
	{
		pNode->bAnaSte = 0;
		pNode->bAnaCnt = 0;
		memset(pNode->strAnaL , 0 , DATA_BUF_LEN);
	}
	//
	return FALSE;	
}


unsigned short LampdataCh; //灯具数据字节长度 
unsigned short LampdataChTwo; //灯具第二个字节
unsigned short AddrIntral[20];//灯具区间搜索数组2位地址
//unsigned char LampAddrBuf[LAMP_ADDR_BUF_LEN];//地址数组
//unsigned short LampAddrBufLen = 0; //接收到的灯具数据字节数量
unsigned char DataLogo=0; //用来区分是地址还是故障
unsigned short FPError;  //16位的电源从机故障
unsigned char ChanlVBuf[10];//通道电压存储
unsigned char ChanlVNum=0;
unsigned char i=0;
unsigned short nTemp = 0;
//接收分配电数据接口
unsigned char FPFProtocolDeal(FRM_NODE* pNode, unsigned char bByte)
{
	static unsigned short LampAddrNum=0;

	if(pNode->bAnaCnt == 0)
	{
		if(bByte == 0x3d)	pNode->bAnaCnt = 1;//上传的是灯具数据
		else if(bByte == 0x3e)pNode->bAnaCnt = 2;//上传的从机数据
		else pNode->bAnaCnt = 0;
	}
	else if(pNode->bAnaCnt == 1)
	{
		if(bByte == 0x9a)pNode->bAnaCnt = 3;
		else pNode->bAnaCnt = 0;
	}else if(pNode->bAnaCnt == 2)
	{
		if(bByte == 0x9c)pNode->bAnaCnt = 4;
		else pNode->bAnaCnt = 0;
	}
	else if(pNode->bAnaCnt == 3)
	{
		LampdataCh = bByte;
		pNode->bAnaCnt = 5; 
	}
	else if(pNode->bAnaCnt == 5)
	{
		LampdataChTwo = bByte;
		LampdataCh = LampdataCh | ((LampdataChTwo << 8) & 0xffff);
		pNode->bAnaCnt = 6;
	}
	else if(pNode->bAnaCnt == 6)
	{		
		if(LampAddrNum > LAMP_ADDR_BUF_LEN)
		{
			pNode->bAnaCnt = 0;
			LampdataCh = 0;
			LampAddrNum = 0;
			return TRUE;
		}
		
//		LampAddrBuf[LampAddrNum++] = bByte;			
		if(LampAddrNum == LampdataCh)
		{
//			LampAddrBufLen = LampdataCh;
//			memset(pNode->strAnaL , LampAddrBufLen , 1000);
			pNode->bAnaCnt = 0;
			LampdataCh = 0;
			LampAddrNum = 0;
			return TRUE;		
		}
	}
	else if(pNode->bAnaCnt == 4)//电源从单片机数据只有故障
	{		
		LampdataCh = bByte;
		pNode->bAnaCnt = 7; 
	}
	else if(pNode->bAnaCnt == 7)
	{
		ChanlVBuf[ChanlVNum++]=bByte;
		if(ChanlVNum==LampdataCh)
		{
			pNode->bAnaCnt = 0;
			LampdataCh = 0;
			ChanlVNum = 0;
			return TRUE;
		}
		if(ChanlVNum > 10)
		{
			pNode->bAnaCnt = 0;
			LampdataCh = 0;
			ChanlVNum = 0;
			return TRUE;
		}
	}
	return FALSE;
}

//================================================
//接收分配电数据处理
//================================================
void FMasterReceiveA(FRM_NODE* pNode, unsigned char bByte)
{
	//
	if(FPFProtocolDeal(pNode, bByte) == TRUE)
	{
		pNode->bState = FRM_ST_END;		
		StopTimer7(); 
	}
}

//================================================
//接收控制器中断数据
//================================================

#define SERCHLEN ControlToPowInfo.nSerchLampSectionNum*sizeof(tSerchLampArea[0])
	
unsigned char nEmgPowerNum = 0; 	//应急的电源
unsigned char EmgPowerArr[80];	//应急的电源编号

void FSlaverReceiveA(FRM_NODE* pNode, unsigned char bByte)
{
	if(FProtocolDeal(pNode, bByte) == TRUE)
	{
		if(pNode->bState != FRM_ST_END)return;
		memcpy(&ControlToPowInfo , pNode->strAnaL , sizeof(ControlToPowInfo)); 
		
		//这里分配数据(报文头数据)
		pNode->frmRx.bType = ControlToPowInfo.nFrameType;
		pNode->frmRx.bAck = ControlToPowInfo.nIsReply;
		pNode->frmRx.bLen = ControlToPowInfo.nDataLengh;
		pNode->frmRx.nMaster = ControlToPowInfo.nMaster;
		pNode->frmRx.nSlaver = ControlToPowInfo.nSlaver;
		
		memset(pNode->frmRx.bDtBuf , 0 , DATA_BUF_LEN);
		memcpy(pNode->frmRx.bDtBuf , pNode->strAnaL , pNode->LEN);
				
		//判断是否有应急的电源
		if(ControlToPowInfo.nEmgPowerNum != 0)
		{
			memset(EmgPowerArr , 0 , 80);
			memcpy(EmgPowerArr , (pNode->strAnaL + (pNode->frmRx.bLen - SERCHLEN - ControlToPowInfo.nEmgPowerNum)),\
			ControlToPowInfo.nEmgPowerNum);
		}	
		memcpy(&tSerchLampArea , (pNode->strAnaL + pNode->frmRx.bLen - SERCHLEN),SERCHLEN);
		pNode->bState = FRM_ST_REV;	
	}
}


//================================================
//set frame state
//================================================
void FSetStateToRdy(FRM_NODE* pNode) {pNode->bState = FRM_ST_READY;}
void FSetStateToEnd(FRM_NODE* pNode) {pNode->bState = FRM_ST_END;}

//================================================
//time out detect
//priority of interrupt must equal to uart receive
//定时器开启条件：数据帧需要应答，广播帧
//================================================
void FTimeOutDetect(FRM_NODE* pNode, void(*pStop)(void))
{	
	if(pNode->bState == FRM_ST_SEND)
	{
		pNode->nOutTime++;
		//
		if(pNode->nOutTime == FRM_OUT_TIME)
		{
			pNode->nOutTime = 0;
			//
			if(pNode->nOutCnt == FRM_OUT_CNT)
			{
				pNode->nOutCnt = 0;
				if(pNode->frmTx.bType == FRM_TYPE_BROAD)
					pNode->bState = FRM_ST_END;
				else
					pNode->bState = FRM_ST_TOUT;
			}else
			{
				pNode->nOutCnt++;
				pNode->bSend = TRUE;
				FSetStateToRdy(pNode);
			}
			//
			FStopTimer(pStop);
		}//if
	}
}

//================================================
//init frame
//================================================
void FFrameInit(FRM_NODE* pNode)
{
	pNode->bSend = FALSE;
	pNode->bState = FRM_ST_END;
	pNode->nOutCnt = 0;
	pNode->nOutTime = 0;
	pNode->bEnRev = TRUE;
	//
	pNode->bAnaCnt = 0;
	pNode->bAnaLen = 0;
	pNode->bAnaSte = 0;
	pNode->nAnaChk = 0;
}

//================================================
//timer
//================================================
void FStartTimer(void(*pStart)(void)) {pStart();}
void FStopTimer(void(*pStop)(void)) {pStop();}






