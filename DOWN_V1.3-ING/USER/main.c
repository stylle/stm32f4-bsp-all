/**
  ******************************************************************************
  * @file    Project/STM32F103RCT6_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    05-April-2018
  * @brief   Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h" 

unsigned int  PWM = 100 ;
 
/*****************************   CANBEGIN *************************************/


uint8_t FirstSend; // 首次发送标志，也可以通过这个标志位上传全部数据

uint16_t lampAddr[250];
uint8_t lampState[250];

/*****************************   CANEND  *************************************/

/**
  * @brief   Main program.
  * @param   None
  * @retval  None
  * @version V1.3
	*
	*
  */

int main(void)
{
//	CanTxMsg TxMessage;
	InitVar();					//变量初始化
	
	SysTickInit();			//系统定时器初始化  
	
	SysTickDelayMs(2000);
	ConfigGPIO();				//GPIO配置
		
	ADCInit();					//ADC配置,严禁放在定时器后面初始化，造成PWM失控
	
	ConfigUart();				//UART配置
		
	RccClkInit();				//RCC初始化
	////////////
	InitElecLable();		//电子标签读取
	
	ReadFlashData();
	
	InitSystem();				//系统设置
	////////////
	TimerInit();				//定时器初始化	  内部有一个心跳包在TIM1中运行
	
	InitTm4Ch3ToPwmMode(0);

	//IWDG_Init(4, 60000); 	 //watch dog init(5s)
	
	LAST_ENABLE;

	
	CAN_Init_All(BOUND20KB);
	UpCanData();
	for(FirstSend = 0; FirstSend < LAMP_NUM;FirstSend++)
	{
		  lampAddr[FirstSend] = FirstSend;
			lampState[FirstSend] = 0x92;
	}
	lampState[8] = 	0x91;
	lampState[40] = 0x91;
	lampState[25] = 0xa2;
	lampState[37] = 0x40;
	lampState[34] = 0x40;
	FirstSend = 1;
			
  while(1)
  {	
		
		UpCanData();	// 更新CAN数据
		
	  CanPoll(); 		// CAN装载完成发送任务  
		
		OutDetect();	    //输出故障检测
		
		IWDG_Feed();		  //喂狗	
		
		EpsWorking();		//国标功能，年月检、应急等

//		MessageDeal();	//处理主机的消息    
				
//		EmgyDetect();		//检测应急状态
	
		EmgyChang();		//应急转换设置
			
		LedFlash(20000);//闪烁指示灯，观察用
	
		Temp(80);

//		IfrScan(); 		//可以修改地址的红外接口，定时器被占用暂时无法使用
		BellContrl(BELL_SHORT,100,30);
		
	  //间隔执行的函数100mS
		UPDATE_START	
		
		HandRead();
			
		FaultLed(16);
			
		InitWorking();		//上电工作延时
			
		//BatteryFault();
			
		UpdateEgTime();		//更新应急时间
			
//	//DischargeDeal();	//电池充放电管理
//			
		UpdateIndicate(); 	//更新蜂鸣器状态
//			
		SystemFaultDetect();	//故障检测
//			
//	PassCurenntDetect();

//	TIM_SetCompare3(TIM4, PWM); 	
//			
 		UPDATE_END
	}
}

/******************* (C) By WangJiuZhou  of 2020.2.20 ********************/



