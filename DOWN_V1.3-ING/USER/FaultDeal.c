
//======================================================
//故障处理
//======================================================

#include "stdafx.h"

//======================================================
//var
//======================================================


unsigned char LedCon = 0;
unsigned short ChrI = 0;
unsigned int  EgSum = 0;
unsigned char gbComCnt;          //通信计数1
unsigned short ComCnt;           //通信计数2
unsigned int  gbMyError;	       //故障表
unsigned char bBuzzer_Faut;      //故障蜂鸣
unsigned int  gbChrDuty;         //充电机占空比

unsigned char CHAROVER = 0;         //判断是否充满
unsigned char CHRState = 0;
unsigned char ifdefault = 0;        //通信计数
unsigned char Loadstate = 0;      //故障表
unsigned char EMGYCharEnable = 0;    //充电机允许
unsigned char OPENCharEnable = 0;    //充电机允许
unsigned char FUALTMEMORY = 0;    //充电机占空比
unsigned int  CHARMEMORY  = 0;  //充电电压暂存
unsigned char PassCurenntState = 0;//充电庄状态机变量

extern unsigned char CHAR_OF;     //关闭充电继电器
extern unsigned int  Chrontim;    //浮充时间记录
extern unsigned int  OutCurrentOpenValue;
extern unsigned int  ChrCurrentShowValue;

//======================================================
//Curennt detect
//过流检测函数
//======================================================
void PassCurenntDetect()
{
		 PassCurenntTime1 = (IDIS > 355)?(PassCurenntTime1 + 1) :(0); //过流保护值
	   if(PassCurenntTime1 >= 10) LOAD_DISABLE;											//3s后过流保护
}

//======================================================
//系统故障检测，为系统动作提供条件与参数
//======================================================
void SystemFaultDetect()
{
	
	if(gbInit != FALSE) return;                                                                                                              

//	//市电故障
//	SetFault(VMAIN < VMAIN_LL_NUM, FUP_MAIN_ERR);    //故障 
//	SetFault((VMAIN > VMAIN_LL_NUM)&&(VMAIN < VMAIN_L_NUM), FUP_MAIN_VLOW); //欠压
	
		if((VMAIN > 140)&&(VMAIN < 170))AddFaultX(FUP_MAIN_VLOW);
		if(VMAIN < 140)AddFaultX(FUP_MAIN_ERR);
		if(VMAIN <= 245)DelFaultX(FUP_MAIN_VHIGH);
		if(VMAIN >= 140)DelFaultX(FUP_MAIN_ERR);
		if(VMAIN >= 180)DelFaultX(FUP_MAIN_VLOW);

	//输出故障检测(开路)
	SetFault((VOUT1 <= VOPEN) ||(VOUT2 <= VOPEN)||(VOUT3 <= VOPEN)||\
      	   (VOUT4 <= VOPEN) ||(VOUT5 <= VOPEN)||(VOUT6 <= VOPEN)||\
		       (VOUT7 <= VOPEN) ||(VOUT8 <= VOPEN),FUP_OPUT_OPEN);

//	//输出故障检测(短路)
//	SetFault((!OUT_SHORT),FUP_OPUT_SHORT);
//
//主机通信检测，接收到主机消息后变量清零
//		gbComCnt =(gbComCnt < 200)?	(gbComCnt + 1): (200);
//		SetFault((gbComCnt >= 200), FUP_SLAVE_NACK);
//		SET_ST_FAUT(EpsState.nState,(gbMyError > 0));
}

//======================================================
//fault add&delete
//======================================================
void AddFault(unsigned char nCode)
{
	if(!IS_ERR(gbMyError,nCode))
		{	
			SysInd.bMute = 0;
		}		
//每一位表示一种故障
	  gbMyError |= (1<<nCode);
}

void DelFault(unsigned char nCode)
{
	gbMyError &= ~(1<<nCode);
}

//添加故障
void AddFaultX(unsigned char nCode)
{
	AddFault(nCode);
}
//删除故障
void DelFaultX(unsigned char nCode)
{
	DelFault(nCode);
}

//设置故障
void SetFault(unsigned char bFlag, unsigned char bCode)
{
	if(bFlag==1)
		AddFaultX(bCode);
	else
		DelFaultX(bCode);
}

void FaultLed(unsigned char LedTim)
{

		if((((gbMyError&0xffff) != 0)||(PassCurenntbFlag == 1)) && (LedCon < LedTim))
		{
			LedCon ++ ;
		if(LedCon < (LedTim / 2))
			SET_FAUL_LED;
		else
			CLR_FAUL_LED;
		}
		else
		{
			LedCon = 0;
			CLR_FAUL_LED;
		}
	
}

//======================================================
//update indicate device
//蜂鸣器状态更新
//======================================================
void UpdateIndicate()
{		
	
//	if((Myec.Beep_nState == 0xDD))//允许添加声音
//	{
		bBuzzer_Faut = IS_ST_FAUT(EpsState.nState);
		bBuzzer_Faut = ((bBuzzer_Faut)||(IS_ST_EMGY(EpsState.nState)));
		bBuzzer_Faut = bBuzzer_Faut&&(!SysInd.bMute);
//	}
//	else
//	{
//	  bBuzzer_Faut = 0;
//	}

}




