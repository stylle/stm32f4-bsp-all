
#ifndef	ADC_DEAL_H
#define	ADC_DEAL_H

//===============================================
//define
//===============================================
#define	VMAIN	     AdcAcquire.nMarket	//市电电压
//#define	VBAT	     VBAT1 + VBAT2 + VBAT3            //电池总电压
//#define	BAT_BUS_V  AdcAcquire.BAT_IN_V 
#define	VMODEL     AdcAcquire.Vmodel 

//#define	FirInPut  AdcAcquire.nVal[2]  //消防联动输入

////#define	VBAT1  AdcAcquire.nVal[0]  //电池总电1
#define 	LI_VBAT  AdcAcquire.nVal[1]  //电池电压2
////#define	VBAT3  AdcAcquire.nVal[2]  //电池电压3

#define	VOUT	 AdcAcquire.nVal[2]  //输出电压
#define	IDIS	 AdcAcquire.nVal[5]  //放电电流
#define	VCHAR  AdcAcquire.nVal[6]  //充电电压
#define ICHR	 AdcAcquire.nVal[7]  //充电电流

#define	VOUT1  AdcAcquire.nVout[0] //输出电压1
#define	VOUT2  AdcAcquire.nVout[1] //输出电压2
#define	VOUT3  AdcAcquire.nVout[2] //输出电压3
#define	VOUT4	 AdcAcquire.nVout[3] //输出电压4
#define	VOUT5	 AdcAcquire.nVout[4] //输出电压5
#define	VOUT6  AdcAcquire.nVout[5] //输出电压6
#define VOUT7	 AdcAcquire.nVout[6] //输出电压7
#define VOUT8	 AdcAcquire.nVout[7] //输出电压8

//#define	TEMP1   AdcAcquire.nTemp[0]
//#define	TEMP2   AdcAcquire.nTemp[1]
//#define	TEMP3   AdcAcquire.nTemp[2]
//#define	TEMP4   AdcAcquire.nTemp[3]
//#define	TEMP5   AdcAcquire.nTemp[4]

#define NWAIT	(AdcAcquire.bTrig==TRUE)&&(AdcAcquire.bWait == FALSE)

//===============================================
//struct
//===============================================

typedef struct
{
	
	unsigned char bWait;    //采样等待
	unsigned char bTrig;	  //采样完成
	unsigned short nMarket;	//市电电压-切换用
	unsigned short nMarket1;	//市电电压-显示用
	unsigned short Vmodel ; //AC-DC输出电压
	//unsigned short nBJ;
	unsigned short nVal[8];
	unsigned short nVout[8];
	unsigned short nTemp[5];
	unsigned short VBAT_2;	//电池总电压
	unsigned short BAT_IN_V;//电池判断	
	unsigned short Boost_V;//升压输出电压	
	
}ADC_ACQUIRE;

//===============================================
//function
//===============================================
void AdcConvert(unsigned char nIndex, unsigned short nVal);
void SetADCSwPin(unsigned char bSw);
void ADCEventDeal(void);

extern ADC_ACQUIRE AdcAcquire;	
extern unsigned int OutCurrentShowValue;
extern unsigned int	ChrCurrentShowValue;
#endif

