#include "Heat_Admin.h"
#include "stdafx.h"

//=====================================================
//MOSFun
//=====================================================
static unsigned char FunState;
unsigned char Fan_Init_OK = FALSE; 
unsigned int FunPWMPeriod1 = 0;
unsigned int FunPWMPeriod2 = 0;


 void Temp(unsigned short temp)
{
	if(Fan_Init_OK == FALSE)
	{
			MosFun(100);
		  ModelFun(100);
	}
	else
	{	           
			if((IDIS >= 100)||(ICHR >= 10)||(temp > 55))//电流大于10A或者温度高于50度全速运行
			{
				FunState = FALSE;
			}
	else 
		if((temp <= 55)&&(IDIS < 45)&&(ICHR < 8))
			{
			  FunState = TRUE;
			} 
			
		if(FunState == FALSE)
			{
				MosFun(100);
				ModelFun(100);
			}
	else
		if((temp > 35)&&(FunState == TRUE))
		{
			MosFun(((temp - 5)*2));	
			ModelFun(((temp - 10)*2));			
		}
	else 
		{
			MosFun(0);
			ModelFun(0);	
		}
	}
}

void MosFun(unsigned int nTp)
{
 
			FunPWMPeriod1 ++ ;
		//
		if(FunPWMPeriod1 <= nTp)
			MOSFUNOPEN;
		else
		if(FunPWMPeriod1 > nTp)
			MOSFUNCLOS;
		
		if(FunPWMPeriod1 >= 100) FunPWMPeriod1 = 0;
}

//=====================================================
//ModelFun
//=====================================================
void ModelFun(unsigned int nTp)
{
	
		  FunPWMPeriod2 ++ ;
		//
//		if(FunPWMPeriod2 <= nTp)
//			MODELFUNOPEN;
//		else
//		if(FunPWMPeriod2 > nTp)
//			MODELFUNCLOS;
		
		if(FunPWMPeriod2 >= 100) FunPWMPeriod2 = 0;
}


