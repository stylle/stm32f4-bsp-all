
#include "stdafx.h"

//=======================================================================
//define
//=======================================================================
#define UPDATE_START	if(gbUpdate == TRUE){
#define	UPDATE_END		gbUpdate = FALSE;}

//=======================================================================
//var
//=======================================================================
FRM_NODE MyFrmNode; 		//从机接收节点

#include <stm32f10x.h>
unsigned char gbInit;		//开机延时
unsigned char gbEFlag;	//独立设置标志
unsigned char gbUpdate; //更新标志

//=======================================================================
//全局标志位变量
//=======================================================================
extern ADC_ACQUIRE AdcAcquire;	
unsigned char bDealing;
unsigned int AC_Reset_time = 0;
unsigned char OutShortFlag = TRUE;//输出短路标志
unsigned char SpaceTimeFul = FALSE;//切换死区时间达到标志，允许打开负载Mos
unsigned char PassCurenntbFlag = FALSE;
unsigned int  Start_Up_PWM = 0;
unsigned int PassCurenntTime1 = 0;//输出过流时间变记录量 
unsigned int PassCurenntTime2 = 0;//输出过流时间变记录量 
unsigned int PassCurenntNum1 = 0;//输出过流时间变记录量 
unsigned int PassCurenntNum2 = 0;//输出过流时间变记录量 
unsigned char Start_Up_OK = TRUE;

unsigned long NTIME = 0;
unsigned int POWERCHOOS = 400;


extern ADC_ACQUIRE AdcAcquire;

//=======================================================================
//init plc
//=======================================================================
void RccClkInit()
{
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
}

//=======================================================================
//init working
//=======================================================================
void InitWorking()
{
	//gbInit在开机后一段时间内始终是非0
	//这个时间内系统某些操作暂停
	//等待硬件稳定后，才开始操作
	if(gbInit > 0)
		gbInit--;
}

//=======================================================================
//init var
//=======================================================================
void InitVar(void)
{
	FFrameInit(&MyFrmNode);
  PFrameInit(&MyPlcNode);
	//
	AdcAcquire.bTrig = 0;
	AdcAcquire.bWait = TRUE;
	//
	SysInd.bCheck = 0;
	SysInd.bEnable = 0;
	SysInd.bMute = 0;
	SysInd.bTone = 0;
	gbMyError = 0x0000;
	
	Myec.AgeTime_S = 0;
	Myec.AgeTime_M = 121;
	
	AdcAcquire.nVal[0] = 130;
	AdcAcquire.nVal[1] = 130;
	AdcAcquire.nVal[2] = 130;
	
	 AdcAcquire.nVout[0] = 0;
	 AdcAcquire.nVout[1] = 0;
	 AdcAcquire.nVout[2] = 0;
	 AdcAcquire.nVout[3] = 0;
	 AdcAcquire.nVout[4] = 0;
	 AdcAcquire.nVout[5] = 0;
	 AdcAcquire.nVout[6] = 0;
	 AdcAcquire.nVout[7] = 0;
	

	gbPrfOn = PRF_ON;//0
	gbEFlag = FALSE;
	gbChrDuty = CHR_INIT;//充电机占空比初始化
	gbInit = 20;//开机延时
	gbComCnt = 0;//通信计数0
	ComCnt= 0;
  ChanlVBuf[9] = 1;
	//
	bDealing=0;
	EpsState.bForce = FALSE;
	EpsState.bFSoft = FALSE;
	EpsState.bReset = FALSE;
	EpsState.bFirEmy = FALSE;
	EpsState.nTpMCk = MCHK_DAYS;
	EpsState.nTpYCk = YCHK_DAYS;
	EpsState.bMCheck = FALSE;
	EpsState.bYCheck = FALSE;
	EpsState.bClear = FALSE;
	EpsState.bMManu = FALSE;
	EpsState.bYManu = FALSE;
	EpsState.bMCkEnd = FALSE;
	EpsState.bAccele = FALSE;
	//
	EpsState.bCtrlSig = 0x80;
	EpsState.bSelfSig = 0x00;
	
}

//=======================================================================
//init ADC
//=======================================================================
void ADCInit()
{
	ADC1Init();	//其它采样
	ADC2Init(); //市电采样
	
}

//=======================================================================
//init dma
//=======================================================================
void DMAInit()
{
//	MYDMA1_Config(DMA1_Channel1, (uint32_t)(&ADC1->DR), (uint32_t)AdcValue, 4);
}

//=======================================================================
//init timer
//t1~t8
//=======================================================================
void TimerInit()
{
	InitTm1ToBaseMode(20000,7199);  //
	StartTimer1();
	
	InitTm2ToBaseMode(100, 7199);   //5ms-ADC
  StartTimer2();

	InitTm3ToBaseMode(10, 71); //10uS -更新用
//	StartTimer3();

	InitTm4ToBaseMode((PWM_CYCLE - 1), 0); 
	StartTimer4();
	
	InitTm5ToBaseMode(1000, 7199); //100mS-年月检
	StartTimer5();

	InitTm6ToBaseMode(QUERY_SPAN1, 7199);  //系统工作状态定时器-5ms//5mS软启动 5*1440=7.2S
	StartTimer6();
		
//	InitTm7ToBaseMode(SPCE_TIM7, 7199);  //切换时序控制定时器-10ms

	InitTm8ToBaseMode(SPCE_KEPP_TIM, 7199); //切换时序保持定时器-10ms 
}

//=======================================================================
//config uart
//=======================================================================
void ConfigUart()
{
	
	ConfigUART5();  	//主从数据串口
	SET_485A_RX;      //灯具数据使用
	
//	ConfigUART2();		//屏幕刷新串口
//	SET_485B_RX;
//
	ConfigUART1();    //数据读取串口
	SET_485C_RX;
	
	SetUartToRxMode();//灯具数据串口	
	Off_Out();	
}

//=======================================================================
//init SPI
//=======================================================================
void InitSystem()
{

	LOAD_DISABLE;
	Li_CHAR_DISABLE;
//	if(Myec.AgeTime_M <= 0)
//	{
//	if(Myec.Test_Model == 0xAA)
//	  TEST_MODEL_OPEN;    //使能通讯
//	else 
//		if(Myec.Test_Model == 0xBB)
//	  TEST_MODEL_CLOSE;   //使能放电
//	}
//	else
//		TEST_MODEL_CLOSE;


}

//=======================================================================
//init gpio
//=======================================================================
 void ConfigGPIO(void)
{
	
  GPIO_InitTypeDef GPIO_InitStructure;

		//开时钟，并映射相关GPIO
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,  ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	//GPIO_PinRemapConfig(GPIO_PartialRemap_USART3,ENABLE);
	
	PWR_BackupAccessCmd(ENABLE);  //允许修改RTC和备份寄存器
	RCC_LSEConfig(RCC_LSE_OFF);   //关闭外部低速时钟
	BKP_TamperPinCmd(DISABLE);
		
	//禁止JTAG口，使能SW口，用JTAG一些IO作为普通IO口使用
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	
	/////////////////////UART IO init/////////////////////	
	//USART_TX(U2)  灯具数据发送
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	//
	GPIO_Init(GPIOA, &GPIO_InitStructure); //
	LOAD_DISABLE;
	
  //USART_RX(U2)   灯具数据读取
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //
//	
	//CAN_TX(CAN1)  系统CAN通讯
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//
	GPIO_Init(GPIOA, &GPIO_InitStructure); //
	 
	//CAN_RX(CAN1)  系统CAN通讯
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //
	
	//CAN_TX(CAN2)  系统CAN通讯
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//
	GPIO_Init(GPIOB, &GPIO_InitStructure); //
	 
	//CAN_RX(CAN2)  系统CAN通讯
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//
	GPIO_Init(GPIOB, &GPIO_InitStructure);  //
	
	//0.1.2.3.4.5.7.
	/////////////////////ADC IO init/////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/////////////////////Flash-LED//////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

//	/////////////////////Load_MOS///////////////
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOA, &GPIO_InitStructure);
//	
	/////////////////////BOOST_MOS/////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/////////////////////风扇控制///////////////
	GPIO_InitStructure.GPIO_Pin =GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
//		
	/////////////////////开关电源输出使能/////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);	
//	
	/////////////////////Charge////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

//	PWR_BackupAccessCmd(DISABLE);  //允许修改RTC和备份寄存器
	
	/////////////////////二总线5V基准使能////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GET_DATA_CLOSE;  
	
	/////////////////////应急二总线使能（电流小）/////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/////////////////////联动检测///////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;; 
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	/////////////////////联动反馈////////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	/////////////////CNL_V_C输出通道采集选择////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14|GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	
	//////////后四路持续控制（扩展功能）/////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
////	
	////////////////////Infrared红外/////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
//	
	/////////////////////Hand///////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
//	/////////////////////TEST/////////////////////
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOB, &GPIO_InitStructure);
//	
//	/////////////////////EMGY///////////////////////
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOB, &GPIO_InitStructure);
//	
	/////////////////////BUS_DOWN总线放电////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
//	
	/////////////////////BELL/////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
		
//	/////////////////////LiChargeEN/////////////////////
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOB, &GPIO_InitStructure);
//	
		////////////AC_CHECK市电检测（正常照明检测）/////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; 
	GPIO_Init(GPIOA, &GPIO_InitStructure);
		/////////////////////指示灯/////////////////////
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

//======================================================
//MessageDeal
//======================================================
extern unsigned char TXFul;
extern unsigned char Dealing;
unsigned char TXopen = 1;
void MessageDeal(void)
{

	if(MyFrmNode.bState == FRM_ST_REV)
	{
		DoReceiveDeal();		
		FSetStateToEnd(&MyFrmNode);
	}
	if((MyQuery.bDealing)&&(MyPlcNode.bState == PLC_ST_END))DoQueryDeal();//轮询灯具数据填充函数
	if(/*(!AC_CHECK)&&*/(!OutShortFlag)&&(LampNode.bLpSt != NOM_EMGY))
		PMasterSendA(&MyPlcNode,SendFrameByUR2, StartPTimer);//灯具轮训函数，采用供电总线轮训	
	if(MyPlcNode.bRevEnd == TRUE)
	{
		MyPlcNode.bRevEnd = FALSE;
		DoPlcRevDeal();//接收灯具数据处理函数
	}
	ReciverOut();	

}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name``
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

