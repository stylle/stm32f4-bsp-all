#ifndef	A_GLOBAL_H
#define	A_GLOBAL_H

#include <stdint.h>

//#define	FRM_FORMAT_OFT 	0
//#define	FRM_SOURCE_OFT	1
//#define	FRM_DESTIN_OFT	2
//#define	FRM_DATA_OFT	3

#define	FRM_CON_LEN_OFT 0	//控制器传来数据长度
#define	FRM_CON_ACK_OFT	1	//是否回答
#define	FRM_CON_TYPE_OFT	2	//帧类型
#define	FRM_CON_MASTER_OFT	3	//主机地址
#define	FRM_CON_SLAVER_OFT	4	//从机地址
#define	FRM_HEADLEN_OFT		5	//前面这几个数据长度

typedef struct
{
//	uint32_t usMinSearchAddr;//3位地址
//	uint32_t usMaxSearchAddr;
	uint16_t usMinSearchAddr;//2位地址
	uint16_t usMaxSearchAddr;
}SearchArea_T;

//控制器发送电源结构体
typedef struct 
{
    unsigned short nHead;

    unsigned short nDataLengh;
    unsigned char nIsReply;     //是否回复
    unsigned char nFrameType;   //帧类型
    unsigned char nMaster;
    unsigned char nSlaver;

    unsigned char nCmd;     //命令
    unsigned char nEmgType; //应急类型
    unsigned char nShieldType; //屏蔽应急类型
    unsigned char nLampSet; //控制灯具状态
	
    unsigned short nOperaLampNo;    //需要控制灯具的编号
    unsigned short nWholeLampNum;   //全部灯具的数量

	unsigned char nBack;
    unsigned char nEmgPowerNum; //电源应急数量
	
		unsigned short nSerchLampSectionNum; //灯具区间数量
//		unsigned short nSerchLampSection[20]; //灯具区间
    //后接分区应急电源编号
}stControlToPow;

typedef struct
{
	unsigned char nCardId;
    unsigned char nMachineId;
    unsigned short nNodeId;
    unsigned char nPowerNum;    //电源数量
    unsigned short nLampNun;    //灯具数量
    unsigned char nDevType;     //询问设备类型
    unsigned char nIsLameTest;  //是否灯具测试
    unsigned char nCommandType; //灯具控制类型
    unsigned char nEmgPowerNum; //电源应急数量
}stPcToControl; 

//电源回复控制器数据结构体(电源)
typedef struct 
{
    unsigned short nHead;

    unsigned short nDataLengh;
    unsigned char nIsReply;     //是否回复
    unsigned char nFrameType;   //帧类型
    unsigned char nMaster;
    unsigned char nSlaver;

    unsigned char nPowerType; //类型 1:集中电源 2：配电箱
    unsigned char nPowOrLamp; //电源数据还是灯具数据 1:电源数据 2：灯具数据
    unsigned char nCityPower;   //市电
    unsigned char nCharge_V;    //充电电压(仅电源有)
    unsigned char nCharge_I;    //市电电流(仅电源有)
    unsigned char output_U;     //输出电压
    unsigned char nOutput_I;    //输出电流

    unsigned char nPowerTemperature1;    //电池温度1(仅电源有)
    unsigned char nPowerTemperature2;    //电池温度2(仅电源有)
    unsigned char nPowerTemperature3;    //电池温度3(仅电源有)

    unsigned char nPower_U1;     //电池电压1(仅电源有)
    unsigned char nPower_U2;     //电池电压2(仅电源有)
    unsigned char nPower_U3;     //电池电压3(仅电源有)

    unsigned char nChannelU0;    //通道1电压
    unsigned char nChannelU1;    //通道2电压
    unsigned char nChannelU2;    //通道3电压
    unsigned char nChannelU3;    //通道4电压
    unsigned char nChannelU4;    //通道5电压
    unsigned char nChannelU5;    //通道6电压
    unsigned char nChannelU6;    //通道7电压
    unsigned char nChannelU7;    //通道8电压
	
	unsigned char nState;        //电源状态

    unsigned char  nEmgType;    //应急类型
	unsigned char  nBack; 		//备用字段
    unsigned int   nError;      //电源故障
}stPowToControl_Pow;

//电源下发从机数据
typedef struct
{
    unsigned short nHead;

    unsigned short nDataLengh;
    unsigned char nMaster;
    unsigned char nSlaver;

    unsigned char nEmgy; //应急类型
    unsigned short nOperaLampNo; //控制灯具的编号
    unsigned char nLampSet; //灯具控制的类型
    unsigned short nWholeLampNum; //灯具的数量
	
		unsigned short nSerchLampSectionNum; //灯具区间数量
	//	unsigned short nSerchLampSection[20]; //灯具区间
	
}stPowToSlaver;

//电源回复控制器数据结构体(灯具)
typedef struct 
{
    unsigned short nHead;

    unsigned short nDataLengh;
    unsigned char nIsReply;     //是否回复
    unsigned char nFrameType;   //帧类型
    unsigned char nMaster;
    unsigned char nSlaver;

    unsigned char nPowerType; //类型 1:集中电源 2：配电箱
    unsigned char nPowOrLamp; //电源数据还是灯具数据 1：电源 2：灯具
	
	unsigned short nLampInfoLen; //灯具信息长度

    //后接灯具数据结构体
}stPowToControl_Lamp;

extern unsigned char nEmgPowerNum; 	//应急的电源
extern unsigned char EmgPowerArr[80];	//应急的电源编号
extern stControlToPow ControlToPowInfo;

#endif
