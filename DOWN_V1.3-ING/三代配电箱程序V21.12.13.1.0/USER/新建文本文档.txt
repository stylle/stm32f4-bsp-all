
#ifndef	INDICATE_H
#define	INDICATE_H

//======================================================
//define
//======================================================


#define	SET_LED GPIO_SetBits(GPIOB,GPIO_Pin_15)
#define	CLR_LED GPIO_ResetBits(GPIOB,GPIO_Pin_15)

#define KEY0 GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15)
#define KEY1 1
#define KEY2 1
#define KEY3 1
#define KEY4 1
#define KEY5 1
#define KEY6 1
#define KEY7 1
#define KEY8 1
#define KEY9 1

#define	KEY_SCAN_CYCLE	10000

//======================================================
//struct
//======================================================
typedef struct
{
	unsigned char bCheck;
	unsigned char bMute;
	unsigned char bTone;
	unsigned char bEnable;
}SYS_INDICATE;

//======================================================
//function
//======================================================
void KeyTaskDeal(void);

extern SYS_INDICATE SysInd;

#endif

