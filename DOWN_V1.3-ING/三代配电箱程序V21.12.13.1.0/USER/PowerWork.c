//======================================================
//电源处理
//======================================================

#include "stdafx.h"
//static int nCnt2 = 0; 
//static int nCnt1 = 0;
unsigned  int sPower=0;
unsigned  char gbOutDuty; //输出占空比
unsigned  char Start_Up_FUL = FALSE;
unsigned char ChangFulState = FALSE;
unsigned char ResetFulState = FALSE;
unsigned char MainToBatState = TRUE;
unsigned char ChangFlag = FALSE;
unsigned char OutLock1 = FALSE;
unsigned char OutLock2 = FALSE;
unsigned short BellnTp = 0;
unsigned char ChangSpaceTime = 0;
unsigned char FirFlag = 0x02;//消防应急标志，默认不触发0X02。
unsigned char ShortEn = 1;
unsigned char EmgySend = 1;
extern unsigned char EMGYCharEnable;
unsigned char StartupBuzz = 0;
extern unsigned int nstate;

unsigned char Boost_En = 0;
unsigned char BoostAdcEn = 1;
unsigned char AcVlotageAdcEn = 1;
unsigned int nCnt = 0;
unsigned int nSum = 0;
float AC_ADC_GET = 0;
unsigned int BoostPWM = 0;
static unsigned int BoostVal = 0;	
unsigned char nInx11 = 0;

//======================================================
////Boost_PWM调节
//======================================================
void SetBoostPwmDuty(unsigned short nDuty)
{
	TIM_SetCompare3(TIM4, nDuty); //set ch1' ccr值
}
//======================================================
////Boost_电压调节
//======================================================
void SetBoostVlotage(unsigned char Vlotage)
{
		
	  AdcAcquire.Boost_V = (uint16_t)((float)BoostVal*0.105);	
	  
		if((Boost_En == 1)&&(BAT_IDIS <= BAT_IDIS_OVR))	
		{				
			 if((AdcAcquire.Boost_V < Vlotage))
					SetBoostPwmDuty(((BoostPWM < PWM_DUTY_MAX)?(BoostPWM += 10):PWM_DUTY_MAX)); 
			 else 
				if((AdcAcquire.Boost_V > (Vlotage + 5))&&(AdcAcquire.Boost_V < (Vlotage + 15)))
					SetBoostPwmDuty(((BoostPWM > PWM_DUTY_MIN )?(BoostPWM -= 10):PWM_DUTY_MIN)); 		
				else
				{
					BoostPWM = 0;
					SetBoostPwmDuty(BoostPWM);	
				}	
			}	
			else
			{
				BoostPWM = 0;
				SetBoostPwmDuty(BoostPWM);	
			}
	
}

//======================================================
////AC主电计算
//======================================================
void GetAcVlotage(void)
{
	

	if(nCnt >= VMAIN_ADC_Num)
		{
	
			AC_ADC_GET = (float)(nSum/VMAIN_ADC_Num); //平均值
			AdcAcquire.nMarket = (uint16_t)((float)AC_ADC_GET*1.791);
			nSum = 0;
			nCnt = 0;		

		}
		
//			if((!BoostAdcEn)&&(!AcVlotageAdcEn))
//		{	
//			nInx11 = 0;
//			ADC_RegularChannelConfig(ADC2, ADC_Channel_4, 1, ADC_SampleTime_239Cycles5);//电压
//			ADC_SoftwareStartConvCmd(ADC2, ENABLE),ADC_ITConfig(ADC2, ADC_IT_EOC, ENABLE);
//		}
//			if(nCnt >= VMAIN_ADC_Num)	AcVlotageAdcEn = 1,nCnt = 0;
		
}

//======================================================
//市电电压检测
//计算方法：二元一次方程组
//======================================================
void DoMarketDeal()
{
		if(ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC)) //转换完毕
			{		
			if(nInx11 == 0)
				{	
					if(nCnt < VMAIN_ADC_Num)	
					{						
						nCnt = (nCnt < VMAIN_ADC_Num)?(nCnt+1):(nCnt);
						nSum += ADC_GetConversionValue(ADC2);
					}
//					if(nCnt >= VMAIN_ADC_Num)
//	        AcVlotageAdcEn = 0;
					nInx11 = 1;// (!BoostAdcEn)?(0):(1);
				}
				else 
					if(nInx11 == 1)
				{					
					nInx11 = 0;//(!AcVlotageAdcEn)?(1):(0);
					//BoostAdcEn = 0;
					BoostVal = ADC_GetConversionValue(ADC2);					
				}
				
 			  if(nInx11 == 0)ADC_RegularChannelConfig(ADC2, ADC_Channel_4, 1, ADC_SampleTime_239Cycles5);//电压
				else if(nInx11 == 1)ADC_RegularChannelConfig(ADC2, ADC_Channel_11, 1, ADC_SampleTime_239Cycles5);//电压
//				 if(BoostAdcEn||AcVlotageAdcEn) //ADC条件使
				  ADC_SoftwareStartConvCmd(ADC2, ENABLE);//,ADC_ITConfig(ADC2, ADC_IT_EOC, ENABLE);	
//				 else	
//					ADC_SoftwareStartConvCmd(ADC2, DISABLE),ADC_ITConfig(ADC2, ADC_IT_EOC, DISABLE);
					 
			}				

}
//======================================================
//应急时进行开关设置
//======================================================
void EmgyChang()
{ 
	if(IS_ST_EMGY(EpsState.nState)&&(!OutShortFlag)) //主电切换至应急
		{
			Boost_En = 1;
			SetTxToPPMode();
			StopTimer8();
	    SET_EMGY_LED;
			CLR_CHAR_LED;
			if((ChangSpaceTime > 0)&&(ChangSpaceTime < 60))//当未完全恢复时。再次触发应急时，需再次设置IO及变量状态
			{
				MainToBatState = FALSE;	
			}
				ChangSpaceTime = 0;
			if(MainToBatState == FALSE)
			{
				 Li_CHAR_DISABLE;
				 LampNode.bLpSt = NOM_EMGY;
			  if(VMAIN < 180) //主电在时不应急
				{					
					LOAD_DISABLE; //应急时进先关闭MOS
					DelayMs(100);	
					EMGY_BUS_ENABLE;		//等待一段时间20ms开始转换继电器至备电
					DelayMs(100);			
				}
				LOAD_ENABLE;   //等待一段时间20ms闭合MOS至备电输出
				DelayMs(100);
				EmgySend = 1;
				EMGY_BACK_OPEN;
				EmgStateKeepTim = 0;
				LampNode.bLpSt = SET_EMGY;
				//CharbError = 0;
				MainToBatState = TRUE;
				InitTm6ToBaseMode(QUERY_SPAN2, 7199); 
				StartTimer6();				
			}
		}			
	else
	if(!IS_ST_EMGY(EpsState.nState)&&(!OutShortFlag))//应急切换至主电
		{ 
			SetTxToPPMode();
			StartTimer8();
			ChangSpaceTime = (ChangSpaceTime == 0)?(1):(ChangSpaceTime);//手动应急和自动应急重置条件
			if(MainToBatState == TRUE)
			{
					LOAD_DISABLE; //应急时进先关闭MOS	
					LampNode.bLpSt = NOM_EMGY;
				if(ChangSpaceTime >= 20)
				{
					StopTimer8();	
					EMGY_BUS_DISABLE;  //等待一段时间20ms开始转换继电器至主电
					DelayMs(100);			
					MainToBatState = FALSE;
					LOAD_ENABLE; //等待一段时间20ms闭合MOS至备电输出
					Boost_En = 0;
					DelayMs(100);
					EmgySend = 1;
					EmgStateKeepTim = 0;
			  	LampNode.bLpSt = CLR_EMGY;
					Li_CHAR_ENABLE;
					EMGY_BACK_CLOS;				
				  SET_MAIN_LED;
					CLR_EMGY_LED;
					SET_CHAR_LED;
					InitTm6ToBaseMode(QUERY_SPAN1, 7199); 
					StartTimer6();
				}
			}
		}
}

//======================================================
//上电时对输出短路检测
//======================================================
void OutDetect()
{
	if(((VOUT1 <= VSHORT) && (VOUT2 <= VSHORT)&&(VOUT3 <= VSHORT) && (VOUT4 <= VSHORT) &&(!IS_ST_EMGY(EpsState.nState))&& \
	  	(VOUT5 <= VSHORT) && (VOUT6 <= VSHORT)&&(VOUT7 <= VSHORT) && (VOUT8 <= VSHORT)))
	{
			StopTimer8();	
			Off_Out();
			ChangSpaceTime = 0;
	    OutShortFlag = TRUE;
//		MainToBatState = TRUE;//后加允许恢复现场会偶尔进不应急函数 64行 导致灯具闪烁
	}
	else
			OutShortFlag = FALSE;	 

} 


//======================================================
//应急检测
//======================================================
void EmgyDetect()
{
	static unsigned char bMarkErr,bMainErr;//必需为静态变量，否则状态无法保持，导致跳动，不能起到滞回效果
	
	if(gbInit != FALSE)		return;
	////////////////////自动应急-主电断电////////////////////
//	if(VMAIN > 190)
//	{	
//		bMarkErr = FALSE;
//		SET_MAIN_LED;		
//	}else
//	if(VMAIN < 180)  		//市电电压检测
//	{
//		CLR_MAIN_LED;
////		LOAD_DISABLE;  		//应急时进先关闭输出
////		EMGY_BUS_ENABLE;	//开始转换至备电模式进行通讯
//		bMarkErr = TRUE;
// 	}

//======================================================
//应急状态设置
//======================================================	
		//自动应急+消防联动检测
	if((bMarkErr||(!FirInPut)))
		EpsState.bSelfSig |= (1<<AUTO_EMGY);
	else
		EpsState.bSelfSig &= ~(1<<AUTO_EMGY);
	//
	SET_ST_MAIN(EpsState.nState,!(bMarkErr||bMainErr)); //主电状态
	
		//强制应急检测
	if((EpsState.bFSoft||(FORCE_IN == 0)))
		EpsState.bSelfSig |= (1<<FORC_EMGY);
	else
		EpsState.bSelfSig &= ~(1<<FORC_EMGY);
		
	//复位时关闭一些操作	(系统复位)	
	if(EpsState.bReset)
	{
			EpsState.bReset = FALSE; //关闭复位
			EpsState.bSelfSig &= ~(1<<AUTO_EMGY); //自动应急
			EpsState.bSelfSig &= ~(1<<MANU_EMGY); //手动应急
//			EpsState.bSelfSig &= ~(1<<MMCK_EMGY);	//手动月检
//			EpsState.bSelfSig &= ~(1<<MYCK_EMGY);	//手动年检
		
			EpsState.bCtrlSig &= ~(1<<AUTO_EMGY);
			EpsState.bCtrlSig &= ~(1<<MANU_EMGY);
			EpsState.bCtrlSig &= ~(1<<FORC_EMGY);
			EpsState.bCtrlSig &= ~(1<<MMCK_EMGY);	//控制器控制月检
			EpsState.bCtrlSig &= ~(1<<MYCK_EMGY);	//控制器控制季/年检
			EpsState.bFSoft = FALSE;
	}
	
	
//#define	AUTO_EMGY		0x00		//自动应急
//#define	MANU_EMGY		0x01		//手动应急
//#define	FORC_EMGY		0x02		//强制应急
//#define	EMGY_RESET	0x03		//手动月检应急
//#define	MYCK_EMGY   0x04		//手动年检应急
//#define	EMGY_RESET	0x05		//应急复位
//#define	EMGY_IDLE		0x06		//设备空闲10011001
	////////////////////使能设置///////////////

	//应急可能是电源单独应急或由控制器控制进入应急
	if(LI_VBAT < VBAT_ON_HNUM)
	{	
		EpsState.bAtEmy =  0;
		EpsState.bMuEmy =  0;
		EpsState.bForce =  0;
		EpsState.bMCheck = 0;
		EpsState.bYCheck = 0;
		EpsState.bMManu =  0;
		EpsState.bYManu =  0;
		EpsState.bReset =  1;
	}
	else 
	{	
		EpsState.bAtEmy  = (EpsState.bSelfSig&0x01)||(EpsState.bCtrlSig&0x01);
		EpsState.bMuEmy  = (EpsState.bCtrlSig&0x02)||(EpsState.bSelfSig&0x02);
		EpsState.bForce  = (EpsState.bCtrlSig&0x04);
//		EpsState.bMCheck = (EpsState.bCtrlSig&0x08);
//		EpsState.bYCheck = (EpsState.bCtrlSig&0x10);
		EpsState.bMManu = (EpsState.bCtrlSig&0x08);
		EpsState.bYManu = (EpsState.bCtrlSig&0x10);
		
	}
}

//======================================================
//电池放电管理
//非强制应急下，如果电池过放电或者过电流，则终止工作
//======================================================
void DischargeDeal(unsigned short Bat_type, unsigned char Bat_idis)
{
	static unsigned char nCnt = 0;
	if(IS_ST_EMGY(EpsState.nState)&&(LI_VBAT < Bat_type)&&(!EpsState.bForce))
	{
		nCnt ++;
		if(nCnt > ((BAT_IDIS > Bat_idis)?(2):(20))) Off_Systm();//电池欠压或者过流关闭输出（还需优化主电在线时）
	}
	else nCnt = 0;
	//强制应急如下
}


//======================================================
//应急时间更新
//======================================================
void UpdateEgTime()
{
	static char nTp = 0;
	//
	nTp++;
	//
	if(nTp == (1000/UPDATE_SPAN))
	{
		nTp = 0;
		//
		if(IS_ST_EMGY(EpsState.nState))
			{
				EpsState.nEgTm++;		
				EpsState.hEgTm++;	
			} 
		else
			{
				EpsState.nEgTm = 0; 			
				EpsState.hEgTm=0;
			}
  }
}

//======================================================
//蜂鸣器控制
//======================================================

void BellContrl(unsigned char tim1 ,unsigned short tim2,unsigned char tim3)
{
	   extern unsigned char bBuzzer_Emgy;
	   extern unsigned char bBuzzer_Faut;

	if(!OutShortFlag)
	{
	 if((bBuzzer_Faut == 0x01)||(PassCurenntbFlag == 1))
		{
				if(StartupBuzz == 0)
				{
					 StartupBuzz = 1;
					 BellnTp = 0;
				}							 
		if(BellnTp <= tim1)
		  	BELL_ON;	
			else 
				if((BellnTp > tim1)&&(BellnTp < tim2))			 
        BELL_OF;
			else
				if(BellnTp >= tim2)
				BellnTp = 0;		
			}
		else
		{
			StartupBuzz = 0;
			BellnTp = 0;
			BELL_OF;
		}
	}
	else
	{
		if(BellnTp <= (tim3>>3))
			TickBell(2);
		else 
			if(BellnTp >= tim3)
				BellnTp = 0;		
	}
}

//=======================================================================
//提示滴答定时器蜂鸣器
//=======================================================================

void TickBell(unsigned char tim1)
{

		BELL_ON;
		DelayMs(100);
		BELL_OF;
		DelayMs(100);
		IWDG_Feed();		//喂狗

}


