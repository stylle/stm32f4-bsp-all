
#ifndef	SERIAL_SCREEN_H
#define	SERIAL_SCREEN_H

#include "FrameDeal.h"

//======================================================
//function
//======================================================
void DoScreenDeal(void);
void UpdateIndicate(void);
void DoScrAckSend(unsigned char nLen);
void SendFrameByUR4(unsigned char*pBuf, unsigned char bLen);
void SetScrAckData(unsigned char i, unsigned char bByte);
void QueryInTurns(void);

#endif


