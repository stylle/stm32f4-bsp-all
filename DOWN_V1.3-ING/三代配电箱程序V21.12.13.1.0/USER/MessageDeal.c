#include "stdafx.h"
//======================================================
//var
//======================================================
unsigned char gbPrfOn;	//打印标志，调试用
extern unsigned char TXFul;
unsigned char EmgStateKeepTim = 0;
//extern	unsigned char  OutZero;
//extern  unsigned char  ChrZero;
//======================================================
//事件处理
//======================================================

void DoEventDeal(unsigned char bEvent, unsigned char bSub)
{
	if(gbPrfOn) printf("power event: %x\r\n", bEvent);
	//
	if(bEvent == EVENT_MUTE) SysInd.bMute = TRUE; //消音
	else if(bEvent == EVENT_MANUAL) EpsState.bSelfSig |= (1<<MANU_EMGY); //手动应急
	else if(bEvent == EVENT_RESET) EpsState.bReset = TRUE;	//复位
	else if(bEvent == EVENT_MCHK) EpsState.bSelfSig |= (1<<MMCK_EMGY);	//月检
	//else if(bEvent == EVENT_YCHK) EpsState.bSelfSig |= (1<<MYCK_EMGY);	//年检
	else if(bEvent == EVENT_FEMGY) EpsState.bFSoft = bSub; //强制启动开关
}
//======================================================
//电源事件处理(调试用)
//======================================================
void DoPowEvent()
{
//	unsigned char bSub = 0;
//	unsigned char bEvent = 0;
//	//
//	bEvent = MyFrmNode.frmRx.bDtBuf[1];
//	bSub = MyFrmNode.frmRx.bDtBuf[2];
//	//
//	DoEventDeal(bEvent,bSub);
//	//
//	SetAckCode(ACD_SUCCESS, 2);
//	SetAckData(1, ACD_EMPTY);
//	DoAckSend();
}

//======================================================
//接收主机消息并处理
//======================================================
void DoReceiveDeal()
{
	unsigned short bCmd = ControlToPowInfo.nCmd; //暂时未用
	
	///////////////////协议处理///////////////////
	if(MyFrmNode.frmRx.bType == FRM_TYPE_BROAD)
	{
		if(bCmd == CMD_QUERY_LABLE) ReadLable(); //读电子标签
		else if(bCmd == CMD_WRITE_LABLE) WriteLable();	//写电子标签
		else if(bCmd == CMD_SET_STAE) SetDevState(FALSE,TRUE);	//状态设置
		else if(bCmd == CMD_PRF_SYS) PrinfSysDat();	//系统参数打印
	}
	else if(MyFrmNode.frmRx.bType == FRM_TYPE_DATA)
	{
		SetDevState(FALSE,FALSE);
		if(MyFrmNode.frmRx.nSlaver == MyLable.nAddress)
		{
			if(bCmd == CMD_QUERY_POWER)QueryPower(); //查询电源参数
			else if(bCmd == CMD_POW_EVENT)DoPowEvent(); //处理电源事件
			else
			{
				if(gbPrfOn) printf("command error!\r\n\r\n");
			}
		}
	}//else
	
	///////////////////通信恢复///////////////////
	gbComCnt = 0;
	ComCnt = 0;
	DelFaultX(FUP_SLAVE_NACK); //通讯恢复，删除故障

}

//======================================================
//向主机(控制器)回传查询的参数
//======================================================
#define	DBUF	MyFrmNode.frmTx.bDtBuf

unsigned char Index=0;
extern unsigned short LampAddrBufLen ;

extern unsigned char LampAddrBuf[LAMP_ADDR_BUF_LEN];//地址数组
extern unsigned short Lampnum ; //灯具控制的具体灯具编号

extern unsigned short MinLampAddr ; //灯具控制的具体灯具编号
extern unsigned short MaxLampAddr ; //灯具控制的具体灯具编号

//extern unsigned short LampSet ; //控制灯具设置状态
extern unsigned short DengJu ;
void QueryPower()
{
	stPowToControl_Pow  ToConForPowInfo; //回传给控制器关于电源的数据
	stPowToControl_Lamp ToConForLampInfo; //回传给控制器关于灯具的数据
	
	memset(&ToConForPowInfo  , 0 , sizeof(ToConForPowInfo));
	memset(&ToConForLampInfo  , 0 , sizeof(ToConForLampInfo));
	
	LampSet = ControlToPowInfo.nLampSet; 	//状态设置 左亮 右亮 熄灭
	Lampnum = ControlToPowInfo.nOperaLampNo; //灯具编号
	DengJu 	= ControlToPowInfo.nWholeLampNum; //是否进行控制操作 
	
	if(Index==0)
	{
		ToConForPowInfo.nHead = 0x9C3B;
		ToConForPowInfo.nDataLengh = sizeof(stPowToControl_Pow);
		ToConForPowInfo.nIsReply = FALSE;
		ToConForPowInfo.nFrameType = FRM_TYPE_ACK; //其实无用
		ToConForPowInfo.nMaster = MyFrmNode.frmRx.nMaster;
		ToConForPowInfo.nSlaver = MyLable.nAddress;
		
		ToConForPowInfo.nPowerType = 2; //集中电源
		ToConForPowInfo.nPowOrLamp = 1; //电源数据
		ToConForPowInfo.nCityPower = AdcAcquire.nMarket1; //市电
		ToConForPowInfo.nCharge_V = 0; //充电电压

		ToConForPowInfo.nCharge_I = 0; //充电电流
		
		ToConForPowInfo.output_U = (VOUT/2); //输出电压
		ToConForPowInfo.nOutput_I = (IDIS/2);//输出电流
		
		ToConForPowInfo.nPowerTemperature1 = 0;//电池1温度
		ToConForPowInfo.nPowerTemperature2 = 0;//电池2温度
		ToConForPowInfo.nPowerTemperature3 = 0;//电池3温度	
		
		ToConForPowInfo.nChannelU0	=	ChanlVBuf[0];
		ToConForPowInfo.nChannelU1	=	ChanlVBuf[1];
		ToConForPowInfo.nChannelU2	=	ChanlVBuf[2];
		ToConForPowInfo.nChannelU3	=	ChanlVBuf[3];
		ToConForPowInfo.nChannelU4	=	ChanlVBuf[4];
		ToConForPowInfo.nChannelU5	=	ChanlVBuf[5];
		ToConForPowInfo.nChannelU6	=	ChanlVBuf[6];
		ToConForPowInfo.nChannelU7	=	ChanlVBuf[7];
		
		ToConForPowInfo.nState		= EpsState.nState;        //电源状态

		ToConForPowInfo.nError		= gbMyError;      //电源故障
		ToConForPowInfo.nEmgType	= EpsState.nType;    //应急类型			
		
		MyFrmNode.frmTx.bLen = sizeof(stPowToControl_Pow);
		MyFrmNode.frmTx.bType = FRM_TYPE_ACK; //暂时未用
		MyFrmNode.frmTx.nMaster = MyFrmNode.frmRx.nMaster;
		MyFrmNode.frmTx.nSlaver = MyLable.nAddress;		
		//
		memset(MyFrmNode.frmTx.bDtBuf, 0, DATA_BUF_LEN);
		memcpy(MyFrmNode.frmTx.bDtBuf , &ToConForPowInfo , sizeof(stPowToControl_Pow)); 
		
		CONFFillTxBufA(&MyFrmNode);	
		FSlaverSendB(&MyFrmNode, SendFrameByUR1);
		
		Index = 1;	 
	}
	else if(Index == 1)//灯具数据
	{
		ToConForLampInfo.nHead = 0x9C3B;
		ToConForLampInfo.nDataLengh = sizeof(stPowToControl_Lamp) + LampAddrBufLen;
		ToConForLampInfo.nIsReply = 0; //暂时未用
		ToConForLampInfo.nFrameType = 0; //暂时未用
		ToConForLampInfo.nMaster = MyFrmNode.frmRx.nMaster;
		ToConForLampInfo.nSlaver = MyLable.nAddress;
		
		ToConForLampInfo.nPowerType = 2;
		ToConForLampInfo.nPowOrLamp = 2;
		
		ToConForLampInfo.nLampInfoLen = LampAddrBufLen;
		
		memset(MyFrmNode.frmTx.bDtBuf, 0, DATA_BUF_LEN);
		memcpy(MyFrmNode.frmTx.bDtBuf , &ToConForLampInfo , sizeof(stPowToControl_Lamp));
		
		if(LampAddrBufLen > 0)
		{
			memcpy(MyFrmNode.frmTx.bDtBuf + sizeof(stPowToControl_Lamp) ,
			LampAddrBuf , LampAddrBufLen);
		}
		
		MyFrmNode.frmTx.bLen = ToConForLampInfo.nDataLengh;
		MyFrmNode.frmTx.nSlaver = MyLable.nAddress;
		MyFrmNode.frmTx.nMaster = MyFrmNode.frmRx.nMaster;

		CONLFFillTxBuf(&MyFrmNode);
		//使用串口发送数据
		FSlaverSendC(&MyFrmNode, SendFrameByUR1);
		Index = 0;
	}
}
	
//======================================================
//控制器控制电源进入应急状态(只接收广播帧)
//======================================================
void SetDevState(unsigned char bFlag, unsigned char bNotice)
{
	unsigned char bType = 0;
	unsigned char bMask =  1;
	
	unsigned char nisPowerEmg = 0;
	unsigned char nPowerEmgIndex = 0;

	bType = ControlToPowInfo.nEmgType; //类型
	bMask = ControlToPowInfo.nShieldType; //是否屏蔽nShieldType
	
	if(!bMask)
	{
		//是否空闲
		if(bType == EMGY_IDLE)	
		{
			//判断是否有区域应急
			if(ControlToPowInfo.nEmgPowerNum != 0)
			{
				for(nPowerEmgIndex = 0 ; nPowerEmgIndex < ControlToPowInfo.nEmgPowerNum ; nPowerEmgIndex++)
				{
					if(MyLable.nAddress == EmgPowerArr[nPowerEmgIndex]) nisPowerEmg = 1;//查看电源是否在应急启动范围内
				}
			}
			if(nisPowerEmg == 1)EpsState.bCtrlSig = (1<<AUTO_EMGY);
			else EpsState.bCtrlSig = 0x80;			
		}
		//如果应急，就直接应急
		else
		{
			nisPowerEmg = 0;
			EpsState.bCtrlSig = (1<<bType);
		}
	}	
}	

//======================================================
//printf system data
//======================================================
void PrinfSysDat()
{
	//printf("-----printf-----\r\n");
//	printf("State:%x\r\n", EpsState.nState);
//	printf("Market:%d V\r\n", AdcAcquire.nMarket1);
//	printf("ChargeI: %d\r\n", ICHR);
//	printf("OutI: %d\r\n", IDIS);
//	printf("Duty: %d\r\n", gbChrDuty);
//	printf("main: %d\r\n", VMAIN);
//	printf("bat1: %d\r\n", VBAT1);
//	printf("bat2: %d\r\n", VBAT2);
//	printf("tem1: %d\r\n", AdcAcquire.nVal[5]);
//	printf("tem2: %d\r\n", AdcAcquire.nVal[6]);
//	printf("Vout: %d\r\n", VOUT);
//	printf("error: %x\r\n", gbMyError);
//	printf("duty(out): %d\r\n\r\n", gbOutDuty);
//  printf("EMGtime): %d\r\n\r\n",EpsState.nEgTm);
//	printf("OP07OUT: %d\r\n\r\n",OutZero);
//	printf("OP07CHR: %d\r\n\r\n",ChrZero);

}

//======================================================
//对主机进行应答
//======================================================

void SetAckCode(unsigned char nCode, unsigned char bLen)
{
	MyFrmNode.frmTx.bLen = bLen;
	MyFrmNode.frmTx.bType = FRM_TYPE_ACK;
	MyFrmNode.frmTx.nSlaver = MyFrmNode.frmRx.nMaster;
	MyFrmNode.frmTx.nMaster = MyLable.nAddress+ASIG_AOFT;
	MyFrmNode.frmTx.bDtBuf[0] = nCode;
}

void SetAckData(unsigned char i, unsigned char bByte)
{
	MyFrmNode.frmTx.bDtBuf[i] = bByte;
}





