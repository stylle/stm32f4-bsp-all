
#ifndef	STANDARD_H
#define	STANDARD_H

//===============================================
//define
//===============================================
#define	MCHK_DAYS				30
#define	YCHK_DAYS				360
#define	MCHK_ETIME			20  //月检时间GB17945-2010为30S-180SGB17945-2022为180S-300S

//===============================================
//struct
//===============================================
typedef struct
{
	unsigned char bAtEmy;		//自动应急
	unsigned char bAtCtrlEmy;//控制器自动应急
	unsigned char bMuEmy;		//手动应急
	unsigned char bForce;		//强制启动

	unsigned char bReset;		//系统复位
	unsigned char bMCheck;	//自动月检
	unsigned char bYCheck;	//自动年检
	unsigned char bMManu;		//手动月检
	unsigned char bYManu;		//手动年检	
  unsigned char bTsEmy;		//手动应急
	
  unsigned char bFirEmy;	//区域应急
	//
	unsigned char nType;	//应急类型
	unsigned char nState;	//系统状态
	unsigned char bFSoft;	//软件强启标志
	unsigned int nEgTm;		//己应急时间
	unsigned int hEgTm;		//己应急时间
	
	//
	unsigned short nTpMCk; //month check(day)
	unsigned short nTpYCk; //year check(day)
	unsigned char bClear;	 //clear flag
	unsigned char bMCkEnd; //check end
	unsigned char bAccele; //add speed
	//
	unsigned char bCtrlSig; //控制器端使能信号
	unsigned char bSelfSig;	//自身设备使能信号
}EPS_STATE;
typedef struct
{
	unsigned char bDealing;
	unsigned short nState;
}QUERY_SITE;


//===============================================
//function
//===============================================
void EpsWorking(void);
void Beep(unsigned char Num,unsigned char Ms,unsigned char Scale);
void SysCheckCount(unsigned char nMinute,unsigned char bAccle);
void HandRead(void);
void Off_Systm(void);
void Off_Out(void);
void Open_Out(void);

extern EPS_STATE EpsState;
extern QUERY_SITE MyQuery;
extern unsigned char ShortEn;

#endif

