
#include "stdafx.h"
#include "SerialScreen.h"
#include "string.h"

//======================================================
//var
//======================================================
FRM_NODE	ScrFrmNode;		//通讯节点
FRM_NODE FPFrmNode;

FRM_NODE  Myfenpei;
//======================================================
//deal
//======================================================
unsigned char bBuzzer_Faut; //故障蜂鸣
unsigned char bBuzzer_Emgy; //应急启动蜂鸣
unsigned char Dealing = 0;

//发送缓冲区和接收缓冲区
#define	STBUF			ScrFrmNode.frmTx.bDtBuf
#define	SRDAT(n)	ScrFrmNode.frmRx.bDtBuf[n]

void DoScrQuery0()
{
	unsigned short nError;
	unsigned short nVal[3] = {0};
	static unsigned char nInx = 0;
	//参数
	if(nInx == 0)
	{
		nVal[0] = AdcAcquire.nMarket; //市电
	//	nVal[1] = ChrCurrentShowValue;//充电电流
	//	nVal[2] = AdcAcquire.nVal[3]; //电池温度1
	}else
	if(nInx == 1)
	{
		nVal[0] = VOUT;	//输出电压   
		nVal[1] = OutCurrentShowValue;	//输出电流
		//nVal[2] = VBAT;	//电池电压 替换温度电池温度2
	}else 
	if(nInx == 2)
	{
 
//		nVal[0] = VBAT1;	//电池电压1
//		nVal[1] = VBAT2;	//电池电压2
//		nVal[2] = VBAT3;  //电池电压3
	}	
	else //增加回路电压参数
	if(nInx == 3)
	{
 
		nVal[0] = VOUT1;	//输出电压1
		nVal[1] = VOUT2;	//输出电压2
		nVal[2] = VOUT3;  //输出电压3
	}else 
	if(nInx == 4)
	{	
 
		nVal[0] = VOUT4;	//输出电压4
		nVal[1] = VOUT5;	//输出电压5
		nVal[2] = VOUT6;  //输出电压6
	}else 
	if(nInx == 5)
	{
 
		nVal[0] = VOUT7;	//输出电压7
		nVal[1] = VOUT8;	//输出电压8
	}
	else 
	if(nInx == 6)
	{
 
	//	nVal[0] = TEMP2;	//电池1温度
		//nVal[1] = TEMP4;	//输池2温度
		//nVal[2] = TEMP5;	//输池3温度
	}
	

	//故障表
	if(nInx%6==0)//原来是2
		nError = gbMyError&0xffff;
	else
		nError = (gbMyError>>16);
	
	memset(STBUF, 0, 8);
	//
	BitSetting(STBUF, 8, 0, 2, 0x00);	//查询索引
	BitSetting(STBUF, 8, 3, 6, EpsState.nState); //状态4bit
	BitSetting(STBUF, 8, 7,  16, nVal[0]);	//参数1
	BitSetting(STBUF, 8, 17, 26, nVal[1]);	//参数2
	BitSetting(STBUF, 8, 27, 36, nVal[2]);	//参数3
	
	BitSetting(STBUF, 8, 37, 43, nInx);	//采样点
	BitSetting(STBUF, 8, 44, 59, nError);	//故障码
	BitSetting(STBUF, 8, 60, 60, bBuzzer_Faut); //消音
	BitSetting(STBUF, 8, 61, 61, SysInd.bTone); //音率
	//
	DoScrAckSend(8); //回传
	//
	nInx = (nInx>=6)?(0):(nInx+1);//参数传入次数原来是2次
}

//参数回传函数1
void DoScrQuery1()
{
	memset(STBUF, 0, 8);
	//
	BitSetting(STBUF, 8, 0, 2, 0x01);	//查询索引
	BitSetting(STBUF, 8, 3, 7, EpsState.nType); 	//保留
	BitSetting(STBUF, 8, 8, 16, EpsState.nTpYCk);	//年检倒计时
	BitSetting(STBUF, 8, 17, 23, EpsState.nTpMCk);//月检倒计时
	BitSetting(STBUF, 8, 24, 39, EpsState.nEgTm); //己应急时间-S
	//
	DoScrAckSend(8); //回传
}

//加速年月检
void ModifyCheckBase()
{
	if((SRDAT(1) != 0))
		EpsState.bAccele = TRUE;
	else
		EpsState.bAccele = FALSE;
	//
	SetScrAckData(0,0xff);
	DoScrAckSend(1);
}

//电源事件处理
void SrcEventDeal()
{
	unsigned char bSub = 0;
	unsigned char bEvent = 0;
	//
	bEvent = SRDAT(1);
	bSub = SRDAT(2);
	//
	DoEventDeal(bEvent,bSub);
	//
	SetScrAckData(0,0xff);
	DoScrAckSend(1);
}

//======================================================
//frame deal
//处理来自显示屏的消息
//======================================================

void DoScreenDeal()
{
	unsigned char bCmd = 0;
	static unsigned char bQuery = 0;
	//
	bCmd = SRDAT(0); //命令
	//
	if(bCmd == CMD_QUERY_POWER) //查询电源参数
	{
		if(bQuery == 0) DoScrQuery0(); //查询1
		else if(bQuery == 1) DoScrQuery1();	//查询2
		//
		bQuery = (bQuery>=1)?(0):(bQuery+1); //依次交替
	}else
	if(bCmd == CMD_POW_EVENT) SrcEventDeal();	//电源事件
	else if(bCmd == CMD_TIME_BASE) ModifyCheckBase(); //加速年月检
	else
	{
		//其它命令则应答即可
		SetScrAckData(0,0xff);
		DoScrAckSend(1);
	}
}

//======================================================
//设置应答数据并进行应答
//======================================================
void SetScrAckData(unsigned char i, unsigned char bByte)
{
	ScrFrmNode.frmTx.bDtBuf[i] = bByte;
}

void DoScrAckSend(unsigned char nLen)
{
	ScrFrmNode.frmTx.bAck = FALSE;
	ScrFrmNode.frmTx.bLen = nLen;
	ScrFrmNode.frmTx.bType = FRM_TYPE_ACK;
	ScrFrmNode.frmTx.nMaster = MyLable.nAddress;
	ScrFrmNode.frmTx.nSlaver = ScrFrmNode.frmRx.nMaster;
	//
	FFillTxBuf(&ScrFrmNode);
	FSlaverSendA(&ScrFrmNode, SendFrameByUR4);
}

//======================================================
//send frame
//串口发送函数
//======================================================
void SendFrameByUR4(unsigned char*pBuf, unsigned char bLen)
{
	SET_485B_TX;
	//
	UartSendBuffer(UART4, pBuf, bLen);
	//
	SET_485B_RX;
}

//======================================================
//update indicate device
//蜂鸣器状态更新
//======================================================
void UpdateIndicate()
{		
	
	if((Myec.Beep_nState == 0xDD))//允许添加声音
	{
		bBuzzer_Faut = IS_ST_FAUT(EpsState.nState);
		bBuzzer_Faut = ((bBuzzer_Faut)||(IS_ST_EMGY(EpsState.nState)));
		bBuzzer_Faut = bBuzzer_Faut&&(!SysInd.bMute);
	}
	else
	{
	  bBuzzer_Faut = 0;
	}

}

