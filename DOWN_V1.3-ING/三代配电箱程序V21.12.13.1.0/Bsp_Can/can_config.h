#ifndef __M_TYPE_H
#define __M_TYPE_H

#include <stm32f10x.h>
#include "stdafx.h"
#include "stm32F10x_ph_ext.h"
#include "string.h"
#include "can_app.h"
#include "can_drive.h"

// 本机地址
#define CAN_ADDR   202 // MyLable.nAddress

// 宏运算
#define MERGE_HL(H,L) 				((unsigned int)H<<8)+((unsigned int)L&0XFF)
#define GETINT16_HIGH(BYTE) 	((unsigned char)(BYTE>>8))
#define GETINT16_LOW(BYTE) 		((unsigned char)BYTE&0x00FF)
#define GETINT32_8(BYTE,n) 		(unsigned char)((BYTE&(0xff<<((4-n)*8)))>>((4-n)*8))

// 数据变化百分比范围
#define VC_PER 5
#define BATTERY_PER 25 // 4V为基准改变1V才做数据上传

// 灯具数量
#define LAMP_NUM 40

// 应急时间发送间隔
#define HEGTM_SEND_TIME_S 60

//心跳时间间隔基数
#define HEARTBEAT_DELAY 	1

#define CAN1_ENABLE 			1 	// CAN2开启标志位	
#define CAN2_ENABLE 			1 	// CAN2开启标志位	

#define HEARTBEAT_EN 			1 	// 心跳使能

#define REVC_SIGNAL 			1		// 电源等其他接受确认信号使能
#define REVC_LAMP_SIGNAL 	    1		// 灯具数据发送确认信号使能

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

#endif


