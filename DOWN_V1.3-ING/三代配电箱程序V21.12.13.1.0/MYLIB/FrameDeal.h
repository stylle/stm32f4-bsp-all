
#ifndef	FRAME_DEAL_H
#define	FRAME_DEAL_H

#ifndef TRUE
#define	TRUE 1
#endif

#ifndef FALSE
#define	FALSE 0
#endif

#define	FRM_TYPE_BROAD	0x01
#define	FRM_TYPE_DATA		0x02
#define	FRM_TYPE_ACK		0x03
#define	FRM_TYPE_NACK		0x04

#define	FRM_FORMAT_OFT 	0
#define	FRM_SOURCE_OFT	1
#define	FRM_DESTIN_OFT	2
#define	FRM_DATA_OFT	3

#define	FRM_HEAD	0x9B3A

#define	FRM_ST_READY	0x01
#define	FRM_ST_SEND		0x02
#define	FRM_ST_TOUT		0x03
#define	FRM_ST_REV		0X04
#define	FRM_ST_END		0x05

#define	FRM_OUT_CNT		1
#define	FRM_OUT_TIME	100  //10ms
#define	FRM_DATA_LEN	8  //MAX
#define FRM_ID_LEN		1  //ID
#define FRM_MAX_LEN		(FRM_DATA_LEN+FRM_ID_LEN+FRM_ID_LEN+5)
//灯具地址+状态数组长度
#define LAMP_ADDR_BUF_LEN		750
//回传给控制器缓存长度
#define DATA_BUF_LEN		1000

typedef struct
{
	unsigned char 	bType;
	unsigned short 	bLen;
	unsigned char 	bAck;
	unsigned short 	nMaster;
	unsigned short 	nSlaver;
	unsigned char 	bDtBuf[DATA_BUF_LEN];
}FRM_ELEM;

typedef struct
{
	unsigned char yanshi;
	
	unsigned char bState;
	unsigned char bSend;
	unsigned char bEnRev;
	unsigned short nOutTime;
	unsigned short nOutCnt;
	unsigned char bTxBuf[100];
	//
	unsigned short LEN; //接收数据总的长度
	unsigned short bAnaLen; //
	unsigned short aAnaLen;	//
	unsigned short bAnaCnt;
	unsigned char bAnaSte; //当前接收到的数据的长度
	unsigned short nAnaChk;//接收数据算出的
//	unsigned char strAnaL[2100]; //接收数据缓冲区
	unsigned char strAnaL[DATA_BUF_LEN]; //接收数据缓冲区
	//
	FRM_ELEM frmTx;
	FRM_ELEM frmRx;
	//
}FRM_NODE;


unsigned char FSlaverSendA(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen));
unsigned char FMasterSendA(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen));

unsigned char FSlaverSendB(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned int bLen));
unsigned char FSlaverSendC(FRM_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned int bLen));

void FSlaverReceiveA(FRM_NODE* pNode, unsigned char bByte);
void FMasterReceiveA(FRM_NODE* pNode, unsigned char bByte);

void FSetStateToRdy(FRM_NODE* pNode);
void FSetStateToEnd(FRM_NODE* pNode);

void FTimeOutDetect(FRM_NODE* pNode, void(*pStop)(void));

void FFrameInit(FRM_NODE* pNode);

void FFillTxBuf(FRM_NODE* pNode);

void FStartTimer(void(*pStart)(void));
void FStopTimer(void(*pStop)(void));
void FenpeiFFillTxBuf(FRM_NODE* pNode);
void CONFFillTxBufA(FRM_NODE* pNode);//与控制器通信数据填充
void CONLFFillTxBuf(FRM_NODE* pNode);//与控制器通信数据填充
extern unsigned int gbChrDuty;
extern unsigned char ChanlVBuf[10];

extern unsigned char LampSet ; //控制灯具设置状态
extern unsigned short Lampnum;

#endif
