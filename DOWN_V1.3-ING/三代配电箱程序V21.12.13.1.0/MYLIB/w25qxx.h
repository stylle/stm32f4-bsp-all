
#ifndef __FLASH_H
#define __FLASH_H

//W25Q80 ID  0XEF13
//W25Q16 ID  0XEF14
//W25Q32 ID  0XEF15
//W25Q64 ID  0XEF16
 
#define SPI_ReadWriteByte SPI2_ReadWriteByte

//command
#define W25X_WriteEnable			0x06 
#define W25X_WriteDisable			0x04 
#define W25X_ReadStatusReg		0x05 
#define W25X_WriteStatusReg		0x01 
#define W25X_ReadData					0x03 
#define W25X_FastReadData			0x0B 
#define W25X_FastReadDual			0x3B 
#define W25X_PageProgram			0x02 
#define W25X_BlockErase				0xD8 
#define W25X_SectorErase			0x20 
#define W25X_ChipErase				0xC7 
#define W25X_PowerDown				0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID					0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 

unsigned short  SPI_Flash_ReadID(void); //read id
unsigned char	 SPI_Flash_ReadSR(void); //read status

void SPI_Flash_Init(void);
void SPI_FLASH_Write_SR(unsigned char sr); //write status
void SPI_FLASH_Write_Enable(void);  //write enable
void SPI_FLASH_Write_Disable(void);	//write protect
void SPI_Flash_Write_NoCheck(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);
void SPI_Flash_Read(unsigned char* pBuffer,unsigned int ReadAddr,unsigned short NumByteToRead);   //read flash
void SPI_Flash_Write(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);//write flash
void SPI_Flash_Erase_Chip(void);    	  //erase chip
void SPI_Flash_Erase_Sector(unsigned int Dst_Addr);	//erase sector
void SPI_Flash_Wait_Busy(void);					//wait
void SPI_Flash_PowerDown(void);				//power down
void SPI_Flash_WAKEUP(void);			  //wake up

void SPI_FLASH_CS(unsigned char bBit);

#endif
















