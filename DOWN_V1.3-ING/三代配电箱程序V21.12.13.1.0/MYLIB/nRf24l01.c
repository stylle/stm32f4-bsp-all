
#include "stdafx.h"
#include "nRf24l01.h"

//=====================================================
//local
//=====================================================
unsigned char	NRF_STATE;

const unsigned char  NRF_TA_SET[NRF_TX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};	//本地地址
const unsigned char  NRF_RA_SET[NRF_RX_ADR_WIDTH]= {0x34,0x43,0x10,0x10,0x01};	//目的地址

//=====================================================
//delay
//=====================================================
void NrfDelayUs(unsigned int nTp)
{
	unsigned char j = 0;
	unsigned int n = nTp;
	//
	for( ;  n > 0; n--)
		for(j=0; j<1; j++);
}

//=====================================================
//init
//=====================================================
unsigned char  InitNRF24L01(void)
{
 	CLR_NRF_CE; 
 	SET_NRF_CSN; 
 	CLR_NRF_SCK; 
	//
	NrfDelayUs(50000);		//delay 10.3ms least
	//
	NrfWriteBuf(NRF_WRITE_REG|NRF_TX_ADDR, (unsigned char*)NRF_TA_SET, NRF_TX_ADR_WIDTH);    // 写本地地址	
	NrfWriteBuf(NRF_WRITE_REG|NRF_RX_ADDR_P0, (unsigned char*)NRF_RA_SET, NRF_RX_ADR_WIDTH); // 写接收端地址
	//
	NrfWriteReg(NRF_EN_AA, 0x00);      //  频道0自动	ACK应答禁止
	NrfWriteReg(NRF_EN_RXADDR, 0x01);  //  允许接收地址只有频道0
	NrfWriteReg(NRF_RF_CH, 0);        //   设置信道工作为2.4GHZ，收发必须一致
	NrfWriteReg(NRF_RX_PW_P0, NRF_RX_PLOAD_WIDTH); //设置接收数据长度
	NrfWriteReg(NRF_RF_SETUP, 0x07);   		//设置发射速率为1MHZ，发射功率为最大值0dB
	//
	NrfIntoPowerDown();	//进入掉电模式
	//
	return 0;
}

//=====================================================
//byre write&read
//=====================================================
unsigned char NrfByteRW(unsigned char nByte)
{
		unsigned char bit_ctr;
		//
   	for(bit_ctr=0;bit_ctr<8;bit_ctr++) 
   	{
			if(nByte & 0x80)
				SET_NRF_MOSI;
			else	
				CLR_NRF_MOSI; // output 'uchar', MSB to MOSI
			//
			nByte = (nByte << 1); // shift next bit into MSB..
			NrfDelayUs(5);
			SET_NRF_SCK;	// Set SCK high..
			nByte |= NRF_MISO;	// capture current MISO bit
			NrfDelayUs(5);
			CLR_NRF_SCK;
   	}
    return(nByte);	
}

//=====================================================
// read reg
//=====================================================
unsigned char NrfReadReg(unsigned char nReg)
{
	unsigned char nRes;
	//
	CLR_NRF_CSN;	// CSN low, initialize SPI communication...
	NrfByteRW(nReg);	// Select register to read from..
	nRes = NrfByteRW(0);	// ..then read registervalue
	SET_NRF_CSN;	// CSN high, terminate SPI communication
	//
	return(nRes);	// return register value
}

//=====================================================
//write reg
//=====================================================
unsigned char NrfWriteReg(unsigned char nReg, unsigned char nValue)
{
	unsigned char nStatus;
	//
	CLR_NRF_CSN;	// CSN low, init SPI transaction
	nStatus = NrfByteRW(NRF_WRITE_REG|nReg);	// select register
	NrfByteRW(nValue);	// ..and write value to it..
	SET_NRF_CSN;	// CSN high again
	//
	return(nStatus);	// return nRF24L01 status uchar
}

//=====================================================
//read buf
//=====================================================
unsigned char NrfReadBuf(unsigned char nReg, unsigned char *pBuf, unsigned char nLen)
{
	unsigned char nStaus,nCnt;
	//
	CLR_NRF_CSN;	// Set CSN low, init SPI tranaction
	//
	nStaus = NrfByteRW(nReg);	// Select register to write to and read status uchar
	//
	for(nCnt=0; nCnt < nLen; nCnt++)
	{
		pBuf[nCnt] = NrfByteRW(0);	// 
	}
	//
	SET_NRF_CSN;                           
	//
	return(nStaus);	// return nRF24L01 status uchar
}

//=====================================================
//write buf
//=====================================================
unsigned char NrfWriteBuf(unsigned char nReg, unsigned char *pBuf, unsigned char nLen)
{
	unsigned char nStaus,nCnt;
	//
	CLR_NRF_CSN;            //SPI使能
	//
	nStaus = NrfByteRW(nReg); 
	//
	for(nCnt=0; nCnt < nLen; nCnt++) 
	{
		NrfByteRW(*pBuf++);
	}
	//
	SET_NRF_CSN;           //关闭SPI
	return(nStaus);    // 
}

//=====================================================
//rx mode
//=====================================================
unsigned char  NrfSetRxMode(void)
{
	CLR_NRF_CE;
	//
	NrfWriteReg(NRF_CONFIG, 0x1f);   		// IRQ使能，16位CRC	，主接收，上电开启
	//
	SET_NRF_CE; 
	//
	NrfDelayUs(150);
	
	return 0;
}

//=====================================================
// read packet
//=====================================================
unsigned char NrfReadPacket(unsigned char* pRxbuf)
{
	unsigned char nRes=0;
	//
	NRF_STATE = NrfReadReg(NRF_STATUS);
	//
	if(NRF_RX_DR)
	{
	  CLR_NRF_CE; 			//进入待机模式
		NrfReadBuf(NRF_RD_RX_PLOAD,pRxbuf,NRF_TX_PLOAD_WIDTH);// read receive payload from RX_FIFO buffer
		NrfWriteReg(NRF_STATUS,NRF_STATE);   //清除中断标志
		NrfByteRW(NRF_FLUSH_RX);	 //清空FIFO
		SET_NRF_CE;		//进入接收模式
		nRes =1;			//读取数据完成标志
	}
	return nRes;
}

//=====================================================
//send packet
//=====================================================
unsigned char NrfSendPacket(unsigned char * pTxbuf)
{
	unsigned char nRes = 0;
	//
	CLR_NRF_CE;			//StandBy I模式	
	NrfWriteBuf(NRF_WRITE_REG|NRF_RX_ADDR_P0, (unsigned char*)NRF_RA_SET, NRF_TX_ADR_WIDTH); // 装载接收端地址
	NrfWriteBuf(NRF_WR_TX_PLOAD, pTxbuf, NRF_TX_PLOAD_WIDTH); 			 // 装载数据	
	NrfWriteReg(NRF_CONFIG, 0x1e);   		 // IRQ使能，16位CRC，主发送，上电开启
	SET_NRF_CE;		 //置高CE，激发数据发送
	NrfDelayUs(150);
	//
	nRes = NrfReadIrq();
	if(nRes == NRF_TX_FLAG)
	{
		NrfWriteReg(NRF_STATUS,NRF_STATE);   //清除中断标志
		NrfByteRW(NRF_FLUSH_TX);	 //清空FIFO
		CLR_NRF_CE;			//发送成功，进入StandBy I模式	
		return TRUE;
	}
	return FALSE;
}

//=====================================================
//read irq
//=====================================================
unsigned char NrfReadIrq(void)
{
	unsigned int nCnt = 0;
	unsigned int nTp = 0;
	//
	while((nCnt>10)||(nTp==NRF_IQA_OVER_TIME))
	{
		if(NRF_IRQ) 	nCnt = 0;
		else 	nCnt++;
		//
		nTp++;
	}
	//
	if(nTp == NRF_IQA_OVER_TIME)	return NRF_IRQ_OVER;
	//
	NRF_STATE = NrfReadReg(NRF_STATUS);
	//
	if(NRF_RX_DR) return	NRF_RX_FLAG;
	//
	if(NRF_TX_DS) return NRF_TX_FLAG;
	//
	return	NRF_IRQ_ERR;
}

//=====================================================
//standby mode
//=====================================================
unsigned char NrfIntoStandBy(void)
{
	NrfWriteReg(NRF_CONFIG, 0x7f);   //上电开启
	NrfDelayUs(3000); //1.5ms least
	return 0;
}

//=====================================================
//power down mode
//=====================================================
unsigned char	NrfIntoPowerDown(void)
{
	NrfWriteReg(NRF_CONFIG, 0x71);   //上电关闭
	return 0;
}





