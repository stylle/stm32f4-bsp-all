#ifndef __M_TYPE_H
#define __M_TYPE_H

#include <stm32f10x.h>

// 宏运算
#define MERGE_HL(H,L) ((unsigned int)H<<8)+((unsigned int)L&0XFF)
#define GETINT16_HIGH(BYTE) ((unsigned char)(BYTE>>8))
#define GETINT16_LOW(BYTE) ((unsigned char)BYTE&0x00FF)
#define GETINT32_8(BYTE,n) (unsigned char)((BYTE&(0xff<<((4-n)*8)))>>((4-n)*8))
#define GETINT32_BIT(BYTE,n) (unsigned char)((BYTE&(0xff<<((4-n)*8)))>>((4-n)*8))
	
typedef struct{
	uint8_t MAIN_VOL_H;					// 主电电压高字节
	uint8_t MAIN_VOL_L;					// 主电电压低字节
	uint8_t OUTPUT_VOL_H;				// 输出电压高字节
	uint8_t OUTPUT_VOL_L;				// 输出电压低字节
	uint8_t OUTPUT_CUR_H;				// 输出电流高字节
	uint8_t OUTPUT_CUR_L;				// 输出电流低字节
	uint8_t BATTERY_VOL_ALL_H; 	// 电池总电压高字节
	uint8_t BATTERY_VOL_ALL_L; 	// 电池总电压低字节
	uint8_t sendStatus;					// 可发送标志在发送之前需要置位
}CAN_POWER_SUPPLY; // 电源相关


typedef struct{
	uint8_t INPUT_VOL_H;//充电电压值高字节
	uint8_t INPUT_VOL_L;//充电电压值低字节
	uint8_t INPUT_CUR_H;//充电电流高字节
	uint8_t INPUT_CUR_L;//充电电流低字节
	uint8_t BATTERY_VOL_1_H;//电池1压高字节
	uint8_t BATTERY_VOL_1_L;//电池1压低字节
	uint8_t BATTERY_VOL_2_H;//电池2压高字节
	uint8_t BATTERY_VOL_2_L;//电池2压低字节
	uint8_t sendStatus;			// 可发送标志在发送之前需要置位
}CAN_INPUT_VC;// 充电电池相关

typedef struct{
	uint8_t BATTERY_VOL_3_H;//电池3压高字节
	uint8_t BATTERY_VOL_3_L;//电池3压低字节
	uint8_t HEGTM_H;				//应急时间
	uint8_t HEGTM_L;				//应急时间
	uint8_t sendStatus;			// 可发送标志在发送之前需要置位
}CAN_TIME;// 电池和应急相关

typedef struct{
	uint8_t LOOP_VOL_CUR_1_H;//回路1电压/电流
	uint8_t LOOP_VOL_CUR_1_L;//回路1电压/电流
	uint8_t LOOP_VOL_CUR_2_H;//回路2电压/电流
	uint8_t LOOP_VOL_CUR_2_L;//回路2电压/电流
	uint8_t LOOP_VOL_CUR_3_H;//回路3电压/电流
	uint8_t LOOP_VOL_CUR_3_L;//回路3电压/电流
	uint8_t LOOP_VOL_CUR_4_H;//回路4电压/电流
	uint8_t LOOP_VOL_CUR_4_L;//回路4电压/电流
	uint8_t sendStatus;					// 可发送标志在发送之前需要置位
}CAN_LOOP_VOL_CUR_14; // 1-4路回路电压电流

typedef struct{
	uint8_t LOOP_VOL_CUR_5_H;//回路5电压/电流
	uint8_t LOOP_VOL_CUR_5_L;//回路5电压/电流
	uint8_t LOOP_VOL_CUR_6_H;//回路6电压/电流
	uint8_t LOOP_VOL_CUR_6_L;//回路6电压/电流
	uint8_t LOOP_VOL_CUR_7_H;//回路7电压/电流
	uint8_t LOOP_VOL_CUR_7_L;//回路7电压/电流
	uint8_t LOOP_VOL_CUR_8_H;//回路8电压/电流
	uint8_t LOOP_VOL_CUR_8_L;//回路8电压/电流
	uint8_t sendStatus;					// 可发送标志在发送之前需要置位
}CAN_LOOP_VOL_CUR_58;// 5-8路回路电压电流


typedef struct{
	uint8_t FAULT_FLAG_H;	//故障状态
	uint8_t FAULT_FLAG_L;	//故障状态
	uint8_t WORK_FLAG_H;	//工作状态
	uint8_t WORK_FLAG_L;	//工作状态
	uint8_t sendStatus;		//可发送标志在发送之前需要置位
}CAN_FLAG; // 状态相关

typedef struct{
	uint8_t LAMP_ID_H;
	uint8_t LAMP_ID_L;
	uint8_t LAMP_DATA;
}CAN_LAMP_DATA;// 灯具数据

// 收到的ID为主机专有ID 长度不判断 扩展帧 
// data数据：第一位表示指令类型 例如03表示接下来的data为查询数据 第二位表示查询指令的类型

//	TxMessage.StdId=0x12;																	// 标准标识符 
//	TxMessage.ExtId=CAN_LOOP_VOL_CUR_14_EXTID;						// 设置扩展标示符 
//	TxMessage.IDE=CAN_Id_Extended; 												// 扩展帧
//	TxMessage.RTR=CAN_RTR_Data;														// 数据帧
//	TxMessage.DLC = 0x08 ;																// 要发送的数据长度

/* 查询命令表 */
#define CMD_CAN_QUERY								0X03					// 查询指令标识
#define CMD_CAN_QUERY_POWER 				0x01 					// 电源查询
#define CMD_CAN_QUERY_DEV_STATE 		0x02 					// 查询工作状态
#define CMD_CAN_QUERY_LAMP 					0x03 					// 查询灯具状态
#define CMD_CAN_QUERY_RANGE_LAMP 		0x04 					// 查询范围灯具状态
#define CMD_CAN_QUERY_VOL14 				0x05 					// 查询输出电压1-4路
#define CMD_CAN_QUERY_VOL58					0x06 					// 查询输出电压5-8路
#define CMD_CAN_QUERY_HEGTM 				0x07 					// 查询应急时间
#define CMD_CAN_QUERY_VERSION 			0x08 					// 查询版本信息

/* 修改|设置命令表 */
#define CMD_CAN_EDIT								0X06					// 修改指令标识
#define CMD_CAN_EDIT_LAMP 					0x01 					// 修改灯具状态 后应跟状态
#define CMD_CAN_EDIT_RESET 					0x02 					// 重启
#define CMD_CAN_EDIT_SILENCING			0x03		 			// 消音
#define CMD_CAN_EDIT_EVENT_MANUAL		0x04 					// 手动应急
#define CMD_CAN_EDIT_EVENT_MCHK 		0x05 					// 月检
// #define CMD_CAN_EDIT_EVENT_YCHK			0x06		 			// 年检
#define CMD_CAN_EDIT_EVENT_FEMGY		0x07		 			// 强制启动开关


#endif


