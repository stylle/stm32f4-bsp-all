#ifndef _DTH11_H
#define _DTH11_H

#include "main.h"


//DATA������ΪPC14

#define DHT11_OUT_H  HAL_GPIO_WritePin(GPIOB,DHT11_Pin,GPIO_PIN_SET)
#define DHT11_OUT_L  HAL_GPIO_WritePin(GPIOB,DHT11_Pin,GPIO_PIN_RESET)
#define DHT11_READ   HAL_GPIO_ReadPin(GPIOB,DHT11_Pin)

void DHT11_MODE_IN(void);
void DHT11_MODE_OUT(void);
uint8_t DHT11_Read_Byte(void);
uint8_t DHT11_ReadData(uint8_t *temp,uint8_t *humi);
void upDate_DHT11(void);


#endif	/* _DTH11_H */
