/**
  ******************************************************************************
  * @file    bsp_TimBase.c
  * @author  stylle
  * @version V1.0
  * @date    2021-xx-xx
  * @brief   DTH11温湿度获取
  ******************************************************************************
  */ 

#include "bsp_DTH11.h" 
#include "bsp_usart1.h"

uint8_t temp=0;
uint8_t humi=0;

//DHT11驱动
void DHT11_Init (void)
{    
        GPIO_InitTypeDef GPIO_InitStruct;
    RCC_APB2PeriphClockCmd(DHT11_CLK, ENABLE);
    
    GPIO_InitStruct.GPIO_Pin=DHT11_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;        /*设置引脚模式为通用推挽输出*/	
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;       /*设置引脚速率为50MHz */	
    GPIO_Init(DHT11_PORT,&GPIO_InitStruct);
}

//输入模式
void DHT11_MODE_IN(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    RCC_APB2PeriphClockCmd(DHT11_CLK, ENABLE);
    
    GPIO_InitStruct.GPIO_Pin=DHT11_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;        /*设置引脚模式为通用推挽输出*/	
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;       /*设置引脚速率为50MHz */	
    GPIO_Init(DHT11_PORT,&GPIO_InitStruct);
}

//输出模式
void DHT11_MODE_OUT(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    
    RCC_APB2PeriphClockCmd(DHT11_CLK, ENABLE);
    

    GPIO_InitStruct.GPIO_Pin=DHT11_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;        /*设置引脚模式为通用推挽输出*/	
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;       /*设置引脚速率为50MHz */	
    GPIO_Init(DHT11_PORT,&GPIO_InitStruct);
}

//读取一个字节    
uint8_t DHT11_Read_Byte(void)
{
    uint8_t i,data;
    for(i=0;i<8;i++)
    {
        if(DHT11_READ==0)
        {
            while(DHT11_READ==0);
        }
        delay_us(40);   //40us
        if(DHT11_READ==1)
        {
            while(DHT11_READ==1);//这里要等待高电平结束，否者读取下一位的时候会因为高电平没有结束而跳过上一个if
            data|=(uint8_t)0x01<<(7-i);
        }
        else
            data&=(uint8_t)~0x01<<(7-i);    
    }
    return data;
}

//读五个字节
uint8_t DHT11_ReadData(uint8_t *temp,uint8_t *humi)
{
    uint8_t buf[5];
    uint8_t i;
    for(i=0;i<5;i++)
    {
        buf[i]=DHT11_Read_Byte();
        
    }
    if(buf[0]+buf[1]+buf[2]+buf[3]==buf[4])
    {
        *temp=buf[2];
        *humi=buf[0];
        return 1;
    }
    else
        return 0;    
}

//DHT11 更新数据
void upDate_DHT11(void)
{
//    DHT11_Init ();
    DHT11_OUT_L;
    delay_us(20000);
    DHT11_OUT_H;
    delay_us(20);
    DHT11_MODE_IN();
    if(DHT11_READ==0)
    {
        while(DHT11_READ==0);
        while(DHT11_READ==1);
        DHT11_ReadData(&temp,&humi);
//        printf("温度:%d   湿度:%d\r\n",temp,humi);
    }
    DHT11_MODE_OUT();
    DHT11_OUT_H;

}    



/*********************************************END OF FILE**********************/
