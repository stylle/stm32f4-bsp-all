/**
  ******************************************************************************
  * @file    bsp_TimBase.c
  * @author  stylle
  * @version V1.0
  * @date    2021-xx-xx
  * @brief   DTH11温湿度获取
  ******************************************************************************
  */ 

#include "dht11.h" 

uint8_t temp=0;
uint8_t humi=0;


//输入模式
void DHT11_MODE_IN(void)
{
   GPIO_InitTypeDef GPIO_InitStruct = {0};
      /*Configure GPIO pins : PBPin PBPin */
  GPIO_InitStruct.Pin = DHT11_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

//输出模式
void DHT11_MODE_OUT(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
      /*Configure GPIO pins : PBPin PBPin */
  GPIO_InitStruct.Pin = DHT11_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

//读取一个字节    
uint8_t DHT11_Read_Byte(void)
{
    uint8_t i,data;
    for(i=0;i<8;i++)
    {
        if(DHT11_READ==0)
        {
            while(DHT11_READ==0);
        }
        HAL_Delay_Us(40);   //40us
        if(DHT11_READ==1)
        {
            while(DHT11_READ==1);//这里要等待高电平结束，否者读取下一位的时候会因为高电平没有结束而跳过上一个if
            data|=(uint8_t)0x01<<(7-i);
        }
        else
            data&=(uint8_t)~0x01<<(7-i);
                     
    }
    return data;
}

//读五个字节
uint8_t DHT11_ReadData(uint8_t *temp,uint8_t *humi)
{
    uint8_t buf[5];
    uint8_t i;
    for(i=0;i<5;i++)
    {
        buf[i]=DHT11_Read_Byte();
        
    }
    if(buf[0]+buf[1]+buf[2]+buf[3]==buf[4])
    {
        *temp=buf[2];
        *humi=buf[0];
        return 1;
    }
    else
        return 0;    
}

//DHT11 更新数据
void upDate_DHT11(void)
{
    DHT11_OUT_L;
    HAL_Delay(20);
    DHT11_OUT_H;
    HAL_Delay_Us(20); // us
    DHT11_MODE_IN();
    if(DHT11_READ==0)
    {
        while(DHT11_READ==0);
        while(DHT11_READ==1);
        DHT11_ReadData(&temp,&humi);
//        printf("温度:%d   湿度:%d\r\n",temp,humi);
			LCD_ShowNum(68,16,temp,2,BLUE);
			LCD_ShowNum(68,48,humi,2,BLUE);
    }
    DHT11_MODE_OUT();
    DHT11_OUT_H;
}    



/*********************************************END OF FILE**********************/
