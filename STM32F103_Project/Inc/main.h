/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "lcd.h"
#include "tim.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LCD_CLK_Pin GPIO_PIN_0
#define LCD_CLK_GPIO_Port GPIOA
#define LCD_MOSI_Pin GPIO_PIN_1
#define LCD_MOSI_GPIO_Port GPIOA
#define LCD_RES_Pin GPIO_PIN_2
#define LCD_RES_GPIO_Port GPIOA
#define LCD_DC_Pin GPIO_PIN_3
#define LCD_DC_GPIO_Port GPIOA
#define LCD_BLK_Pin GPIO_PIN_4
#define LCD_BLK_GPIO_Port GPIOA
#define LCD_MISO_Pin GPIO_PIN_5
#define LCD_MISO_GPIO_Port GPIOA
#define LCD_CS1_Pin GPIO_PIN_6
#define LCD_CS1_GPIO_Port GPIOA
#define LCD_CS2_Pin GPIO_PIN_7
#define LCD_CS2_GPIO_Port GPIOA
#define DHT11_Pin GPIO_PIN_13
#define DHT11_GPIO_Port GPIOB
#define MG995_Pin GPIO_PIN_6
#define MG995_GPIO_Port GPIOC
#define USER_GPIO_LED_Pin GPIO_PIN_5
#define USER_GPIO_LED_GPIO_Port GPIOB
#define USER_GPIO_KEY_Pin GPIO_PIN_6
#define USER_GPIO_KEY_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
