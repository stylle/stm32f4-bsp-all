#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h"
#include "iap.h"

void write_iap(u32);



int main(void)
{
	int i=0;           //信号灯计数
	int j=0;
	u32 last_cnt=0;    //旧长度
	delay_init();	    	 //延时函数初始化	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
	uart1_init(115200);	 
	LED_Init();		  	 //初始化与LED连接的硬件接口 
	while(1)
	{
		delay_ms(20);
		if((usart1_cnt)&&(usart1_state==2))  //sram程序区有数据
		{
			if(last_cnt==usart1_cnt)
			{
				j++;
				printf("已接收第%d\r\n",j);
				usart1_state=3;
			}
			else last_cnt=usart1_cnt;
		}
		i++;if(i==16){i=0;
		LED1=~LED1;}

		
		if(usart1_state==3)
		{
			write_iap(usart1_cnt);
			usart1_cnt=0;
			usart1_state=2;
		}
		
		
		if((STMFLASH_ReadHalfWord(FLASH_JUMP_FLAG_ADDR)==Flag_Buff[0]))usart1_state=7;//读取状态标志  两种进入方式 1修改flash 2拉低PA8引脚电平
		if(!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_8))
		{
			usart1_state=0;
			STMFLASH_Write((u32)(FLASH_JUMP_FLAG_ADDR),Flag_Buff,1);//拉低复位后强制修改flash标志位  
		}
		


		if(usart1_state==7)
		{
			if(((*(vu32*)(FLASH_APP_LOAD_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
			{	 
				printf("用户程序开始跳转\r\n");
				iap_load_app(FLASH_APP_LOAD_ADDR);//Flash地址
			}
			else printf("Flash地址非法！\r\n");
		}
	}	 
}

void write_iap(u32 Iap_lenth)
{
	static u32 have_write=0;
	printf("开始升级\r\n");
	if((((*(vu32*)(0X20001000+4))&0xFF000000)==0x08000000)|(have_write>0))//判断是否为0X08XXXXXX.
	{	 
		iap_write_appbin(FLASH_APP_LOAD_ADDR+have_write,Appreceive_buff,Iap_lenth);//更新FLASH代码   
		have_write += Iap_lenth;
		printf("升级完成\r\n");
	}else printf("Err66");
}