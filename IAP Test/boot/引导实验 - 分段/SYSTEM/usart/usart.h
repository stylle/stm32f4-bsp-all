#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 
#include <stdbool.h> 

#define Max_lenth        41*1024
#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收
extern u8 Appreceive_buff[Max_lenth];  
extern u32 usart1_cnt;   
extern u8 usart1_state;
void uart1_init(u32 bound);
#endif


