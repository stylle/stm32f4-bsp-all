#include "sys.h"
#include "usart.h"	  
#include <stdbool.h> 
#include "iap.h"
#include "led.h"
////////////////////////////////////////////////////////////////////////////////// 	 

#if SYSTEM_SUPPORT_OS
#include "includes.h"					//ucos 使用	  
#endif

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) ch;      
	return ch;
}
#endif 
//////////////////////////////////////////////////////////////////
u8 Appreceive_buff[Max_lenth]  __attribute__  ((at(0x20001000)));                //强制定义Appreceive_buff的起始地址为0x20001000
u32 usart1_cnt=0;                             //接收计数值


u8 usart1_state=0;
#if EN_USART1_RX   //如果使能了接收
//串口1中断服务程序

  
void uart1_init(u32 bound){
  //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟
  
	//USART1_TX   GPIOA.9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.9
   
  //USART1_RX	  GPIOA.10初始化
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.10  

  //Usart1 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		//子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
  
   //USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式

  USART_Init(USART1, &USART_InitStructure); //初始化串口1
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启串口接受中断
  USART_Cmd(USART1, ENABLE);                    //使能串口1 
}
//usart1_state  2开始接收
void USART1_IRQHandler(void)                	//串口1中断服务程序
{
	u8 Res;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断
		{
			Res =USART_ReceiveData(USART1);//(USART1->DR);	//读取接收到的数据
			if(usart1_state==2)
			{
				if(usart1_cnt<Max_lenth)Appreceive_buff[usart1_cnt++]=Res;
				else printf("Save_Err\r\n");
			}						
			//接收到"ok"即可开始发送程序包  不能加发送新行!
			if((usart1_state==1)&&(Res==0x6B))//字母'k'
			{
				usart1_state=2;   
				printf("ok\r\n");
			}
			else if((usart1_state==1)&&(Res!=0x6B))
			{
				printf("Err105\r\n");
				usart1_state=0;
			}
			if((usart1_state==0)&&(Res==0x6F))usart1_state=1;   //字母'0'
			else if((usart1_state==0)&&(Res!=0x6F))printf("Err109\r\n");
			//接收完成后 发送"++"即可开始升级
//			if((usart1_state==4)&&(Res==0x2B))usart1_state=5;  //'+'
//			else if((usart1_state==4)&&(Res!=0x2B))
//				{
//					usart1_state=2;
//					printf("Err115\r\n");
//				}
//			if((usart1_state==2)&&(Res==0x2B))usart1_state=4;  //'+'
			//else if((usart1_state==3)&&(Res!=0x2B))printf("Error118\r\n");
		}
} 
#endif	

