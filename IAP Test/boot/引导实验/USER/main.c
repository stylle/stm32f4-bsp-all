#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h"
#include "iap.h"


int main(void)
{
	u16 Iap_lenth=0;   //程序包长度
	int i=0;           //信号灯计数
	u16 last_cnt=0;    //旧长度
	delay_init();	    	 //延时函数初始化	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
	uart1_init(115200);	 
	LED_Init();		  	 //初始化与LED连接的硬件接口 
	//_485_control_init();
	while(1)
	{
		if((STMFLASH_ReadHalfWord(FLASH_JUMP_FLAG_ADDR)==Flag_Buff[0]))usart1_state=7;//读取状态标志  两种进入方式 1修改flash 2拉低PA8引脚电平
		if(!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_8))
		{
			usart1_state=0;
			STMFLASH_Write((u32)(FLASH_JUMP_FLAG_ADDR),Flag_Buff+1,1);//拉低复位后强制修改flash标志位  
		}
		delay_ms(20);
		if((usart1_cnt)&&(usart1_state==2))  //sram程序区有数据
		{
			if(last_cnt==usart1_cnt)
			{
				printf("程序接收完成\r\n");
				usart1_state=3;
			}
			else last_cnt=usart1_cnt;
		}
		i++;if(i==16){i=0;
		LED1=~LED1;}
		if(usart1_state==5)  //sram->flash
		{
			Iap_lenth=usart1_cnt;
			usart1_cnt=0;
			printf("用户程序开始升级!\r\n");
			if(((*(vu32*)(0X20001000+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
			{	 
				iap_write_appbin(FLASH_APP_LOAD_ADDR,Appreceive_buff,Iap_lenth);//更新FLASH代码   
				printf("用户程序升级完成\r\n");
				STMFLASH_Write((u32)(FLASH_JUMP_FLAG_ADDR),Flag_Buff,1);   //向Flash里写一个标志位
				usart1_state=6;
			}
			else 
			{
				printf("程序升级失败\r\n");
				usart1_state=0;
			}
		}
		if(usart1_state==7)
		{
			if(((*(vu32*)(FLASH_APP_LOAD_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
			{	 
				printf("用户程序开始跳转\r\n");
				iap_load_app(FLASH_APP_LOAD_ADDR);//Flash地址
			}
			else printf("Flash地址非法！\r\n");
		}
	}	 
}


