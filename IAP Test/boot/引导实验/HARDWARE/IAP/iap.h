#ifndef __IAP_H__
#define __IAP_H__
#include "sys.h"  

#define  FLASH_APP_LOAD_ADDR   0x08005000   //升级程序起始地址
#define  FLASH_JUMP_FLAG_ADDR  0x08004FE0   //0x5000-32 跳转标志地址
extern u16 Flag_Buff[2];
typedef void (*IAP_function)(void);
void iap_load_app(u32 appxaddr);			//跳转到APP程序执行
void iap_write_appbin(u32 appxaddr,u8 *appbuf,u32 applen);	//在指定地址开始,写入bin

#endif
