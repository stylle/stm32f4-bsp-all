/********************************************************************************
** Form generated from reading UI file 'iap_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IAP_WINDOW_H
#define UI_IAP_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IAP_Window
{
public:
    QWidget *centralwidget;
    QPushButton *send_all_Button;
    QPlainTextEdit *state_Edit;
    QPushButton *send_all_Button_2;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QPushButton *clear_Button;
    QLineEdit *file_path_Edit;
    QPushButton *search_file_Button;
    QPushButton *information_Buttton;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QComboBox *Byte_size_Box;
    QLabel *label_5;
    QLabel *label_2;
    QLineEdit *time_Edit_line;
    QLabel *label_3;
    QPushButton *stop_send_Button;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *IAP_Window)
    {
        if (IAP_Window->objectName().isEmpty())
            IAP_Window->setObjectName(QString::fromUtf8("IAP_Window"));
        IAP_Window->resize(552, 338);
        centralwidget = new QWidget(IAP_Window);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        send_all_Button = new QPushButton(centralwidget);
        send_all_Button->setObjectName(QString::fromUtf8("send_all_Button"));
        send_all_Button->setGeometry(QRect(320, 210, 93, 31));
        state_Edit = new QPlainTextEdit(centralwidget);
        state_Edit->setObjectName(QString::fromUtf8("state_Edit"));
        state_Edit->setGeometry(QRect(10, 10, 301, 231));
        state_Edit->setReadOnly(true);
        send_all_Button_2 = new QPushButton(centralwidget);
        send_all_Button_2->setObjectName(QString::fromUtf8("send_all_Button_2"));
        send_all_Button_2->setGeometry(QRect(320, 70, 101, 31));
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 250, 501, 35));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        clear_Button = new QPushButton(layoutWidget);
        clear_Button->setObjectName(QString::fromUtf8("clear_Button"));

        gridLayout->addWidget(clear_Button, 0, 0, 1, 1);

        file_path_Edit = new QLineEdit(layoutWidget);
        file_path_Edit->setObjectName(QString::fromUtf8("file_path_Edit"));
        file_path_Edit->setMinimumSize(QSize(200, 31));

        gridLayout->addWidget(file_path_Edit, 0, 1, 1, 1);

        search_file_Button = new QPushButton(layoutWidget);
        search_file_Button->setObjectName(QString::fromUtf8("search_file_Button"));

        gridLayout->addWidget(search_file_Button, 0, 2, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        information_Buttton = new QPushButton(layoutWidget);
        information_Buttton->setObjectName(QString::fromUtf8("information_Buttton"));

        horizontalLayout->addWidget(information_Buttton);

        layoutWidget1 = new QWidget(centralwidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(322, 12, 134, 51));
        gridLayout_2 = new QGridLayout(layoutWidget1);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font;
        font.setPointSize(9);
        label_4->setFont(font);
        label_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_4, 0, 0, 1, 1);

        Byte_size_Box = new QComboBox(layoutWidget1);
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->addItem(QString());
        Byte_size_Box->setObjectName(QString::fromUtf8("Byte_size_Box"));
        Byte_size_Box->setToolTipDuration(-1);

        gridLayout_2->addWidget(Byte_size_Box, 0, 1, 1, 2);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font1;
        font1.setPointSize(12);
        label_5->setFont(font1);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_5, 0, 3, 1, 1);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        time_Edit_line = new QLineEdit(layoutWidget1);
        time_Edit_line->setObjectName(QString::fromUtf8("time_Edit_line"));
        time_Edit_line->setMaximumSize(QSize(60, 16777215));

        gridLayout_2->addWidget(time_Edit_line, 1, 1, 1, 1);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font2;
        font2.setPointSize(13);
        label_3->setFont(font2);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_3, 1, 2, 1, 2);

        stop_send_Button = new QPushButton(centralwidget);
        stop_send_Button->setObjectName(QString::fromUtf8("stop_send_Button"));
        stop_send_Button->setGeometry(QRect(430, 70, 93, 31));
        IAP_Window->setCentralWidget(centralwidget);
        menubar = new QMenuBar(IAP_Window);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 552, 26));
        IAP_Window->setMenuBar(menubar);
        statusbar = new QStatusBar(IAP_Window);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        IAP_Window->setStatusBar(statusbar);

        retranslateUi(IAP_Window);

        Byte_size_Box->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(IAP_Window);
    } // setupUi

    void retranslateUi(QMainWindow *IAP_Window)
    {
        IAP_Window->setWindowTitle(QApplication::translate("IAP_Window", "MainWindow", nullptr));
        send_all_Button->setText(QApplication::translate("IAP_Window", "\347\233\264\346\216\245\345\217\221\351\200\201", nullptr));
        send_all_Button_2->setText(QApplication::translate("IAP_Window", "\345\210\206\346\256\265\345\256\232\346\227\266\345\217\221\351\200\201", nullptr));
        clear_Button->setText(QApplication::translate("IAP_Window", "\346\270\205\347\251\272", nullptr));
        search_file_Button->setText(QApplication::translate("IAP_Window", "\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
        information_Buttton->setText(QApplication::translate("IAP_Window", "\346\226\207\344\273\266\344\277\241\346\201\257", nullptr));
        label_4->setText(QApplication::translate("IAP_Window", "\351\225\277\345\272\246", nullptr));
        Byte_size_Box->setItemText(0, QApplication::translate("IAP_Window", "1 ", nullptr));
        Byte_size_Box->setItemText(1, QApplication::translate("IAP_Window", "2 ", nullptr));
        Byte_size_Box->setItemText(2, QApplication::translate("IAP_Window", "4 ", nullptr));
        Byte_size_Box->setItemText(3, QApplication::translate("IAP_Window", "8 ", nullptr));
        Byte_size_Box->setItemText(4, QApplication::translate("IAP_Window", "16 ", nullptr));
        Byte_size_Box->setItemText(5, QApplication::translate("IAP_Window", "32 ", nullptr));
        Byte_size_Box->setItemText(6, QApplication::translate("IAP_Window", "64 ", nullptr));

        label_5->setText(QApplication::translate("IAP_Window", "KB", nullptr));
        label_2->setText(QApplication::translate("IAP_Window", "\351\227\264\351\232\224", nullptr));
        label_3->setText(QApplication::translate("IAP_Window", "ms", nullptr));
        stop_send_Button->setText(QApplication::translate("IAP_Window", "\345\217\221\351\200\201\344\270\255\346\255\242", nullptr));
    } // retranslateUi

};

namespace Ui {
    class IAP_Window: public Ui_IAP_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IAP_WINDOW_H
