/********************************************************************************
** Form generated from reading UI file 'framewindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRAMEWINDOW_H
#define UI_FRAMEWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_frameWindow
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *frame_Layout;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout;
    QCheckBox *data1_checkBox;
    QLabel *data1_label;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *timer_label;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *frameWindow)
    {
        if (frameWindow->objectName().isEmpty())
            frameWindow->setObjectName(QString::fromUtf8("frameWindow"));
        frameWindow->resize(1117, 658);
        centralwidget = new QWidget(frameWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(9, 9, 971, 511));
        frame_Layout = new QGridLayout(gridLayoutWidget);
        frame_Layout->setObjectName(QString::fromUtf8("frame_Layout"));
        frame_Layout->setHorizontalSpacing(6);
        frame_Layout->setContentsMargins(0, 0, 0, 0);
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 540, 293, 57));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        data1_checkBox = new QCheckBox(layoutWidget);
        data1_checkBox->setObjectName(QString::fromUtf8("data1_checkBox"));
        QFont font;
        font.setPointSize(14);
        data1_checkBox->setFont(font);

        verticalLayout->addWidget(data1_checkBox);

        data1_label = new QLabel(layoutWidget);
        data1_label->setObjectName(QString::fromUtf8("data1_label"));
        QFont font1;
        font1.setPointSize(12);
        data1_label->setFont(font1);
        data1_label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(data1_label);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(820, 550, 101, 51));
        layoutWidget1 = new QWidget(centralwidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(950, 560, 121, 41));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget1);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font2;
        font2.setPointSize(13);
        label->setFont(font2);
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);

        spinBox = new QSpinBox(layoutWidget1);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMinimum(10);
        spinBox->setMaximum(1000);

        horizontalLayout->addWidget(spinBox);

        timer_label = new QLabel(centralwidget);
        timer_label->setObjectName(QString::fromUtf8("timer_label"));
        timer_label->setGeometry(QRect(990, 520, 71, 21));
        frameWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(frameWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1117, 26));
        frameWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(frameWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        frameWindow->setStatusBar(statusbar);

        retranslateUi(frameWindow);

        QMetaObject::connectSlotsByName(frameWindow);
    } // setupUi

    void retranslateUi(QMainWindow *frameWindow)
    {
        frameWindow->setWindowTitle(QApplication::translate("frameWindow", "MainWindow", nullptr));
        data1_checkBox->setText(QApplication::translate("frameWindow", "data1", nullptr));
        data1_label->setText(QApplication::translate("frameWindow", "0", nullptr));
        pushButton->setText(QApplication::translate("frameWindow", "\345\274\200\345\247\213\346\230\276\347\244\272", nullptr));
        label->setText(QApplication::translate("frameWindow", "\345\270\247\346\225\260", nullptr));
        timer_label->setText(QApplication::translate("frameWindow", "\345\215\225\344\275\2151ms", nullptr));
    } // retranslateUi

};

namespace Ui {
    class frameWindow: public Ui_frameWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRAMEWINDOW_H
