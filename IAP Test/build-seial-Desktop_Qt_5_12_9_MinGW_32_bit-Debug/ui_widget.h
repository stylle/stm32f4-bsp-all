/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QPlainTextEdit *recvEdit;
    QPlainTextEdit *sendEdit;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *openBt;
    QPushButton *closeBt;
    QPushButton *sendBt;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *stopCb;
    QComboBox *checkCb;
    QComboBox *baundrateCb;
    QComboBox *filectrlCb;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QComboBox *dataCb;
    QComboBox *serialCb;
    QLabel *label_6;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *clear2Bt;
    QPushButton *clearBt;
    QLineEdit *stateEdit;
    QLineEdit *pathEdit;
    QPushButton *pushButton;
    QLabel *label_8;
    QLabel *time_label;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *buttonRead2;
    QPushButton *buttonSend;
    QPushButton *buttonRead;
    QPushButton *buttonRead_2;
    QProgressBar *send_progressBar;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton;
    QCheckBox *checkBox_2;
    QLabel *label_9;
    QCheckBox *checkBox;
    QCheckBox *timer_Box;
    QLineEdit *timer_lineEdit;
    QLabel *blog_label;
    QLabel *label_7;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *IAP_Button;
    QPushButton *IAP_Button_2;
    QPushButton *saveButton;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(1185, 672);
        recvEdit = new QPlainTextEdit(Widget);
        recvEdit->setObjectName(QString::fromUtf8("recvEdit"));
        recvEdit->setGeometry(QRect(380, 10, 751, 431));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(85);
        sizePolicy.setHeightForWidth(recvEdit->sizePolicy().hasHeightForWidth());
        recvEdit->setSizePolicy(sizePolicy);
        recvEdit->setMinimumSize(QSize(0, 0));
        recvEdit->setReadOnly(true);
        sendEdit = new QPlainTextEdit(Widget);
        sendEdit->setObjectName(QString::fromUtf8("sendEdit"));
        sendEdit->setGeometry(QRect(80, 450, 731, 101));
        layoutWidget = new QWidget(Widget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(820, 450, 95, 98));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        openBt = new QPushButton(layoutWidget);
        openBt->setObjectName(QString::fromUtf8("openBt"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(80);
        sizePolicy1.setHeightForWidth(openBt->sizePolicy().hasHeightForWidth());
        openBt->setSizePolicy(sizePolicy1);
        openBt->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(openBt);

        closeBt = new QPushButton(layoutWidget);
        closeBt->setObjectName(QString::fromUtf8("closeBt"));
        sizePolicy1.setHeightForWidth(closeBt->sizePolicy().hasHeightForWidth());
        closeBt->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(closeBt);

        sendBt = new QPushButton(layoutWidget);
        sendBt->setObjectName(QString::fromUtf8("sendBt"));
        sizePolicy1.setHeightForWidth(sendBt->sizePolicy().hasHeightForWidth());
        sendBt->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(sendBt);

        layoutWidget1 = new QWidget(Widget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(20, 20, 351, 341));
        verticalLayout_2 = new QVBoxLayout(layoutWidget1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(layoutWidget1);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(80);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        QFont font;
        font.setPointSize(15);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 1, 1, 1);

        stopCb = new QComboBox(layoutWidget1);
        stopCb->addItem(QString());
        stopCb->addItem(QString());
        stopCb->addItem(QString());
        stopCb->setObjectName(QString::fromUtf8("stopCb"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(80);
        sizePolicy3.setHeightForWidth(stopCb->sizePolicy().hasHeightForWidth());
        stopCb->setSizePolicy(sizePolicy3);
        stopCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(stopCb, 3, 0, 1, 1);

        checkCb = new QComboBox(layoutWidget1);
        checkCb->addItem(QString());
        checkCb->addItem(QString());
        checkCb->addItem(QString());
        checkCb->setObjectName(QString::fromUtf8("checkCb"));
        sizePolicy3.setHeightForWidth(checkCb->sizePolicy().hasHeightForWidth());
        checkCb->setSizePolicy(sizePolicy3);
        checkCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(checkCb, 4, 0, 1, 1);

        baundrateCb = new QComboBox(layoutWidget1);
        baundrateCb->addItem(QString());
        baundrateCb->addItem(QString());
        baundrateCb->addItem(QString());
        baundrateCb->addItem(QString());
        baundrateCb->addItem(QString());
        baundrateCb->addItem(QString());
        baundrateCb->setObjectName(QString::fromUtf8("baundrateCb"));
        sizePolicy3.setHeightForWidth(baundrateCb->sizePolicy().hasHeightForWidth());
        baundrateCb->setSizePolicy(sizePolicy3);
        baundrateCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(baundrateCb, 1, 0, 1, 1);

        filectrlCb = new QComboBox(layoutWidget1);
        filectrlCb->addItem(QString());
        filectrlCb->setObjectName(QString::fromUtf8("filectrlCb"));
        sizePolicy3.setHeightForWidth(filectrlCb->sizePolicy().hasHeightForWidth());
        filectrlCb->setSizePolicy(sizePolicy3);
        filectrlCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(filectrlCb, 5, 0, 1, 1);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy2.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy2);
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_3, 2, 1, 1, 1);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy2.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy2);
        label_4->setFont(font);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_4, 3, 1, 1, 1);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_5, 4, 1, 1, 1);

        dataCb = new QComboBox(layoutWidget1);
        dataCb->addItem(QString());
        dataCb->addItem(QString());
        dataCb->addItem(QString());
        dataCb->addItem(QString());
        dataCb->setObjectName(QString::fromUtf8("dataCb"));
        sizePolicy3.setHeightForWidth(dataCb->sizePolicy().hasHeightForWidth());
        dataCb->setSizePolicy(sizePolicy3);
        dataCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(dataCb, 2, 0, 1, 1);

        serialCb = new QComboBox(layoutWidget1);
        serialCb->setObjectName(QString::fromUtf8("serialCb"));
        sizePolicy3.setHeightForWidth(serialCb->sizePolicy().hasHeightForWidth());
        serialCb->setSizePolicy(sizePolicy3);
        serialCb->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(serialCb, 0, 0, 1, 1);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        sizePolicy2.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy2);
        label_6->setFont(font);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_6, 5, 1, 1, 1);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy2.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy2);
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_2, 1, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        clear2Bt = new QPushButton(layoutWidget1);
        clear2Bt->setObjectName(QString::fromUtf8("clear2Bt"));

        horizontalLayout_2->addWidget(clear2Bt);

        clearBt = new QPushButton(layoutWidget1);
        clearBt->setObjectName(QString::fromUtf8("clearBt"));
        sizePolicy1.setHeightForWidth(clearBt->sizePolicy().hasHeightForWidth());
        clearBt->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(clearBt);


        verticalLayout_2->addLayout(horizontalLayout_2);

        stateEdit = new QLineEdit(Widget);
        stateEdit->setObjectName(QString::fromUtf8("stateEdit"));
        stateEdit->setGeometry(QRect(10, 560, 141, 28));
        stateEdit->setReadOnly(true);
        pathEdit = new QLineEdit(Widget);
        pathEdit->setObjectName(QString::fromUtf8("pathEdit"));
        pathEdit->setGeometry(QRect(290, 560, 341, 28));
        pathEdit->setReadOnly(true);
        pushButton = new QPushButton(Widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(170, 560, 101, 28));
        label_8 = new QLabel(Widget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 520, 72, 31));
        QFont font1;
        font1.setPointSize(12);
        label_8->setFont(font1);
        label_8->setAlignment(Qt::AlignCenter);
        time_label = new QLabel(Widget);
        time_label->setObjectName(QString::fromUtf8("time_label"));
        time_label->setGeometry(QRect(880, 600, 191, 21));
        layoutWidget2 = new QWidget(Widget);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(660, 560, 421, 30));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        buttonRead2 = new QPushButton(layoutWidget2);
        buttonRead2->setObjectName(QString::fromUtf8("buttonRead2"));

        horizontalLayout_3->addWidget(buttonRead2);

        buttonSend = new QPushButton(layoutWidget2);
        buttonSend->setObjectName(QString::fromUtf8("buttonSend"));

        horizontalLayout_3->addWidget(buttonSend);

        buttonRead = new QPushButton(layoutWidget2);
        buttonRead->setObjectName(QString::fromUtf8("buttonRead"));

        horizontalLayout_3->addWidget(buttonRead);

        buttonRead_2 = new QPushButton(layoutWidget2);
        buttonRead_2->setObjectName(QString::fromUtf8("buttonRead_2"));

        horizontalLayout_3->addWidget(buttonRead_2);

        send_progressBar = new QProgressBar(Widget);
        send_progressBar->setObjectName(QString::fromUtf8("send_progressBar"));
        send_progressBar->setGeometry(QRect(10, 600, 381, 28));
        send_progressBar->setValue(24);
        groupBox_2 = new QGroupBox(Widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(920, 450, 219, 101));
        groupBox_2->setMinimumSize(QSize(0, 0));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        radioButton_3 = new QRadioButton(groupBox_2);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
        radioButton_3->setMinimumSize(QSize(100, 0));
        QFont font2;
        font2.setPointSize(10);
        radioButton_3->setFont(font2);
        radioButton_3->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(radioButton_3, 0, 0, 1, 1);

        radioButton = new QRadioButton(groupBox_2);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        radioButton->setMinimumSize(QSize(100, 0));
        radioButton->setFont(font2);
        radioButton->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(radioButton, 1, 0, 1, 1);

        checkBox_2 = new QCheckBox(groupBox_2);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        gridLayout_2->addWidget(checkBox_2, 0, 6, 1, 1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        QFont font3;
        font3.setPointSize(13);
        label_9->setFont(font3);
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_9, 2, 2, 1, 1);

        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(checkBox, 1, 6, 1, 1);

        timer_Box = new QCheckBox(groupBox_2);
        timer_Box->setObjectName(QString::fromUtf8("timer_Box"));

        gridLayout_2->addWidget(timer_Box, 2, 6, 1, 1);

        timer_lineEdit = new QLineEdit(groupBox_2);
        timer_lineEdit->setObjectName(QString::fromUtf8("timer_lineEdit"));
        timer_lineEdit->setMaximumSize(QSize(50, 16777215));

        gridLayout_2->addWidget(timer_lineEdit, 2, 0, 1, 1);

        blog_label = new QLabel(Widget);
        blog_label->setObjectName(QString::fromUtf8("blog_label"));
        blog_label->setGeometry(QRect(440, 610, 381, 16));
        blog_label->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(Widget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(300, 380, 70, 20));
        label_7->setFont(font1);
        label_7->setAlignment(Qt::AlignCenter);
        layoutWidget3 = new QWidget(Widget);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(80, 417, 295, 32));
        gridLayout_3 = new QGridLayout(layoutWidget3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        IAP_Button = new QPushButton(layoutWidget3);
        IAP_Button->setObjectName(QString::fromUtf8("IAP_Button"));

        horizontalLayout->addWidget(IAP_Button);

        IAP_Button_2 = new QPushButton(layoutWidget3);
        IAP_Button_2->setObjectName(QString::fromUtf8("IAP_Button_2"));

        horizontalLayout->addWidget(IAP_Button_2);


        gridLayout_3->addLayout(horizontalLayout, 0, 0, 1, 1);

        saveButton = new QPushButton(layoutWidget3);
        saveButton->setObjectName(QString::fromUtf8("saveButton"));

        gridLayout_3->addWidget(saveButton, 0, 1, 1, 1);


        retranslateUi(Widget);

        baundrateCb->setCurrentIndex(1);
        dataCb->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        openBt->setText(QApplication::translate("Widget", "\346\211\223\345\274\200", nullptr));
        closeBt->setText(QApplication::translate("Widget", "\345\205\263\351\227\255", nullptr));
        sendBt->setText(QApplication::translate("Widget", "\345\217\221\351\200\201", nullptr));
        label->setText(QApplication::translate("Widget", "\344\270\262\345\217\243\345\217\267", nullptr));
        stopCb->setItemText(0, QApplication::translate("Widget", "1", nullptr));
        stopCb->setItemText(1, QApplication::translate("Widget", "1.5", nullptr));
        stopCb->setItemText(2, QApplication::translate("Widget", "2", nullptr));

        checkCb->setItemText(0, QApplication::translate("Widget", "none", nullptr));
        checkCb->setItemText(1, QApplication::translate("Widget", "odd", nullptr));
        checkCb->setItemText(2, QApplication::translate("Widget", "even", nullptr));

        baundrateCb->setItemText(0, QApplication::translate("Widget", "4800", nullptr));
        baundrateCb->setItemText(1, QApplication::translate("Widget", "9600", nullptr));
        baundrateCb->setItemText(2, QApplication::translate("Widget", "115200", nullptr));
        baundrateCb->setItemText(3, QApplication::translate("Widget", "19200", nullptr));
        baundrateCb->setItemText(4, QApplication::translate("Widget", "38400", nullptr));
        baundrateCb->setItemText(5, QApplication::translate("Widget", "57600", nullptr));

        filectrlCb->setItemText(0, QApplication::translate("Widget", "noflowcontrol", nullptr));

        label_3->setText(QApplication::translate("Widget", "\346\225\260\346\215\256\344\275\215", nullptr));
        label_4->setText(QApplication::translate("Widget", "\345\201\234\346\255\242\344\275\215", nullptr));
        label_5->setText(QApplication::translate("Widget", "\346\240\241\351\252\214\344\275\215", nullptr));
        dataCb->setItemText(0, QApplication::translate("Widget", "5", nullptr));
        dataCb->setItemText(1, QApplication::translate("Widget", "6", nullptr));
        dataCb->setItemText(2, QApplication::translate("Widget", "7", nullptr));
        dataCb->setItemText(3, QApplication::translate("Widget", "8", nullptr));

        label_6->setText(QApplication::translate("Widget", "\346\265\201\346\216\247", nullptr));
        label_2->setText(QApplication::translate("Widget", "\346\263\242\347\211\271\347\216\207", nullptr));
        clear2Bt->setText(QApplication::translate("Widget", "\346\270\205\347\251\272\345\217\221\351\200\201", nullptr));
        clearBt->setText(QApplication::translate("Widget", "\346\270\205\347\251\272\346\216\245\346\224\266", nullptr));
        pushButton->setText(QApplication::translate("Widget", "\346\211\223\345\215\260\346\226\207\344\273\266\344\277\241\346\201\257", nullptr));
        label_8->setText(QApplication::translate("Widget", "\345\217\221\351\200\201\345\214\272:", nullptr));
        time_label->setText(QApplication::translate("Widget", "Timer", nullptr));
        buttonRead2->setText(QApplication::translate("Widget", "\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
        buttonSend->setText(QApplication::translate("Widget", "\345\217\221\351\200\201\346\226\207\344\273\266", nullptr));
        buttonRead->setText(QApplication::translate("Widget", "\346\211\223\345\274\200\346\226\207\346\234\254", nullptr));
        buttonRead_2->setText(QApplication::translate("Widget", "\346\211\223\345\274\200\345\233\276\347\211\207", nullptr));
        groupBox_2->setTitle(QString());
        radioButton_3->setText(QApplication::translate("Widget", "c\346\216\245\346\224\266", nullptr));
        radioButton->setText(QApplication::translate("Widget", "hex\346\216\245\346\224\266", nullptr));
        checkBox_2->setText(QApplication::translate("Widget", "\346\227\266\351\227\264\346\210\263", nullptr));
        label_9->setText(QApplication::translate("Widget", "ms", nullptr));
        checkBox->setText(QApplication::translate("Widget", "\345\217\221\351\200\201\346\226\260\350\241\214", nullptr));
        timer_Box->setText(QApplication::translate("Widget", "\345\256\232\346\227\266\345\217\221\351\200\201", nullptr));
        blog_label->setText(QApplication::translate("Widget", "\344\275\234\350\200\205\344\270\252\344\272\272\345\215\232\345\256\242>_<  \347\202\271\350\277\231\351\207\214", nullptr));
        label_7->setText(QApplication::translate("Widget", "\346\216\245\346\224\266\345\214\272:", nullptr));
        IAP_Button->setText(QApplication::translate("Widget", "IAP", nullptr));
        IAP_Button_2->setText(QApplication::translate("Widget", "\346\263\242\345\275\242\346\230\276\347\244\272", nullptr));
        saveButton->setText(QApplication::translate("Widget", "\344\277\235\345\255\230\346\216\245\346\224\266", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
