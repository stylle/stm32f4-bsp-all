#include "iap_window.h"
#include "ui_iap_window.h"

IAP_Window::IAP_Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::IAP_Window)
{
    ui->setupUi(this);
    this->setWindowTitle("IAP_Window >_<");
}

IAP_Window::~IAP_Window()
{
    delete ui;
}

void IAP_Window::iap_Edit_show(QString str) //界面输出
{
    this->ui->state_Edit->appendPlainText(str);
}

void IAP_Window::on_send_all_Button_clicked()//发送全部
{
    ui->state_Edit->appendPlainText("发送文件F_F");
    emit send_all_signal();
}

void IAP_Window::on_clear_Button_clicked()//清空窗口
{
    ui->state_Edit->clear();
}

void IAP_Window::on_search_file_Button_clicked() //文件选择
{
    ui->state_Edit->appendPlainText("选择文件");
    path = QFileDialog::getOpenFileName(this,"open","../",
    "BIN(*.bin);;TXT(*.txt);;SOURCE(*.c *.cpp *.h);;ALL(*.*)");
    if(path.isEmpty() == false)
    {
        ui->file_path_Edit->setText(path);
        emit change_mypath(path);
        ui->state_Edit->appendPlainText("选择成功^_^");
    }else ui->state_Edit->appendPlainText("选择失败TAT");
}

void IAP_Window::on_information_Buttton_clicked() //文件信息
{
    if(path.isEmpty()==false){
    QFileInfo info(path);
    ui->state_Edit->appendPlainText("文件名字："+info.fileName());
    ui->state_Edit->appendPlainText("文件后缀："+info.suffix());
    ui->state_Edit->appendPlainText("文件大小："+QString::number(info.size())+"  Byte;"+QString::number(info.size()/1024)+" KB");
    ui->state_Edit->appendPlainText("文件创建时间："+info.created().toString("yyyy-MM-dd")+"\r\n");
    }else ui->state_Edit->appendPlainText("未找到文件QAQ");
}

void IAP_Window::iap_show_clear(){
    ui->state_Edit->clear();
}

void IAP_Window::on_send_all_Button_2_clicked()  //申请分段定时发送
{
    if(path.isEmpty()==false){
    ui->state_Edit->appendPlainText("正在请求发送");
    emit send_pertime_request(ui->Byte_size_Box->currentText().toInt(),ui->time_Edit_line->text().toUInt());
    //qDebug()<<ui->Byte_size_Box->currentText().toInt()<<"\r\n"<<ui->time_Edit_line->text().toUInt();
    }else ui->state_Edit->appendPlainText("未找到文件QAQ");
}

void  IAP_Window::close_time_request_Button()//关闭按钮防止误触
{
    ui->send_all_Button_2->setEnabled(false);
}
void  IAP_Window::open_time_request_Button()//关闭按钮防止误触
{
    ui->send_all_Button_2->setEnabled(true);
}

void IAP_Window::on_stop_send_Button_clicked()//请求关闭定时器
{
    emit stop_send_request();
}
