#include "widget.h"
#include "ui_widget.h"
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <QFile>

#define maxsize  2000
static int count = 0, have_send_count=0;static bool End_flag = false;
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setWindowTitle("鹤星串口助手Ver0.2");
    ui->openBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->closeBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->sendBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->clearBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->clear2Bt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->radioButton_3->setChecked(true);per_Byte=0;

    connect(&iap_window,&IAP_Window::send_all_signal,this,&Widget::on_buttonSend_clicked);//关联子窗口全部发送
    connect(&iap_window,&IAP_Window::change_mypath,this,&Widget::change_path); //路径变更请求
    connect(&iap_window,&IAP_Window::send_pertime_request,this,&Widget::set_send_pertime_timer); //设置pertime定时器请求
    connect(&iap_window,&IAP_Window::stop_send_request,this,&Widget::answer_stop_send_request);

    QTimer *timer = new QTimer(this);//时间显示关联
    connect(timer,SIGNAL(timeout()),SLOT(time_update()));timer->start(1000);

    frame_window = new frameWindow(this);//图形显示
    frame_show_timer = new QTimer(this);
    connect(frame_show_timer,&QTimer::timeout,this,&Widget::deal_frame_time_out);
    connect(frame_window,&frameWindow::start_frame_timer_request,this,&Widget::answer_start_frame_show_request);
    connect(frame_window,&frameWindow::stop_frame_timer_request,this,&Widget::answer_stop_frame_show_request);
    connect(frame_window,&frameWindow::change_frame_timer_request,this,&Widget::answer_change_frame_show_request);

    ui->blog_label->setOpenExternalLinks(true);//个人链接
    ui->blog_label->setText("<a href=\"https://blog.csdn.net/whx190418?spm=1018.2226.3001.5343/\">作者个人博客点这里^_^");

    send_text_timer = new QTimer(this); //定时发送文本内容
    connect(send_text_timer,&QTimer::timeout,this,&Widget::on_sendBt_released);

    send_pertime_timer = new QTimer(this);
    connect(send_pertime_timer,&QTimer::timeout,this,&Widget::send_per_bin_file);

    QMessageBox::information(this,"Hello user~^_^","目前尚处开发阶段，还有很多不完善的地方，欢迎与作者联系^_^");

    //QStringList serialNamePort;//串口初始化
    serialport = new QSerialPort(this);
    connect(serialport, SIGNAL(readyRead()),this,SLOT(serial_deal_port()));
    foreach(const QSerialPortInfo &info , QSerialPortInfo::availablePorts())
    {
        serialNamePort<<info.portName();
    };
    ui->serialCb->addItems(serialNamePort);

    ui->send_progressBar->setValue(0);

    QTimer *serial_check_timer = new QTimer(this);
    connect(serial_check_timer,&QTimer::timeout,this,&Widget::serial_check);
    serial_check_timer->start(1000);//每秒检测一次

    /*for (int i=0;i<maxsize;i++) {
        data1_point_stream.push_back(QPointF(i,0));
    }*/
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_openBt_clicked()   //串口打开
{
    QSerialPort::BaudRate baud_rate;
    QSerialPort::DataBits data_bits;
    QSerialPort::StopBits stop_bits;
    QSerialPort::Parity   check_bits;

    if(ui->baundrateCb->currentText() == "4800")baud_rate = QSerialPort::Baud4800;
    else if(ui->baundrateCb->currentText() == "9600")baud_rate = QSerialPort::Baud9600;
    else if(ui->baundrateCb->currentText() == "115200")baud_rate = QSerialPort::Baud115200;
    else if(ui->baundrateCb->currentText() == "19200")baud_rate = QSerialPort::Baud19200;
    else if(ui->baundrateCb->currentText() == "38400")baud_rate = QSerialPort::Baud38400;
    else baud_rate = QSerialPort::Baud57600;

    if(ui->dataCb->currentText() == "5")data_bits = QSerialPort::Data5;
    else if(ui->dataCb->currentText() == "6")data_bits = QSerialPort::Data6;
    else if(ui->dataCb->currentText() == "7")data_bits = QSerialPort::Data7;
    else data_bits = QSerialPort::Data8;

    if(ui->stopCb->currentText() == "1")stop_bits = QSerialPort::OneStop;
    else if(ui->stopCb->currentText() == "1.5")stop_bits = QSerialPort::OneAndHalfStop;
    else stop_bits = QSerialPort::TwoStop;

    if(ui->checkCb->currentText() == "none")check_bits = QSerialPort::NoParity;
    else if(ui->checkCb->currentText() == "odd")check_bits = QSerialPort::OddParity;
    else check_bits = QSerialPort::EvenParity;

    serialport->setPortName(ui->serialCb->currentText());
    serialport->setBaudRate(baud_rate);
    serialport->setDataBits(data_bits);
    serialport->setStopBits(stop_bits);
    serialport->setParity(check_bits);

    serialport->setFlowControl(QSerialPort::NoFlowControl);

    if(serialport->open(QIODevice::ReadWrite) == true )
    {
        ui->openBt->setText("已打开");
        ui->openBt->setStyleSheet("background-color:rgb(255,0,0)");
    }
}


void Widget::on_closeBt_pressed()   //关闭按钮
{
    serialport->close();ui->openBt->setText("打开");
    ui->openBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->closeBt->setStyleSheet("background-color:rgb(255,0,0)");
}

void Widget::on_closeBt_released()  //关闭按钮
{
    ui->closeBt->setStyleSheet("background-color:rgb(255,255,255)");
}

void Widget::serial_deal_port()  //接收buff
{
    QString Buff;
    //Buff = QString (serialport->readAll().toStdString().c_str());//中文乱码
    QByteArray Array = serialport->readAll();
    receive_number += Array.size();


    if(frame_window->isHidden() == false){
        if(check_data(Array) == true ){
            deal_send_data(&Array);     //去除协议帧
        }else ui->recvEdit->appendPlainText("协议错误");
    }else{
        if(rece_show_flag==0)Buff = QString::fromLocal8Bit(Array);
    }
    if(rece_show_flag==1)Buff = QString(Array.toHex(' '));
    if(ui->checkBox_2->isChecked()==true){
        QString framedata = QDateTime::currentDateTime().toString("yyyy-mm-dd hh:mm:ss");//时间戳
        ui->recvEdit->appendPlainText('['+framedata+']');
    }
    ui->recvEdit->insertPlainText(Buff.toUpper());
    ui->stateEdit->setText("Tx:"+QString::number(send_number)+"   Rx:"+QString::number(receive_number));

}

void Widget::on_sendBt_pressed()  //发送按钮
{
    ui->sendBt->setStyleSheet("background-color:rgb(255,0,0)");
}

void Widget::on_sendBt_released()  //发送按钮
{
    ui->sendBt->setStyleSheet("background-color:rgb(255,255,255)");
    if(serialport->isOpen() == true){
        send_number += ui->sendEdit->toPlainText().size();
        ui->stateEdit->setText("Tx:"+QString::number(send_number)+"   Rx:"+QString::number(receive_number));
        serialport->write(ui->sendEdit->toPlainText().toLocal8Bit());
        if(ui->checkBox->isChecked()==true)serialport->write("\r\n");
    }else ui->recvEdit->appendPlainText("请先打开串口>~< >~<");
}

void Widget::on_clearBt_pressed()  //清除按钮1
{
    ui->clearBt->setStyleSheet("background-color:rgb(255,0,0)");
}

void Widget::on_clearBt_released()  //清除按钮1
{
    ui->clearBt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->recvEdit->clear();
}

void Widget::on_clear2Bt_pressed()//清除按钮2
{
    ui->clear2Bt->setStyleSheet("background-color:rgb(255,0,0)");
}

void Widget::on_clear2Bt_released()//清除按钮2
{
    ui->clear2Bt->setStyleSheet("background-color:rgb(255,255,255)");
    ui->sendEdit->clear();
}

void Widget::on_radioButton_clicked()  //hex显示
{
   if(rece_show_flag!=1)
   {
       temp = ui->recvEdit->toPlainText();
       QString buff = ui->recvEdit->toPlainText().toUtf8().toHex(' ').toUpper();
       ui->recvEdit->clear();
       rece_show_flag=1;
       ui->recvEdit->appendPlainText(buff);
   }
}

void Widget::on_radioButton_3_clicked()  //C显示
{
    if(rece_show_flag!=0)
    {
        rece_show_flag=0;
        ui->recvEdit->clear();
        ui->recvEdit->appendPlainText(temp);
    }

}

void Widget::on_buttonRead_clicked()  //打开文本
{
    mypath = QFileDialog::getOpenFileName(this,"open","../",
    "TXT(*.txt);;SOURCE(*.c *.cpp *.h);;ALL(*.*)");
    if(mypath.isEmpty() == false)
    {
         QFile file(mypath);ui->pathEdit->setText(mypath);
         bool IsOk = file.open(QIODevice::ReadOnly);
         if(IsOk){
         ui->recvEdit->insertPlainText(file.readAll());file.close();
        }
    }
}

void Widget::on_recvEdit_textChanged()  //滑动条滑动
{
    ui->recvEdit->moveCursor(QTextCursor::End);
}

void Widget::on_buttonRead_2_clicked()
{
    mypath = QFileDialog::getOpenFileName(this,"open","../",
                                                "Images (*.png *.bmp *.jpg *.tif *.GIF )");
    QLabel* labelImage = new QLabel(this, Qt::Dialog);
    labelImage->setWindowTitle("图片窗口>3<");
    QFileInfo file(mypath);
    if(file.exists())
      {
          QImage image;
          image.load(mypath);ui->pathEdit->setText(mypath);
          labelImage->resize(500,500);
          labelImage->setPixmap(QPixmap::fromImage(image.scaled(500, 500, Qt::KeepAspectRatio)));
          labelImage->setScaledContents(true);
          labelImage->show();
      }
}

void Widget::on_buttonRead2_clicked()    //选择文件
{
    mypath = QFileDialog::getOpenFileName(this,"open","../",
                      "TXT(*.txt);;SOURCE(*.c *.cpp *.h);;BIN(*.bin);;ALL(*.*)");
    ui->pathEdit->setText(mypath);
    /*QFile readfile(mypath);
    if(!readfile.open(QIODevice::ReadOnly | QIODevice::Text)) {
           QMessageBox::information(this,"错误","无法打开文件");
       }
    while (!readfile.atEnd()) {
        binByteArray.append(readfile.readLine(ui->seperatebox->currentText().toInt()));
    }*/
    //ui->recvEdit->setPlainText(binByteArray.toHex(' ').toUpper());//hex读取显示
    //ui->recvEdit->insertPlainText(QString::number(readfile.size()));//文件长度
}



void Widget::on_pushButton_clicked() // 获取文件信息
{
    if(mypath.isEmpty()==false){
    QFileInfo info(mypath);
    ui->recvEdit->appendPlainText("文件名字："+info.fileName());
    ui->recvEdit->appendPlainText("文件后缀："+info.suffix());
    ui->recvEdit->appendPlainText("文件大小："+QString::number(info.size())+"  Byte;"+QString::number(info.size()/1024)+" KB");
    ui->recvEdit->appendPlainText("文件创建时间："+info.created().toString("yyyy-MM-dd")+"\r\n");
    }else ui->recvEdit->appendPlainText("未找到文件QAQ");
}



void Widget::on_buttonSend_clicked()  //发送文件
{
    if(serialport->isOpen() == true){
        QFile readfile(mypath);
        QFileInfo readfile_info(mypath);
        if(readfile.open(QIODevice::ReadOnly) == true){
            ui->buttonSend->setEnabled(false);  //关闭按钮
            int i = 0;
            ui->send_progressBar->setRange(0,readfile_info.size());
            ui->send_progressBar->setValue(i);
            i = serialport->write(readfile.readAll());
            if(i == readfile_info.size()){
                QMessageBox::information(this,"^_^","发送成功");
                send_number += readfile_info.size();
            }
            readfile.close();
        }else
        {
            if(iap_window.isHidden()==false)iap_window.iap_Edit_show("未找到文件QAQ");
            else ui->recvEdit->appendPlainText("未找到文件QAQ");
        }
    }else QMessageBox::information(this,"串口未找到>~<","请检查串口是否打开>~");

    ui->buttonSend->setEnabled(true);  //打开按钮
}

void Widget::on_saveButton_clicked()  //保存文本
{
    QFileDialog filedialog;
    QString filename = filedialog.getSaveFileName(this,"Open file","/data","Text File(*.txt");
    if(filename == ""){
        return;
    }
    QFile file(filename);
    if(!file.open((QIODevice::WriteOnly|QIODevice::Text))){
        QMessageBox::warning(this,"警告","打开文件失败TAT");return;
    }else{
        QTextStream textStream(&file);
        QString str = ui->recvEdit->toPlainText();
        textStream<<str;
        QMessageBox::information(this,"提示","保存文件成功^_^");
        file.close();
    }
}


void Widget::time_update() //显示系统时间的功能
{
    QDateTime time = QDateTime::currentDateTime();
    QString str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
    ui->time_label->setText(str);
}


void Widget::on_IAP_Button_clicked()   //IAP显示
{
    iap_window.show();iap_window.iap_show_clear();
    iap_window.iap_Edit_show("    这是IAP界面.用于程序的在线升级^_^\r\n"
    "您可以对文件以设定的形式进行发送\r\n>_< >_< >_<\r\n"
    "当前串口号:"+ui->serialCb->currentText()+"当前波特率："+ui->baundrateCb->currentText());
}

void Widget::change_path(QString a)  //path转化
{
    mypath = a;
    ui->pathEdit->setText(mypath);
}


void Widget::on_timer_Box_stateChanged(int arg1) //定时文本发送
{
    if(arg1 == Qt::Checked){
        if(serialport->isOpen() == false){
            QMessageBox::information(this,"无效TAT","请打开串口>~<");
            ui->timer_Box->setCheckState(Qt::Unchecked);
        }
        else if(ui->timer_lineEdit->text()==NULL){
            QMessageBox::information(this,"无效TAT","请输入参数>~<");
            ui->timer_Box->setCheckState(Qt::Unchecked);
        }
        else send_text_timer->start(ui->timer_lineEdit->text().toInt());
    }else send_text_timer->stop();
}

void Widget::set_send_pertime_timer(int Byte,int ms){
    //qDebug()<<Byte<<"\r\n"<<ms;
    QFileInfo info(mypath);
    file_lenth = info.size(); //获取文件长度
    iap_window.iap_Edit_show(QString("文件长度为%1 字节，共%2 KB").arg(file_lenth).arg(file_lenth/1024));
    QFile *binFile = new QFile(mypath);
    if(binFile->open(QIODevice::ReadOnly) == true)
    {
        binByteArray = binFile->readAll();
        iap_window.iap_Edit_show("开始分包发送文件");
        send_pertime_timer->start(ms);
        iap_window.iap_Edit_show("定时器开启>.<");
        iap_window.close_time_request_Button();
        per_Byte = Byte;
    }else iap_window.iap_Edit_show("文件打开失败TAT");
}

void Widget::send_per_bin_file() //分包发送
{
    //qDebug()<<"ok";
    //static int count = 0, have_send_count=0;static bool End_flag = false;
    if(binByteArray.isEmpty() == false)
    {
        count++;
        if(count > file_lenth/(per_Byte*1024))
        {
            iap_window.iap_Edit_show(QString("最后第%1次发送,共%2字节,%3KB").arg(count).arg(file_lenth - have_send_count).arg((file_lenth-have_send_count)/1024));
            serialport->write(binByteArray.mid(have_send_count,file_lenth - have_send_count));
            End_flag = true;
        }else{
            iap_window.iap_Edit_show(QString("开始第%1次发送").arg(count));
            serialport->write(binByteArray.mid(have_send_count,per_Byte*1024));
            have_send_count += per_Byte*1024;
            iap_window.iap_Edit_show(QString("完成第%1次发送,共%2字节,已发送%3字节").arg(count).arg(per_Byte).arg(have_send_count));
        }

    }else iap_window.iap_Edit_show("文件为空TAT");
    if(End_flag == true){
        End_flag = false;
        send_pertime_timer->stop();
        iap_window.iap_Edit_show("定时器关闭");
        iap_window.open_time_request_Button();
        iap_window.iap_Edit_show(QString("IAP发送完成>_<,共发送%1字节").arg(file_lenth));
        count=0;have_send_count=0;
    }
}


void Widget::answer_stop_send_request(){
    if(send_pertime_timer->isActive()){
        send_pertime_timer->stop();count = 0; have_send_count=0;End_flag = false;
        iap_window.iap_Edit_show("定时器关闭，发送中止QAQ");
        iap_window.open_time_request_Button();
    }else iap_window.iap_Edit_show("定时器还未开启>0<");
}

void Widget::on_IAP_Button_2_clicked()   //波形显示调用
{
    //QMessageBox::information(this,"><","更多功能敬请期待>&<");
    frame_window->show();
}

void Widget::serial_check(){
    QStringList newPortStringList;
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
            newPortStringList += info.portName();
    if(newPortStringList.size() != serialNamePort.size())
        {
            serialNamePort = newPortStringList;
            ui->serialCb->clear();
            ui->serialCb->addItems(serialNamePort);
        }
}
////串口状态改变关闭串口/////////
void Widget::on_baundrateCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}

void Widget::on_serialCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}

void Widget::on_dataCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}

void Widget::on_stopCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}

void Widget::on_checkCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}

void Widget::on_filectrlCb_currentIndexChanged(int )
{
    on_closeBt_pressed();on_closeBt_released();
}
////////////////////////////////////////
bool start_flag = false;
bool Widget::check_data(QByteArray data){
    qDebug()<<data.toHex(' ');
    if((data[0] == 'H')&&(data[1] == 'X')&&(start_flag == true)
    &&(data[data.size()-2] == 'O')&&(data[data.size()-1] == 'K')){
        return true;
    }else return false;
}

int bytesToInt(QByteArray bytes) {
    int addr = bytes[0] & 0x000000FF;
    addr |= ((bytes[1] << 8) & 0x0000FF00);
    addr |= ((bytes[2] << 16) & 0x00FF0000);
    addr |= ((bytes[3] << 24) & 0xFF000000);
    return addr;
}

void Widget::deal_send_data(QByteArray *datap){
    //QVector<QPointF>show_vector;
    datap->remove(0,2);
    datap->remove(datap->size()-2,2);
    //qDebug()<<bytesToInt( datap->mid(0,4));
    //qDebug()<<bytesToInt( datap->mid(4,8));
    //qDebug()<<bytesToInt( datap->mid(8,12));
    /*if(data1_stream.size()>=maxsize)
    {
        data1_stream.pop_front();
        data1_stream.push_back(bytesToInt( datap->mid(0,4)));
    }else data1_stream.push_back(bytesToInt( datap->mid(0,4)));
    for (int i=0;i<data1_stream.size();i++) {
        show_vector.push_back(QPoint(i,data1_stream[i]));
    }
    frame_window->series->replace(show_vector);*/
    frame_window->ui->data1_label->setText(QString("%1").arg(bytesToInt( datap->mid(0,4))));
    if(data1_point_stream.size()>=maxsize){
        data1_point_stream.pop_front();
        data1_point_stream.push_back(QPointF(t_cont,bytesToInt( datap->mid(0,4))));
    }else data1_point_stream.push_back(QPointF(t_cont,bytesToInt( datap->mid(0,4))));
}

void Widget::deal_frame_time_out()
{
    t_cont++;
    if(frame_window->ui->data1_checkBox->isChecked()){
        frame_window->series->replace(data1_point_stream);
    }else frame_window->series->clear();
    if(t_cont>1000){
        frame_window->axisX->setRange(t_cont-1000,t_cont);
    }
}

void Widget::answer_start_frame_show_request(int K){
    if(serialport->isOpen() == true){
    frame_show_timer->start(1000/K);
    frame_window->ui->timer_label->setText(QString("单位%1ms").arg(1000/K));
    start_flag = true;
    data1_point_stream.clear();
    t_cont=0;frame_window->axisX->setRange(0,1000);
    }else
    {
        QMessageBox::information(this,"><","未找到串口");
        frame_window->ui->pushButton->setStyleSheet("background-color:rgb(255,255,255)");
        frame_window->ui->pushButton->setText("开始显示");
    }
}
void Widget::answer_stop_frame_show_request(){
    frame_show_timer->stop();
}
void Widget::answer_change_frame_show_request(int K){
    frame_show_timer->start(1000/K);
    frame_window->ui->timer_label->setText(QString("单位%1ms").arg(1000/K));
}
