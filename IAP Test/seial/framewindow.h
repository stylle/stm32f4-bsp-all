#ifndef FRAMEWINDOW_H
#define FRAMEWINDOW_H

#include <QMainWindow>
#include <QtCharts>

#include <QValueAxis>
namespace Ui {
class frameWindow;
}

class frameWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit frameWindow(QWidget *parent = nullptr);
    ~frameWindow();
    void Chart_init();
    Ui::frameWindow *ui;
    QSplineSeries *series;                     //线
    QSplineSeries *series2;                     //线
    QSplineSeries *series3;                     //线
    QValueAxis *axisX;                    //轴
    QValueAxis *axisY;

signals:
    void start_frame_timer_request(int);
    void stop_frame_timer_request();
    void change_frame_timer_request(int);
private slots:
    void on_pushButton_clicked();

    void on_spinBox_valueChanged(int arg1);

private:
    //Ui::frameWindow *ui;

    QChart *chart;                           //画布
};

#endif // FRAMEWINDOW_H
