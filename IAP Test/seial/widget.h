#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSerialPort>
#include <QString>
#include <QDebug>
#include <QTimer>
#include <QDateTime>
#include <iap_window.h>
#include "framewindow.h"
#include "ui_framewindow.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QSerialPort *serialport;

    QStringList serialNamePort;//串口名单

    int rece_show_flag=0;

    int receive_number=0;

    int send_number=0;

    QString mypath;

    QByteArray binByteArray;

    QString temp;  //显示部分转码寄存

    int per_Byte=0;

    int file_lenth=0;

    //QVector<int> data1_stream;

    QVector<QPointF> data1_point_stream;

    qint64 t_cont=0;
private slots:
    void on_openBt_clicked();

    void on_closeBt_pressed();

    void on_closeBt_released();

    void serial_deal_port();

    void on_sendBt_pressed();

    void on_sendBt_released();

    void on_clearBt_pressed();

    void on_clearBt_released();

    void on_clear2Bt_pressed();

    void on_clear2Bt_released();

    void on_radioButton_clicked();

    void on_radioButton_3_clicked();

    void on_buttonRead_clicked();

    void on_recvEdit_textChanged();

    void on_buttonRead_2_clicked();

    void on_buttonRead2_clicked();

    void on_pushButton_clicked();

    void on_buttonSend_clicked();

    void on_saveButton_clicked();

    void time_update();

    void on_IAP_Button_clicked();

    void change_path(QString);

    void on_timer_Box_stateChanged(int arg1);

    void set_send_pertime_timer(int,int);

    void send_per_bin_file();

    void answer_stop_send_request();

    void on_IAP_Button_2_clicked();//frame

    void serial_check();

    void on_baundrateCb_currentIndexChanged(int);

    void on_serialCb_currentIndexChanged(int index);

    void on_dataCb_currentIndexChanged(int index);

    void on_stopCb_currentIndexChanged(int index);

    void on_checkCb_currentIndexChanged(int index);

    void on_filectrlCb_currentIndexChanged(int index);

    bool check_data(QByteArray data);

    void deal_send_data(QByteArray *datap);

    void answer_start_frame_show_request(int);

    void answer_stop_frame_show_request();

    void answer_change_frame_show_request(int);

    void deal_frame_time_out();
private:
    Ui::Widget *ui;
    IAP_Window iap_window;
    frameWindow *frame_window;
    QTimer *send_text_timer; //文本定时器
    QTimer *send_pertime_timer;//文件定时器
    QTimer *frame_show_timer;//图像定时器
};
#endif // WIDGET_H
