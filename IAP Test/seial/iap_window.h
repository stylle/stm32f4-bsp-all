#ifndef IAP_WINDOW_H
#define IAP_WINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QDialog>
#include <QString>
#include <QFileDialog>
#include <QTime>
#include <QtDebug>
namespace Ui {
class IAP_Window;
}

class IAP_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit IAP_Window(QWidget *parent = nullptr);
    ~IAP_Window();
    void iap_Edit_show(QString str);
    void iap_show_clear();
    void close_time_request_Button();
    void open_time_request_Button();
    QString path;
signals:
    void send_all_signal();
    void change_mypath(QString);
    void send_pertime_request(int Byte, int ms);
    void stop_send_request();
private slots:

    void on_send_all_Button_clicked();

    void on_clear_Button_clicked();

    void on_search_file_Button_clicked();

    void on_information_Buttton_clicked();

    void on_send_all_Button_2_clicked();

    void on_stop_send_Button_clicked();

private:
    Ui::IAP_Window *ui;
};

#endif // IAP_WINDOW_H
