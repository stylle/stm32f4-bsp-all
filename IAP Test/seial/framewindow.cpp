#include "framewindow.h"
#include "ui_framewindow.h"
#include <QtMath>

frameWindow::frameWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::frameWindow)
{
    ui->setupUi(this);
    Chart_init();
    ui->spinBox->setValue(1000);
}

frameWindow::~frameWindow()
{
    delete ui;
}

void frameWindow::Chart_init(){
    QPen peny(Qt::darkRed , 3 , Qt::SolidLine , Qt::RoundCap , Qt::RoundJoin);
    chart = new QChart();
    series = new QSplineSeries();
    axisX = new QValueAxis();
    axisY = new QValueAxis();

    chart->legend()->hide();        //隐藏图例
    chart->addSeries(series);       //把线添加到chart中
    axisX->setTickCount(10);   //设置坐标轴格数
    axisY->setTickCount(5);


    axisX->setRange(0,1000);
    axisY->setRange(0,100);
    axisX->setTitleText("时间");
    axisY->setLinePenColor(QColor(Qt::darkBlue));       //设置坐标轴颜色
    axisY->setGridLineColor(QColor(Qt::darkBlue));
    axisX->setLinePenColor(QColor(Qt::darkBlue));       //设置坐标轴颜色
    axisX->setGridLineColor(QColor(Qt::darkBlue));

    axisX->setGridLineVisible(false);        //设置Y轴网格不显示
    axisY->setGridLineVisible(false);        //设置Y轴网格不显示
    axisY->setLinePen(peny);
    axisX->setLinePen(peny);

    chart->addAxis(axisX , Qt::AlignBottom);
    chart->addAxis(axisY , Qt::AlignLeft);
    chart->setTheme(QtCharts::QChart::ChartThemeBlueCerulean);//设置主题
    series->attachAxis(axisX);
    series->attachAxis(axisY);

    /*series2->attachAxis(axisX);
    series2->attachAxis(axisY);

    series3->attachAxis(axisX);
    series3->attachAxis(axisY);*/

    axisY->setTitleText("para1");

    QChartView *chartview = new QChartView(chart);//创建图表显示
    ui->frame_Layout->addWidget(chartview);
}
static bool frame_is_active =false;
void frameWindow::on_pushButton_clicked()
{
    if(frame_is_active == false){
        frame_is_active =true;
        ui->pushButton->setStyleSheet("background-color:rgb(255,0,0)");
        ui->pushButton->setText("停止显示");
        emit start_frame_timer_request(ui->spinBox->value());
    }else {
        ui->pushButton->setStyleSheet("background-color:rgb(255,255,255)");
        ui->pushButton->setText("开始显示");
        emit stop_frame_timer_request();
        frame_is_active = false;
    }

}

void frameWindow::on_spinBox_valueChanged(int arg1)
{
    emit change_frame_timer_request(arg1);
}
