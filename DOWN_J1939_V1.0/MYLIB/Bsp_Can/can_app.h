#ifndef __BSP_CAN_H
#define __BSP_CAN_H	    

#include "can_config.h"

#pragma pack (1) /*指定按1字节对齐*/

typedef enum {
    R_POWER     = 0x05,
    R_INPUT     = 0x05,
    R_FLAG      = 0x05,
    R_TIME      = 0x05,
    R_HEARTBEAT = 0x05,
    R_LOOP14    = 0x05,
    R_LOOP58    = 0x05,
    R_LAMP      = 0x05,
    R_ALL       = 0X05, // 广播帧
}ENUM_R;

typedef enum{
    PF_POWER =  0X15,
    PF_INPUT =  0X16, // 
    PF_TIME =   0X17, // 
    PF_HEARTBEAT= 0X18,
    PF_LOOP14   = 0X19, // 
    PF_LOOP58   = 0X1A,
    PF_FLAG     = 0X1B,
    PF_LAMP     = 0X1C,
    PF_ALL      = 0X1D, // 广播帧
}ENUM_PF;


#define CAN_POWER_SUPPLY_DLC 8 // 电源相关		
#define CAN_INPUT_VC_DLC    8// 充电电池相关		
#define CAN_TIME_DLC        4 // 电池和应急相关			
#define CAN_LOOP_VOL_CUR_14_DLC 8// 1-4路回路电压电流   
#define CAN_LOOP_VOL_CUR_58_DLC 8// 1-4路回路电压电流
#define CAN_FLAG_DLC 4  // 状态相关


typedef struct{
	uint8_t MAIN_VOL_H;					// 主电电压高字节
	uint8_t MAIN_VOL_L;					// 主电电压低字节
	uint8_t OUTPUT_VOL_H;				// 输出电压高字节
	uint8_t OUTPUT_VOL_L;				// 输出电压低字节
	uint8_t OUTPUT_CUR_H;				// 输出电流高字节
	uint8_t OUTPUT_CUR_L;				// 输出电流低字节
	uint8_t BATTERY_VOL_ALL_H; 	        // 电池总电压高字节
	uint8_t BATTERY_VOL_ALL_L; 	        // 电池总电压低字节
	uint8_t sendStatus;					// 可发送标志在发送之前需要置位
}CAN_POWER_SUPPLY;                      // 电源相关


typedef struct{
	uint8_t INPUT_VOL_H;//充电电压值高字节
	uint8_t INPUT_VOL_L;//充电电压值低字节
	uint8_t INPUT_CUR_H;//充电电流高字节
	uint8_t INPUT_CUR_L;//充电电流低字节
	uint8_t BATTERY_VOL_1_H;//电池1压高字节
	uint8_t BATTERY_VOL_1_L;//电池1压低字节
	uint8_t BATTERY_VOL_2_H;//电池2压高字节
	uint8_t BATTERY_VOL_2_L;//电池2压低字节
	uint8_t sendStatus;			// 可发送标志在发送之前需要置位
}CAN_INPUT_VC;// 充电电池相关

typedef struct{
	uint8_t LOOP_VOL_CUR_1_H;//回路1电压/电流
	uint8_t LOOP_VOL_CUR_1_L;//回路1电压/电流
	uint8_t LOOP_VOL_CUR_2_H;//回路2电压/电流
	uint8_t LOOP_VOL_CUR_2_L;//回路2电压/电流
	uint8_t LOOP_VOL_CUR_3_H;//回路3电压/电流
	uint8_t LOOP_VOL_CUR_3_L;//回路3电压/电流
	uint8_t LOOP_VOL_CUR_4_H;//回路4电压/电流
	uint8_t LOOP_VOL_CUR_4_L;//回路4电压/电流
	uint8_t sendStatus;		 // 可发送标志在发送之前需要置位
}CAN_LOOP_VOL_CUR_14; // 1-4路回路电压电流

typedef struct{
	uint8_t LOOP_VOL_CUR_5_H;//回路5电压/电流
	uint8_t LOOP_VOL_CUR_5_L;//回路5电压/电流
	uint8_t LOOP_VOL_CUR_6_H;//回路6电压/电流
	uint8_t LOOP_VOL_CUR_6_L;//回路6电压/电流
	uint8_t LOOP_VOL_CUR_7_H;//回路7电压/电流
	uint8_t LOOP_VOL_CUR_7_L;//回路7电压/电流
	uint8_t LOOP_VOL_CUR_8_H;//回路8电压/电流
	uint8_t LOOP_VOL_CUR_8_L;//回路8电压/电流
	uint8_t sendStatus;		// 可发送标志在发送之前需要置位
}CAN_LOOP_VOL_CUR_58;       // 5-8路回路电压电流

typedef struct{
	uint8_t BATTERY_VOL_3_H;//电池3压高字节
	uint8_t BATTERY_VOL_3_L;//电池3压低字节
	uint8_t HEGTM_H;	    //应急时间
	uint8_t HEGTM_L;		//应急时间
	uint8_t sendStatus;		// 可发送标志在发送之前需要置位
}CAN_TIME;                  // 电池和应急相关

typedef struct{
	uint8_t FAULT_FLAG_H;	//故障状态
	uint8_t FAULT_FLAG_L;	//故障状态
	uint8_t WORK_FLAG_H;	//工作状态
	uint8_t WORK_FLAG_L;	//工作状态
	uint8_t sendStatus;		//可发送标志在发送之前需要置位
}CAN_FLAG;                  // 状态相关

// 重新发送标志结构体
typedef union
{
	uint8_t resetSend;
	struct{
		uint8_t power;
		uint8_t input;
		uint8_t vout14;
		uint8_t vout58;
		uint8_t time;
		uint8_t flag;
	}ResetData;
}RESET_SEND;	

// stm32为小端存储
typedef union{
	unsigned int id;
	struct{
		unsigned short addrBit:16; 		// 地址 short长度
		unsigned char pfBit:8;				// pdu格式
		unsigned char dataPageBit:1;	// 数据页为0
		unsigned char retainBit:1;		// 保留位置为0
		unsigned char priorityBit:3; 	// 优先级
		unsigned char disableBit:3;		// 禁止设置的前三位
	}bit;
}TYPEID;

/* 查询命令表 */
#define CMD_CAN_QUERY					0X03					// 查询指令标识
#define CMD_CAN_QUERY_POWER 			0x01 					// 电源查询
#define CMD_CAN_QUERY_DEV_STATE 		0x02 					// 查询工作状态
#define CMD_CAN_QUERY_LAMP 				0x03 					// 查询灯具状态
#define CMD_CAN_QUERY_RANGE_LAMP 		0x04 					// 查询范围灯具状态
#define CMD_CAN_QUERY_VOL14 			0x05 					// 查询输出电压1-4路
#define CMD_CAN_QUERY_VOL58				0x06 					// 查询输出电压5-8路
#define CMD_CAN_QUERY_HEGTM 			0x07 					// 查询应急时间
#define CMD_CAN_QUERY_VERSION 			0x08 					// 查询版本信息

/* 修改|设置命令表 */
#define CMD_CAN_EDIT					0X06					// 修改指令标识
#define CMD_CAN_EDIT_LAMP 				0x01 					// 修改灯具状态 后应跟状态
#define CMD_CAN_EDIT_RESET 				0x02 					// 重启
#define CMD_CAN_EDIT_SILENCING			0x03		 			// 消音
#define CMD_CAN_EDIT_EVENT_MANUAL		0x04 					// 手动应急
#define CMD_CAN_EDIT_EVENT_MCHK 		0x05 					// 月检
#define CMD_CAN_EDIT_EVENT_FEMGY		0x07		 			// 强制启动开关

//装载灯具数据:传入第一个参数为id数组，第二个参数为灯具数据
void upCanLampChar(uint16_t *id,uint8_t *data);
//can数据轮询发送任务
void CanPoll(void);
//处理主机消息
void Process_host_msg(CanRxMsg ISR_RxMessage); 
//更新发送数据
void UpCanData(void);
//设置状态判断发送状态，这里传入0Xff表示全部进入发送判断状态
void UpDataSendStatus(uint8_t _u8F);
// 设置直接发送标志这里设置为0xff表示无需数据变化直接发送
void setCanInfoSendStaus(uint8_t _u8F);

#endif

