#ifndef __M_TYPE_H
#define __M_TYPE_H

#include "stdafx.h"
#include "string.h"
#include "can_app.h"
#include "can_drive.h"
#include "j1939.h"

// 本机地址
#define CAN_ADDR   203 // MyLable.nAddress

// 宏运算
#define MERGE_HL(H,L) 			((unsigned int)H<<8)|((unsigned int)L&0XFF)
#define GETINT16_HIGH(BYTE) 	((unsigned char)(BYTE>>8))
#define GETINT16_LOW(BYTE) 		((unsigned char)BYTE&0x00FF)

// 数据变化百分比范围
#define VC_PER 5
#define BATVC_PER 80

// 灯具数量
#define LAMP_NUM 40

// 应急时间发送间隔
#define HEGTM_SEND_TIME_S 60

//心跳时间间隔基数
#define HEARTBEAT_DELAY 	300  // 10ms为基数s

#define CAN1_ENABLE 			1 	// CAN2开启标志位	
#define CAN2_ENABLE 			1 	// CAN2开启标志位	
#define HEARTBEAT_EN 			1 	// 心跳使能

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

#endif


