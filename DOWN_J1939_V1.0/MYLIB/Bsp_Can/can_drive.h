#ifndef __CAN_DRIVE_H
#define __CAN_DRIVE_H

#include "can_config.h"

//extern J1939_MESSAGE J1939CanBuf;

// 初始化CAN波特率
typedef enum{
	BOUND10KB = 1,
	BOUND20KB,
	BOUND50KB,
	BOUND100KB,
	BOUND200KB,
	BOUND500KB,
	BOUND1M,
}CAN_BOUND;

// 开启CAN1
#if 	CAN1_ENABLE					
uint8_t CAN1_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode); // can初始化
uint8_t Can1_Send_Msg(CanTxMsg TxMessage);													//发送数据
#endif

// 开启CAN2
#if CAN2_ENABLE
uint8_t CAN2_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode); // can初始化
uint8_t Can2_Send_Msg(CanTxMsg TxMessage);													//发送数据
#endif

// 同时初始化两个can并重新初始化波特率
void CAN_Init_All(CAN_BOUND bound);

// can同时发送接口
uint8_t Can_Send(CanTxMsg TxMessage);

#endif
