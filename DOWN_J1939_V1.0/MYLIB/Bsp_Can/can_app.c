#include "can_app.h"

// 结构体数据变量
static CAN_POWER_SUPPLY power_supply; 
static CAN_INPUT_VC input_vc;
static CAN_TIME canTime;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8;
static CAN_FLAG canFlag;

// 结构体历史数据变量
static CAN_POWER_SUPPLY power_supply_tmp;
static CAN_INPUT_VC input_vc_tmp;
static CAN_TIME canTime_tmp;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4_tmp;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8_tmp;
static CAN_FLAG canFlag_tmp;

// Can_extid
TYPEID power_supply_extid   = {.id = ((R_POWER<<26)|(0x00<<25)|(0x00<<24)|(PF_POWER<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))};
TYPEID input_vc_extid 		= {.id = ((R_INPUT<<26)|(0x00<<25)|(0x00<<24)|(PF_INPUT<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff)) };
TYPEID canTime_extid 		= {.id = ((R_TIME<<26)|(0x00<<25)|(0x00<<24)|(PF_TIME<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))};
TYPEID VolCur_1To4_extid 	= {.id = ((R_LOOP14<<26)|(0x00<<25)|(0x00<<24)|(PF_LOOP14<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))}; 
TYPEID VolCur_5To8_extid 	= {.id = ((R_LOOP58<<26)|(0x00<<25)|(0x00<<24)|(PF_LOOP58<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))};
TYPEID canFlag_extid 		= {.id = ((R_FLAG<<26)|(0x00<<25)|(0x00<<24)|(PF_FLAG<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))};
TYPEID heartbeat_extid = {.id = ((R_HEARTBEAT<<26)|(0x00<<25)|(0x00<<24)|(PF_HEARTBEAT<<16)|(CAN_ADDR&0xff<<8)|(CAN_ADDR&0x00ff))};

uint8_t tp_send_buf[512];

/////////////////////////////////////////////////////////////////////////////////////////////////////
// 连接drive
/////////////////////////////////////////////////////////////////////////////////////////////////////

static int SEND_TYPE_CAN_POWER_SUPPLY_DATA(CAN_POWER_SUPPLY power_supply)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=power_supply_extid.id;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_POWER_SUPPLY_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&power_supply,CAN_POWER_SUPPLY_DLC * sizeof(uint8_t));
	return Can_Send(TxMessage);
}

static int SEND_TYPE_CAN_INPUT_VC_DATA(CAN_INPUT_VC input_vc)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.ExtId=input_vc_extid.id;						// 设置扩展标示符 
	TxMessage.DLC = CAN_INPUT_VC_DLC ;						// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&input_vc,CAN_INPUT_VC_DLC * sizeof(uint8_t));
	 
	return Can_Send(TxMessage);
}

static int SEND_TYPE_CAN_TIME_DATA(CAN_TIME canTime)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.ExtId=canTime_extid.id;						// 设置扩展标示符 
	TxMessage.DLC = CAN_TIME_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canTime,CAN_TIME_DLC * sizeof(uint8_t));
	
	return Can_Send(TxMessage);	
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(CAN_LOOP_VOL_CUR_14 loop_vol_cur_14)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.ExtId=VolCur_1To4_extid.id;						// 设置扩展标示符 
	TxMessage.DLC = CAN_LOOP_VOL_CUR_14_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_14,CAN_LOOP_VOL_CUR_14_DLC * sizeof(uint8_t));
	 
	return Can_Send(TxMessage);
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(CAN_LOOP_VOL_CUR_58 loop_vol_cur_58)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.ExtId=VolCur_5To8_extid.id;						// 设置扩展标示符 
	TxMessage.DLC = CAN_LOOP_VOL_CUR_58_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_58,CAN_LOOP_VOL_CUR_58_DLC * sizeof(uint8_t));
	 
	return Can_Send(TxMessage);
}
static int SEND_TYPE_CAN_FLAG_DATA(CAN_FLAG canflag)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.ExtId=canFlag_extid.id;				// 设置扩展标示符 
	TxMessage.DLC = CAN_FLAG_DLC;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canflag,CAN_FLAG_DLC * sizeof(uint8_t));
	 
	return Can_Send(TxMessage);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// 逻辑运行函数
/////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * can数据轮询发送任务
 */
static RESET_SEND resetSend; //  重发标志位也可以作为直接发送标志
void CanPoll(void)
{
	int byte_tmp[4]; 	// 用来保存高低字节合并的上一次数据
	int byte[4];		// 用于保存高低字节新的数据
    static uint32_t sendFlag = 1; // 轮询切片
	
	//电源相关 
	if (TRUE == power_supply.sendStatus && sendFlag	== 1000)
	{
        // 乘100是为了防止取百分比变化的时候小于100被编译器优化掉的小数的情况
		byte_tmp[0] = MERGE_HL(power_supply_tmp.MAIN_VOL_H,power_supply_tmp.MAIN_VOL_L);
		byte[0] = MERGE_HL(power_supply.MAIN_VOL_H,power_supply.MAIN_VOL_L);
		
		byte_tmp[1] = MERGE_HL(power_supply_tmp.OUTPUT_VOL_H,power_supply_tmp.OUTPUT_VOL_L);
		byte[1] = MERGE_HL(power_supply.OUTPUT_VOL_H,power_supply.OUTPUT_VOL_L);
		
		byte_tmp[2] = MERGE_HL(power_supply_tmp.OUTPUT_CUR_H,power_supply_tmp.OUTPUT_CUR_L);
		byte[2] = MERGE_HL(power_supply.OUTPUT_CUR_H,power_supply.OUTPUT_CUR_L);
		
		byte_tmp[3] =  MERGE_HL(power_supply_tmp.BATTERY_VOL_ALL_H,power_supply_tmp.BATTERY_VOL_ALL_L);
		byte[3] =  MERGE_HL(power_supply.BATTERY_VOL_ALL_H,power_supply.BATTERY_VOL_ALL_L);
		
        // 与上一次发送的数据做比对 VC_PER 为变化范围在CONFIG文件中修改比例
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) || 
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*BATVC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*BATVC_PER)) || resetSend.ResetData.power)
			{
				if(SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply)) //1为发送成功
				{
					// 发送数据成功过后、将标注位关闭并保存上次发送的数据
					memcpy(&power_supply_tmp, &power_supply ,sizeof(power_supply));
					power_supply.sendStatus = FALSE;
					resetSend.ResetData.power = FALSE;
				}else
				{
					resetSend.ResetData.power = TRUE; // 重发标志
				}
			}
	}
	
	//充电电池相关
	if (TRUE == input_vc.sendStatus && sendFlag == 2000)
	{
		byte_tmp[0] = MERGE_HL(input_vc_tmp.BATTERY_VOL_1_H,input_vc_tmp.BATTERY_VOL_1_L);
		byte[0] = MERGE_HL(input_vc.BATTERY_VOL_1_H,input_vc.BATTERY_VOL_1_L);
		
		byte_tmp[1] = MERGE_HL(input_vc_tmp.BATTERY_VOL_2_H,input_vc_tmp.BATTERY_VOL_2_L);
		byte[1] = MERGE_HL(input_vc.BATTERY_VOL_2_H,input_vc.BATTERY_VOL_2_L);
		
		byte_tmp[2] = MERGE_HL(input_vc_tmp.INPUT_CUR_H,input_vc_tmp.INPUT_CUR_L);
		byte[2] = MERGE_HL(input_vc.INPUT_CUR_H,input_vc.INPUT_CUR_L);
		
		byte_tmp[3] = MERGE_HL(input_vc_tmp.INPUT_VOL_H,input_vc_tmp.INPUT_VOL_L);
		byte[3] = MERGE_HL(input_vc.INPUT_VOL_H,input_vc.INPUT_VOL_L);
		
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*BATVC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*BATVC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.input)
		  {
				if(SEND_TYPE_CAN_INPUT_VC_DATA(input_vc))
				{
					// 保存上次数据
					memcpy(&input_vc_tmp, &input_vc ,sizeof(input_vc));
					input_vc.sendStatus = FALSE;
					resetSend.ResetData.input = FALSE;
				}else{
					resetSend.ResetData.input = TRUE; // 重发标志
				}
			}
	}
	//电池和应急相关 
	if (TRUE == canTime.sendStatus && sendFlag == 3000)
	{
		byte_tmp[0] = MERGE_HL(canTime_tmp.BATTERY_VOL_3_H,canTime_tmp.BATTERY_VOL_3_L);
		byte[0] = MERGE_HL(canTime.BATTERY_VOL_3_H,canTime.BATTERY_VOL_3_L);
		byte_tmp[1] = MERGE_HL(canTime_tmp.HEGTM_H,canTime_tmp.HEGTM_L);
		byte[1] = MERGE_HL(canTime.HEGTM_H,canTime.HEGTM_L);

			if(byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*BATVC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*BATVC_PER) ||
					byte[1] > (byte_tmp[1] + HEGTM_SEND_TIME_S) || byte[1] < (byte_tmp[1]) || resetSend.ResetData.time) 
			// 定时更新一次应急时间，如果状态从新计数表示重新应急需要发送一次
			{
				if(SEND_TYPE_CAN_TIME_DATA(canTime)) 
				{
					// 保存上次数据
					memcpy(&canTime_tmp, &canTime ,sizeof(canTime));
					canTime.sendStatus = FALSE;
					resetSend.ResetData.time = FALSE;
				}else{
					resetSend.ResetData.time = TRUE;
				}
			}
	}
	//1-4路回路电压电流
	if (TRUE == VolCur_1To4.sendStatus && sendFlag == 4000)
	{
			byte_tmp[0] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_1_H,VolCur_1To4_tmp.LOOP_VOL_CUR_1_L);
			byte[0] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_1_H,VolCur_1To4.LOOP_VOL_CUR_1_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_2_H,VolCur_1To4_tmp.LOOP_VOL_CUR_2_L);
			byte[1] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_2_H,VolCur_1To4.LOOP_VOL_CUR_2_L);
			
			byte_tmp[2] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_3_H,VolCur_1To4_tmp.LOOP_VOL_CUR_3_L);
			byte[2] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_3_H,VolCur_1To4.LOOP_VOL_CUR_3_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_4_H,VolCur_1To4_tmp.LOOP_VOL_CUR_4_L);
			byte[3] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_4_H,VolCur_1To4.LOOP_VOL_CUR_4_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.vout14)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4))
				{
					// 保存上次数据
					memcpy(&VolCur_1To4_tmp, &VolCur_1To4 ,sizeof(VolCur_1To4));
					VolCur_1To4.sendStatus = FALSE;
					resetSend.ResetData.vout14 = FALSE;
				}else{
					resetSend.ResetData.vout14 = TRUE;
				}
			}
	}
	//5-8路回路电压电流
	if (TRUE == VolCur_5To8.sendStatus && sendFlag == 5000)
	{
			byte_tmp[0] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_5_H,VolCur_5To8_tmp.LOOP_VOL_CUR_5_L);
			byte[0] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_5_H,VolCur_5To8.LOOP_VOL_CUR_5_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_6_H,VolCur_5To8_tmp.LOOP_VOL_CUR_6_L);
			byte[1] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_6_H,VolCur_5To8.LOOP_VOL_CUR_6_L) ;
			
			byte_tmp[2] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_7_H,VolCur_5To8_tmp.LOOP_VOL_CUR_7_L);
			byte[2] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_7_H,VolCur_5To8.LOOP_VOL_CUR_7_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_8_H,VolCur_5To8_tmp.LOOP_VOL_CUR_8_L);
			byte[3] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_8_H,VolCur_5To8.LOOP_VOL_CUR_8_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.ResetData.vout58)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8)) // 1为发送成功
				{
					// 保存上次数据
					memcpy(&VolCur_5To8_tmp, &VolCur_5To8 ,sizeof(VolCur_5To8));
					VolCur_5To8.sendStatus = FALSE;
					resetSend.ResetData.vout58 = FALSE;
				}else
				{
					resetSend.ResetData.vout58 = TRUE;
				}
			}
	}
	// 状态相关
	if (TRUE == canFlag.sendStatus && sendFlag == 6000)
	{
			if((canFlag_tmp.FAULT_FLAG_H != canFlag.FAULT_FLAG_H) || (canFlag_tmp.FAULT_FLAG_L != canFlag.FAULT_FLAG_L) ||
					(canFlag_tmp.WORK_FLAG_H != canFlag.WORK_FLAG_H) || (canFlag_tmp.WORK_FLAG_L != canFlag.WORK_FLAG_L) ||
                    (resetSend.ResetData.flag))
			{
				if(SEND_TYPE_CAN_FLAG_DATA(canFlag)) 
				{
					// 发送上传数据
					memcpy(&canFlag_tmp, &canFlag ,sizeof(canFlag));
					canFlag.sendStatus = FALSE;
					resetSend.ResetData.flag = FALSE;
				}else{
					resetSend.ResetData.flag = TRUE;
				}
			}
	}
	if (sendFlag++ > 7000)
	{			
        sendFlag = 1; // 重新开始下一次轮询
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// 下面属于中断处理函数尽量不出现费时的操作在里面
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// 处理主机命令事件 广播帧处理
void Process_host_msg(CanRxMsg ISR_RxMessage)
{
//	int indexAddr = 0,endAddr = 0; // 灯具地址范围
	
	if (ISR_RxMessage.ExtId == heartbeat_extid.id) // 表示广播帧
	{
		gbComCnt = 0;
		// SET_CAN_LED;
	}
	
	// 查询信息指令
	if(ISR_RxMessage.Data[0] == CMD_CAN_QUERY)  // 03指令 
	{
		// 电源查询
		if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_POWER)
		{
            resetSend.ResetData.power = TRUE;
		}
		
		// 工作状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_DEV_STATE) 
		{
            resetSend.ResetData.flag = TRUE;
		}
		
		// 灯具状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_LAMP) 
		{
			// 该种方式会影响主循环的运行
			// while(SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[ISR_RxMessage.Data[2]])); // 等待发送成功
			// 采用下次轮询发送来实现重发
			// lampData_tmp[MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3])].LAMP_DATA = 0xff;// 重发标志						
		}
		
		// 批量灯具查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_RANGE_LAMP) 
		{
			// REVC_LAMP_SIGNAL
//			indexAddr = MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3]); // 查询首地址
//			endAddr = MERGE_HL(ISR_RxMessage.Data[4],ISR_RxMessage.Data[5]); // 查询尾地址
			
//			if (endAddr > LampAddrBufLen) return;// 查询长度超过灯具总数
//			for(;indexAddr <= endAddr;indexAddr++)
//			{
//				lampData_tmp[indexAddr].LAMP_DATA = 0xff;// 重发标志					
//			}
		}
		// 查询1-4路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL14) 
		{
            resetSend.ResetData.vout14 = TRUE;
		}
		// 查询5-8路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL58)
		{
            resetSend.ResetData.vout58 = TRUE;
		}
		
		// 查询应急时间
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_HEGTM)
		{
            resetSend.ResetData.time = TRUE;
		}
	} 
	// 修改指令
	else if(ISR_RxMessage.Data[0] == CMD_CAN_EDIT)
	{
		// 改变灯具状态
		if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_LAMP) 				
		{
			// ISR_RxMessage.Data[2]; // 灯具ID高字节
			// ISR_RxMessage.Data[3]; // 灯具ID低字节
			// ISR_RxMessage.Data[4]; // 左亮，右亮，熄灭
//			Lampnum = MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3]);
//			LampSet = ISR_RxMessage.Data[4];
		}
		
		// 重启复位：应该是全部重发
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_RESET) 
		{
			// SendAll = TRUE; // 重发标志
            setCanInfoSendStaus(0xff); // 重发置位
			EpsState.bReset = TRUE;	//复位
		}
		
		// 消音
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_SILENCING) 
		{
			SysInd.bMute = ISR_RxMessage.Data[2]; //消音标志
		}
		//#define	AUTO_EMGY		0x00		//自动应急
		//#define	MANU_EMGY		0x01		//手动应急
		//#define	FORC_EMGY		0x02		//强制应急
		//#define	EMGY_RESET	    0x03		//手动月检应急
		//#define	MYCK_EMGY       0x04		//手动年检应急
		//#define	EMGY_RESET	    0x05		//应急复位
		//#define	EMGY_IDLE		0x06		//设备空闲
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MANUAL)//增加一个时间判断保护防止重复
		{				
			if(ISR_RxMessage.Data[2] == 0x05)
				{
					EpsState.bReset = TRUE;	//复位
					return;
				}
			if(!IS_ST_EMGY(EpsState.nState))
			 EpsState.bCtrlSig = (1<<ISR_RxMessage.Data[2]);
		}
		// 月检
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MCHK) 
		{
			EpsState.bCtrlSig = (1<<ISR_RxMessage.Data[2]);
		}
		// 强制启动开关
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_FEMGY) 
		{
			EpsState.bCtrlSig = (1<<ISR_RxMessage.Data[2]);
		}
	}
}


void UpCanData(void)
{
	// 输出电压1-4
	VolCur_1To4.LOOP_VOL_CUR_1_H = GETINT16_HIGH(AdcAcquire.nVout[0]);
	VolCur_1To4.LOOP_VOL_CUR_1_L = GETINT16_LOW(AdcAcquire.nVout[0]);
	VolCur_1To4.LOOP_VOL_CUR_2_H = GETINT16_HIGH(AdcAcquire.nVout[1]);
	VolCur_1To4.LOOP_VOL_CUR_2_L = GETINT16_LOW(AdcAcquire.nVout[1]);
	VolCur_1To4.LOOP_VOL_CUR_3_H = GETINT16_HIGH(AdcAcquire.nVout[2]);
	VolCur_1To4.LOOP_VOL_CUR_3_L = GETINT16_LOW(AdcAcquire.nVout[2]);
	VolCur_1To4.LOOP_VOL_CUR_4_H = GETINT16_HIGH(AdcAcquire.nVout[3]);
	VolCur_1To4.LOOP_VOL_CUR_4_L = GETINT16_LOW(AdcAcquire.nVout[3]);

	// 输出电压5-8
	VolCur_5To8.LOOP_VOL_CUR_5_H = GETINT16_HIGH(AdcAcquire.nVout[4]);
	VolCur_5To8.LOOP_VOL_CUR_5_L = GETINT16_LOW(AdcAcquire.nVout[4]);
	VolCur_5To8.LOOP_VOL_CUR_6_H = GETINT16_HIGH(AdcAcquire.nVout[5]);
	VolCur_5To8.LOOP_VOL_CUR_6_L = GETINT16_LOW(AdcAcquire.nVout[5]);
	VolCur_5To8.LOOP_VOL_CUR_7_H = GETINT16_HIGH(AdcAcquire.nVout[6]);
	VolCur_5To8.LOOP_VOL_CUR_7_L = GETINT16_LOW(AdcAcquire.nVout[6]);
	VolCur_5To8.LOOP_VOL_CUR_8_H = GETINT16_HIGH(AdcAcquire.nVout[7]);
	VolCur_5To8.LOOP_VOL_CUR_8_L = GETINT16_LOW(AdcAcquire.nVout[7]);

	// 主电压、输出电压电流、总电压
	power_supply.MAIN_VOL_H = GETINT16_HIGH(VMAIN);
	power_supply.MAIN_VOL_L = GETINT16_LOW(VMAIN);
	power_supply.OUTPUT_VOL_H = GETINT16_HIGH(VOUT);
	power_supply.OUTPUT_VOL_L = GETINT16_LOW(VOUT);
	power_supply.OUTPUT_CUR_H = GETINT16_HIGH(IDIS);
	power_supply.OUTPUT_CUR_L = GETINT16_LOW(IDIS);
	power_supply.BATTERY_VOL_ALL_H = GETINT16_HIGH(LI_VBAT);
	power_supply.BATTERY_VOL_ALL_L = GETINT16_LOW(LI_VBAT);

	// 充电电压和电流
	input_vc.INPUT_VOL_H = GETINT16_HIGH(VCHAR);
	input_vc.INPUT_VOL_L = GETINT16_LOW(VCHAR);
	input_vc.INPUT_CUR_H = GETINT16_HIGH(ICHR);
	input_vc.INPUT_CUR_L = GETINT16_LOW(ICHR);
	// 电池1和2
	input_vc.BATTERY_VOL_1_H = GETINT16_HIGH(0);
	input_vc.BATTERY_VOL_1_L = GETINT16_LOW(0);
	input_vc.BATTERY_VOL_2_H = GETINT16_HIGH(LI_VBAT);
	input_vc.BATTERY_VOL_2_L = GETINT16_LOW(LI_VBAT);

	// 应急时间和电池3
	canTime.BATTERY_VOL_3_H = GETINT16_HIGH(0);
	canTime.BATTERY_VOL_3_L = GETINT16_LOW(0);
	canTime.HEGTM_H = GETINT16_HIGH(EpsState.hEgTm);
	canTime.HEGTM_L = GETINT16_LOW(EpsState.hEgTm);

	// 故障状态和工作状态
	canFlag.FAULT_FLAG_H = GETINT16_HIGH(gbMyError);
	canFlag.FAULT_FLAG_H = GETINT16_LOW(gbMyError);
	canFlag.WORK_FLAG_H = GETINT16_HIGH(EpsState.nState);
	canFlag.WORK_FLAG_L = GETINT16_LOW(EpsState.nState);
    
    UpDataSendStatus(0xff); // 全部进入发送状态判断队列
}

// 轮询-> 是否进入状态变化判断 -> 直接发送不进行状态的判断

// 进入状态判断层
void UpDataSendStatus(uint8_t _u8F)
{
	canFlag.sendStatus      = (_u8F&(1 << 0))>0?1:0;
	canTime.sendStatus      = (_u8F&(1 << 1))>0?1:0;
	input_vc.sendStatus     = (_u8F&(1 << 2))>0?1:0;
	power_supply.sendStatus = (_u8F&(1 << 3))>0?1:0;
	VolCur_5To8.sendStatus  = (_u8F&(1 << 4))>0?1:0;
	VolCur_1To4.sendStatus  = (_u8F&(1 << 5))>0?1:0;
}

// 直接发送控制不对数据变化做判断
void setCanInfoSendStaus(uint8_t _u8F)
{
    resetSend.ResetData.flag    = (_u8F&(1 << 0))>0?1:0;
    resetSend.ResetData.input   = (_u8F&(1 << 1))>0?1:0;
    resetSend.ResetData.power   = (_u8F&(1 << 2))>0?1:0;
    resetSend.ResetData.time    = (_u8F&(1 << 3))>0?1:0;
    resetSend.ResetData.vout14  = (_u8F&(1 << 4))>0?1:0;
    resetSend.ResetData.vout58  = (_u8F&(1 << 5))>0?1:0;
}
