
#include "stdafx.h"
#include "SimuIIc.h"

void IIC_Start(void)
{
	SDA_OUT();
	IIC_SDA(1);	  	  
	IIC_SCL(1);
	I2C_DelayUs(4);
 	IIC_SDA(0);//START:when CLK is high,DATA change form high to low 
	I2C_DelayUs(4);
	IIC_SCL(0);//ready
}	  

void IIC_Stop(void)
{
	SDA_OUT();
	IIC_SCL(0);
	IIC_SDA(0);//STOP:when CLK is high DATA change form low to high
 	I2C_DelayUs(4);
	IIC_SCL(1); 
	IIC_SDA(1);//end
	I2C_DelayUs(4);							   	
}

unsigned char IIC_Wait_Ack(void)
{
	unsigned char ucErrTime=0;
	//
	SDA_IN();
	IIC_SDA(1);
	I2C_DelayUs(1);	   
	IIC_SCL(1);
	I2C_DelayUs(1);
	//
	while(READ_SDA())
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCL(0);
	return 0;  
} 

void IIC_Ack(void)
{
	IIC_SCL(0);
	SDA_OUT();
	IIC_SDA(0);
	I2C_DelayUs(2);
	IIC_SCL(1);
	I2C_DelayUs(2);
	IIC_SCL(0);
}


void IIC_NAck(void)
{
	IIC_SCL(0);
	SDA_OUT();
	IIC_SDA(1);
	I2C_DelayUs(2);
	IIC_SCL(1);
	I2C_DelayUs(2);
	IIC_SCL(0);
}					 				     

//1�aack
//0��nack			  
void IIC_Send_Byte(unsigned char txd)
{                        
	unsigned char t;
	//
	SDA_OUT(); 	    
	IIC_SCL(0);
	//
	for(t=0;t<8;t++)
	{              
		if((txd&0x80)>>7)
			IIC_SDA(1);
		else
			IIC_SDA(0);
		txd<<=1; 	  
		I2C_DelayUs(2);
		IIC_SCL(1);
		I2C_DelayUs(2); 
		IIC_SCL(0);	
		I2C_DelayUs(2);
	}	 
} 	    


unsigned char IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0;
	//
	SDA_IN();
	//
  for(i=0;i<8;i++ )
	{
		IIC_SCL(0); 
		I2C_DelayUs(2);
		IIC_SCL(1);
		receive<<=1;
		if(READ_SDA())receive++;   
		I2C_DelayUs(1); 
   }					 
	if (!ack)
		IIC_NAck();
	else
		IIC_Ack();
	return receive;
}

void SDA_IN() 
{
	GPIOB->CRH&=0XFFFF0FFF;
	GPIOB->CRH|=8<<12;
}

void SDA_OUT() 
{
	GPIOB->CRH&=0XFFFF0FFF;
	GPIOB->CRH|=3<<12;
}

void IIC_SCL(unsigned char bVal)
{
	if(bVal) GPIO_SetBits(GPIOB, GPIO_Pin_10);
	else GPIO_ResetBits(GPIOB, GPIO_Pin_10);
}

void IIC_SDA(unsigned char bVal)
{
	if(bVal) GPIO_SetBits(GPIOB, GPIO_Pin_11);
	else GPIO_ResetBits(GPIOB, GPIO_Pin_11);
}

unsigned char READ_SDA(void)
{
	return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11);
}


void I2C_DelayUs(unsigned int nUs)
{
	SysTickDelayUs(nUs);
}

void I2C_DelayMs(unsigned int nMs)
{
	SysTickDelayMs(nMs);
}






