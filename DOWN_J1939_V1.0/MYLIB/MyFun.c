
#include "MyFun.h"
#include "stdafx.h"

//========================================
//delay
//A for cycle takes 7 instruction cycles
//========================================
 void DelayMs(unsigned int nTp)
 {
	 unsigned int i = 0;
	 for(i=0; i < nTp; i++)
		 {
			DelayUs(1000);
		 }
 }
 
//========================================
//delay
//A for cycle takes 7 instruction cycles
//========================================
 void DelayUs(unsigned int nTp)
 {
	  unsigned int i,j;
		for(i = 0; i<nTp; i++)
			for(j = 0; j<1; j++);
 }

 //=====================================================
//debug send
//=====================================================
//void 	ByteSend(unsigned char nDat)
//{
//		int i = 0;
//		unsigned char nByte = nDat;
//		//
//		CLR_SPI_NSS;
//		DelayUs(10);
//		//
//		for(i = 0; i <8; i++)
//		{
//				if(nByte&0x80)
//						SET_SPI_MOSI;
//				else
//						CLR_SPI_MOSI;
//				DelayUs(10);
//				nByte = nByte<<1;
//				SET_SPI_SCK;
//				DelayUs(10);
//				CLR_SPI_SCK;
//		}
//		//
//		DelayUs(10);
//		SET_SPI_NSS;
//		DelayUs(10);
//}

//void		BufSend(unsigned char* buf, int nLen)
//{
//		int i = 0;
//		for(i = 0; i<nLen; i++)
//		{
//				ByteSend(buf[i]);
//		}
//}

//=====================================================
//dump fun
//=====================================================
 void  DumpFun(unsigned char nDat){}

//=====================================================
//CRC
//=====================================================
void InvertUint8(unsigned char *dBuf,unsigned char *srcBuf)  
{  
    int i;  
    unsigned char tmp[4];  
    tmp[0] = 0;  
    for(i=0;i< 8;i++)  
    {  
      if(srcBuf[0]& (1 << i))  
        tmp[0]|=1<<(7-i);  
    }  
    dBuf[0] = tmp[0];  
}  

void InvertUint16(unsigned short *dBuf,unsigned short *srcBuf)  
{  
    int i;  
    unsigned short tmp[4];  
    tmp[0] = 0;  
    for(i=0;i< 16;i++)  
    {  
      if(srcBuf[0]& (1 << i))  
        tmp[0]|=1<<(15 - i);  
    }  
    dBuf[0] = tmp[0];  
} 

unsigned short CRC16_CCITT(unsigned char *puchMsg, unsigned int usDataLen)  
{
  
	unsigned char i = 0;
  unsigned short wCRCin = 0x0000;  
  unsigned short wCPoly = 0x1021;  
  unsigned char wChar = 0;  
    
  while (usDataLen--)     
  {  
        wChar = *(puchMsg++);  
        InvertUint8(&wChar,&wChar);  
        wCRCin ^= (wChar << 8);  
        for(i = 0;i < 8;i++)  
        {  
          if(wCRCin & 0x8000)  
            wCRCin = (wCRCin << 1) ^ wCPoly;  
          else  
            wCRCin = wCRCin << 1;  
        }  
  }  
  InvertUint16(&wCRCin,&wCRCin);  
  return (wCRCin) ;  
}

//=====================================================
//led trig
//=====================================================
unsigned char K[2];
void LedFlash(unsigned int nTp)
{
	static int nCnt = 0;

	//
	   nCnt++;
	//	
		//EmgStateKeepTim = (EmgStateKeepTim < 20)?(EmgStateKeepTim + 1): EmgStateKeepTim;
		if(nCnt == nTp) nCnt = 0;
		if(nCnt <= (nTp>>1))
		{
			SET_FLASH_LED;
			EmgStateKeepTim = (EmgStateKeepTim < 20)?(EmgStateKeepTim + 1): EmgStateKeepTim;
		}
		else 
			CLR_FLASH_LED;	
}


//=====================================================
//key input
//=====================================================
unsigned char KeyScan(unsigned int nTm, unsigned char bMode)
{
	unsigned char i = 0;
	unsigned char bRes = 0;
	static unsigned char bKey = 0xff;
	static unsigned int nCnt = 0;
	//
	if(bKey != 0xff)
	{
		if(nCnt >= nTm)
		{
			if(nCnt == nTm)
			{
				bRes = bKey;
				bKey = 0xff;
				nCnt++;
				return bRes;
			}else
			if((KeyRead(bKey)!= 0)||bMode)
			{
				nCnt = 0;
				bKey = 0xff;
				return 0xff;
			}else
			{
				return 0xff;
			}
		}else
		if(!KeyRead(bKey))
		{
			nCnt++;
			return 0xff;
		}else
		{
			nCnt = 0;
			bKey = 0xff;
			return 0xff;
		}
	}else
	{
		if((KEY0==0)||(KEY1==0)||(KEY2==0)||(KEY3==0)||(KEY4==0)
		 ||(KEY5==0)||(KEY6==0)||(KEY7==0)||(KEY8==0)||(KEY8==0))
		{
			for(i=0; i<10; i++)
			{
				if(KeyRead(i)==0)
				{
					bKey = i;
					break;
				}//if
			}//for
		}//if
	}//else
	return 0xff;
}

unsigned char KeyRead(unsigned char bIndex)
{
	if/***/(bIndex == 0) return KEY0;
	else if(bIndex == 1) return KEY1;
	else if(bIndex == 2) return KEY2;
	else if(bIndex == 3) return KEY3;
	else if(bIndex == 4) return KEY4;
	else if(bIndex == 5) return KEY5;
	else if(bIndex == 6) return KEY6;
	else if(bIndex == 7) return KEY7;
	else if(bIndex == 8) return KEY8;
	else								 return KEY9;
}


//=====================================================
//bit acquire
//=====================================================
#define	BIT_INX(n)	(n/8)
unsigned int BitAcquire(unsigned char*pBuf, unsigned char nLen, int nStart, int nStop)
{
	int i = 0;
	unsigned int nRes = 0;
	//
	if(((nStart/8)>=nLen)||((nStop/8)>=nLen))
	{
		return FALSE;
	}
	//
	for(i=nStop; i>=nStart; i--)
	{
		nRes |= (pBuf[i/8]>>(i%8))&0x01;
		nRes = (i==nStart)?nRes:(nRes<<1);
	}
	//
	return nRes;
}

unsigned int BitSetting(unsigned char*pBuf, unsigned char nLen, int nStart, int nStop, int nData)
{
	int i = 0;
	unsigned int nBit = 0;
	//
	if(((nStart/8)>=nLen)||((nStop/8)>=nLen))
	{
		return FALSE;
	}
	//
	for(i=nStart; i<=nStop; i++)
	{
		nBit = nData&0x01;
		nData = nData>>1;
		pBuf[i/8] |= (nBit<<(i%8));
	}
	//
	return TRUE;
}

//累加和检验
unsigned char RX_CheckSum(unsigned char *buf, unsigned char len) //buf????飬len????鳤??
{ 
    unsigned char i, ret = 0;
 
    for(i=0; i<len; i++)
    {
        ret += *(buf++);
    }
     ret = ret;
    return ret+1;
}

//累加和检验
unsigned char TX_CheckSum(unsigned char *buf, unsigned char len) //buf????飬len????鳤??
{ 
    unsigned char i, ret = 0;
 
    for(i=0; i<len; i++)
    {
        ret += *(buf++);
    }
     ret = (ret+0x33);
    return ret;
}

