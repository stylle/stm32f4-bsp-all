
#ifndef	SEQUE_LIST_H
#define	SEQUE_LIST_H

struct FAULT_ELEM;

#define	SEQUE_LEN		512

#define	SEQUE_ELEM	FAULT_ELEM

unsigned char SequeAddElem(SEQUE_ELEM* pElem);
unsigned char SequeDelElem(unsigned int nIndex);
unsigned int SeQueGetLen(void);
SEQUE_ELEM SequeGetElem(unsigned int nIndex);
void SequeSetElem(unsigned int nIndex, SEQUE_ELEM* pElem);
void SeQueClearList(void);

#endif


