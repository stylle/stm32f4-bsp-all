
#ifndef 	FIFO_H
#define		FIFO_H

#ifndef TRUE
#define	TRUE 1
#endif

#ifndef FALSE
#define	FALSE 0
#endif

#define	FIFO_MAX_LEN		32
#define	FIFO_BUF_LEN		8

void FifoClear(void);
unsigned char FifoEmpty(void);
unsigned char FifoFull(void);
void FifoOut(unsigned char*pBuf);
void FifoIn(unsigned char*pBuf);


#endif



