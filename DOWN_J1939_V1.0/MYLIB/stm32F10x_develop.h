
#ifndef	STM32F10X_DEVELOP_H
#define	STM32F10X_DEVELOP_H

void StartTimer1(void);
void StopTimer1(void);
void StartPTimer(void);
void InitTm1ToBaseMode(unsigned short nArr,unsigned short nPsc);


void StartTimer2(void);
void StopTimer2(void);
void InitTm2ToBaseMode(unsigned short nArr,unsigned short nPsc);

void StartTimer3(void);
void StopTimer3(void);
void InitTm3ToBaseMode(unsigned short nArr,unsigned short nPsc);
void InitTm3Ch2ToPwmMode(unsigned short nArr,unsigned short nPsc, unsigned short nCcr);

void StartTimer4(void);
void StopTimer4(void);
void InitTm4Ch3ToPwmMode(unsigned short nCcr);
void InitTm4ToBaseMode(unsigned short nArr,unsigned short nPsc);

void StartTimer5(void);
void StopTimer5(void);
void InitTm5ToBaseMode(unsigned short nArr,unsigned short nPsc);

void StartTimer6(void);
void StopTimer6(void);
void InitTm6ToBaseMode(unsigned short nArr,unsigned short nPsc);

void StartTimer7(void);
void StopTimer7(void);
void InitTm7ToBaseMode(unsigned short nArr,unsigned short nPsc);

void StartTimer8(void);
void StopTimer8(void);
void InitTm8ToBaseMode(unsigned short nArr,unsigned short nPsc);

void Start_GPIO_EXTI(void);
void Stop_GPIO_EXTI(void);

void ConfigUART1(void);
void SetUartToTxMode(void);
void SetUartToRxMode(void);
void ConfigUART3(void);
void ConfigUART4(void);
void ConfigUART5(void);
void OpenUart(void);
void CloseUart(void);

uint8_t CanReceiveMsg(uint8_t *pBuf);
uint8_t CanSendMsg(uint8_t* pMsg,uint8_t nLen);
void CanModeInit(uint8_t nTsjw,uint8_t nTbs2,uint8_t nTbs1,uint16_t nBrp,uint8_t nMode);

//void GPIO_EXTI_Init(void);

void ADC1Init(void);
void ADC2Init(void);
void SetTxToPPMode(void);
void SetTxToAFMode(void);
void SetTxValue(unsigned char bValue);

void MYDMA1_Config(DMA_Channel_TypeDef* DMA_CHx,uint32_t nDevAddr,uint32_t nBaseAddr,uint16_t nSize);

void SPI2_Init(void);
void SPI2_SetSpeed(uint8_t SPI_BaudRatePrescaler);
uint8_t SPI2_ReadWriteByte(uint8_t nData);

void NVIC_Configuration(void);

#endif


