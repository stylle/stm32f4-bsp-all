
#include "fifo.h"

unsigned int nWrite;
unsigned int nRead;
unsigned char bEmpty;

unsigned char strFifo[FIFO_MAX_LEN][FIFO_BUF_LEN];

void FifoClear()
{
	nWrite = 0;
	nRead = 0;
	bEmpty = FALSE;
}

void FifoIn(unsigned char*pBuf)
{
	unsigned char i = 0;
	//
	if(FifoFull()==TRUE) return;
	//
	if(nWrite == FIFO_MAX_LEN)
		nWrite = 0;
	//
	for(i=0; i<FIFO_BUF_LEN; i++)
		strFifo[nWrite][i] = pBuf[i];
	//
	nWrite++;
}

void FifoOut(unsigned char*pBuf)
{
	unsigned char i = 0;
	//
	if(FifoEmpty()==TRUE) return;
	//
	if(nRead == FIFO_MAX_LEN)
		nRead = 0;
	//
	for(i=0; i<FIFO_BUF_LEN; i++)
		 pBuf[i] = strFifo[nRead][i];
	//
	nRead++;
}

unsigned char FifoFull()
{
	if(((nRead == 0)&&(nWrite==FIFO_MAX_LEN))||(nWrite== nRead-1))
		return TRUE;
	else
		return FALSE;
}

unsigned char FifoEmpty()
{
	if(nWrite == nRead)
		return TRUE;
	else
		return FALSE;
}




