/**
  ******************************************************************************
  * @file    Project/STM32F103RCT6_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    05-April-2018
  * @brief   Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h" 

unsigned int  PWM = 100 ;
 
/*****************************   CANBEGIN *************************************/
uint8_t CanVout_14[8];
uint8_t CanVout_58[8];
uint8_t CanPower[8];
uint8_t CanInput[8];
uint8_t CanHegtm[8];
uint8_t CanFlag[8];

extern unsigned short LampAddrBuf[250];//地址数组
extern unsigned char LampState[250];// 状态数组
CAN_LAMP_DATA data[512];
uint8_t FirstSend; // 首次发送标志，也可以通过这个标志位上传全部数据

uint16_t lampAddr[250];
uint8_t lampState[250];

void UpCanData(void)
{
	// 输出电压1-4
	CanVout_14[0] = GETINT16_HIGH(AdcAcquire.nVout[0]);
	CanVout_14[1] = GETINT16_LOW(AdcAcquire.nVout[0]);
	CanVout_14[2] = GETINT16_HIGH(AdcAcquire.nVout[1]);
	CanVout_14[3] = GETINT16_LOW(AdcAcquire.nVout[1]);
	CanVout_14[4] = GETINT16_HIGH(AdcAcquire.nVout[2]);
	CanVout_14[5] = GETINT16_LOW(AdcAcquire.nVout[2]);
	CanVout_14[6] = GETINT16_HIGH(AdcAcquire.nVout[3]);
	CanVout_14[7] = GETINT16_LOW(AdcAcquire.nVout[3]);

	// 输出电压5-8
	CanVout_58[0] = GETINT16_HIGH(AdcAcquire.nVout[4]);
	CanVout_58[1] = GETINT16_LOW(AdcAcquire.nVout[4]);
	CanVout_58[2] = GETINT16_HIGH(AdcAcquire.nVout[5]);
	CanVout_58[3] = GETINT16_LOW(AdcAcquire.nVout[5]);
	CanVout_58[4] = GETINT16_HIGH(AdcAcquire.nVout[6]);
	CanVout_58[5] = GETINT16_LOW(AdcAcquire.nVout[6]);
	CanVout_58[6] = GETINT16_HIGH(AdcAcquire.nVout[7]);
	CanVout_58[7] = GETINT16_LOW(AdcAcquire.nVout[7]);

	// 主电压、输出电压电流、总电压
	CanPower[0] = GETINT16_HIGH(VMAIN);
	CanPower[1] = GETINT16_LOW(VMAIN);
	CanPower[2] = GETINT16_HIGH(VOUT);
	CanPower[3] = GETINT16_LOW(VOUT);
	CanPower[4] = GETINT16_HIGH(IDIS);
	CanPower[5] = GETINT16_LOW(IDIS);
	CanPower[6] = GETINT16_HIGH(LI_VBAT);
	CanPower[7] = GETINT16_LOW(LI_VBAT);

	// 充电电压和电流
	CanInput[0] = GETINT16_HIGH(VCHAR);
	CanInput[1] = GETINT16_LOW(VCHAR);
	CanInput[2] = GETINT16_HIGH(ICHR);
	CanInput[3] = GETINT16_LOW(ICHR);
	// 电池1和2
	CanInput[4] = GETINT16_HIGH(0);
	CanInput[5] = GETINT16_LOW(0);
	CanInput[6] = GETINT16_HIGH(LI_VBAT);
	CanInput[7] = GETINT16_LOW(LI_VBAT);

	// 应急时间和电池3
	CanHegtm[0] = GETINT16_HIGH(0);
	CanHegtm[1] = GETINT16_LOW(0);
	CanHegtm[2] = GETINT16_HIGH(EpsState.hEgTm);
	CanHegtm[3] = GETINT16_LOW(EpsState.hEgTm);

	// 故障状态和工作状态
	CanFlag[0] = GETINT16_HIGH(gbMyError);
	CanFlag[1] = GETINT16_LOW(gbMyError);
	CanFlag[2] = GETINT16_HIGH(EpsState.nState);
	CanFlag[3] = GETINT16_LOW(EpsState.nState);

	// 更新数据
	upCanDataChar(TYPE_CAN_POWER_SUPPLY,CanPower);
	upCanDataChar(TYPE_CAN_INPUT_VC,CanInput);
	upCanDataChar(TYPE_CAN_TIME,CanHegtm);
	upCanDataChar(TYPE_CAN_LOOP_VOL_CUR_14,CanVout_14);	
	upCanDataChar(TYPE_CAN_LOOP_VOL_CUR_58,CanVout_58);
	
	// 更新灯具
	upCanDataChar(TYPE_CAN_FLAG,CanFlag);
//	upCanLampChar(LampAddrBuf,LampState);
	upCanLampChar(lampAddr,lampState);
}

/*****************************   CANEND  *************************************/

/**
  * @brief   Main program.
  * @param   None
  * @retval  None
  * @version V1.3
	*
	*
  */

int main(void)
{
//	CanTxMsg TxMessage;
	InitVar();					//变量初始化
	
	SysTickInit();			//系统定时器初始化  
	
	ConfigGPIO();				//GPIO配置
	DelayMs(2000);
		
	ADCInit();					//ADC配置,严禁放在定时器后面初始化，造成PWM失控
	
	ConfigUart();				//UART配置
		
	RccClkInit();				//RCC初始化
	////////////
	InitElecLable();		//电子标签读取
	
	ReadFlashData();
	
	InitSystem();				//系统设置
	////////////
	TimerInit();				//定时器初始化	  内部有一个心跳包在TIM1中运行
	
	InitTm4Ch3ToPwmMode(0);

	//IWDG_Init(4, 60000); 	 //watch dog init(5s)
	
	LAST_ENABLE;

	
	CAN_Init_All(BOUND20KB);
	UpCanData();
	for(FirstSend = 0; FirstSend < LAMP_NUM;FirstSend++)
	{
		  lampAddr[FirstSend] = FirstSend;
			lampState[FirstSend] = 2;
	}
	lampState[8] = 1;
	lampState[110] = 1;
	lampState[25] = 1;
	lampState[87] = 1;
	lampState[44] = 1;
	
	FirstSend = 1; // 初始化完所有数据开始第一次数据发送标志
	// 发送的数据结构
			
//			TxMessage.StdId=0x12;						// 标准标识符 
//			TxMessage.ExtId=0x12;						// 设置扩展标示符 
//			TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
//			TxMessage.RTR=CAN_RTR_Data;			// 数据帧
//			TxMessage.DLC = 0x03 ;					// 要发送的数据长度
//			
//			// 填充数据
//			TxMessage.Data[0] = sizeof(unsigned char);
//			TxMessage.Data[1] = sizeof(unsigned short);
//		  TxMessage.Data[2] = sizeof(unsigned int);

//			Can1_Send_Msg(TxMessage);
			
  while(1)
  {	
		
		UpCanData();	// 更新CAN数据
		CanPoll(); // CAN装载完成发送任务  
		
		OutDetect();	    //输出故障检测
		
		IWDG_Feed();		  //喂狗	
		
		EpsWorking();		//国标功能，年月检、应急等

//		MessageDeal();	//处理主机的消息    
				
//		EmgyDetect();		//检测应急状态
	
		EmgyChang();		//应急转换设置
			
		LedFlash(20000);//闪烁指示灯，观察用
	
		Temp(80);

//		IfrScan(); 		//可以修改地址的红外接口，定时器被占用暂时无法使用
		BellContrl(BELL_SHORT,100,30);
		
	  //间隔执行的函数100mS
		UPDATE_START	
		
		HandRead();
			
		FaultLed(16);
			
		InitWorking();		//上电工作延时
			
		//BatteryFault();
			
		UpdateEgTime();		//更新应急时间
			
//	//DischargeDeal();	//电池充放电管理
//			
		UpdateIndicate(); 	//更新蜂鸣器状态
//			
		SystemFaultDetect();	//故障检测
//			
//	PassCurenntDetect();

//	TIM_SetCompare3(TIM4, PWM); 	
//			
 		UPDATE_END
	}
}

/******************* (C) By WangJiuZhou  of 2020.2.20 ********************/



