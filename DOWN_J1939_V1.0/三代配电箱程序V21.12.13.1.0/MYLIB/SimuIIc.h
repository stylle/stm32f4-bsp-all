
#ifndef SIMU_IIC_H
#define SIMU_IIC_H

void IIC_Start(void);	
void IIC_Stop(void);

void IIC_Ack(void);
void IIC_NAck(void);

void IIC_Send_Byte(unsigned char txd);
unsigned char IIC_Read_Byte(unsigned char ack);
unsigned char IIC_Wait_Ack(void);

void IIC_Write_One_Byte(unsigned char daddr,unsigned char addr,unsigned char data);
unsigned char IIC_Read_One_Byte(unsigned char daddr,unsigned char addr);	  

void IIC_SCL(unsigned char bVal);

void IIC_SDA(unsigned char bVal);

unsigned char READ_SDA(void);

void SDA_IN(void);

void SDA_OUT(void);

void I2C_DelayUs(unsigned int nUs);
void I2C_DelayMs(unsigned int nMs);

#endif






