//=================================================================================
//header of nRf24l01
//=================================================================================
#ifndef	NRF24L01_H
#define	NRF24L01_H

//=====================================================
//define
//=====================================================
#define NRF_TX_ADR_WIDTH    5   	// 5 uints TX address width
#define NRF_RX_ADR_WIDTH    5   	// 5 uints RX address width
#define NRF_TX_PLOAD_WIDTH  20  	// 20 uints TX payload
#define NRF_RX_PLOAD_WIDTH  20  	// 20 uints TX payload

#define NRF_READ_REG        0x00	// 读寄存器指令
#define NRF_WRITE_REG       0x20 	// 写寄存器指令
#define NRF_RD_RX_PLOAD     0x61	// 读取接收数据指令
#define NRF_WR_TX_PLOAD     0xA0	// 写待发数据指令
#define NRF_FLUSH_TX        0xE1 	// 冲洗发送 FIFO指令
#define NRF_FLUSH_RX        0xE2	// 冲洗接收 FIFO指令
#define NRF_REUSE_TX_PL     0xE3	// 定义重复装载数据指令
#define NRF_NOP             0xFF  // 保留

#define NRF_CONFIG          0x00  // 配置收发状态，CRC校验模式以及收发状态响应方式
#define NRF_EN_AA           0x01  // 自动应答功能设置
#define NRF_EN_RXADDR       0x02  // 可用信道设置
#define NRF_SETUP_AW        0x03  // 收发地址宽度设置
#define NRF_SETUP_RETR      0x04  // 自动重发功能设置
#define NRF_RF_CH           0x05  // 工作频率设置
#define NRF_RF_SETUP        0x06  // 发射速率、功耗功能设置
#define NRF_STATUS          0x07  // 状态寄存器
#define NRF_OBSERVE_TX      0x08  // 发送监测功能
#define NRF_CD              0x09  // 地址检测           
#define NRF_RX_ADDR_P0      0x0A  // 频道0接收数据地址
#define NRF_RX_ADDR_P1      0x0B  // 频道1接收数据地址
#define NRF_RX_ADDR_P2      0x0C  // 频道2接收数据地址
#define NRF_RX_ADDR_P3      0x0D  // 频道3接收数据地址
#define NRF_RX_ADDR_P4      0x0E  // 频道4接收数据地址
#define NRF_RX_ADDR_P5      0x0F  // 频道5接收数据地址
#define NRF_TX_ADDR         0x10  // 发送地址寄存器
#define NRF_RX_PW_P0        0x11  // 接收频道0接收数据长度
#define NRF_RX_PW_P1        0x12  // 接收频道0接收数据长度
#define NRF_RX_PW_P2        0x13  // 接收频道0接收数据长度
#define NRF_RX_PW_P3        0x14  // 接收频道0接收数据长度
#define NRF_RX_PW_P4        0x15  // 接收频道0接收数据长度
#define NRF_RX_PW_P5        0x16  // 接收频道0接收数据长度
#define NRF_FIFO_STATUS     0x17  // FIFO栈入栈出状态寄存器设置

#define	NRF_RX_DR	((NRF_STATE>>6)&0x01)
#define	NRF_TX_DS	((NRF_STATE>>5)&0x01)
#define	NRF_MAX_RT	((NRF_STATE>>4)&0x01)

#define	NRF_RX_FLAG		0x01
#define	NRF_TX_FLAG		0x02
#define	NRF_IRQ_OVER	0x03
#define	NRF_IRQ_ERR			0x04

#define NRF_IQA_OVER_TIME		100000	//clock count

//=====================================================
//prototypes-inner
//=====================================================
void NrfDelayUs(unsigned int nTp);		//内部用延时函数，根据时钟修改计数值

unsigned char NrfByteRW(unsigned char nByte);		//字节读写函数

unsigned char NrfReadBuf(unsigned char nReg, unsigned char *pBuf, unsigned char nLen);		//读取缓冲区

unsigned char NrfWriteBuf(unsigned char nReg, unsigned char *pBuf, unsigned char nLen);			//发送缓冲区

//=====================================================
//prototypes-interface
//=====================================================
unsigned char  InitNRF24L01(void);		//初始化，进入PoweDown模式

unsigned char  NrfSetRxMode(void);		//进入接收模式

unsigned char NrfIntoStandBy(void); 	//进入待机模式

unsigned char	NrfIntoPowerDown(void); //进入掉电模式

unsigned char NrfReadReg(unsigned char nReg);		//读取寄存器值

unsigned char NrfWriteReg(unsigned char nReg, unsigned char nValue);		//写寄存器值

unsigned char NrfReadPacket(unsigned char* pRxbuf);		//读取数据包

unsigned char  NrfSendPacket(unsigned char * pTxbuf);		//发送数据包

unsigned char NrfReadIrq(void);	//读取中断标志，超时返回错误


//=====================================================
//How to use
//Rx: InitNRF24L01()=>NrfIntoStandBy()=>NrfSetRxMode()=>NrfReadIrq()
//=>NrfReadPacket
//Tx: InitNRF24L01()=>NrfIntoStandBy()=>NrfSendPacket()
//=====================================================

#endif
