
#include "stm32F10x_ph_ext.h"

unsigned int SysUsTime;
unsigned int SysMsTime;

//=======================================================================
//printf
//=======================================================================
#if 1
#pragma import(__use_no_semihosting)                         
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       

 void _sys_exit(int x) 
{ 
	x = x; 
} 

int fputc(int ch, FILE *f)
{
	
//	if(UR_Choose)
//	{
//		SET_485C_TX;
//	//
//		UartSendByte(USART1,(uint8_t)ch);
//		//
//		SET_485C_RX;
//	}
//	else
//	{
//		SET_485A_TX;
//		//
//		UartSendByte(UART5,(uint8_t)ch);
//		//
//		SET_485A_RX;
//	}
//	//
//	return ch;	
	return 0;

}
#endif 

//=======================================================================
//systick
//=======================================================================

void SysTickInit()
{
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8); //9M
	//
	SysUsTime = SystemCoreClock/8000000; //1us = 9 clocks
	SysMsTime = SysUsTime*1000;
	
	//RCC_HSEConfig(RCC_HSE_ON);
}

void SysTickDelayUs(uint32_t nUs)
{		
	uint32_t temp;
	//
	SysTick->LOAD = nUs*SysUsTime;
	SysTick->VAL = 0x00;        //clear timer
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; //counter
	do
	{
		temp = SysTick->CTRL;
	}
	while(temp&0x01&&!(temp&(1<<16)));//wait
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk; //clock
	SysTick->VAL =0X00; //clear timer
}
void SysTickDelayMs(uint32_t nMs)
{		
	uint32_t temp;
	//
	SysTick->LOAD = nMs*SysMsTime;
	SysTick->VAL = 0x00;        //clear timer
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; //counter
	do
	{
		temp = SysTick->CTRL;
	}
	while(temp&0x01&&!(temp&(1<<16)));//wait
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk; //clock
	SysTick->VAL =0X00; //clear timer
}

//=======================================================================
//uart message send
//=======================================================================
void UartSendBuffer(USART_TypeDef* USARTx, unsigned char*pBuf, int  nLen)
{
	int i = 0;
	//
	for(i=0; i < nLen; i++)
	{
		UartSendByte(USARTx, pBuf[i]);
		SysTickDelayMs(1);
	}
}

//=======================================================================
//uart data send
//=======================================================================
void UartSendByte(USART_TypeDef* USARTx, unsigned char bByte)
{
	while(USART_GetFlagStatus(USARTx,USART_FLAG_TXE)!=SET);
	USART_SendData(USARTx, bByte);
	while(USART_GetFlagStatus(USARTx,USART_FLAG_TC)!=SET);
}

//=======================================================================
//write flash
//=======================================================================
void FlashWriteMoreData(uint32_t nAddr, uint16_t *pData, uint16_t nCount)
{
	uint32_t nIndex;
	uint32_t nAddrOft;
	uint32_t nAddrSector;
	uint32_t nSectorInx;
	//
	if(nAddr < CHIP_FLASH_BASE)//||((nAddr+nCount*2)>=(CHIP_FLASH_BASE+1024*CHIP_FLASH_SIZE)))
	{
		return;
	}
	//
	

	FLASH_Unlock();
	
	
	nAddrOft = nAddr - CHIP_FLASH_BASE;
	nSectorInx = nAddrOft/CHIP_SECTOR_SIZE;
	nAddrSector = nSectorInx*CHIP_SECTOR_SIZE + CHIP_FLASH_BASE;
	//
	FLASH_ErasePage(nAddrSector);
	//
	for(nIndex = 0; nIndex<nCount; nIndex++)
	{
		FLASH_ProgramHalfWord(nAddr + nIndex*2, pData[nIndex]);
	}
	
	FLASH_Lock();

}

//=======================================================================
//read flash
//=======================================================================
void FlashReadMoreData(uint32_t nAddr, uint16_t *pData,uint16_t nCount)   	
{
	uint16_t i;
	for(i=0; i<nCount; i++)
	{
		pData[i] = FlashReadHalfWord(nAddr);
		nAddr += 2;
	}
}

//=======================================================================
//flash read
//=======================================================================
uint16_t FlashReadHalfWord(uint32_t nAddr)
{
	return *(vu16*)nAddr; 
}

//=======================================================================
//systick
//=======================================================================
void ConfigSystick(unsigned int nDiv)
{
	if(SysTick_Config(SystemCoreClock / nDiv))
	{
		while(1);
	}
	//
	NVIC_SetPriority(SysTick_IRQn, 0x00); 
}

//=======================================================================
//watch dog
//Ex: nPre=4(divider/64),clock=40k/64=0.625k(1.6ms)
//		feed time is 1s when nRlr is 625(625*1.6 = 1000ms)
//=======================================================================
void IWDG_Init(uint8_t nPre,uint16_t nRlr)
{	
	//write enable for IWDG_PR and IWDG_RLR
 	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	//set prescaler
	IWDG_SetPrescaler(nPre);
	//set reload value
	IWDG_SetReload(nRlr);
	//reload
	IWDG_ReloadCounter();
	//enable
	IWDG_Enable();
}

void IWDG_Feed(void)
{
 	IWDG_ReloadCounter();	
}

//======================================================
//send frame by uart
//======================================================
//向控制器发送灯具数据
void SendFrameByUR1(unsigned char*pBuf, unsigned int bLen)
{
	SET_485C_TX;
	//
	UartSendBuffer(USART1, pBuf, bLen);
	//
	SET_485C_RX;
}


void SendFrameByUR5(unsigned char*pBuf, unsigned char bLen)
{

	//
	UartSendBuffer(UART5, pBuf, bLen);
	//

}

	
void SendFrameByURA(void)
	{
			FSlaverSendB(&MyFrmNode, SendFrameByUR1);
	}
	
	
void SendFrameByURB(void)
	{
		
		
				FSlaverSendC(&MyFrmNode, SendFrameByUR1);
		
	}
	
