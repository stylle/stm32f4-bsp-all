
#ifndef	MESSAGE_DEAL_H
#define	MESSAGE_DEAL_H

//======================================================
//define
//======================================================
//解析地址
#define	GET_PADDR(addr)	addr
#define	GET_AADDR(addr) (addr|0x80)

//======================================================
//function
//======================================================

void DoPowEvent(void);
void DoReceiveDeal(void);
void QueryPower(void);
void PrinfSysDat(void);
void QueryFault(void);
void DoAckSend(void);
void SetDbgSwitch(void);
void SetAckCode(unsigned char nCode, unsigned char bLen);
void SetAckData(unsigned char i, unsigned char bByte);
void SetDevState(unsigned char bFlag, unsigned char bNotice);
void DoEventDeal(unsigned char bEvent, unsigned char bSub);
void DoQueryDeal(void);
extern unsigned char gbPrfOn;	//打印开关，用于调试
extern unsigned char EmgStateKeepTim ;
#endif

