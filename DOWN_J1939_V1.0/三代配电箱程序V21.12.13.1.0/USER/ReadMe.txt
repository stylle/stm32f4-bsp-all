
引脚功能

充电电流ADC - PA0, ADC0
放电电流ADC - PC5, ADC15
输出电压ADC - PA4, ADC4
主电电压ADC - PA5, ADC5

电池电压1 - PA1, ADC1
电池电压2 - PB0, ADC8
电池温度1 - PA7, ADC7
电池温度2 - PA6, ADC6

市电电压ADC - PB1, ADC9

屏幕(UART3) TXD-PB10,RXD-PB11,DIR-PB2

RS485-1 TXD-PA9,RXD-PA10,DIR-PC9  *

CAN	TXD-PB8,RXD-PB9

RS485-2 TXD-PA2,RXD-PA3,DIR-PC4   *

强启输入-PC14
手动应急-PC15

电池采样继电器-PC12
LED-PC11

主电开关-PB4
电池开关-PB3
输出开关-PA11
开关电源电压调节-PA8

电池检测 PC0
输出短路检测-PB6
输出短路复位-PA15

灯 PB14

不应急:
PA11，主电电压大于24，PWM调节，低于置高
PB3，电池电压低于27.5一直打开，充电开关
PB4,一直打开
PA8,调节电流

应急:
PA11，电池电压大于24V，调节输出电压
PB3,开启
PB4,关断
PA8,不动作



