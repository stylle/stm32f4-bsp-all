
//======================================================
//ADC处理
//======================================================

#include "stdafx.h"

//======================================================
//var
//======================================================
ADC_ACQUIRE AdcAcquire; //ADC deal

//======================================================
//将采样值转换为电压或电流值
//
//电流采样计算
//I = Adc * [(0.8057 * 10) / (1 * 180)] 0.04476
//0.8057 = 3300/4096即ADC分辨率
//*10是因为电流单位取为100mA
//1是采样电阻，单位毫欧
//180采样后电压放大倍数
//
//电压采样计算
//V = ((0.8057 * Adc * 分压倍数)/100
//电压单位为100mV，所以结果要除以100
//======================================================


		unsigned int OutCurrentShowValue;
		unsigned int OutCurrentOpenValue;
		unsigned int ChrCurrentShowValue;
		unsigned int ChrCurrentOpenValue;
		unsigned char ON_State = 0;
		unsigned int  OutZero = 0 ;
    unsigned int  ChrZero = 0 ;
		static uint8_t bTrack = 1;
		//unsigned int Ch = 10;
		
		unsigned char MC4051_ABCBuf[8] = {3,0,1,2,5,7,6,4};
void AdcConvert(unsigned char nIndex, unsigned short nVal)
{
	
	 float fVal = 0;

			if(nIndex == 0)//通道电压
				{					
					fVal = ((float)nVal*0.19) + 0.5; 
					AdcAcquire.nVout[bTrack-1] = (uint16_t)fVal; 
					SetADCSwPin(((bTrack > 8)?(bTrack = 1):(bTrack += 1))); //切换通道
				}
		else
			if(nIndex == 1)//电池电压
				{
					fVal = ((float)nVal*0.121) + 0.5; 
					AdcAcquire.nVal[nIndex] = (uint16_t)fVal; 
				}
	 else
			if(nIndex == 2)//输出总电压
				{
					fVal = ((float)nVal*0.1538) + 0.5; 
					AdcAcquire.nVal[nIndex] = (uint16_t)fVal; 
				}
	 else
			if(nIndex == 3)//开关电源电压
				{
					fVal = ((float)nVal*0.1538) + 0.5; 
					AdcAcquire.nVal[nIndex] = (uint16_t)fVal; 
				}
		else
			if(nIndex == 4)//BOOST输出电流
				{
					fVal = ((float)nVal*0.217) + 0.5; 
					AdcAcquire.nVal[nIndex] = (uint16_t)fVal; 
				}	
}

//======================================================
//adc deal
//依次切换通道，进行数据采样
//======================================================

void ADCEventDeal()
{
	uint16_t nAdc = 0;
	uint8_t nSet = 0;
	//
	static uint8_t nInx = 0;
	static uint8_t nCnt = 0;
	static uint32_t nVal = 0;
	static uint8_t nDly = 0;

	//////////////////////ADC WAIT//////////////////////
	if(AdcAcquire.bWait)
	{
		//当进行开关操作时，电压电流变化缓慢
		//此时暂停读取采样数据，待稳定后再采样
		nDly ++;
		nCnt = 0;
		nVal = 0;
		AdcAcquire.bTrig = FALSE;
		//
		if(nDly >= 2) //wait cycles采样讲间隔时间
		{
			nDly = 0;
			AdcAcquire.bWait = FALSE;
		}
	}
	//////////////////////ADC READ//////////////////////
	if(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC)) //转换完毕
	{
		nAdc = ADC_GetConversionValue(ADC1);
		//
		//4和7需等待bWait，电压电流响应要时间
		if((AdcAcquire.bWait == TRUE)&&((nInx== 1)||(nInx== 2)))
		{
			nInx ++;
			nCnt = 0;
			nVal = 0;
			nSet = TRUE;
		}else
		{
			nCnt++;
			nVal += nAdc;
			if(nCnt == 20)
			{
				AdcConvert(nInx, nVal/20);
				AdcAcquire.bTrig = TRUE;
				nVal = 0;
				nInx++;
				nCnt = 0;
				nSet = TRUE;

			}//if
		}
		//
		if(nInx > 4) nInx = 0;
		//依次切换通道
		if(nSet)
		{
			nSet = FALSE;
			//
					if(nInx == 0) 	   ADC_RegularChannelConfig(ADC1, ADC_Channel_9, 1, ADC_SampleTime_239Cycles5); //输出通道电压
					else if(nInx == 1) ADC_RegularChannelConfig(ADC1, ADC_Channel_10,1, ADC_SampleTime_239Cycles5);//电池电压
					else if(nInx == 2) ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1, ADC_SampleTime_239Cycles5); //输出总电压
					else if(nInx == 3) ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_239Cycles5); //开关电源电压
					else if(nInx == 4) ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5); //BOOST输出电流
//					else if(nInx == 6) ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 1, ADC_SampleTime_239Cycles5); //电池充电电压
//					else if(nInx == 7) ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1, ADC_SampleTime_239Cycles5); //电池充电电流
     }
			//
		ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	}
}


//======================================================
//ADC SW ABC=0X00，X0输入
//======================================================
void SetADCSwPin(unsigned char bSw)
{
    bSw = bSw - 1;
	//
	if(MC4051_ABCBuf[bSw]&0x01)//使能A
		GPIO_SetBits(GPIOC, GPIO_Pin_6);
	else
		GPIO_ResetBits(GPIOC, GPIO_Pin_6);
	//
	if(MC4051_ABCBuf[bSw]&0x02)//使能B
		GPIO_SetBits(GPIOB, GPIO_Pin_15);
	else
		GPIO_ResetBits(GPIOB, GPIO_Pin_15);
	//
	if(MC4051_ABCBuf[bSw]&0x04)//使能C
		GPIO_SetBits(GPIOB, GPIO_Pin_14);
	else
		GPIO_ResetBits(GPIOB, GPIO_Pin_14);
}


