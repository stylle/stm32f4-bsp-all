
#ifndef STDAFX_H
#define	STDAFX_H

//#include "stdio.h"
#include "stm32f10x.h"
#include "MyFun.h"
#include "system.h"
#include <stdio.h>
#include "stm32F10x_ph_ext.h"
#include "stm32F10x_pwr.h"
#include "FrameDeal.h"
#include "stm32f10x_it.h"
#include "stm32f10x_develop.h"
#include "at24cxx.h"
#include "fifo.h"
#include "a_global.h"
#include "string.h"
#include "SerialScreen.h"
#include "AdcDeal.h"
#include "FaultDeal.h"
#include "Standard.h"
#include "Indicate.h"
#include "SysDataDeal.h"
#include "MessageDeal.h"
#include "PowerWork.h"
#include "stm32f10x.h"
#include "plc.h"
#include "LampDeal.h"
#include "can_config.h"

//====================================================
//define极限参数设置
//====================================================
#ifndef	TRUE
#define	TRUE		1
#endif

#ifndef FALSE
#define	FALSE		0
#endif

#define	PRF_ON  0
//======================================================
//输出开路参数宏
//======================================================
#define VSHORT           5 //输出短路判断值0.3V/1V阳光项目出现过问题
#define VOPEN            50 //输出开路判断值5V

//======================================================
//充电状态参数宏
//======================================================
#define CHAR_CHECK       0   //电池充电前检测
#define CHAR_READY1      1   //电池充电准备1
#define CHAR_READY2      2   //电池充电准备2
#define QUICK_CHAR       3   //进入均衡充电
#define CNSTAT_CHAR      4   //进入恒压充电
#define CPSTE_CHAR       5   //进入浮充充电


//======================================================
//电池充电参数宏
//======================================================
#define	MAX_NUM_BAT			3		//电池数量3只
#define	BAT_CAP_NUM			20			//电池容量55AH

#define	MY_TYPE				  DEV_POWER		//设备类型

#define CHAR_OPEN       2   //电池开路判断电流阀值0.2A
#define CHAR_MAX_I      ChrI+5 //电池充电电流最大值0.8C20A
#define CHAR_MIN_I      ChrI  //电池充电电流最小值0.7C20A1

#define BAT_BACK_V     (115*MAX_NUM_BAT) //放电恢复值11.7V/节

#define BAT_CHAR_MAX_V_L   143 //充电限制电压值14.0V/节

#define BAT_QUICK_STAR_V   129 //均充启动电压值12.9V/节
#define BAT_CHAR_MAX_V     153 //充电过压值15.0V/节
#define BAT_CPSTE_CHAR_V_L 137 //浮充低压值13.7V/节

#define CHAR_MAX_V      (MAX_NUM_BAT*BAT_CHAR_MAX_V) //充电过压值45.0V/3节
#define QUICK_STAR_V    (MAX_NUM_BAT*BAT_QUICK_STAR_V) //均冲电压启动值38.7V/3节

#define CHAR_CHANG_I    (BAT_CAP_NUM*0.08)   //充电状态转换电压阀值0.08C20A
#define QUICK_MAX_V_L   (MAX_NUM_BAT*BAT_CHAR_MAX_V_L) //均充电压值42V/3节

#define DUTY_STEP_NUM   1   //充电占空比步进值 1

#define CPSTE_CHAR_V_L  (MAX_NUM_BAT*BAT_CPSTE_CHAR_V_L) //浮充最低电压41.1V/3节
#define CHAR_ADJUST_TIM 5  //充电调整间隔100ms*3=300ms
#define CPSTE_CHAR_TIM  1000//浮充充电时间100ms*5*1000=500s充电


#define CPSTE_CHAR_V_H  (CPSTE_CHAR_V_L + 5)
#define CPSTE_STAR_V    (QUICK_STAR_V + 5)

#define QUICK_MAX_V_LH  (QUICK_MAX_V_L + 6)
#define QUICK_MAX_V_LL  (QUICK_MAX_V_L - 6)

//#define INIT_PWM       	100

//======================================================
//主电保护参数宏
//======================================================
#define VMAIN_LL_NUM    140 //主电故障值140VAC
#define VMAIN_L_NUM     180 //主电欠压值180VAC
#define VMAIN_LH_NUM    250 //主电过压值250VAC
#define FELI_VBAT			  260 //铁锂离子电池// ((MAX_NUM_BAT < 3)? 112 : 109) //正常放电结束终止电压10.7V/节
#define GULI_VBAT 			260 //三元锂离子电池
#define VMODEL_L_VEF    50 //模块电压欠压值025.0V
#define VMODEL_H_VEF    150 //模块电压过压值45.0V
#define VBAT_FSTOP1     80 //强制放电结束终止电压9.0V/节

#define OUT_OPEN_NUM    10 //开路电流阀值0.1A
#define VBAT_ON_SHORT   30 //充电短路阀值0.3V

#define BAT_IDIS_OVR    15//电池过流保护点1.5A

#define VBAT_ON_NUM     60 //电池在位低阀值0.6V
#define VBAT_ON_HNUM    150//电池在位高阀值1.5V


#define VBAT_STOP1  1   

#define VMODEL_L_NUM   (VMODEL_L_VEF * MAX_NUM_BAT) //模块电压欠压值10V*电池数量
#define VMODEL_H_NUM   (VMODEL_H_VEF * MAX_NUM_BAT) //模块电压过压值15V*电池数量
#define VBAT_STOP2     (VBAT_STOP1 * MAX_NUM_BAT) //正常放电结束终止电压10.7V/节
#define VBAT_FSTOP     (VBAT_FSTOP1 * MAX_NUM_BAT) //强制放电结束终止电压10.7V/节



//======================================================
//占空比参数宏
//================================================1.515======
#define	DUTY_INIT					0   //初始占空比0%
#define	CHR_INIT					0   //充电初始化占空比0%
#define	PWM_DUTY_MIN			10   //输出最小占空比%
#define	PWM_DUTY_MAX			(int)(PWM_CYCLE * 0.66)//输出最大占空比%
#define MINCHARPWM        0   //充电最小占空比限制到0%
#define MAXCHARPWM        500 //充电最大占空比限制到50%

//======================================================
//定时器参数宏
//======================================================
#define	SPCE_KEPP_TIM	    1000  //切换死区时间10ms
#define	PWM_CYCLE			  	720 //PWM周期  20uS

#define	SPCE_TIM7	        200  //时间10ms


#define	BUF_UP_TIM	  		20  //软启动时间5*1440=7.2s
#define	UPDATE_SPAN			  100	//间隔执行函数时间间隔100ms
#define	SHORT_RECOVER_V		10  //短路恢复电压点 1V
#define	SHORT_RECOVER_T		3000//恢复时间点 10SV


//======================================================
//散热器参数宏
//======================================================
#define	FAN_INIT_TIME			2000//启动风扇旋转时间 

//======================================================
//功率参数宏
//======================================================
#define PAASCURENNT1      ((MAX_NUM_BAT == 3)? 360 :330)//1.3倍过载 保护限值
#define PAASCURENNT2    	120000  //1.5倍过载 保护限值

//======================================================
//采样器参数宏
//======================================================
#define	VMAIN_ADC_Num	  200  //市电切换采样次数

//======================================================
//通讯参数宏
//======================================================
#define HEAD							0x0A //贞头
#define UR_Choose         1    //0为第一主串口UART1，1为第二备用串口USART1

#define	SET_485A_TX	GPIO_SetBits(GPIOB, GPIO_Pin_3)//主485
#define	SET_485A_RX	GPIO_ResetBits(GPIOB, GPIO_Pin_3)//主485

#define	SET_485B_TX	GPIO_SetBits(GPIOA, GPIO_Pin_15)//屏幕
#define	SET_485B_RX	GPIO_ResetBits(GPIOA, GPIO_Pin_15)//屏幕

#define	SET_485C_TX	GPIO_SetBits(GPIOA, GPIO_Pin_8)//备用
#define	SET_485C_RX	GPIO_ResetBits(GPIOA, GPIO_Pin_8)//备用


//======================================================
//蜂鸣器参数宏
//======================================================
#define	BELL_SHORT    	  5 //蜂鸣时间1.0s
                                                                                                                           
#define	BELL_TWO	    	  13 //蜂鸣时间1.3s
#define	BELL_LONG			  	20 //蜂鸣时间2.0s

//输出模式
#define	TEST_MODEL_OPEN		GPIO_SetBits(GPIOB, GPIO_Pin_12)//正常模式
#define	TEST_MODEL_CLOSE	GPIO_ResetBits(GPIOB, GPIO_Pin_12)//放电模式

//电池采样开关信号继电器
#define	BAT_SAP_ON		GPIO_SetBits(GPIOC, GPIO_Pin_0)
#define	BAT_SAP_OF		GPIO_ResetBits(GPIOC, GPIO_Pin_0)

//主电开关
#define	MAIN_ENABLE		1 //GPIO_ResetBits(GPIOB, GPIO_Pin_14)
#define	BAT_ENABLE	  1 //GPIO_SetBits(GPIOB, GPIO_Pin_14)

//铅酸电池充电器输出开关
#define	CHAR_ENABLE		GPIO_SetBits(GPIOC, GPIO_Pin_6)
#define	CHAR_DISABLE	GPIO_ResetBits(GPIOC, GPIO_Pin_6)

//铅酸电池充电器输出开关
#define	Li_CHAR_ENABLE	 GPIO_ResetBits(GPIOC, GPIO_Pin_11)
#define	Li_CHAR_DISABLE	 GPIO_SetBits(GPIOC, GPIO_Pin_11) 

//MOS风扇控制
#define	MOSFUNOPEN    GPIO_SetBits(GPIOC, GPIO_Pin_12)
#define	MOSFUNCLOS	  GPIO_ResetBits(GPIOC, GPIO_Pin_12)

//模块风扇控制
//#define	MODELFUNOPEN    GPIO_SetBits(GPIOC, GPIO_Pin_9)
//#define	MODELFUNCLOS	  GPIO_ResetBits(GPIOC, GPIO_Pin_9)

//#define	FAULTLEDOPEN  	GPIO_SetBits(GPIOC, GPIO_Pin_9)
//#define	FAULTLEDCLOS	  GPIO_ResetBits(GPIOC, GPIO_Pin_9)

//#define Read_OUSHORT    GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13)

//强制应急
#define	FORCE_IN		GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8)

//手动输入
#define	HAND_IN		  GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9)

////Flash-LED
//#define	SET_FLASH_LED GPIO_SetBits(GPIOA,GPIO_Pin_12)
//#define	CLR_FLASH_LED GPIO_ResetBits(GPIOA,GPIO_Pin_12)

////Flash-LED
//#define	BELL_ON     GPIO_SetBits(GPIOC,GPIO_Pin_1)
//#define	BELL_OF     GPIO_ResetBits(GPIOC,GPIO_Pin_1)

//AC-Reset
#define	AC_RESET_ON GPIO_SetBits(GPIOA,GPIO_Pin_11)
#define	AC_RESET_OF GPIO_ResetBits(GPIOA,GPIO_Pin_11)

//照明灯具模式设置
#define	VMAIN_ERR    GPIO_SetBits(GPIOB,GPIO_Pin_6)
#define	VMAIN_NOM    GPIO_ResetBits(GPIOB,GPIO_Pin_6)

#define	FirInPut   GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8) //消防联动输入

//AC市电检测
#define AC_CHECK   GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6) //市电检测-正常照明检测

//Flash-LED
#define	SET_FLASH_LED GPIO_SetBits(GPIOA,GPIO_Pin_8)
#define	CLR_FLASH_LED GPIO_ResetBits(GPIOA,GPIO_Pin_8)

//FAUL-LED
#define	SET_FAUL_LED GPIO_SetBits(GPIOA,GPIO_Pin_15)
#define	CLR_FAUL_LED GPIO_ResetBits(GPIOA,GPIO_Pin_15)

//MAIN-LED
#define	SET_MAIN_LED GPIO_SetBits(GPIOB,GPIO_Pin_6)
#define	CLR_MAIN_LED GPIO_ResetBits(GPIOB,GPIO_Pin_6)

//EMGY-LED
#define	SET_EMGY_LED GPIO_SetBits(GPIOB,GPIO_Pin_4)
#define	CLR_EMGY_LED GPIO_ResetBits(GPIOB,GPIO_Pin_4)

//CHAR-LED
#define	SET_CHAR_LED GPIO_SetBits(GPIOB,GPIO_Pin_5)
#define	CLR_CHAR_LED GPIO_ResetBits(GPIOB,GPIO_Pin_5)

//CAN-LED
#define	SET_CAN_LED GPIO_SetBits(GPIOB,GPIO_Pin_3)
#define	CLR_CAN_LED GPIO_ResetBits(GPIOB,GPIO_Pin_3)
//BELL
#define	BELL_ON     GPIO_SetBits(GPIOC,GPIO_Pin_9)
#define	BELL_OF     GPIO_ResetBits(GPIOC,GPIO_Pin_9)

//输出
#define	LOAD_ENABLE	   GPIO_SetBits(GPIOA, GPIO_Pin_2)
#define	LOAD_DISABLE	 GPIO_ResetBits(GPIOA, GPIO_Pin_2)

//应急二总线回路
#define	EMGY_BUS_ENABLE		GPIO_SetBits(GPIOB, GPIO_Pin_11)
#define	EMGY_BUS_DISABLE	GPIO_ResetBits(GPIOB, GPIO_Pin_11)

//数据回传基准平台
#define	GET_DATA_OPEN		GPIO_SetBits(GPIOB, GPIO_Pin_10)
#define	GET_DATA_CLOSE	GPIO_ResetBits(GPIOB, GPIO_Pin_10)  

//二总线放电使能
#define	PULL_CLOSE		GPIO_SetBits(GPIOA, GPIO_Pin_5)
#define	PULL_OPEN	    GPIO_ResetBits(GPIOA, GPIO_Pin_5) 

//后四路持续
#define	LAST_ENABLE		GPIO_SetBits(GPIOA, GPIO_Pin_7)
#define	LAST_DISABLE	GPIO_ResetBits(GPIOA, GPIO_Pin_7)

//联动反馈
#define	EMGY_BACK_OPEN	GPIO_SetBits(GPIOC, GPIO_Pin_7)
#define	EMGY_BACK_CLOS 	GPIO_ResetBits(GPIOC, GPIO_Pin_7)


//短路读取
#define	OUT_SHORT		ADCBUF[9]
//====================================================
//global declare
//====================================================
extern unsigned char gbInit;
extern unsigned char gbEFlag;
extern unsigned char gbUpdate;
extern FRM_NODE MyFrmNode;

#endif

