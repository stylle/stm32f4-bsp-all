
//======================================================
//国标基本功能
//自检、自动应急、手动应急、强制应急、年检、月检
//======================================================


#include "stdafx.h"

//======================================================
//var
//======================================================

EPS_STATE EpsState;			//power state
unsigned char nstate = 0;//判断是否是在手动按键应急状态
unsigned char Test_State = 0;
static unsigned char KeyTim = 0;
static unsigned char KeyNum = 0;
unsigned int Chrontim = 0;
//======================================================
//Eps working state machine
//======================================================
void EpsWorking()
{
	
	static char nState = 0;
	//
	if(gbInit != FALSE) return;//延时
	//
	switch(nState)
	{
		//=====================================
		//空闲
		//=====================================
		case 0:
		{
			EpsState.nEgTm = 0;
			EpsState.nType = EMGY_IDLE; //空闲
			SET_ST_YCHK(EpsState.nState, FALSE);//2020.4.30改动

			if(nstate == 0)	SET_ST_EMGY(EpsState.nState, FALSE);
			
			if(EpsState.bForce) nState = 1;       //强起应急关闭
			else if(EpsState.bAtEmy) nState = 2;	//自动应急关闭
			else if(EpsState.bMuEmy) nState = 3;	//手动应急关闭
			else if(EpsState.bMManu) nState = 4;	//手动月检关闭
			else if(EpsState.bYManu) nState = 5;	//手动年检关闭
			else nState = 0;		 
	}
		break;
		//=====================================
		//强制应急
		//=====================================
		case 1:
		{
			 if(IS_ERR(gbMyError, FUP_BAT_VLOW)) //电池错误己发生,后期改为电池开路已发生
			{
				nState = 0;
				return;
			}	
			EpsState.nType = FORC_EMGY;	//强制应急
	    SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			SysInd.bMute = FALSE; //关闭消音
			//
			nState = 6;

	}break;
		//
		case 6:
		{
			EpsState.bClear = TRUE; //清空计数
			if((!EpsState.bForce)&&(!EpsState.bAtEmy)) //停止强制应急
			{
				nState = 0;
			}else
			if((!EpsState.bForce)&&(EpsState.bAtEmy)) //停止强制应急
			{
				nState = 2;
			}
    	else
			if(EpsState.bMuEmy || EpsState.bMManu || EpsState.bYManu)
			{  
				//强制应急发生时，下面的操作无效
				EpsState.bSelfSig &= ~(1<<MANU_EMGY);
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
				EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
		}

		break;
		//=====================================
		//自动应急
		//=====================================
		case 2:
		{

			EpsState.nType = AUTO_EMGY;	//自动应急
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			//
			SysInd.bMute = FALSE; //关闭消音
			//
			nState = 7;
		}

		break;
		//
		case 7:
		{

			EpsState.bClear = TRUE; //清空计数	
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状
			if(!EpsState.bAtEmy) //停止自动应急
			{
				nState = 0;
			}else
			if(EpsState.bForce)
			{
				nState = 1; //跳转到强制应急
			}else
			if(EpsState.bMuEmy || EpsState.bMManu || EpsState.bYManu)
			{
				EpsState.bSelfSig &= ~(1<<MANU_EMGY);
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
				EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
			
	  }
		break;
		//=====================================
		//手动应急
		//=====================================
		case 3:
		{

			EpsState.nType = MANU_EMGY;		//手动应急
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			//
			SysInd.bMute = FALSE; //关闭消音
			//
			nState = 8;
	  }
		break;
		//
		case 8:

		{
			EpsState.bClear = TRUE; //清空计数
			//
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			if(!EpsState.bMuEmy) //停止手动应急
			{
				nState = 0;
		  	EpsState.bSelfSig &= ~(1<<MANU_EMGY);
			}else
			if(EpsState.bForce)
			{
				nState = 1; //跳转到强制应急
			}else
			if(EpsState.bAtEmy)
			{
				nState = 2; //跳转到自动应急
			}else
			if(EpsState.bMManu || EpsState.bYManu)
			{
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			  EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}

	 }
		break;
		//=====================================
		//手动月检
		//=====================================
		case 4:
		{
//			if((IS_ERR(gbMyError, FUP_YCK_VLOW))||(IS_ERR(gbMyError, FUP_MCK_VLOW))||\
//				 (IS_ERR(gbMyError, FUP_MAIN_ERR))||(IS_ERR(gbMyError, FUP_BAT_OPEN))) //年检错误己发生//月检错误己发生
//			{
//			nState=0;
//			return;
//			}
			//
			EpsState.nType = MMCK_EMGY;
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			//
			SysInd.bMute = FALSE; //关闭消音
			//
			nState = 9;
		}
		break;
		//
		case 9:
		{

			EpsState.bClear = TRUE; //清空计数
			//
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			if(!EpsState.bMManu) //停止操作
			{
				nState = 0;
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			}else
			if(EpsState.bForce)
			{
				nState = 1; //跳转到强制应急
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			}
			else
			if(EpsState.bAtEmy)
			{
				nState = 2; //跳转到自动应急
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			}
			else
			if(EpsState.bMuEmy)
			{
				nState = 3; //跳转到手动应急
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			}
			else
			if(EpsState.nEgTm > MCHK_ETIME) //30-180s后停止应急
			{
				EpsState.bCtrlSig &= ~(1<<MMCK_EMGY);
				nState = 0;
			}
		}	
		break;
		//=====================================
		//手动年检
		//=====================================
		case 5:
		{

//			if((IS_ERR(gbMyError, FUP_YCK_VLOW))||(IS_ERR(gbMyError, FUP_MCK_VLOW))||\
//				 (IS_ERR(gbMyError, FUP_MAIN_ERR))||(IS_ERR(gbMyError, FUP_BAT_OPEN))) //年检错误己发生//月检错误己发生
//			{
//				nState=0;
//				return;
//			}
				//
			EpsState.nType = MYCK_EMGY;
			SET_ST_EMGY(EpsState.nState, TRUE);	//设置应急状态
			//
			SysInd.bMute = FALSE; //关闭消音
			//
			nState = 10;				 
		}
		break;
		//
		case 10:
		{

			EpsState.bClear = TRUE; //清空计数
			SET_ST_YCHK(EpsState.nState, TRUE);
			//
			if(!EpsState.bYManu) //停止操作
			{
				nState = 0;
				SET_ST_YCHK(EpsState.nState, FALSE);
				//EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
			else
			if(EpsState.bForce)
			{
				nState = 1; //跳转到强制应急
				SET_ST_YCHK(EpsState.nState, FALSE);
				EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
			else
			if(EpsState.bAtEmy)
			{
				nState = 2; //跳转到自动应急
				SET_ST_YCHK(EpsState.nState, FALSE);
				EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
			else
			if(EpsState.bMuEmy)
			{
				nState = 3; //跳转到手动应急
				SET_ST_YCHK(EpsState.nState, FALSE);
				EpsState.bSelfSig &= ~(1<<MYCK_EMGY);
			}
			else
			if(EpsState.bMManu)
			{
				EpsState.bSelfSig &= ~(1<<MMCK_EMGY);
			}
		}
		break;
	}
}

//======================================================
//系统年检或月检
//======================================================
void SysCheckCount(unsigned char nMinute,unsigned char bAccle)
{
//	static unsigned char nHour = 0;
//	static unsigned char nMain = 0; //main power working
//	//
//	if(nMinute == 60) 
//	{
//		nHour++; //小时计数
//	}
//	//
//	if(EpsState.bClear == TRUE)
//	{
//		nMain = 0;
//		EpsState.bClear = FALSE;
//		EpsState.bSelfSig &= ~(1<<AMCK_EMGY);
//		EpsState.bSelfSig &= ~(1<<AYCK_EMGY);
//	}else
//	{
//		if(nMain >= 24) nMain = 24;
//		else if((nMinute == 60)||bAccle) nMain++;
//		//
//		if((nHour == 24)||(bAccle && (!IS_ERR(gbMyError, FUP_BAT_CHAR))&&\
//			(!IS_ERR(gbMyError, FUP_YCK_VLOW))&&(!IS_ERR(gbMyError, FUP_MCK_VLOW)))) //模拟倒计时是在所有条件正常时进行
//		{
//			if(EpsState.nTpYCk > 0) EpsState.nTpYCk--;
//			if(EpsState.nTpMCk > 0) EpsState.nTpMCk--;
//		
//			nHour = 0;
//		}
//	}
//	//
//	if((EpsState.nTpMCk == 0)&&(nMain >= 24)) 
//		EpsState.bSelfSig |= (1<<AMCK_EMGY);
//	//
//	if((EpsState.nTpYCk == 0)&&(nMain >= 24)) 
//		EpsState.bSelfSig |= (1<<AYCK_EMGY);
//	//
//	if((EpsState.nTpYCk == 0)||(EpsState.bSelfSig&(1<<AYCK_EMGY)))
//	{
//		EpsState.bMCkEnd = TRUE;
//		EpsState.bSelfSig &= ~(1<<AMCK_EMGY);
//	}
//	//
//	if(EpsState.bMCkEnd) 
//	{
//		EpsState.bMCkEnd = FALSE;
//		EpsState.nTpMCk = MCHK_DAYS;
//	}
}

//手动按钮处理
void HandRead()
{
	   if(HAND_IN == RESET)
				{
						KeyTim = (KeyTim >=100)? (100):(KeyTim + 1);
					if((KeyTim == 3)||(KeyTim == 12)||(KeyTim == 22)\
						||(KeyTim == 32)||(KeyTim == 42)||(KeyTim == 52)||(KeyTim == 100))
					{
						Beep(2,80,2);
					}		
				  if(HAND_IN == RESET) return;					
				}
			if((KeyTim  > 0)&&(KeyTim <= 3))
					{				
						KeyTim = 0;		
						if(IS_ST_EMGY(EpsState.nState)) KeyNum ++;
						if(KeyNum > 2)
						{
						  KeyNum = 0;
							Off_Systm();
					 }
					}
			else
			if((KeyTim  > 3)&&(KeyTim <= 10))//一声
					{				
						  KeyTim = 0;							
						  SysInd.bMute = TRUE; //消音12
					  	PassCurenntbFlag = 0;
							Myec.Test_Model = 0xAA;
//						if(Myec.AgeTime_M <= 0)TEST_MODEL_OPEN;
						  TEST_MODEL_OPEN;
							SaveSysData();
              Beep(3,100,1);
					}
			else
			 if((KeyTim >= 10)&&(KeyTim < 20)&&(!IS_ST_EMGY(EpsState.nState)))//2声
					{ 
						KeyTim = 0;	
						nstate = 1;
						SET_ST_EMGY(EpsState.nState, TRUE);
						EpsState.bSelfSig |= (1<<MANU_EMGY);
					}
			else
			 if((KeyTim >= 20)&&(KeyTim < 30))//3声
					{
					  KeyTim = 0;
					 if(Myec.Beep_nState == 0xCC) Myec.Beep_nState = 0xDD;
					 else Myec.Beep_nState = 0xCC,SaveSysData();	
	
					}
			else
			 if((KeyTim >= 30)&&(KeyTim < 40))//4声
					{
						KeyTim = 0;					
						Myec.Test_Model = 0xBB;
						TEST_MODEL_CLOSE;	
						SaveSysData();
						Beep(10,100,1);
					}
			else
				if((KeyTim >= 40)&&(KeyTim < 50))//5声
					{
						 KeyTim = 0;						
						if(Myec.LampZlSet == 0xBF)
						{
							Myec.LampZlSet = 0x08;	
						}							
						else
						if(Myec.LampZlSet == 0x08)	
						{
							Myec.LampZlSet = 0xBF;				
						}				
						 SaveSysData();	
//						 ShortEn = 0;  //屏蔽短路检测
					}
			else
				if((KeyTim >= 50)&&(KeyTim < 100))//6声
					{
						 KeyTim = 0;
						if(Myec.AgeTime_M > 0)
						{
							Myec.AgeTime_S = 60;
							Myec.AgeTime_M = 0;
							//SaveSysData();	
						}
						else
						{
						 Myec.AgeTime_S = 0;						
						 Myec.AgeTime_M = 120;
						 //SaveSysData();	
						}
					}			
			else
				 KeyTim = 0;
		
	if((EpsState.hEgTm > 60)&&(nstate == 1))
		{
			EpsState.bReset = 1;
			SET_ST_EMGY(EpsState.nState, FALSE);
			EpsState.hEgTm =0;
			nstate=0;
		}		

}

void Beep(unsigned char Num, unsigned char Ms, unsigned char Scale)
{
	unsigned char Bell_Num = 0;
	for(Bell_Num = 0;Bell_Num < Num;Bell_Num ++ )
		{								
			BELL_ON;
			DelayMs(Ms);
			BELL_OF;
			DelayMs(Ms/Scale);
		}	
}

void Off_Systm(void)
{
	 GET_DATA_CLOSE;   //发送完成初始化成Out_PP模式 2
	 SetTxToPPMode();  //发送完成初始化成Out_PP模式 2
	 LOAD_DISABLE;
	 SetBoostPwmDuty(0);
	 while(1);
}

void Off_Out(void)
{
	 GET_DATA_CLOSE;
	 PULL_CLOSE;    //失能放电	
	 SetTxToPPMode();  
	 LOAD_DISABLE;
}


void Open_Out(void)
{
	unsigned char i = 0;
	SetTxToAFMode();
	InitTm2Ch3ToPwmMode(0);
	TIM_CtrlPWMOutputs(TIM2, ENABLE);
	for(i = 0;i < 100;i ++)
	{
	 TIM_SetCompare3(TIM2, i); //
		DelayMs(10);
	}
	TIM_CtrlPWMOutputs(TIM2, DISABLE);
	
}


