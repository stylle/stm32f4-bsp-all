
#ifndef	SYSDATADEAL_H
#define	SYSDATADEAL_H

#include "system.h"

//======================================================
//define
//======================================================
#define	ADDR_OFT_EPS		0x0801FF08
#define	ADDR_OFT_LABLE	0x0801FF00

//======================================================
//struct
//======================================================
typedef struct
{
	unsigned char bSoftVer;
	unsigned char bDevType;
	unsigned char bItemNo;
	unsigned char nAddress;

}ELEC_LABLE;


typedef struct
{
	unsigned char bPowerStyle;
  unsigned char Test_Model;
  unsigned char Beep_nState;
	unsigned char AgeTime_M;
	unsigned short AgeTime_S;
	unsigned char LampZlSet; //灯具持续非持续转换
	
}ELEC_L;



//======================================================
//function
//======================================================

void IfrScan(void);
void ReadLable(void);
void WriteLable(void);
void SaveSysData(void);
void InitElecLable(void);
void ReadFlashData(void);
void Writestate(unsigned char);
void WriteEmgy(unsigned char);
void Writeaddra(unsigned char);


extern ELEC_LABLE MyLable;
extern ELEC_L Myec;

#endif

