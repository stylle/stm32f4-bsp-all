
#ifndef	LAMP_DEAL_H
#define	LAPM_DEAL_H

//====================================================
//define
//====================================================
#define TCCH(n)					(1<<(n-1))	//通道值
#define	GET_LAM_CMD	(n)	n&0x0f //灯具命令
#define	QUERY_SPAN1			500		//查询间隔(QUERY_SPAN1/10)ms
#define	QUERY_SPAN2			5000		//查询间隔(QUERY_SPAN2/10)ms
#define	QUERY_SPAN3			200		//查询间隔(QUERY_SPAN3/10)ms

#define	QUERY_NUM		  	50		//
//typedef struct
//{
//	unsigned char bDealing;
//	unsigned short nState;
//	unsigned char ling;
//}QUERY_SITE;

typedef struct
{
	unsigned char bError;	//当前灯具故障表
	unsigned char bLpSt;	//当前灯具状态
	unsigned char bWait;	//查询等待
//	unsigned short nAddr;	//当前灯具地址2位
	unsigned int nAddr;	//当前灯具地址3位
	unsigned short nLampVal; //查询灯具参数(调试)
	unsigned char lampstate; //灯具的状态
}LAMP_NODE;

typedef struct
{
	unsigned char bMask;		//track enable-通道使能
	unsigned char bTcch;		//tc100b channel-通道码(1~8)
	unsigned char bChMd;		//channel mode
}CHANNEL;

//====================================================
//extern
//====================================================
extern unsigned char gbMask;		//track enable-通道使能
extern unsigned char gbTcch;		//tc100b channel-通道码(1~8)
extern unsigned char gbLpSt;		//state for lamp(是否应急)
extern unsigned char gbChMd;		//channel mode(收or发)
extern unsigned char OneReduct;
extern unsigned short bAddrInx;

extern PLC_NODE MyPlcNode;
//extern QUERY_SITE MyQuery;
extern LAMP_NODE LampNode;
extern CHANNEL ChanSet;

extern unsigned short LampAddrBufLen;


//====================================================
//function
//====================================================
void DoPlcRevDeal(void);
void DoQueryDeal(void);
void OpenUart(void);
void CloseUart(void);
void SetUartToRxMode(void);
void SetUartToTxMode(void);
void SetTxValue(unsigned char bValue);
void SetRxToIpuMode(void);
void SetTxToPPMode(void);
void SetTc100BRxMode(unsigned char bFlag);
void SetTc100BTxMode(unsigned char bFlag);
void SendStaMachine(void);
void StartPTimer(void);
void StopPTimer(void);
//void QueryInTurns(void);
void SendFrameByUR2(unsigned char*pBuf, unsigned char bLen);
void initRT(void);

unsigned char IsMyLamp(unsigned short nAddr);
unsigned char CountLampTrack(unsigned short nAddr);
unsigned char CountLampAddr(unsigned short nAddr);
unsigned short CountLampPhyAddr(unsigned char nTrack, unsigned char nAddr);
void QueryLampInTurn(void);
void ReciverOut(void);

#endif

