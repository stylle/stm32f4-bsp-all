#include "stdafx.h"
//======================================================
//灯具管理
//======================================================
PLC_NODE MyPlcNode; 		//master frame node(通信节点)
LAMP_NODE LampNode;			//lamp parameter(灯具参数)
QUERY_SITE MyQuery;
unsigned char reduct=0; //复位指令
unsigned char OneReduct=1;
unsigned char tabx = 0;
unsigned char bTInx = 0;
unsigned short bAddrInx = 0;//地址适应3位  2位用short
extern unsigned char times;
//extern unsigned short AddrIntral[20];
extern unsigned short AddrIntral[20];//3位
extern unsigned short nSerchLampSectionNum; //灯具区间数量
unsigned short MaxTemp = 1; //假设如果没有上位机，就1个灯
unsigned char Gallerynum = 0;
//unsigned short Lampnum = 0;
//unsigned short LampAddrBuf[250];//地址数组
unsigned short LampAddrBuf[250];//地址数组
unsigned short LampAddrBufLen;//读取的第一次在线的长度
unsigned short len = 0;  //记录复位后第一次在线的灯具数量变量加
unsigned short bLen=0;//发送停止标记
extern unsigned short LAMPNUM ; //灯具控制的具体灯具编号
unsigned char TimeOutCnt=0;
unsigned char  StarIntral = 0;
unsigned char LampState[250];
//======================================================
//plc deal
//灯具数据回传处理
//======================================================

void DoPlcRevDeal()//54049地址
{	
	LampNode.nAddr = MyPlcNode.frmTx.nSlaver; //count lamp physic addr		
	if((OneReduct == 1)&&(EmgStateKeepTim > 15))
	{
		LampAddrBuf[len] = LampNode.nAddr;//第一次在线灯具地址存储数组
		LampState[len] = LampNode.bError;
//	if(EmgStateKeepTim >= 10) 				
		bAddrInx ++;//第一次地址都是从最小区间开始
		len ++;	
			if((bAddrInx > AddrIntral[(StarIntral*2)+1])&&(len != 0))//当第一次1000个灯轮询完成
		{
			if(StarIntral <= (nSerchLampSectionNum - 1))
			{
				StarIntral ++;
		  	bAddrInx = AddrIntral[(StarIntral*2)];	
			}
			else
			{   
				StarIntral = 0;
				bAddrInx = AddrIntral[StarIntral];	
				OneReduct = 0;//复位标记置0，表示从新开始不按1000个轮询，而是按照地址数组轮询
				LampAddrBufLen = len; //变量BUFSUM表示第一次轮询在线的灯具总个数，也是在线地址数组的长度
				len = 0;
			}		
		}
		else if((bAddrInx > AddrIntral[(StarIntral*2)+1])&&(len == 0))
		{
			if(StarIntral <= (nSerchLampSectionNum - 1))
			{
				StarIntral ++ ;
		  	bAddrInx = AddrIntral[(StarIntral*2)];	
			}
			else
			{   
				StarIntral = 0;
				bAddrInx = AddrIntral[StarIntral];	
			}		
		}
	}
	else if((OneReduct == 0)&&(EmgStateKeepTim > 15))
	{		
		LampState[bLen] = MyPlcNode.frmRx.bDtBuf[0];//故障状态存储数组
		bLen ++ ;			
	}				
	TimeOutCnt = 0;	
	MyPlcNode.bDealEnd = TRUE;
}
//======================================================
//query deal-分配电进行灯具轮询
//控制器读取完本次的查询结果后才会继续轮询
//使用广播帧对短路的支路进行定时唤醒
//======================================================
void DoQueryDeal()
{ 
//	if(!gbTurn) return;
	QueryLampInTurn();
	MyQuery.bDealing = 0;
}

//======================================================
//query lamp-分配电进行灯具轮询
//======================================================
extern unsigned char EmgySend;//应急广播帧
extern unsigned char LampModelSet ;
unsigned char Oncereduct=0;//每次复位后的第一次轮询都是重新开始标记
unsigned char Broadcast = 0;   
void QueryLampInTurn(void)
{
	if(EmgySend == 0)
	{
			if(OneReduct == 1)MyPlcNode.frmTx.nSlaver = bAddrInx;//判断是否为复位
			else                                                 //如果是第二次开始轮询就按地址数组中地址轮询，每次读取地址数组。
			{	
				bLen = (bLen >=	LampAddrBufLen)?(0):(bLen);        //每次轮询到数组最后一个变量后，重新开始
				MyPlcNode.frmTx.nSlaver = LampAddrBuf[bLen] ;
			}	
				MyPlcNode.frmTx.bType = PLC_TYPE_DATA;
				MyPlcNode.frmTx.bDtBuf[0] = LampSet;	

				PFillTxBuf(&MyPlcNode, TRUE);
				PSetStateToRdy(&MyPlcNode);
	}
	else if(EmgySend == 1)
	{		
			TimeOutCnt = 0;
	
			MyPlcNode.frmTx.bType = PLC_TYPE_BROAD;
			MyPlcNode.frmTx.bDtBuf[0] = LampNode.bLpSt;
			PFillTxBuf(&MyPlcNode, TRUE);
			PSetStateToRdy(&MyPlcNode);
		
			if((EmgStateKeepTim < 15)) return ;
			EmgySend = (MaxTemp != 0)?(0):(1);
		
	}
}

//======================================================
//send machine
//======================================================
void SendStaMachine()
{
	static unsigned short nCnt = 0;
	if(MyPlcNode.bState == PLC_ST_SEND0) //uart send ok
	{
		nCnt++;
		if(nCnt == TP_DLY_CMD) //command delay下行完成后电立即延时（给总线充电且准备发送一个下降沿）
		{ 
			nCnt = 0;
			SetTxValue(0); //set tx high
			MyPlcNode.bState = PLC_ST_SEND1;
		}
	}else
	if(MyPlcNode.bState == PLC_ST_SEND1)
	{
		nCnt++;
		if(nCnt == TP_KEEP_LOW)
		{
			nCnt = 0;
			PULL_CLOSE;    //失能放电
			GET_DATA_OPEN; //打开数据获取基准
			MyPlcNode.bState = PLC_ST_SEND2;
		}

	}else
	if(MyPlcNode.bState == PLC_ST_SEND2)
	{
		nCnt++;
		if(nCnt == TP_KEEP_RX)
		{		
			nCnt = 0;
		  SetUartToRxMode();//21.12.20取消
			MyPlcNode.bState = PLC_ST_SEND3;
		}
	}else
	if(MyPlcNode.bState == PLC_ST_SEND3)
	{
		nCnt++;
		//
		if((MyPlcNode.bRxFlag)||(nCnt == PLC_OUT_TIME))
		{
			nCnt = 0;
			MyPlcNode.bState = PLC_ST_SEND4;
			//
		  GET_DATA_CLOSE; //关闭数据获取基准
			PULL_OPEN;      //使能放电
  		SetTxToPPMode();
			SetTxValue(1);  //将总 23.线设置高输出
			SetRxToIpuMode();
		}
	}else
	if(MyPlcNode.bState == PLC_ST_SEND4)
	{
		nCnt++;
		if(nCnt == TP_DLY_FRM)
		{
			nCnt = 0;
			//
			if(MyPlcNode.bRxFlag)
			{
				MyPlcNode.bRxFlag = 0;
				MyPlcNode.bState = PLC_ST_SEND5;
				MyPlcNode.nOutCnt = 0;
			}else
			{
				MyPlcNode.nOutCnt++;
				//
				if(MyPlcNode.nOutCnt == PLC_OUT_CNT)
				{
					MyPlcNode.nOutCnt = 0;
					//
					if(MyPlcNode.frmTx.bType == 0)
					{
						MyPlcNode.bState = PLC_ST_END; //time over
					}else
					{	
						MyPlcNode.bState = PLC_ST_TOUT; //time over//进入多次判断函数
					}
						//
					StopPTimer();
				}else
				{
					MyPlcNode.bSend = TRUE;
					MyPlcNode.bState = PLC_ST_READY;
					StopPTimer();
				}//else
			}//else
		}//if
	}else
	if(MyPlcNode.bState == PLC_ST_SEND5)
	{
		if(MyPlcNode.bDealEnd) //wait deal end
		{
			MyPlcNode.bDealEnd = 0;
			MyPlcNode.nOutCnt = 0;
			MyPlcNode.bState = PLC_ST_END;
			StopPTimer();
		}
	}//if
}

//======================================================
//QueryInTurns
//======================================================
void QueryInTurns()
{
	if(MyQuery.bDealing) return; //wait deal end at last
	else MyQuery.bDealing = 1;
}

//======================================================
//send frame
//统计低电平的个数
//======================================================
unsigned char bLenAddr[5]={0xaa,0x00,0xaa,0x00,0xaa};//测试用临时数据

void SendFrameByUR2(unsigned char*pBuf, unsigned char bLen)
{	
		unsigned char WordNum = 0; 
	
 //  SetTxToAFMode();  //发送时立即初始化成串口模式
	   SetUartToTxMode();//初始化串口时，串口回有一段低电平，注意硬件辅助，保证初始化不要有低电平
//			OpenUart();
//		pBuf[1]= (pBuf[1]==0xff)? (0xEE):(pBuf[1]);
//		pBuf[2]= (pBuf[2]==0xff)? (0xEE):(pBuf[2]);
		for(WordNum = 0; WordNum < bLen; WordNum ++)
		{			
			UartSendByte(USART2, pBuf[WordNum]); //sned byte
		//if(WordNum <= 1)SysTickDelayUs(1000);//2021.6.24屏蔽
	  }
		CloseUart();
		SetTxToPPMode();  //发送完成初始化成Out_PP模式 9 	
		SetTxValue(1);	
}

//======================================================
//lamp addr check
//======================================================
unsigned char IsMyLamp(unsigned short nAddr)
{
	unsigned short nMax,nMin;
	//
	nAddr -= LAMP_AOFT;
	//
	nMin = MyLable.nAddress*(MAX_NUM_TRACK*MAX_NUM_LAMP);
	nMax = nMin + ((MAX_NUM_TRACK*MAX_NUM_LAMP)-1);
	//
	if((nAddr >= nMin)&&(nAddr <= nMax))
		return TRUE;
	else
		return FALSE;
}

unsigned char CountLampTrack(unsigned short nAddr)
{
	unsigned short nBase;
	//
	nAddr -= LAMP_AOFT;
	//
	nBase = nAddr - MyLable.nAddress*(MAX_NUM_TRACK*MAX_NUM_LAMP);
	//
	return (nBase/MAX_NUM_LAMP) + 1; //1~8
}

unsigned char CountLampAddr(unsigned short nAddr)
{
	unsigned short nBase;
	//
	nAddr -= LAMP_AOFT;
	//
	nBase = nAddr - MyLable.nAddress*(MAX_NUM_TRACK*MAX_NUM_LAMP);
	//
	return (nBase%MAX_NUM_LAMP); //0~29
}

unsigned short CountLampPhyAddr(unsigned char nTrack, unsigned char nAddr)
{
	unsigned short nAd = 0;
	//
 	nAd = MyLable.nAddress*(MAX_NUM_TRACK*MAX_NUM_LAMP)+(nTrack - 1)*MAX_NUM_LAMP+nAddr;
	nAd += LAMP_AOFT;
	//
	return nAd; //1024~65535
}

void ReciverOut()
{
	if((MyPlcNode.bState == PLC_ST_TOUT)&&(EmgStateKeepTim > 15))//如果是通信超时
	{
		if(TimeOutCnt >= 3)
		{
			if(OneReduct == 1)//是第一次按1000个轮询的，超时不存入flash中
			{
				bAddrInx ++;//第一次地址都是从最小区间开始	
				PSetStateToEnd(&MyPlcNode);
				if((bAddrInx > AddrIntral[(StarIntral*2)+1])&&(len != 0))//当第一次1000个灯轮询完成
			{
				if((StarIntral) < ( nSerchLampSectionNum-1))
				{
					StarIntral ++ ;	
	//				StarIntral = (StarIntral*2);
					bAddrInx = AddrIntral[(StarIntral*2)];	
				}
				else
				{   
						StarIntral = 0;
						bAddrInx = AddrIntral[StarIntral];	
						OneReduct = 0;//复位标记置0，表示从新开始不按1000个轮询，而是按照地址数组轮询
						LampAddrBufLen = len; //变量BUFSUM表示第一次轮询在线的灯具总个数，也是在线地址数组的长度
						len = 0;
				}		
			}
			else if((bAddrInx > AddrIntral[(StarIntral*2)+1])&&(len == 0))
			{
				if((StarIntral) <= (nSerchLampSectionNum-1))
				{
					StarIntral ++ ;
	//				StarIntral = (StarIntral*2);
					bAddrInx = AddrIntral[(StarIntral*2)];	
				}
				else
				{   
					StarIntral = 0;
					bAddrInx = AddrIntral[StarIntral];	
				}		
			}
		}
			else if(OneReduct == 0)//在线地址后再次不在线的地址，那么才添加到故障中
			{
				if(MyPlcNode.frmTx.nSlaver == LampAddrBuf[bLen])
				{						
					LampState[bLen] = (1 << FUL_LAMP_NACK);//故障数组从标记0开始记录每个不在线的通信故障
					bLen ++;//bLen是代表
					PSetStateToEnd(&MyPlcNode);						
				}
			}
			TimeOutCnt = 0;
		}
		else
		{
			TimeOutCnt ++;
			PSetStateToEnd(&MyPlcNode);				
		}	
	}
}
