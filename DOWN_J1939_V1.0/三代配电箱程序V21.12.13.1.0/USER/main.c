/**
  ******************************************************************************
  * @file    Project/STM32F103RCT6_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    05-April-2018
  * @brief   Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h" 
/**
  * @brief   Main program.
  * @param   None
  * @retval  None
  * @version V1.3
	*
	*
  */
unsigned int  PWM = 100 ;
/*****************************   CANBEGIN *************************************/
uint8_t CanVout_14[8];
uint8_t CanVout_58[8];
uint8_t CanPower[8];
uint8_t CanInput[8];
uint8_t CanHegtm[8];
uint8_t CanFlag[8];

extern unsigned short LampAddrBuf[250];//地址数组
extern unsigned char LampState[250];// 状态数组
CAN_LAMP_DATA data[512];
uint8_t FirstSend; // 首次发送标志，也可以通过这个标志位上传全部数据

uint16_t lampAddr[250];
uint8_t lampState[250];

int main(void)
{

	SysTickInit();			   //系统定时器初始化
	
	SysTickDelayMs(2000);
	
	ConfigGPIO();				//GPIO配置
	
	InitVar();					//变量初始化	

	ADCInit();					//ADC配置,严禁放在定时器后面初始化，造成PWM失控
	
	ConfigUart();				//UART配置
		
	RccClkInit();				//RCC初始化
	////////////
	InitElecLable();		//电子标签读取
	
	ReadFlashData();
	
	InitSystem();				//系统设置
	////////////
	TimerInit();				//定时器初始化	
	
	InitTm4Ch3ToPwmMode(0);

	//IWDG_Init(4, 60000); 	 //watch dog init(5s)

	//Open_Out();
	
	CAN_Init_All(BOUND20KB);
	
	UpCanData();
	FirstSend = 1; // 初始化完所有数据开始第一次数据发送标志
	
	LAST_ENABLE;
  while(1)		
  {
		
		UpCanData();	// 更新CAN数据
	
		CanPoll(); // CAN装载完成发送任务  
		
		OutDetect();	    //输出故障检测
		
		GetAcVlotage();
		
		SetBoostVlotage(155);
	//		
		IWDG_Feed();		  //喂狗
	//		
		EpsWorking();		//国标功能，年月检、应急等

    MessageDeal();	//处理主机的消息    
				
		EmgyDetect();		//检测应急状态
	//		
		EmgyChang();		//应急转换设置	
		
//	IfrScan();      //红外遥控

		CanComLed();
		
		UPDATE_START		//间隔执行的函数100mS
		
		LedFlash(20);		//闪烁指示灯，观察用
		
		HandRead();
			
		FaultLed(16);
			
		InitWorking();		//上电工作延时
			
		BatteryFault();
			
		UpdateEgTime();		//更新应急时间
//			
		UpdateIndicate(); //更新蜂鸣器状态
//			
		SystemFaultDetect();	//故障检测
				
		BellContrl(BELL_SHORT,100,30);
			
//  	DischargeDeal(GULI_VBAT,BAT_IDIS_OVR);	//电池充放电管理
//				
 		UPDATE_END
	}
}

/******************* (C) By WangJiuZhou  of 2020.2.20 ********************/



