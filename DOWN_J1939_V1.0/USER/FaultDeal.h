
#ifndef	FAULT_DEAL_H
#define	FAULT_DEAL_H
#include "MyFun.h"
//===============================================
//define
//===============================================


//===============================================
//struct
//===============================================


//===============================================
//extern
//===============================================
extern unsigned int gbMyError; //故障表
extern unsigned char gbComCnt; //通信计数
extern unsigned short ComCnt;   //通信计数2
extern unsigned int POWERCHOOS;

//===============================================
//function
//===============================================
void ShortSet(void);
void Char0(void);
void ChangInit(void);
void CharReady(void);
void BatteryFault(void);
void SystemFaultDetect(void);
void PassCurenntDetect(void);
void AddFaultX(unsigned char nCode);
void DelFaultX(unsigned char nCode);
void FaultLed(unsigned  char LedTim);
void SetFault(unsigned char bFlag, unsigned char bCode);


#endif

