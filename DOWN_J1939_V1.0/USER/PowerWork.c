//======================================================
//电源处理
//======================================================

#include "stdafx.h"
//static int nCnt2 = 0; 
//static int nCnt1 = 0;
unsigned  int sPower=0;
unsigned  char gbOutDuty; //输出占空比
unsigned  char Start_Up_FUL = FALSE;
unsigned char ChangFulState = FALSE;
unsigned char ResetFulState = FALSE;
unsigned char MainToBatState = TRUE;
unsigned char BatToMainState = FALSE;
unsigned char OutLock1 = FALSE;
unsigned char OutLock2 = FALSE;
unsigned short BellnTp = 0;
unsigned char ChangSpaceTime = 0;
unsigned char FirFlag = 0x02;//消防应急标志，默认不触发0X02。
unsigned char ShortEn = 1;

unsigned char EmgySend = 1;

extern unsigned char EMGYCharEnable;
unsigned char StartupBuzz = 0;
extern unsigned int nstate;
//======================================================
//应急时进行开关设置
//======================================================
unsigned char emgy=0x05;
unsigned char Boost_En = 0;
//======================================================
////充电ModelPWM调节
//======================================================

void SetPwmChrDuty(unsigned short nDuty)
{
	TIM_SetCompare3(TIM4, nDuty); //set ch1' ccr值
}

void EmgyChang()
{ 
	if(IS_ST_EMGY(EpsState.nState)&&(!OutShortFlag)) //主电切换至应急
		{
			Li_CHAR_DISABLE;
			StartTimer4();
			Boost_En = 1;
			SetTxToPPMode();
			// StopTimer8();
	    SET_EMGY_LED;
			CLR_MAIN_LED;
			CLR_CHAR_LED;
			if((ChangSpaceTime > 0)&&(ChangSpaceTime < 60))//当未完全恢复时。再次触发应急时，需再次设置IO及变量状态
			{
				MainToBatState = FALSE;	
			}
				ChangSpaceTime = 0;
			if(MainToBatState == FALSE)
			{
			   if(VMAIN < 180) //主电在时不应急
				{					
					LOAD_DISABLE; //应急时进先关闭MOS
					DelayMs(100);	
					EMGY_BUS_ENABLE;;		//等待一段时间20ms开始转换继电器至备电
					DelayMs(100);			
				}
				LOAD_ENABLE;   //等待一段时间20ms闭合MOS至备电输出
				DelayMs(100);
				EmgySend = 1;
				EmgStateKeepTim = 0;
				LampNode.bLpSt = 0x05;
				//CharbError = 0;
				MainToBatState = TRUE;

			}
		}			
	else
	if(!IS_ST_EMGY(EpsState.nState)&&(!OutShortFlag))//应急切换至主电
		{ 
			SetTxToPPMode();
			StartTimer8();
			ChangSpaceTime = (ChangSpaceTime == 0)?(1):(ChangSpaceTime);//手动应急和自动应急重置条件
			if(MainToBatState == TRUE)
			{
					LOAD_DISABLE; //应急时进先关闭MOS	
				if(ChangSpaceTime >= 10)
				{
//					StopTimer8();	
					EMGY_BUS_DISABLE;  //等待一段时间20ms开始转换继电器至主电
					DelayMs(100);			
					MainToBatState = FALSE;
					LOAD_ENABLE; //等待一段时间20ms闭合MOS至备电输出
			  	SetUartToTxMode();
					StopTimer4();
					Boost_En = 0;
					DelayMs(100);
					EmgySend = 1;
					EmgStateKeepTim = 0;
			  	LampNode.bLpSt = 0x06;
					Li_CHAR_ENABLE;	
				  SET_MAIN_LED;
					CLR_EMGY_LED;
					SET_CHAR_LED;
				}
			}
		}
}
	static unsigned int nCnt = 0;
	static unsigned int nSum = 0;
	float AC_ADC_GET = 0;
unsigned int BoostPWM = 0;
	static unsigned int BoostVal = 0;	
//======================================================
//上电时对输出短路检测
//======================================================
void OutDetect()
{
//	if(((VOUT1 <= VSHORT) && (VOUT2 <= VSHORT)&&(VOUT3 <= VSHORT) && (VOUT4 <= VSHORT) &&(!IS_ST_EMGY(EpsState.nState))&& \
//	  	(VOUT5 <= VSHORT) && (VOUT6 <= VSHORT)&&(VOUT7 <= VSHORT) && (VOUT8 <= VSHORT)))
//	{
//			StopTimer8();	
//			LOAD_DISABLE;
//			ChangSpaceTime = 0;
//	    OutShortFlag = TRUE;
////		MainToBatState = TRUE;//后加允许恢复现场会偶尔进不应急函数 64行 导致灯具闪烁
//	}
//	else
			OutShortFlag = FALSE;	
	
								//////////////////////MARK_V_ADC_Charg//////////////////////	
					if(nCnt >= VMAIN_ADC_Num)
						{
							nCnt = 0;
							AC_ADC_GET = (float)(nSum/VMAIN_ADC_Num); //平均值
							nSum = 0;
							AdcAcquire.nMarket = (uint16_t)((float)AC_ADC_GET*0.16278);
						}				 
						AdcAcquire.Boost_V = (uint16_t)((float)BoostVal*0.105);	

							if(Boost_En == 1)	
							{				
							 if((AdcAcquire.Boost_V < 155))
									SetPwmChrDuty(((BoostPWM<498)?(BoostPWM += 2):BoostPWM)); 
							 else 
								if((AdcAcquire.Boost_V > 140)&&(AdcAcquire.Boost_V < 155))
									SetPwmChrDuty(((BoostPWM >2 )?(BoostPWM -= 2):BoostPWM)); 		
								else
								{
									BoostPWM = 0;
									SetPwmChrDuty(BoostPWM);	
								}	
							}	
					else
						{
							BoostPWM = 0;
							SetPwmChrDuty(BoostPWM);	
						}	
} 

//======================================================
//市电电压检测
//计算方法：二元一次方程组
//======================================================
unsigned char nInx11 = 0;
//unsigned int BoostPWM = 0;
void DoMarketDeal()
{
//	float AC_ADC_GET = 0;


//	static unsigned int nCnt = 0;
//	static unsigned int nSum = 0;
//	static unsigned int BoostVal = 0;	
		if(ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC)) //转换完毕
			{		
			if(nInx11 == 0)
				{		
					nCnt ++;
					nSum += ADC_GetConversionValue(ADC2);
					nInx11 = 1;
				}
				else 
					if(nInx11 == 1)
				{					
					nInx11 = 0;
					BoostVal = ADC_GetConversionValue(ADC2);					
				}
				
				 if(nInx11 == 0)ADC_RegularChannelConfig(ADC2, ADC_Channel_4, 1, ADC_SampleTime_239Cycles5);//市电电压
				 else if(nInx11 ==1) ADC_RegularChannelConfig(ADC2, ADC_Channel_11, 1, ADC_SampleTime_239Cycles5);//市电电压
				  ADC_SoftwareStartConvCmd(ADC2, ENABLE);				
			}				

}
//======================================================
//应急检测
//======================================================
void EmgyDetect()
{
	static unsigned char bMarkErr,bMainErr;//必需为静态变量，否则状态无法保持，导致跳动，不能起到滞回效果
	
	if(gbInit != FALSE)		return;
	////////////////////自动应急////////////////////
	if((VMAIN > 190)&&(FirInPut))
	{
		bMarkErr = FALSE;
	}else
	if((VMAIN < 180)||(!FirInPut))  //市电电压检测
	{
//		BAT_ENABLE;  // dkj
		bMarkErr = TRUE;
 	}


//======================================================
//应急状态设置
//======================================================	
		//自动应急检测
	if((bMarkErr||bMainErr))
		EpsState.bSelfSig |= (1<<AUTO_EMGY);
	else
		EpsState.bSelfSig &= ~(1<<AUTO_EMGY);
	//
	SET_ST_MAIN(EpsState.nState,!(bMarkErr||bMainErr)); //主电状态
	
		//强制应急检测
	if((EpsState.bFSoft||(FORCE_IN == 0)))
		EpsState.bSelfSig |= (1<<FORC_EMGY);
	else
		EpsState.bSelfSig &= ~(1<<FORC_EMGY);
		
	//复位时关闭一些操作		
	if(EpsState.bReset)
	{
			EpsState.bReset = FALSE; //关闭复位
			EpsState.bSelfSig &= ~(1<<MANU_EMGY); //手动应急
			EpsState.bSelfSig &= ~(1<<MMCK_EMGY);	//手动月检
			EpsState.bSelfSig &= ~(1<<MYCK_EMGY);	//手动年检
			EpsState.bFSoft = FALSE;
	}
	
	////////////////////使能设置///////////////

	//应急可能是电源单独应急或由控制器控制进入应急
	if(LI_VBAT < VBAT_ON_HNUM)
	{	
		EpsState.bAtEmy =  0;
		EpsState.bAtCtrlEmy=0;
		EpsState.bMuEmy =  0;
		EpsState.bForce =  0;
		EpsState.bMCheck = 0;
		EpsState.bYCheck = 0;
		EpsState.bMManu =  0;
		EpsState.bYManu =  0;
		EpsState.bReset = 1;
	}
	else 
	{	
		EpsState.bAtEmy = (EpsState.bSelfSig&0x01)||(EpsState.bCtrlSig&0x01);
		EpsState.bMuEmy = (EpsState.bCtrlSig&0x02)||(EpsState.bSelfSig&0x02);
		EpsState.bForce = (EpsState.bCtrlSig&0x04);
//		EpsState.bMCheck = (EpsState.bCtrlSig&0x08)||(EpsState.bSelfSig&0x08);
//		EpsState.bYCheck = (EpsState.bCtrlSig&0x10)||(EpsState.bSelfSig&0x10);
//		EpsState.bMManu = (EpsState.bCtrlSig&0x20)||(EpsState.bSelfSig&0x20);
//		EpsState.bYManu = (EpsState.bCtrlSig&0x40)||(EpsState.bSelfSig&0x40);
		//EpsState.bAtCtrlEmy = (EpsState.bCtrlSig&0x01);//增加控制器自动应急
		
	}
}

//======================================================
//应急时间更新
//======================================================
void UpdateEgTime()
{
	static char nTp = 0;
	//
	nTp++;
	//
	if(nTp == (1000/UPDATE_SPAN))
	{
		nTp = 0;
		//
		if(IS_ST_EMGY(EpsState.nState))
			{
				EpsState.nEgTm++;		
				EpsState.hEgTm++;	
			} 
		else
			{
				EpsState.nEgTm = 0; 			
				EpsState.hEgTm=0;
			}
  }
}



//======================================================
//蜂鸣器控制
//======================================================

void BellContrl(unsigned char tim1 ,unsigned short tim2,unsigned char tim3)
{
	   extern unsigned char bBuzzer_Emgy;
	   extern unsigned char bBuzzer_Faut;
	   extern unsigned short bError;

	if(!OutShortFlag)
	{
	 if((bBuzzer_Faut == 0x01)||(PassCurenntbFlag == 1))
		{
				if(StartupBuzz == 0)
				{
					 StartupBuzz = 1;
					 BellnTp = 0;
				}							 
		if(BellnTp <= tim1)
		  	BELL_ON;	
			else 
				if((BellnTp > tim1)&&(BellnTp < tim2))			 
        BELL_OF;
			else
				if(BellnTp >= tim2)
				BellnTp = 0;		
			}
		else
		{
			StartupBuzz = 0;
			BellnTp = 0;
			BELL_OF;
		}
	}
	else
	{
		if(BellnTp <= (tim3>>3))
			TickBell(10);
		else 
			if(BellnTp >= tim3)
				BellnTp = 0;
			
	}
}

//=======================================================================
//提示滴答定时器蜂鸣器
//=======================================================================

void TickBell(unsigned char tim1)
{
	unsigned char i;
	for(i = 0;i < 3;i++)
	{
		BELL_ON;
		DelayMs(50);
		BELL_OF;
		DelayMs(10);
		IWDG_Feed();		//喂狗
	}
}


