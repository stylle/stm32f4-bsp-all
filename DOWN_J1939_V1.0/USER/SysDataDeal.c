
//======================================================
//系统数据处理
//电子标签、日志、设备数量设置
//======================================================

#include "stdafx.h"

//======================================================
//var
//======================================================
ELEC_LABLE MyLable;			//电子标签

ELEC_L Myec;

//======================================================
//lable
//======================================================

//读取电子标签
void InitElecLable() 
{
	unsigned short sBuf[sizeof(ELEC_LABLE)];
	//
	FlashReadMoreData(ADDR_OFT_LABLE, sBuf, sizeof(ELEC_LABLE));
	//
	MyLable.bDevType = MY_TYPE;
	MyLable.bSoftVer = 0x01;
	MyLable.bItemNo = sBuf[1];
	MyLable.nAddress = sBuf[3]; //设备地址
	//MyLable.nAddress = 2; //设备地址0
	
}
//======================================================
//ReadFlash
//======================================================

void ReadFlashData()
{
	
	unsigned short nIndex = 0;
	unsigned short sBuf[sizeof(ELEC_L)];
	FlashReadMoreData(ADDR_OFT_EPS, sBuf, sizeof(ELEC_L));
	
	Myec.bPowerStyle = sBuf[nIndex++];
	
	Myec.Test_Model =  sBuf[nIndex++];
	
  Myec.Beep_nState = sBuf[nIndex++];
	
	Myec.AgeTime_S = sBuf[nIndex++];
	
	Myec.AgeTime_M = sBuf[nIndex++];
	
  Myec.LampZlSet = sBuf[nIndex++];
	
	Myec.LampZlSet = ((Myec.LampZlSet != 0x08)&&(Myec.LampZlSet != 0xBF))? (0x08): (Myec.LampZlSet);
	
	Myec.Test_Model = ((Myec.Test_Model != 0xBB)&&(	Myec.Test_Model != 0xAA))? (0xAA): (Myec.Test_Model);
	
	Myec.Beep_nState =	(( Myec.Beep_nState != 0xCC)&&( Myec.Beep_nState != 0xDD))? (0xCC): (Myec.Beep_nState);
  
	Myec.AgeTime_S = (Myec.AgeTime_S > 60)? (60):(Myec.AgeTime_S);
	
	Myec.AgeTime_M = (Myec.AgeTime_M > 120)? (120):(Myec.AgeTime_M);
	
	
}

//======================================================
//Writeaddr
//======================================================

void Writeaddra(unsigned char addra)
{
	MyLable.bItemNo = 0;
	MyLable.nAddress = addra;
	SaveSysData();
	TickBell(10);
}
//======================================================
//WritePowerStyle
//======================================================

void Writestate(unsigned char PowerRank)
{
	Myec.bPowerStyle = PowerRank;
	SaveSysData();
}


//打印电子标签
void ReadLable()
{
	printf("Device type: %d\r\n", MyLable.bDevType);
	printf("ItemNo: %d\r\n", MyLable.bItemNo);
	printf("Soft version: %d\r\n", MyLable.bSoftVer);
	printf("Address: %d\r\n", MyLable.nAddress);
}

//重写电子标签
void WriteLable()
{
//	MyLable.bItemNo = ;
//	MyLable.nAddress = ;
//	//
//	SaveSysData();
}

//======================================================
//save data
//将参数存储到FLASH中
//======================================================
void SaveSysData()
{
	unsigned short nIndex = 0;
	unsigned short sBuf[sizeof(ELEC_LABLE)+sizeof(ELEC_L)];
	//
	sBuf[nIndex++] = MyLable.bDevType;
	
	sBuf[nIndex++] = MyLable.bItemNo;
	
	sBuf[nIndex++] = MyLable.bSoftVer;
	
	sBuf[nIndex++] = MyLable.nAddress;//地址
	
	sBuf[nIndex++] = Myec.bPowerStyle;//功率等级
	
	sBuf[nIndex++] = Myec.Test_Model; //输出模式//默认通讯输出

	sBuf[nIndex++] = Myec.Beep_nState;//故障屏蔽
	
	sBuf[nIndex++] = Myec.AgeTime_S;//老化时间S
	
	sBuf[nIndex++] = Myec.AgeTime_M;//老化时间H
	
	sBuf[nIndex++] = Myec.LampZlSet;//
	
	FlashWriteMoreData(ADDR_OFT_LABLE, sBuf, (sizeof(ELEC_LABLE)+sizeof(ELEC_L)));
	//
	//printf("Save sys data end!\r\n");
	
}



