#include "stdafx.h"



unsigned char  RmtSta = 0;	  
unsigned int   GetData = 0;	    //红外接收到的数据	  
unsigned int   LowDataTime = 0;	//下降沿时计数器的值,低电平时间
 

unsigned char EmgyFlag = FALSE;
unsigned char SendFulFlag = 0;

void Remote1(void)
{
		   RmtSta &=~ 0X10;						//取消上升沿已经被捕获标记
			
			if((RmtSta & 0X0F)<14)RmtSta++;
			else
			{
				RmtSta &=~(1<<7);//清空引导标识
				RmtSta &= 0XF0;	//清空计数器	
			}	
}

void Remote2(void)
{
		if(GPIO_ReadInputDataBit(GPIOB,  GPIO_Pin_7))//上升沿捕获
		{

				TIM_OC2PolarityConfig(TIM4,TIM_ICPolarity_Falling);		//CC2P=1 设置为下降沿捕获				
	      TIM_SetCounter(TIM4,0);	   	//清空定时器值
		    RmtSta |= 0X10;				//标记上升沿已经被捕获
		}
		else //下降沿捕获
		{			
  		 LowDataTime = TIM_GetCapture2(TIM4);//读取CCR2也可以清CC2IF标志位
			 TIM_OC2PolarityConfig(TIM4,TIM_ICPolarity_Rising); //CC2P=0	设置为上升沿捕获
 			
			if(RmtSta & 0X10)					//完成一次高电平捕获 
			{
 				 if(LowDataTime > 300 && LowDataTime < 900)		
					{
						GetData <<= 1;	//左移一位.
						GetData |= 0;	//接收到0	   
					}
					else
						if(LowDataTime > 1100 && LowDataTime < 1600)	
					{
						GetData <<= 1;	//左移一位.
						GetData |= 1;	//接收到1
					}
					else
						if(LowDataTime > 2200)	
					{
						GetData = GetData;
						SendFulFlag = 1;
						RmtSta &=0XF0;	//清空计时器		
					}
 						 
			}
			RmtSta &=~(1<<4);
		}
}
                                                      
void IfrScan()
{        
  unsigned char Data = 0;
  unsigned char MenuState = 0;  
	//unsigned char track=0;
	//unsigned char lamp=0;
	
  //一次按键码成功
  if(SendFulFlag == 1)
  {
		if(MenuState == 0)
		{
		  if(((GetData >> 12) & 0xf) == 0xA)//帧头判断
		  {
			 MenuState = 1;
		  }
		  else
		  {
			 MenuState = 0;
			 GetData = 0;		//
			 SendFulFlag = 0; //清除标记
			 RmtSta &=~0x10;	//清除上升沿标记
		  }
		}
		if(MenuState == 1)//判断是否是电源信息
		{
		   if((GetData >> 10)&0x1)
		   {
			  MenuState = 2;
		   }
		   else
		   {
			  MenuState = 0;
			  GetData = 0;		//
			  SendFulFlag = 0; //清除标记
			  RmtSta &=~0X10;	//清除上升沿标记
		   }
		}
		if(MenuState == 2)
		{
			
			if(((GetData >> 8)&0x3)==0X03)//功率设置
			{
				MenuState = 5;
			}else
			if(((GetData >> 8)&0x1)==1)//地址设置
			{
        MenuState = 3;
			}
			else
			if(((GetData >>8 )&0x2)==0X02)//状态设置
			{
				MenuState = 4;
			}
			else
			{
				MenuState = 0;
				GetData = 0;		//
				SendFulFlag = 0; //清除标记
				RmtSta &=~0X10;	//清除上升沿标记
			}
		}
		if(MenuState == 3)//地址设置
		{
		   Data = GetData&0xff;
		   Writeaddra(Data);
			 MenuState = 0;
		   GetData = 0;		//
		   SendFulFlag = 0; //清除标记
		   RmtSta &=~0X10;	//清除上升沿标记
		}
		if(MenuState == 4)//应急状态设置
		{
			if(EmgyFlag == 0) 
			{
				SET_ST_EMGY(EpsState.nState, 1);
				EmgyFlag = TRUE;
			}
			else
			{
				SET_ST_EMGY(EpsState.nState, 0);
				EmgyFlag = FALSE;
			}
		  MenuState = 0;
		  GetData = 0;		//
		  SendFulFlag = 0; //清除标记
		  RmtSta &=~0X10;	//清除上升沿标记
		}
		if(MenuState == 5)
		 {
			Data = GetData & 0xff;
			Writestate(Data);
			switch(Data)
		  {
			case 0:                         //150w
				POWERCHOOS = 150;
			break;
			case 1:                         //300w
				POWERCHOOS = 200;
			break;												
			case 2:                         //400w
				POWERCHOOS = 300;
			break;   
			case 3:                         //150w
				POWERCHOOS = 400;
			break;
			case 4:                         //300w
				POWERCHOOS = 500;
			break;												
			case 5:                         //400w
				POWERCHOOS = 600;
			break; 
			
//      default:
//			break; 
		  }
			
		  MenuState = 0;
		  GetData = 0;		 //
		  SendFulFlag = 0; //清除标记
		  RmtSta &=~0X10;	 //清除上升沿标记
			
		}
	}
}


