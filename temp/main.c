/********user lib************/
#include "bsp_led.h"
#include "bsp_485.h" // uart2
#include "bsp_delay.h"
#include "bsp_iwdg.h"
#include "bsp_24C02.h"
#include "bsp_i2c.h"
#include "bsp_relay.h"
#include "bsp_timer.h"
#include "bsp_debug_usart.h"

#define DEBUG_MODE

// debug:2  release:15
#define RELAY_MINUTE 15 // 测量周期分钟
const uint8_t RELAY_NUM = 8;

/**********std lib**********/
#include "stm32f4xx.h"

uint8_t ee_write_buf[EEPROM_SIZE] = {0x5b}; // 默认设备地址 10进制表示(150) 16进制表示(96)
uint8_t ee_read_buf[EEPROM_SIZE] = {0x5b};  // 读取到的buf存放地址
uint8_t system_state = 0;

extern uint8_t rs485_read_resistance[24]; // 读取地阻仪发送指令
extern uint8_t rs485_text_resistance[24]; // 开始读取指令
extern uint8_t resistance_buff[24];       // 地阻阻值存放
extern uint8_t oltage_buff[24];           // 电流存放数组
extern uint8_t float_buff[24];            // 小数点位数

/*  系统外设初始化  */
void system_init()
{
  Debug_USART_Config();
  delay_init(168);
  delay_ms(100);
#ifdef DEBUG_MODE
  printf("定时器3初始化开始....\r\n");
#endif

  TIM3_Int_Init(167, 999); //2ms
  delay_ms(50);

#ifdef DEBUG_MODE
  printf("定时器3初始化完成！\r\n");
#endif

#ifdef DEBUG_MODE
  printf("看门狗初始化开始....\r\n");
#endif

  IWDG_Config(IWDG_Prescaler_64, 625);
  delay_ms(50);

#ifdef DEBUG_MODE
  printf("看门狗初始化完成....\r\n");
#endif

#ifdef DEBUG_MODE
  printf("RS485初始化开始....\r\n");
#endif

  _485_Config(9600); //uart2
  delay_ms(50);

#ifdef DEBUG_MODE
  printf("RS485初始化完成！\r\n");
#endif

  LED_GPIO_Config();

#ifdef DEBUG_MODE
  printf("继电器初始化开始....\r\n");
#endif

//  relay_init();
  delay_ms(50);

#ifdef DEBUG_MODE
  printf("继电器初始化完成\r\n");
#endif

#ifdef DEBUG_MODE
  printf("24c04外设初始化开始....\r\n");
#endif
  ee_Init();
  delay_ms(50);
#ifdef DEBUG_MODE
  printf("24c04外设初始化完成！\r\n");
#endif

#ifdef DEBUG_MODE
  printf("开始获取设备地址....\r\n");
  delay_ms(50);
#endif

//  ee_ReadBytes(ee_read_buf, 0, 1);
//  if (ee_read_buf[0] < 0x5b || ee_read_buf[0] > 0x64)
//  {
//    ee_Erase();
//    ee_WriteBytes(ee_write_buf, 0, 1);
//    ee_ReadBytes(ee_read_buf, 0, 1);
//    delay_ms(50);
//  }

#ifdef DEBUG_MODE
  _485_SendStr_length(ee_read_buf, 1);
  printf("当前设备RS485地址为：%x\r\n", ee_read_buf[0]);
#endif

  /*  查询和检测指令添加crc校验  */
  rs485_crc_modbus(rs485_read_resistance, 6);
  rs485_crc_modbus(rs485_text_resistance, 6);
}

uint16_t displacement[2] = {0, 1};

int main(void)
{
  uint8_t receive_data[64] = {0}; // rs485接受数据存放buf
  uint64_t time_t = 0;            // 模拟线程定时
  uint8_t detection_mark = 0;               // 打开和关闭继电器标志位

  system_init();

  while (1)
  {
    uint8_t rs485_read_buff[64] = {0};                 //485读取buff
    uint8_t rs485_read_buff_len = 0;                   //485读取buf长度
    RS485_Receive_Data(rs485_read_buff, &rs485_read_buff_len);
    /*  当发送首位为地址并长度大于8位则进入命令判断 */
    if (rs485_read_buff[0] >= 0x01 && rs485_read_buff[0] <= 0xfe && rs485_read_buff_len >= 8)
    {
      rs485_search_resistance_cmd(rs485_read_buff);
    }
    delay_ms(1);
    time_t++;

    if (time_t % (RELAY_MINUTE * 10) == 0 && detection_mark == 0 && (time_t / (RELAY_MINUTE * 10)) <= RELAY_NUM * 30)
    {
      system_state = 1; // 系统定时器开始执行标志位
#ifdef DEBUG_MODE
      printf("\r\n开始定时检测第%d组设备地阻阻值....\r\n", (unsigned int)time_t / ((RELAY_MINUTE * 10) * 30) + 1);
#endif
      relay_all_off();
      relay_on(time_t / ((RELAY_MINUTE * 10) * 30) + 1); // 开继电器
      _485_SendStr_length(rs485_text_resistance, 8);     // 发送测量信号
        detection_mark = 1;                                        // 测量开始等待测量标志位
    }
    else if (time_t % ((RELAY_MINUTE * 10) * 30) == 0 && detection_mark == 1 && (time_t / (RELAY_MINUTE * 10)) <= (RELAY_NUM * 30 + 30))
    {
#ifdef DEBUG_MODE
      printf("\r\n开始定时读取第%d组设备地阻阻值....\r\n", (unsigned int)time_t / ((RELAY_MINUTE * 10) * 30));
#endif
      // 读取电阻值、小数位、电压值
      _485_SendStr_length(rs485_read_resistance, 8); // 读取电阻值
      delay_ms(20);
      RS485_Receive_Data(receive_data, &rs485_read_buff_len);
      delay_ms(20);  // 后添加
        
        
      resistance_buff[displacement[0]] = receive_data[1]; // 地阻数值
      resistance_buff[displacement[1]] = receive_data[2];

      float_buff[displacement[0]] = receive_data[3]; // 小数位数
      float_buff[displacement[1]] = receive_data[4];

      oltage_buff[displacement[0]] = receive_data[5]; // 电压值
      oltage_buff[displacement[1]] = receive_data[6];

#ifdef DEBUG_MODE
      printf("关闭第%d组继电器\r\n", (unsigned int)time_t / ((RELAY_MINUTE * 10) * 30));
#endif
      relay_off(time_t / ((RELAY_MINUTE * 10) * 30) + 1); // 关继电器
      detection_mark = 0;
      displacement[0] += 2;
      displacement[1] += 2;
    }

    // 97 03 06 02 EA 00 01 00 00 EF 26
    if (time_t == 600 * RELAY_MINUTE * 10)
    {
      displacement[0] = 0;  // 清楚数据记录数组位移标志
      displacement[1] = 1;
      time_t = 0;     // 时间标志清楚
    }
  }
}


/*********************************************END OF FILE**********************/
