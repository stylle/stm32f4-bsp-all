#ifndef __LED_H
#define __LED_H

#include "stm32f4xx.h"

//引脚定义
/*******************************************************/
#define LED1_PIN GPIO_Pin_0
#define LED1_GPIO_PORT GPIOA
#define LED1_GPIO_CLK RCC_AHB1Periph_GPIOA

/************************************************************/

/* 直接操作寄存器的方法控制IO */
#define digitalHi(p, i) \
  {                     \
    p->BSRRH = i;       \
  } //设置为高电平
#define digitalLo(p, i) \
  {                     \
    p->BSRRL = i;       \
  } //输出低电平
#define digitalToggle(p, i) \
  {                         \
    p->ODR ^= i;            \
  } //输出反转状态

/* 定义控制IO的宏 */
#define LED1_TOGGLE digitalToggle(LED1_GPIO_PORT, LED1_PIN)
#define LED1_OFF digitalHi(LED1_GPIO_PORT, LED1_PIN)
#define LED1_ON digitalLo(LED1_GPIO_PORT, LED1_PIN)


void LED_GPIO_Config(void);

#endif /* __LED_H */
