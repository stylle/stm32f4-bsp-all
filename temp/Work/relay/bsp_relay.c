#include "bsp_relay.h"

uint8_t relay_buff[24] = {0};
extern uint8_t RELAY_NUM;

void relay_init(void)
{
  int i = 1;
  /*定义一个GPIO_InitTypeDef类型的结构体*/
  GPIO_InitTypeDef GPIO_InitStructure;

  /*开启相关的GPIO外设时钟*/
  RCC_AHB1PeriphClockCmd(RELAY_GPIO_CLK, ENABLE);

  GPIO_InitStructure.GPIO_Pin = RELAY_GPIOA_PIN_ALL; /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;      /*设置引脚模式为输出模式*/
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;     /*设置引脚的输出类型为推挽输出*/
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;   /*设置引脚为上拉模式*/
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;  /*设置引脚速率为50MHz */

  /*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RELAY_GPIOB_PIN_ALL; /*选择要控制的GPIO引脚*/
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RELAY_GPIOC_PIN_ALL;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  for (; i <= RELAY_NUM; i++)
    relay_off(i);
}

void relay_on(uint8_t relay_gpio)
{
  switch (relay_gpio)
  {
  case 1:
    RELAY1_ON;
    break;
  case 2:
    RELAY2_ON;
    break;
  case 3:
    RELAY3_ON;
    break;
  case 4:
    RELAY4_ON;
    break;
  case 5:
    RELAY5_ON;
    break;
  case 6:
    RELAY6_ON;
    break;
  case 7:
    RELAY7_ON;
    break;
  case 8:
    RELAY8_ON;
    break;
  case 9:
    RELAY9_ON;
    break;
  case 10:
    RELAY10_ON;
    break;
  case 11:
    RELAY11_ON;
    break;
  case 12:
    RELAY12_ON;
    break;
  case 13:
    RELAY13_ON;
    break;
  case 14:
    RELAY14_ON;
    break;
  case 15:
    RELAY15_ON;
    break;
  case 16:
    RELAY16_ON;
    break;
  case 17:
    RELAY17_ON;
    break;
  case 18:
    RELAY18_ON;
    break;
  case 19:
    RELAY19_ON;
    break;
  case 20:
    RELAY20_ON;
    break;
  case 21:
    RELAY21_ON;
    break;
  case 22:
    RELAY22_ON;
    break;
  case 23:
    RELAY23_ON;
    break;
  case 24:
    RELAY24_ON;
    break;
  }
}

void relay_off(uint8_t relay_gpio)
{
  switch (relay_gpio)
  {
  case 1:
    RELAY1_OFF;
    break;
  case 2:
    RELAY2_OFF;
    break;
  case 3:
    RELAY3_OFF;
    break;
  case 4:
    RELAY4_OFF;
    break;
  case 5:
    RELAY5_OFF;
    break;
  case 6:
    RELAY6_OFF;
    break;
  case 7:
    RELAY7_OFF;
    break;
  case 8:
    RELAY8_OFF;
    break;
  case 9:
    RELAY9_OFF;
    break;
  case 10:
    RELAY10_OFF;
    break;
  case 11:
    RELAY11_OFF;
    break;
  case 12:
    RELAY12_OFF;
    break;
  case 13:
    RELAY13_OFF;
    break;
  case 14:
    RELAY14_OFF;
    break;
  case 15:
    RELAY15_OFF;
    break;
  case 16:
    RELAY16_OFF;
    break;
  case 17:
    RELAY17_OFF;
    break;
  case 18:
    RELAY18_OFF;
    break;
  case 19:
    RELAY19_OFF;
    break;
  case 20:
    RELAY20_OFF;
    break;
  case 21:
    RELAY21_OFF;
    break;
  case 22:
    RELAY22_OFF;
    break;
  case 23:
    RELAY23_OFF;
    break;
  case 24:
    RELAY24_OFF;
    break;
  }
}

//all read
void relay_gpio_flag_update(void)
{
  int i;
  for (i = 1; i <= 24; i++)
  {
    relay_buff[i] = relay_gpio_flag_read(i);
  }
}

//onec read
uint8_t relay_gpio_flag_read(uint8_t relay_gpio)
{
  int flag = 0;
  switch (relay_gpio)
  {
  case 1:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY1_PIN);
    break;
  case 2:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY2_PIN);
    break;
  case 3:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY3_PIN);
    break;
  case 4:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY4_PIN);
    break;
  case 5:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY5_PIN);
    break;
  case 6:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY6_PIN);
    break;
  case 7:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY7_PIN);
    break;
  case 8:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY8_PIN);
    break;
  case 9:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY9_PIN);
    break;
  case 10:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY10_PIN);
    break;
  case 11:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY11_PIN);
    break;
  case 12:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY12_PIN);
    break;
  case 13:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY13_PIN);
    break;
  case 14:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY14_PIN);
    break;
  case 15:
    flag = GPIO_ReadOutputDataBit(GPIOB, RELAY15_PIN);
    break;
  case 16:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY16_PIN);
    break;
  case 17:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY17_PIN);
    break;
  case 18:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY18_PIN);
    break;
  case 19:
    flag = GPIO_ReadOutputDataBit(GPIOC, RELAY19_PIN);
    break;
  case 20:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY20_PIN);
    break;
  case 21:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY21_PIN);
    break;
  case 22:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY22_PIN);
    break;
  case 23:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY23_PIN);
    break;
  case 24:
    flag = GPIO_ReadOutputDataBit(GPIOA, RELAY24_PIN);
    break;
  }
  return flag;
}

void relay_all_off()
{
  int i = 24;
  for (; i > 0; i--)
  {
    relay_off(i);
  }
}

/*********************************************END OF FILE**********************/
