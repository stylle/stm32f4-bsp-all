#ifndef __RELAY_H
#define __RELAY_H

#include "stm32f4xx.h"

//引脚定义
/*******************************************************/
#define RELAY1_PIN GPIO_Pin_4
#define RELAY2_PIN GPIO_Pin_5
#define RELAY3_PIN GPIO_Pin_6
#define RELAY4_PIN GPIO_Pin_7
#define RELAY5_PIN GPIO_Pin_4
#define RELAY6_PIN GPIO_Pin_5
#define RELAY7_PIN GPIO_Pin_0
#define RELAY8_PIN GPIO_Pin_1
#define RELAY9_PIN GPIO_Pin_11
#define RELAY10_PIN GPIO_Pin_10
#define RELAY11_PIN GPIO_Pin_2
#define RELAY12_PIN GPIO_Pin_12
#define RELAY13_PIN GPIO_Pin_13
#define RELAY14_PIN GPIO_Pin_14
#define RELAY15_PIN GPIO_Pin_15
#define RELAY16_PIN GPIO_Pin_6
#define RELAY17_PIN GPIO_Pin_7
#define RELAY18_PIN GPIO_Pin_8
#define RELAY19_PIN GPIO_Pin_9
#define RELAY20_PIN GPIO_Pin_8
#define RELAY21_PIN GPIO_Pin_9
#define RELAY22_PIN GPIO_Pin_10
#define RELAY23_PIN GPIO_Pin_11
#define RELAY24_PIN GPIO_Pin_12

#if 0

#define RELAY_GPIOA_PIN_ALL RELAY1_PIN | RELAY2_PIN | RELAY3_PIN | RELAY4_PIN | RELAY20_PIN | RELAY21_PIN | RELAY22_PIN | RELAY23_PIN | RELAY24_PIN
#define RELAY_GPIOB_PIN_ALL RELAY7_PIN | RELAY8_PIN | RELAY9_PIN | RELAY10_PIN | RELAY11_PIN | RELAY12_PIN | RELAY13_PIN | RELAY14_PIN | RELAY15_PIN
#define RELAY_GPIOC_PIN_ALL RELAY5_PIN | RELAY6_PIN | RELAY16_PIN | RELAY17_PIN | RELAY18_PIN | RELAY19_PIN

#else

#define RELAY_GPIOA_PIN_ALL RELAY1_PIN | RELAY2_PIN | RELAY3_PIN | RELAY4_PIN
#define RELAY_GPIOB_PIN_ALL RELAY7_PIN | RELAY8_PIN
#define RELAY_GPIOC_PIN_ALL RELAY5_PIN | RELAY6_PIN

#endif

#define RELAY_GPIO_CLK RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC

/************************************************************/

/* 直接操作寄存器的方法控制IO */
#define digitalHi(p, i) \
  {                     \
    p->BSRRH = i;       \
  } //设置为高电平
#define digitalLo(p, i) \
  {                     \
    p->BSRRL = i;       \
  } //输出低电平
#define digitalToggle(p, i) \
  {                         \
    p->ODR ^= i;            \
  } //输出反转状态

/* 定义控制 GPIOA IO的宏 */
#define RELAY1_TOGGLE digitalToggle(GPIOA, RELAY1_PIN)
#define RELAY1_OFF digitalHi(GPIOA, RELAY1_PIN)
#define RELAY1_ON digitalLo(GPIOA, RELAY1_PIN)

#define RELAY2_TOGGLE digitalToggle(GPIOA, RELAY2_PIN)
#define RELAY2_OFF digitalHi(GPIOA, RELAY2_PIN)
#define RELAY2_ON digitalLo(GPIOA, RELAY2_PIN)

#define RELAY3_TOGGLE digitalToggle(GPIOA, RELAY3_PIN)
#define RELAY3_OFF digitalHi(GPIOA, RELAY3_PIN)
#define RELAY3_ON digitalLo(GPIOA, RELAY3_PIN)

#define RELAY4_TOGGLE digitalToggle(GPIOA, RELAY4_PIN)
#define RELAY4_OFF digitalHi(GPIOA, RELAY4_PIN)
#define RELAY4_ON digitalLo(GPIOA, RELAY4_PIN)

#define RELAY20_TOGGLE digitalToggle(GPIOA, RELAY20_PIN)
#define RELAY20_OFF digitalHi(GPIOA, RELAY20_PIN)
#define RELAY20_ON digitalLo(GPIOA, RELAY20_PIN)

#define RELAY21_TOGGLE digitalToggle(GPIOA, RELAY21_PIN)
#define RELAY21_OFF digitalHi(GPIOA, RELAY21_PIN)
#define RELAY21_ON digitalLo(GPIOA, RELAY21_PIN)

#define RELAY22_TOGGLE digitalToggle(GPIOA, RELAY22_PIN)
#define RELAY22_OFF digitalHi(GPIOA, RELAY22_PIN)
#define RELAY22_ON digitalLo(GPIOA, RELAY22_PIN)

#define RELAY23_TOGGLE digitalToggle(GPIOA, RELAY23_PIN)
#define RELAY23_OFF digitalHi(GPIOA, RELAY23_PIN)
#define RELAY23_ON digitalLo(GPIOA, RELAY23_PIN)

#define RELAY24_TOGGLE digitalToggle(GPIOA, RELAY24_PIN)
#define RELAY24_OFF digitalHi(GPIOA, RELAY24_PIN)
#define RELAY24_ON digitalLo(GPIOA, RELAY24_PIN)

/* 定义控制 GPIOB IO的宏 */
#define RELAY7_TOGGLE digitalToggle(GPIOB, RELAY7_PIN)
#define RELAY7_OFF digitalHi(GPIOB, RELAY7_PIN)
#define RELAY7_ON digitalLo(GPIOB, RELAY7_PIN)

#define RELAY8_TOGGLE digitalToggle(GPIOB, RELAY8_PIN)
#define RELAY8_OFF digitalHi(GPIOB, RELAY8_PIN)
#define RELAY8_ON digitalLo(GPIOB, RELAY8_PIN)

#define RELAY9_TOGGLE digitalToggle(GPIOB, RELAY9_PIN)
#define RELAY9_OFF digitalHi(GPIOB, RELAY9_PIN)
#define RELAY9_ON digitalLo(GPIOB, RELAY9_PIN)

#define RELAY10_TOGGLE digitalToggle(GPIOB, RELAY10_PIN)
#define RELAY10_OFF digitalHi(GPIOB, RELAY10_PIN)
#define RELAY10_ON digitalLo(GPIOB, RELAY10_PIN)

#define RELAY11_TOGGLE digitalToggle(GPIOB, RELAY11_PIN)
#define RELAY11_OFF digitalHi(GPIOB, RELAY11_PIN)
#define RELAY11_ON digitalLo(GPIOB, RELAY11_PIN)

#define RELAY12_TOGGLE digitalToggle(GPIOB, RELAY12_PIN)
#define RELAY12_OFF digitalHi(GPIOB, RELAY12_PIN)
#define RELAY12_ON digitalLo(GPIOB, RELAY12_PIN)

#define RELAY13_TOGGLE digitalToggle(GPIOB, RELAY13_PIN)
#define RELAY13_OFF digitalHi(GPIOB, RELAY13_PIN)
#define RELAY13_ON digitalLo(GPIOB, RELAY13_PIN)

#define RELAY14_TOGGLE digitalToggle(GPIOB, RELAY14_PIN)
#define RELAY14_OFF digitalHi(GPIOB, RELAY14_PIN)
#define RELAY14_ON digitalLo(GPIOB, RELAY14_PIN)

#define RELAY15_TOGGLE digitalToggle(GPIOB, RELAY15_PIN)
#define RELAY15_OFF digitalHi(GPIOB, RELAY15_PIN)
#define RELAY15_ON digitalLo(GPIOB, RELAY15_PIN)

/* 定义控制 GPIOC IO的宏 */
#define RELAY5_TOGGLE digitalToggle(GPIOC, RELAY5_PIN)
#define RELAY5_OFF digitalHi(GPIOC, RELAY5_PIN)
#define RELAY5_ON digitalLo(GPIOC, RELAY5_PIN)

#define RELAY6_TOGGLE digitalToggle(GPIOC, RELAY6_PIN)
#define RELAY6_OFF digitalHi(GPIOC, RELAY6_PIN)
#define RELAY6_ON digitalLo(GPIOC, RELAY6_PIN)

#define RELAY16_TOGGLE digitalToggle(GPIOC, RELAY16_PIN)
#define RELAY16_OFF digitalHi(GPIOC, RELAY16_PIN)
#define RELAY16_ON digitalLo(GPIOC, RELAY16_PIN)

#define RELAY17_TOGGLE digitalToggle(GPIOC, RELAY17_PIN)
#define RELAY17_OFF digitalHi(GPIOC, RELAY17_PIN)
#define RELAY17_ON digitalLo(GPIOC, RELAY17_PIN)

#define RELAY18_TOGGLE digitalToggle(GPIOC, RELAY18_PIN)
#define RELAY18_OFF digitalHi(GPIOC, RELAY18_PIN)
#define RELAY18_ON digitalLo(GPIOC, RELAY18_PIN)

#define RELAY19_TOGGLE digitalToggle(GPIOC, RELAY19_PIN)
#define RELAY19_OFF digitalHi(GPIOC, RELAY19_PIN)
#define RELAY19_ON digitalLo(GPIOC, RELAY19_PIN)

void relay_init(void);

// onec write
void relay_on(uint8_t relay_gpio);
void relay_off(uint8_t relay_gpio);

//all read
void relay_gpio_flag_update(void);
//onec read
uint8_t relay_gpio_flag_read(uint8_t relay_gpio);
// all off
void relay_all_off(void);

#endif /* __RELAY_H */
