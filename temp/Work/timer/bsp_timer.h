#ifndef _TIMER_H
#define _TIMER_H
#include "stm32f4xx.h"

void TIM3_Int_Init(uint16_t arr, uint16_t psc);

#endif
