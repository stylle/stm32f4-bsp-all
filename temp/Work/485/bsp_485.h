#ifndef __485_H
#define __485_H

#include "stm32f4xx.h"

#define _485_USART USART2
#define _485_USART_CLK RCC_APB1Periph_USART2
#define _485_USART_BAUDRATE 9600

#define _485_USART_RX_GPIO_PORT GPIOA
#define _485_USART_RX_GPIO_CLK RCC_AHB1Periph_GPIOA
#define _485_USART_RX_PIN GPIO_Pin_2
#define _485_USART_RX_AF GPIO_AF_USART2
#define _485_USART_RX_SOURCE GPIO_PinSource2

#define _485_USART_TX_GPIO_PORT GPIOA
#define _485_USART_TX_GPIO_CLK RCC_AHB1Periph_GPIOA
#define _485_USART_TX_PIN GPIO_Pin_3
#define _485_USART_TX_AF GPIO_AF_USART2
#define _485_USART_TX_SOURCE GPIO_PinSource3

#define _485_RE_GPIO_PORT GPIOA
#define _485_RE_GPIO_CLK RCC_AHB1Periph_GPIOA
#define _485_RE_PIN GPIO_Pin_1

#define _485_INT_IRQ USART2_IRQn
#define _485_IRQHandler USART2_IRQHandler

/// 不精确的延时
static void _485_delay(__IO u32 nCount)
{
  for (; nCount != 0; nCount--)
    ;
}

/*控制收发引脚*/
//进入接收模式,必须要有延时等待485处理完数据
#define _485_RX_EN()                              \
  _485_delay(1000);                               \
  GPIO_ResetBits(_485_RE_GPIO_PORT, _485_RE_PIN); \
  _485_delay(1000);
//进入发送模式,必须要有延时等待485处理完数据
#define _485_TX_EN()                            \
  _485_delay(1000);                             \
  GPIO_SetBits(_485_RE_GPIO_PORT, _485_RE_PIN); \
  _485_delay(1000);

void _485_Config(u32 bound);
void _485_SendByte(uint8_t ch);
void _485_SendStr_length(uint8_t *str, uint32_t strlen);
void _485_SendString(uint8_t *str);
void rs485_crc_modbus(uint8_t *rs485_write_buff, uint8_t len); // crc加密
uint16_t crc16bitbybit(uint8_t *ptr, uint16_t len);

void bsp_485_IRQHandler(void);
void RS485_Receive_Data(u8 *buf, u8 *len);
void rs485_search_resistance_cmd(uint8_t *rs485_read_buff); // rs485接受指令处理函数

#endif /* __485_H */
