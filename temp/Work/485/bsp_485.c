#include "bsp_485.h"
#include "bsp_relay.h"
#include "bsp_delay.h"
#include "bsp_24c02.h"
#include "bsp_debug_usart.h"

#define DEBUG_MODE

extern uint8_t relay_buff[24];
extern uint8_t ee_read_buf[EEPROM_SIZE];
extern uint8_t ee_write_buf[EEPROM_SIZE];

extern const uint8_t RELAY_NUM;

uint8_t rs485_read_resistance[24] = {0x97, 0x03, 0x00, 0x0d, 0x00, 0x03}; //read
uint8_t rs485_text_resistance[24] = {0x97, 0x06, 0x00, 0x11, 0x00, 0x01}; //start

uint8_t resistance_buff[24]; // 地阻阻值存放
uint8_t oltage_buff[24];     // 电流存放数组
uint8_t float_buff[24];      // 小数点位数

/// 配置USART接收中断
static void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  /* Configure the NVIC Preemption Priority Bits */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

  /* Enable the USARTy Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = _485_INT_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
/*
 * 函数名：_485_Config
 * 描述  ：USART GPIO 配置,工作模式配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void _485_Config(u32 bound)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;
  USART_ClockInitTypeDef USART_ClockInitStruct;

  RCC_AHB1PeriphClockCmd(_485_USART_RX_GPIO_CLK | _485_USART_TX_GPIO_CLK | _485_RE_GPIO_CLK, ENABLE);
  RCC_APB1PeriphClockCmd(_485_USART_CLK, ENABLE);
  USART_DeInit(_485_USART);

  USART_StructInit(&USART_InitStructure);
  USART_ClockStructInit(&USART_ClockInitStruct);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; //复用
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽输出
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = _485_USART_TX_PIN;
  GPIO_Init(_485_USART_TX_GPIO_PORT, &GPIO_InitStructure);

  GPIO_PinAFConfig(_485_USART_TX_GPIO_PORT, GPIO_PinSource3, GPIO_AF_USART2);

  GPIO_InitStructure.GPIO_Pin = _485_USART_RX_PIN;
  GPIO_Init(_485_USART_RX_GPIO_PORT, &GPIO_InitStructure);

  GPIO_PinAFConfig(_485_USART_RX_GPIO_PORT, GPIO_PinSource2, GPIO_AF_USART2);

  USART_ClockInit(_485_USART, &USART_ClockInitStruct);

  USART_InitStructure.USART_BaudRate = bound; //波特率设置
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No; //无校验
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(_485_USART, &USART_InitStructure);

  USART_ITConfig(_485_USART, USART_IT_RXNE, ENABLE); //接收中断使能
  USART_ClearITPendingBit(_485_USART, USART_IT_TC);  //清除中断TC位
  USART_Cmd(_485_USART, ENABLE);                     //使能串口

  USART_ClearFlag(_485_USART, USART_FLAG_TC);

  /***********************************GPIOA 1,RS485方向控制******************************/
  GPIO_InitStructure.GPIO_Pin = _485_RE_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(_485_RE_GPIO_PORT, &GPIO_InitStructure);

  NVIC_Configuration();
  /* 使能串口接收中断 */
  USART_ITConfig(_485_USART, USART_IT_RXNE, ENABLE);
  GPIO_ResetBits(_485_RE_GPIO_PORT, _485_RE_PIN); //默认进入接收模式
}

/***************** 发送一个字符  **********************/
//使用单字节数据发送前要使能发送引脚，发送后要使能接收引脚。
void _485_SendByte(uint8_t ch)
{
  /* 发送一个字节数据到USART1 */
  USART_SendData(_485_USART, ch);
  /* 等待发送完毕 */
  while (USART_GetFlagStatus(_485_USART, USART_FLAG_TXE) == RESET)
    ;
}
/*****************  发送指定长度的字符串 **********************/
void _485_SendStr_length(uint8_t *str, uint32_t strlen)
{
  unsigned int k = 0;
  _485_TX_EN(); //	使能发送数据
  do
  {
    _485_SendByte(*(str + k));
    k++;
  } while (k < strlen);
  _485_delay(0xfff);
  _485_RX_EN(); //	使能接收数据
}

/*****************  发送字符串 **********************/
void _485_SendString(uint8_t *str)
{
  unsigned int k = 0;
  _485_TX_EN(); //	使能发送数据
  do
  {
    _485_SendByte(*(str + k));
    k++;
  } while (*(str + k) != '\0');
  _485_delay(0xff);
  _485_RX_EN(); //	使能接收数据
}

//中断缓存串口数据
#define UART_BUFF_SIZE 64
//接收缓存区
u8 RS485_RX_BUF[64]; //接收缓冲,最大64个字节.
//接收到的数据长度
u8 RS485_RX_CNT = 0;

void bsp_485_IRQHandler(void)
{
  u8 res;
  if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) //接收到数据
  {
    res = USART_ReceiveData(USART2); //;读取接收到的数据USART2->DR
    if (RS485_RX_CNT < UART_BUFF_SIZE)
    {
      RS485_RX_BUF[RS485_RX_CNT] = res; //记录接收到的值
      RS485_RX_CNT = RS485_RX_CNT + 1;  //接收数据增加1
      if (RS485_RX_CNT >= UART_BUFF_SIZE)
      {
        RS485_RX_CNT = 0;
      }
    }
    else
    {
      RS485_RX_CNT = 0;
      RS485_RX_BUF[RS485_RX_CNT] = res; //记录接收到的值
    }
  }
}

//RS485查询接收到的数据
//buf:接收缓存首地址
//len:读到的数据长度
void RS485_Receive_Data(u8 *buf, u8 *len)
{
  u8 rxlen = RS485_RX_CNT;
  u8 i = 0;
  *len = 0;                           //默认为0
  delay_ms(2);                        //等待10ms,连续超过10ms没有接收到一个数据,则认为接收结束
  if (rxlen == RS485_RX_CNT && rxlen) //接收到了数据,且接收完成了
  {
    for (i = 0; i < rxlen; i++)
    {
      buf[i] = RS485_RX_BUF[i];
    }
    *len = RS485_RX_CNT; //记录本次数据长度
    RS485_RX_CNT = 0;    //清零
  }
}

//uint16_t ccr_data_con = crc16bitbybit(rs485buf,length-2);
//rs485buf[length-1] = ccr_data_con&0XFF;  //低八位
//rs485buf[length-2] = ccr_data_con>>8;    //高八位
uint16_t crc16bitbybit(uint8_t *ptr, uint16_t len)
{
  uint8_t i, ch;
  uint16_t crc = 0xffff;
  static const uint16_t polynom = 0xA001;

  while (len--)
  {
    ch = *ptr;
    crc ^= ch;
    for (i = 0; i < 8; i++)
    {
      if (crc & 1)
      {
        crc >>= 1;
        crc ^= polynom;
      }
      else
      {
        crc >>= 1;
      }
    }
    ptr++;
  }
  return crc;
}

void rs485_crc_modbus(uint8_t *rs485_write_buff, uint8_t len)
{
  uint16_t ccr_data_con = crc16bitbybit(rs485_write_buff, len);
  rs485_write_buff[len] = ccr_data_con & 0XFF;   //低八位
  rs485_write_buff[len + 1] = ccr_data_con >> 8; //高八位
}

/*****************************************************************************
	传参：uint8 *  无返回
	说明：接受Modbus指令，处理功能码为0x06和0x03的指令 
				0x5b 0x06 0x00 0x08 0x00 0x5C crcl crch   表示修改设备地址为5c
				0x5b 0x03 0x00 0x00 0x00 0x03 crcl crch   表示查询第一路设备地阻阻值、电压、小数点
				0x5b 0x03 0x00 0x01 0x00 0x06 crcl crch   表示查询第二、三路设备地阻阻值、电压、小数点
	作者：代空军
	日期：2021-9-7
******************************************************************************/
void rs485_search_resistance_cmd(uint8_t *rs485_read_buff)
{
  uint8_t i = 0, count = 0;
  uint8_t crc_temp[128] = {0};
  uint8_t rs485_send_buff_temp[128] = {0};  // 返回buff
  uint8_t displacement[3] = {0, 0, 0};      // 位移

  for (i = 0; i < 6; i++)
  {
    crc_temp[i] = rs485_read_buff[i];
  }
  rs485_crc_modbus(crc_temp, 6);

  // cmd and crc   0x5b 0x03 0x00 0x00 0x00 0x01 crcl crch
  if (rs485_read_buff[0] == ee_read_buf[0] && rs485_read_buff[1] == 0x03 &&      /* 地址和命令类型校验 */ \
      rs485_read_buff[6] == crc_temp[6] && rs485_read_buff[7] == crc_temp[7] &&  /* crc校验 */ \
     (rs485_read_buff[5] + rs485_read_buff[3]) <= (RELAY_NUM * 3))               /* 位数校验 */ 
  {

#ifdef DEBUG_MODE
    printf("接收到正确的查询指令,crc校验正常\r\n");
#endif

    rs485_send_buff_temp[0] = ee_read_buf[0];
    rs485_send_buff_temp[1] = 0x03;
    rs485_send_buff_temp[2] = rs485_read_buff[5] * 2;

#ifdef DEBUG_MODE
    printf("当前设备地址:%x,功能码：%x,查询位数：%x\r\n", ee_read_buf[0], rs485_read_buff[1], rs485_read_buff[5]);
#endif
    // 这个算法没有问题查询几位就是几位不会有错乱
    for (i = 0; i < rs485_read_buff[5]; i++)
    {
      if (i % 3 == 0)
      {
        rs485_send_buff_temp[count + 3] = resistance_buff[rs485_read_buff[3] + displacement[0] + rs485_read_buff[3]];
        rs485_send_buff_temp[count + 4] = resistance_buff[rs485_read_buff[3] + displacement[0] + 1 + rs485_read_buff[3]];
        displacement[0] += 2;    // 数组displacement[0]表示resistance_buff的位置移动
        count += 2;              // 表示485buf的数组位移
      }
      else if (i % 3 == 1)
      {
        rs485_send_buff_temp[count + 3] = float_buff[rs485_read_buff[3] + displacement[1] + rs485_read_buff[3]];
        rs485_send_buff_temp[count + 4] = float_buff[rs485_read_buff[3] + displacement[1] + 1 + rs485_read_buff[3]];
        displacement[1] += 2;    // 数组displacement[1]表示float_buff的位置移动
        count += 2;              // 表示485buf的数组位移
      }
      else
      {
        rs485_send_buff_temp[count + 3] = oltage_buff[rs485_read_buff[3] + displacement[2] + rs485_read_buff[3]];
        rs485_send_buff_temp[count + 4] = oltage_buff[rs485_read_buff[3] + displacement[2] + 1 + rs485_read_buff[3]];
        displacement[2] += 2;    // 数组displacement[2]表示oltage_buff的位置移动
        count += 2;              // 表示485buf的数组位移
      }
    }

    rs485_crc_modbus(rs485_send_buff_temp, 3 + count);    // 添加CRC校验 + 3为前三位
    _485_SendStr_length(rs485_send_buff_temp, count + 5); // 总长度 = 地址：1 + 功能码：1 + 字节长度：1 + count（4倍数）+ 校验：2
    delay_ms(2);

#ifdef DEBUG_MODE
    printf("查询数据完成\r\n");
#endif
  }
  // 0x5b 0x06 0x00 0x08 0x00 0x52 crcl crch
  else if (rs485_read_buff[0] == ee_read_buf[0] && rs485_read_buff[1] == 0x06 &&  /* 地址命令校验 */ \
           rs485_read_buff[2] == 0x00 && rs485_read_buff[3] == 0x08 &&  /* 查询地址 */  
           rs485_read_buff[6] == crc_temp[6] && rs485_read_buff[7] == crc_temp[7]) /* crc校验 */
  {
#ifdef DEBUG_MODE
    printf("接收到正确的修改地址指令,crc校验正常\r\n");
#endif

    rs485_send_buff_temp[0] = ee_read_buf[0];
    rs485_send_buff_temp[1] = 0x06;
    rs485_send_buff_temp[2] = 0x00;
    rs485_send_buff_temp[3] = 0x08;

#ifdef DEBUG_MODE
    printf("当前设备地址:%x,功能码：%x,地址寄存器地址：%x，修改地址：%x\r\n", ee_read_buf[0], rs485_read_buff[1], rs485_read_buff[3], rs485_read_buff[5]);
#endif

    // 地址的修改接口
    ee_write_buf[0] = rs485_read_buff[5];
    ee_WriteBytes(ee_write_buf, 0, 1);
    delay_ms(5);
    ee_ReadBytes(ee_read_buf, 0, 1);

    rs485_send_buff_temp[4] = 0x00;
    rs485_send_buff_temp[5] = ee_read_buf[0];

    rs485_crc_modbus(rs485_send_buff_temp, 6);    // 添加CRC校验
    _485_SendStr_length(rs485_send_buff_temp, 8); // 总长度 = 地址：1 + 功能码：1 + 字节长度：1 + count（4倍数）+ 校验：2
    delay_ms(2);

#ifdef DEBUG_MODE
    printf("当前设备地址修改完成，新地址为：%x\r\n", rs485_send_buff_temp[5]);
#endif
  }
}
