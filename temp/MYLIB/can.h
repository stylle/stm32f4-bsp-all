#ifndef __CAN_H
#define __CAN_H	    

#include <stm32f10x.h>
#include "stdafx.h"
#include "mType.h"
#include "stm32F10x_ph_ext.h"
#include "string.h"

#define CAN1_ENABLE 			1 	// CAN2开启标志位	

#define CAN2_ENABLE 			0 	// CAN2开启标志位	

#define HEARTBEAT_EN 			1 	// 心跳使能

#define REVC_SIGNAL 			0		// 电源等其他接受确认信号使能

#define REVC_LAMP_SIGNAL 	1		// 灯具数据发送确认信号使能


// 结构体类型用于传值
typedef enum{
	TYPE_CAN_POWER_SUPPLY = 1,
	TYPE_CAN_INPUT_VC,
	TYPE_CAN_TIME,
	TYPE_CAN_LOOP_VOL_CUR_14,
	TYPE_CAN_LOOP_VOL_CUR_58,
	TYPE_CAN_FLAG,
	TYPE_CAN_LAMP,
}CAN_DATA_TYPE;


// 初始化CAN波特率
typedef enum{
	BOUND10KB = 1,
	BOUND20KB,
	BOUND50KB,
	BOUND100KB,
	BOUND200KB,
	BOUND500KB,
	BOUND1M,
}CAN_BOUND;


// 重新发送标志结构体
typedef struct{
	uint8_t power;
	uint8_t input;
	uint8_t vout14;
	uint8_t vout58;
	uint8_t time;
	uint8_t flag;
}RESET_SEND;

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

// 本机地址
#define CAN_ADDR   MyLable.nAddress

// 数据变化百分比范围
#define VC_PER 5

// 灯具数量
#define LAMP_NUM 250

// 应急时间发送间隔
#define HEGTM_SEND_TIME_S 60

// 接受收到回复消息等待时间
#define REVC_TIMEOUT_MS 100
#define LAMP_REVC_TIMEOUT_MS 100

// can发送间隔延时时间
#define CAN_SEND_DELAY_MS 20

//心跳时间间隔基数
#define HEARTBEAT_DELAY 100

// 心跳包
#define CAN_HEARTBEAT_EXTID 				(0x01<<24|0x02<<20|0x03<<16|0x04<<12|0x05<<8|CAN_ADDR)
#define HEARTBEATARRGY {0x12,0x12,0x12,0x12,0x12,0x12,0x12,0x12}
#define CAN_HEARTBEAT_DLC 8  

 // 电源相关
#define CAN_POWER_PF 		0x15	
#define CAN_POWER_R     0x05	   
#define CAN_POWER_SUPPLY_EXTID 			(0x01<<24|0x00<<20|0x00<<16|0x00<<12|CAN_POWER_PF<<8|CAN_ADDR)
#define CAN_POWER_SUPPLY_DLC 8

// 充电电池相关
#define CAN_INPUT_PF 		0x16	
#define CAN_INPUT_R     0x05	 
#define CAN_INPUT_VC_EXTID 					(0x02<<24|0x00<<20|0x00<<16|0x00<<12|CAN_INPUT_PF<<8|CAN_ADDR)
#define CAN_INPUT_VC_DLC 8

// 电池和应急相关
#define CAN_TIME_PF 		0x17	
#define CAN_TIME_R     	0x05	 
#define CAN_TIME_EXTID 							(0x03<<24|0x00<<20|0x00<<16|0x00<<12|CAN_TIME_PF<<8|CAN_ADDR)
#define CAN_TIME_DLC 4

// 1-4路回路电压电流
#define CAN_VOL14_PF 		0x19	
#define CAN_VOL14_R     0x05	 
#define CAN_LOOP_VOL_CUR_14_EXTID 	(0x04<<24|0x00<<20|0x00<<16|0x00<<12|CAN_VOL14_PF<<8|CAN_ADDR)
#define CAN_LOOP_VOL_CUR_14_DLC 8

// 5-8路回路电压电流
#define CAN_VOL58_PF 		0x1A	
#define CAN_VOL58_R     0x05	 
#define CAN_LOOP_VOL_CUR_58_EXTID 	(0x05<<24|0x00<<20|0x00<<16|0x00<<12|CAN_VOL58_PF<<8|CAN_ADDR)
#define CAN_LOOP_VOL_CUR_58_DLC 8

// 状态相关
#define CAN_FLAG_PF 		0x1B	
#define CAN_FLAG_R     	0x05	 
#define CAN_FLAG_EXTID 							(0x06<<24|0x00<<20|0x00<<16|0x00<<12|CAN_FLAG_PF<<8|CAN_ADDR)
#define CAN_FLAG_DLC 4

// 灯具相关
#define CAN_LAMP_PE 		0x1C	
#define CAN_LAMP_R     	0x05	 
#define CAN_LAMP_DATA_EXTID 				(0x07<<24|0x00<<20|0x00<<16|0x00<<12|CAN_LAMP_PE<<8|CAN_ADDR)
#define CAN_LAMP_DATA_DLC 3

// 开启CAN1
#if 	CAN1_ENABLE					
uint8_t CAN1_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode); // can初始化
uint8_t Can1_Send_Msg(CanTxMsg TxMessage);													//发送数据
#endif

// 开启CAN2
#if CAN2_ENABLE
uint8_t CAN2_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode); // can初始化
uint8_t Can2_Send_Msg(CanTxMsg TxMessage);													//发送数据
#endif


#if CAN1_ENABLE|CAN2_ENABLE
// 同时初始化两个can并重新初始化波特率
void CAN_Init_All(CAN_BOUND bound);
#endif

//can心跳任务需要一直发送数据包到控制器保证通信是正常在线状态
uint8_t CanHeartBeatPoll(void);
//装载发送数据并打开标志位
void upCanDataChar(CAN_DATA_TYPE type, void *data);
//装载灯具数据:传入第一个参数为id数组，第二个参数为灯具数据
void upCanLampChar(uint16_t *id,uint8_t *data);
//can数据轮询发送任务
void CanPoll(void);
//处理主机消息
void Process_host_msg(CanRxMsg ISR_RxMessage); 

#endif

