
#include "stdafx.h"
//灯具模式读IO
//================================================
//timer
//================================================
void PStartTimer(void(*pStart)(void)) {pStart();}
void PStopTimer(void(*pStop)(void)) {pStop();}
unsigned char FindNum;

extern unsigned char DataTimFlag;
//================================================
//master send frame
//================================================
unsigned char PMasterSendA(PLC_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen), void(*pStart)(void))
{
	if((pNode->bSend == FALSE)||(pNode->bState != PLC_ST_READY)) return FALSE;
	//
	pNode->bSend = FALSE;
	pSend(pNode->bTxBufS, PLC_MAX_LENS);//发送缓冲区总长度4字节

	//在间隙时间到时，可能程序先执行此处，所以会造成有5.0V平台，不影响功能
	
		if((((pNode->bTxBufS[0])>>7)&1)== PLC_TYPE_DATA)//判断是否是数据帧
		{
			//数据帧需要进行超时检测
			PStartTimer(pStart);
			pNode->bState = PLC_ST_SEND0;
		}else
		if((((pNode->bTxBufS[0])>>7)&1) == PLC_TYPE_BROAD)//判断是否是广播帧
		{
			//广播帧需要多次发送
			//PStartTimer(pStart);//广播帧不需要回传
			pNode->bState = PLC_ST_SEND0;
			MyPlcNode.bState = PLC_ST_END;
		}else
		{
			pNode->bState = PLC_ST_END;
		}
			//
		return TRUE;
}



//================================================
//fill frame
//================================================
void PFillTxBuf(PLC_NODE* pNode, unsigned char bDir)
{	
	unsigned char bIndex = 0;
	unsigned char bData = 0;
	unsigned char nCheck = 0;
		
	//
	pNode->bTxBufS[bIndex++] = (((PLC_HEAD&0X7F)|pNode->frmTx.bType<<7));  //首字节8位（校验1位+帧类型1位+6位帧头）

	//
	if(pNode->frmTx.bType == 1)//如果是数据帧那么加入正确的从机地址
	{
		bData = (unsigned char)((pNode->frmTx.nSlaver)&0xff);
		//bData = ~bData;	//invert
		pNode->bTxBufS[bIndex++] = bData;
		//
		bData = (unsigned char)(((pNode->frmTx.nSlaver)>>8)&0xff);
		pNode->bTxBufS[bIndex++] = bData;
//		
//		bData = (unsigned char)(((pNode->frmTx.nSlaver)>>16)&0xff);
//		pNode->bTxBufS[bIndex++] = bData;
	}
	else//如果是广播帧那么就从机地址填0xaaaa不做判断
	{
		bData = 0xEE;
		//bData = ~bData;	//invert
		pNode->bTxBufS[bIndex++] = bData;
		//
		bData = 0XEE;
		pNode->bTxBufS[bIndex++] = bData;
		
//		bData = 0XFE;
//		pNode->bTxBufS[bIndex++] = bData;

	}
	
	pNode->bTxBufS[bIndex++] = pNode -> frmTx.bDtBuf[0];//数据(应急或者非应急或者灯具左亮右亮等)
//	
	nCheck = TX_CheckSum(pNode->bTxBufS,4);//一个字节累加和校验
	
	pNode->bTxBufS[bIndex++] = nCheck;

	//
	pNode->bSend = TRUE;
 }


//======================================================
//frame protocol analyze
//======================================================
unsigned char PProtocolDeal(unsigned char bByte, unsigned char *pBuf)
{    
   if((bByte & 0x80) != 0)
	 {
		  pBuf[0] = bByte;//过滤无用数据;
			return TRUE;
	 }

	 else return FALSE;
}

//================================================
//master receive frame
//================================================
void PMasterReceiveA(PLC_NODE* pNode, unsigned char bByte)
{
	static unsigned char strBuf[1]; //-(head+check)
	//
	if(PProtocolDeal(bByte, strBuf) == TRUE)
	{
		
		pNode->frmRx.bDtBuf[0] = strBuf[0];//存储数据到数组中灯具的故障状态共8位

		pNode->bRevEnd = TRUE;
		pNode->bRxFlag = TRUE;
	}
}

//================================================
//slaver receive frame
//================================================
//void PSlaverReceiveA(PLC_NODE* pNode, unsigned char bByte)
//{
//	unsigned char i = 0;
//	static unsigned char strBuf[PLC_MAX_LEN-2]; //-(head+check)
//	//
//	if(PProtocolDeal(bByte, strBuf) == TRUE)
//	{
//		if(pNode->bState != PLC_ST_END) return;
//		//revceive
//		if(strBuf[PLC_DATA_OFT]&0x80)
//			pNode->frmRx.bType = PLC_TYPE_BROAD;
//		else
//			pNode->frmRx.bType = PLC_TYPE_DATA;
//		//
//		pNode->frmRx.nSlaver  = ~strBuf[PLC_DESTIN_OFT];
//		//
//		for(i=0; i< PLC_DATA_LEN; i++)
//		{
//			pNode->frmRx.bDtBuf[i] = strBuf[i+PLC_DATA_OFT];
//		}//for
//		//
//		pNode->bRevEnd = TRUE;
//	}
//}


//================================================
//set frame state
//================================================
void PSetStateToRdy(PLC_NODE* pNode) {pNode->bState = PLC_ST_READY;}
void PSetStateToEnd(PLC_NODE* pNode) {pNode->bState = PLC_ST_END;}


//================================================
//init frame
//================================================
void PFrameInit(PLC_NODE* pNode)
{
	pNode->bSend = FALSE;
	pNode->bState = PLC_ST_END;
	pNode->nOutCnt = 0;
	pNode->bRxFlag = 0;
	pNode->bRevEnd = 0;
	pNode->bDealEnd = 0;
}

