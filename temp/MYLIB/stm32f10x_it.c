/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stdafx.h"

/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
//unsigned int time=0;

/*void SysTick_Handler(void)
{
		if(time)
		time--;
}
void delay(unsigned int n)
{
	time=n*1000;
	while(time);
}
*/
/**
  * @brief  This function handles uart
  * @param  None
  * @retval None
  */
//接收控制器数据中断
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		/* Read one byte from the receive data register */
		FSlaverReceiveA(&MyFrmNode, USART_ReceiveData(USART1));
	}
	//
	if(USART_GetITStatus(USART1, USART_IT_ORE) != RESET)
	{
		USART_ClearITPendingBit(USART1, USART_IT_ORE);
		USART_ReceiveData(USART1);
	}
}

/**
  * @brief  This function handles uart
  * @param  None
  * @retval None
  */
void USART2_IRQHandler(void)
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		/* Read one byte from the receive data register */
		PMasterReceiveA(&MyPlcNode, USART_ReceiveData(USART2)); 
	}
	//
	if(USART_GetITStatus(USART2, USART_IT_ORE) != RESET)
	{
		USART_ClearITPendingBit(USART2, USART_IT_ORE);
		USART_ReceiveData(USART2);
	}
}

/**
  * @brief  This function handles uart
  * @param  None
  * @retval None
  */
void USART3_IRQHandler(void)
{
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
	}
}

/**
  * @brief  This function handles uart
  * @param  None
  * @retval None
  */
void UART4_IRQHandler(void)
{
	
  if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
  {
    /* Read one byte from the receive data register */
	
  }
	//
	if(USART_GetITStatus(UART4, USART_IT_ORE) != RESET)
	{
		USART_ClearITPendingBit(UART4, USART_IT_ORE);
		USART_ReceiveData(UART4);
	}
}

/**
  * @brief  This function handles uart
  * @param  None
  * @retval None
  */
//接收分配电数据中断
void UART5_IRQHandler(void)
{
	if(USART_GetITStatus(UART5, USART_IT_RXNE) != RESET)
	{

	}
	//
	if(USART_GetITStatus(UART5, USART_IT_ORE) != RESET)
	{
		USART_ClearITPendingBit(UART5, USART_IT_ORE);
		USART_ReceiveData(UART5);
	}
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */


//void TIM1_UP_IRQHandler(void)
//{
//	static int i = 0;
//	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) 
//		{			
//			TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );
//			if(i++ >= HEARTBEAT_DELAY)
//			{
//				CanHeartBeatPoll();		
//				i = 0;
//			}
//		}
//}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */

void TIM2_IRQHandler(void)
{
		static unsigned char a=0;
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) 
		{
      TIM_ClearITPendingBit(TIM2, TIM_IT_Update  ); 		
			ADCEventDeal();
			if(MyFrmNode.yanshi == 0)
			{
				a++;
				if(a >= 4)
				{
					MyFrmNode.bState = FRM_ST_REV;
					MyFrmNode.yanshi = 1;			
					a = 0;
				}
			}
			if( InitFanTime < FAN_INIT_TIME)
			{
				InitFanTime ++;
				
			}
			else
			{
				InitFanTime = FAN_INIT_TIME;
				Fan_Init_OK = TRUE ;
			}
		}
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) 
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update); 
		//
		
				SendStaMachine();
//======================================================
//age time
//======================================================	
//		if(Myec.AgeTime_S < 60)Myec.AgeTime_S ++;
//		else if((Myec.AgeTime_M >= 1)&&(Myec.AgeTime_S >= 60)&&(IDIS >= 50)) Myec.AgeTime_S = 0 ,Myec.AgeTime_M --;	
//		if(Myec.AgeTime_S % 59 == 0) SaveSysData(); //1分钟存一次老化时间	
//		
//		if((Myec.AgeTime_S < 60)&&(Myec.AgeTime_M <= 0)) MainToBatState = TRUE;			
	}
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
*/
unsigned short time=0;

void TIM4_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) 
	{	
		Remote1();		 
	}
	if(TIM_GetITStatus(TIM4,TIM_IT_CC2)!=RESET)
	{
		Remote2();
	}
	
	if(time >= 150)
	{
		time=0;
		QueryInTurns();
	}else
	{
		time++;
	}
	TIM_ClearFlag(TIM4,TIM_IT_Update|TIM_IT_CC2);
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */
void TIM5_IRQHandler(void)
{
	static unsigned char nTp = 0;
	static unsigned char nSecond = 0;
	static unsigned char nMinute = 0;
	static unsigned char nAccle = 0;
	//
	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET) 
	{
			
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update); 
		//
		nTp ++;
		BellnTp ++;
		//
		gbUpdate = TRUE;
		if(nTp == 10)
		{
			nTp = 0;
			nSecond++;
		}
		if(nSecond == 60) nMinute++;
		//
		if(EpsState.bAccele)
		{
			nAccle ++;
			if(nAccle == 6) 
			{
				nAccle = 0;
				SysCheckCount(0, TRUE);
			}
		}
		else
		{
			SysCheckCount(nMinute, FALSE);
		}
		//
		if(nSecond >= 60) nSecond = 0;
		if(nMinute >= 60) nMinute = 0;
		
	}
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */
void TIM6_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET) 
		{		
			TIM_ClearITPendingBit(TIM6, TIM_IT_Update  ); 
			QueryInTurns();
		}
}

/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */
void TIM7_IRQHandler(void)
{
	
	if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET) 
		{
			TIM_ClearITPendingBit(TIM7, TIM_IT_Update  ); 
//			FPFrmNode.bState = FRM_ST_END;
		}
}


/**
  * @brief  This function handles timer
  * @param  None
  * @retval None
  */
void TIM8_UP_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM8, TIM_IT_Update) != RESET) 
		{
			TIM_ClearITPendingBit(TIM8, TIM_IT_Update  ); 
					
			if(ChangSpaceTime <= 60)  //6000ms切换死区，保证继电器充分切换
			{
			  ChangSpaceTime ++;
			}

		}
}

/**
  * @brief  This function handles exti
  * @param  None
  * @retval None
  */
//短路中断处理函数执行时间大概4.760uS//
void EXTI14_IRQHandler(void)
{    
  	if (EXTI_GetITStatus(EXTI_Line4) != RESET) 
		{
			if((Start_Up_OK == TRUE)&&(NTIME >= 200000))
			{		
		  	EXTI_ClearITPendingBit(EXTI_Line4);	
			}

    }
}


/**
  * @brief  This function handles adc
  * @param  None
  * @retval None
  */
void ADC1_2_IRQHandler(void)
{
	if(ADC_GetITStatus(ADC2, ADC_IT_EOC) != RESET)
	{
		DoMarketDeal();
		ADC_ClearITPendingBit(ADC2, ADC_IT_EOC);
	}
}
/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
