
#include "stdafx.h"
#include "w25qxx.h" 

//==========================================================
//write by a page(256 bytes)
//erase by a sector(16 pages, 4k bytes)
//==========================================================


//==========================================================
//status
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
//==========================================================
unsigned char SPI_Flash_ReadSR(void)   
{  
	unsigned char byte=0;   
	SPI_FLASH_CS(0);
	SPI_ReadWriteByte(W25X_ReadStatusReg);
	byte=SPI_ReadWriteByte(0Xff);
	SPI_FLASH_CS(1);
	return byte;   
} 

//==========================================================
//写SPI_FLASH状态寄存器
//只有SPR,TB,BP2,BP1,BP0(bit 7,5,4,3,2)可以写
//==========================================================
void SPI_FLASH_Write_SR(unsigned char sr)   
{   
	SPI_FLASH_CS(0);
	SPI_ReadWriteByte(W25X_WriteStatusReg);
	SPI_ReadWriteByte(sr);
	SPI_FLASH_CS(1);
}

//==========================================================
//set well
//==========================================================
void SPI_FLASH_Write_Enable(void)   
{
	SPI_FLASH_CS(0);
  SPI_ReadWriteByte(W25X_WriteEnable);
	SPI_FLASH_CS(1);    
} 

//==========================================================
//clear well
//==========================================================
void SPI_FLASH_Write_Disable(void)   
{  
	SPI_FLASH_CS(0);
  SPI_ReadWriteByte(W25X_WriteDisable);
	SPI_FLASH_CS(1);
} 			    

//==========================================================
//read id
//==========================================================
unsigned short SPI_Flash_ReadID(void)
{
	unsigned short Temp = 0;
	//
	SPI_FLASH_CS(0);
	SPI_ReadWriteByte(0x90);//read id
	SPI_ReadWriteByte(0x00); 	    
	SPI_ReadWriteByte(0x00); 	    
	SPI_ReadWriteByte(0x00); 	 			   
	Temp|=SPI_ReadWriteByte(0xFF)<<8;  
	Temp|=SPI_ReadWriteByte(0xFF);	 
	SPI_FLASH_CS(1);
	//
	return Temp;
}

//==========================================================
//读取SPI FLASH
//在指定地址开始读取指定长度的数据
//pBuffer:数据存储区
//ReadAddr:开始读取的地址(24bit)
//NumByteToRead:要读取的字节数(最大65535)
//==========================================================
void SPI_Flash_Read(unsigned char* pBuffer,unsigned int ReadAddr,unsigned short NumByteToRead)   
{
 	unsigned short i;
	//
	SPI_FLASH_CS(0);
  SPI_ReadWriteByte(W25X_ReadData);
  SPI_ReadWriteByte((unsigned char)((ReadAddr)>>16));
  SPI_ReadWriteByte((unsigned char)((ReadAddr)>>8));
  SPI_ReadWriteByte((unsigned char)ReadAddr);
	//
  for(i=0;i<NumByteToRead;i++)
	{
     pBuffer[i]=SPI_ReadWriteByte(0XFF);
  }
	SPI_FLASH_CS(1); 
}

//==========================================================
//页编程的写入数量不能超过当页，否则会从页头重新开始写入
//==========================================================
void SPI_Flash_Write_Page(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite)
{
 	unsigned short i;
	//
  SPI_FLASH_Write_Enable();
	SPI_FLASH_CS(0);
  SPI_ReadWriteByte(W25X_PageProgram);
  SPI_ReadWriteByte((unsigned char)((WriteAddr)>>16));
  SPI_ReadWriteByte((unsigned char)((WriteAddr)>>8));
  SPI_ReadWriteByte((unsigned char)WriteAddr);
  for(i=0;i<NumByteToWrite;i++)SPI_ReadWriteByte(pBuffer[i]);
	SPI_FLASH_CS(1);
	SPI_Flash_Wait_Busy();
}


//==========================================================
//无检验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//具有自动换页功能 
//在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//CHECK OK
//==========================================================
void SPI_Flash_Write_NoCheck(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite)   
{ 			 		 
	unsigned short pageremain;	   
	pageremain=256-WriteAddr%256; //单页剩余的字节数		 	    
	if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//不大于256个字节
	while(1)
	{	   
		SPI_Flash_Write_Page(pBuffer,WriteAddr,pageremain);
		if(NumByteToWrite==pageremain)break;//写入结束了
	 	else //NumByteToWrite>pageremain
		{
			pBuffer+=pageremain;
			WriteAddr+=pageremain;	

			NumByteToWrite-=pageremain;			  //减去已经写入了的字节数
			if(NumByteToWrite>256)pageremain=256; //一次可以写入256个字节
			else pageremain=NumByteToWrite; 	  //不够256个字节了
		}
	}
}

//==========================================================
//写SPI FLASH  
//在指定地址开始写入指定长度的数据
//该函数带擦除操作!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//==========================================================
unsigned char SPI_FLASH_BUF[4096];
void SPI_Flash_Write(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite)
{ 
	unsigned int secpos;
	unsigned short secoff;
	unsigned short secremain;	   
 	unsigned short i;    

	secpos=WriteAddr/4096;//扇区地址 0~511 for w25x16
	secoff=WriteAddr%4096;//在扇区内的偏移
	secremain=4096-secoff;//扇区剩余空间大小   

	if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
	while(1) 
	{	
		SPI_Flash_Read(SPI_FLASH_BUF,secpos*4096,4096);//读出整个扇区的内容
		for(i=0;i<secremain;i++)//校验数据
		{
			if(SPI_FLASH_BUF[secoff+i]!=0XFF)break;//需要擦除  	  
		}
		if(i<secremain)//需要擦除
		{
			SPI_Flash_Erase_Sector(secpos);//擦除这个扇区
			for(i=0;i<secremain;i++)	   //复制
			{
				SPI_FLASH_BUF[i+secoff]=pBuffer[i];	  
			}
			SPI_Flash_Write_NoCheck(SPI_FLASH_BUF,secpos*4096,4096);//写入整个扇区  

		}else SPI_Flash_Write_NoCheck(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   
		if(NumByteToWrite==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;//扇区地址增1
			secoff=0;//偏移位置为0 	 

		  pBuffer+=secremain;  //指针偏移
			WriteAddr+=secremain;//写地址偏移	   
		  NumByteToWrite-=secremain;				//字节数递减
			if(NumByteToWrite>4096)secremain=4096;	//下一个扇区还是写不完
			else secremain=NumByteToWrite;			//下一个扇区可以写完了
		}	 
	}
}

//==========================================================
//ease chip
//W25X16:25s 
//W25X32:40s 
//W25X64:40s 
//==========================================================
void SPI_Flash_Erase_Chip(void)   
{                                             
    SPI_FLASH_Write_Enable();
    SPI_Flash_Wait_Busy();
  	SPI_FLASH_CS(0);
    SPI_ReadWriteByte(W25X_ChipErase);
		SPI_FLASH_CS(1);
		SPI_Flash_Wait_Busy();
}

//==========================================================
//erase a sector(150ms)
//==========================================================
void SPI_Flash_Erase_Sector(unsigned int Dst_Addr)   
{
	Dst_Addr*=4096;
	SPI_FLASH_Write_Enable();
	SPI_Flash_Wait_Busy();
	SPI_FLASH_CS(0);
	SPI_ReadWriteByte(W25X_SectorErase);
	SPI_ReadWriteByte((unsigned char)((Dst_Addr)>>16));
	SPI_ReadWriteByte((unsigned char)((Dst_Addr)>>8));
	SPI_ReadWriteByte((unsigned char)Dst_Addr);
	SPI_FLASH_CS(1);
	SPI_Flash_Wait_Busy();
}

//==========================================================
//wait busy
//==========================================================
void SPI_Flash_Wait_Busy(void)   
{   
	while ((SPI_Flash_ReadSR()&0x01)==0x01); //wait busy
}  


//==========================================================
//power down
//==========================================================
void SPI_Flash_PowerDown(void)   
{ 
  	SPI_FLASH_CS(0);
    SPI_ReadWriteByte(W25X_PowerDown);
		SPI_FLASH_CS(1);
//  delay_us(3);//wait TPD  
}


//==========================================================
//awake up
//==========================================================
void SPI_Flash_WAKEUP(void)   
{  
  	SPI_FLASH_CS(0);
    SPI_ReadWriteByte(W25X_ReleasePowerDown);
		SPI_FLASH_CS(1);
//  delay_us(3);
}   

//==========================================================
//NSS
//==========================================================
void SPI_FLASH_CS(unsigned char bBit)
{
	if(bBit) GPIO_SetBits(GPIOB, GPIO_Pin_12);
	else GPIO_ResetBits(GPIOB, GPIO_Pin_12);
}







