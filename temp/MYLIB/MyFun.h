
#ifndef	MY_FUN_H
#define MY_FUN_H

#ifndef	FALSE
#define	FALSE		0
#endif

#ifndef	TRUE
#define	TRUE		1
#endif

#ifndef	NULL
#define	NULL		0
#endif

void  DelayUs(unsigned int nTp); 
void  DelayMs(unsigned int nTp);

void 	ByteSend(unsigned char nDat);
void	BufSend(unsigned char* buf, int nLen);

void  DumpFun(unsigned char nDat);

void InvertUint8(unsigned char *dBuf,unsigned char *srcBuf);
void InvertUint16(unsigned short *dBuf,unsigned short *srcBuf);

unsigned char RX_CheckSum(unsigned char *buf, unsigned char len); 
unsigned char TX_CheckSum(unsigned char *buf, unsigned char len);
unsigned short CRC16_CCITT(unsigned char *puchMsg, unsigned int usDataLen);

void LedFlash(unsigned int nTp);
void MosFun(unsigned int nTp);
void ModelFun(unsigned int nTp);
void Temp(unsigned short temp);

unsigned char KeyScan(unsigned int nTm, unsigned char bMode);
unsigned char KeyRead(unsigned char bIndex);

unsigned int BitAcquire(unsigned char*pBuf, unsigned char nLen, int nStart, int nStop);
unsigned int BitSetting(unsigned char*pBuf, unsigned char nLen, int nStart, int nStop, int nData);

#endif

