
#ifndef PLC_H
#define PLC_H


#define	PLC_TYPE_BROAD	0
#define	PLC_TYPE_DATA   1
#define	PLC_TYPE_ACK    0x03
#define	PLC_TYPE_NACK   0x04

#define	PLC_DESTIN_OFT	0
#define	PLC_DATA_OFT    1

#define PLC_HEAD        0xFF

#define PLC_ST_END      0x00
#define PLC_ST_READY    0x01
#define PLC_ST_SEND0    0x02
#define PLC_ST_SEND1    0x03
#define PLC_ST_SEND2    0x04
#define PLC_ST_SEND3    0x05
#define PLC_ST_SEND4    0x06
#define PLC_ST_SEND5    0x07
#define PLC_ST_SEND8    0x10
#define PLC_ST_TOUT    	0x08

#define	PLC_OUT_CNT     3
#define	PLC_OUT_TIME		170
#define	PLC_DATA_LENS		1  
#define	PLC_DATA_LENR	  1  //max
#define PLC_ID_LEN      1  //ID
//#define PLC_MAX_LENS     (PLC_DATA_LENS+4)  //2位地址1+3 3位地址//1+4
#define PLC_MAX_LENS     (PLC_DATA_LENS+4)  //2位地址1+2+1校验 3位地址//1+3+1校验
#define PLC_MAX_LENR		 (PLC_DATA_LENR+1)  //2

#define TP_DLY_TRIG			50
#define TP_OV_TRIG			600 //slaver detect edge
#define TP_DLY_SEND			29
#define	TP_ZERO_DLY			400
#define	TP_DLY_CMD			1//发送完成立即延时50uS
#define	TP_KEEP_LOW			5//发送完成立即延时50uS后保持低放电时间50uS
#define	TP_KEEP_RX		  20//读数据准备200uS
#define	TP_DLY_FRM			500 //charge

#define	TP_KEEP      		8
#define	TP_KEE      		8
typedef struct
{
//  unsigned short nSlaver;//2位地址
	unsigned int  nSlaver;//三位地址
	unsigned char bType;
  unsigned char bDtBuf[PLC_DATA_LENS];
}PLC_ELEMS;
typedef struct
{
	unsigned short nSlaver;
	unsigned char bType;
  unsigned char bDtBuf[PLC_DATA_LENR];
}PLC_ELEMR;

typedef struct
{
	unsigned char bState;
	unsigned char Outstate;
	unsigned char bSend;
	unsigned char bRxFlag;
	unsigned char bRevEnd;
	unsigned char bDealEnd;
	unsigned short nOutCnt;
	unsigned char bTxBufS[PLC_MAX_LENS];
	unsigned char bTxBufR[PLC_MAX_LENR];
	//
	PLC_ELEMS frmTx;
	PLC_ELEMR frmRx;
}PLC_NODE;


unsigned char PSlaverSendA(PLC_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen));
unsigned char PMasterSendA(PLC_NODE* pNode, void(*pSend)(unsigned char*pBuf, unsigned char bLen), void(*pStart)(void));

void PSlaverReceiveA(PLC_NODE* pNode, unsigned char bByte);
void PMasterReceiveA(PLC_NODE* pNode, unsigned char bByte);

void PSetStateToRdy(PLC_NODE* pNode);
void PSetStateToEnd(PLC_NODE* pNode);

void PFTimeOutDetect(PLC_NODE* pNode, void(*pStop)(void));

void PFrameInit(PLC_NODE* pNode);

void PFillTxBuf(PLC_NODE* pNode, unsigned char bDir);

void PStartTimer(void(*pStart)(void));
void PStopTimer(void(*pStop)(void));

#endif


