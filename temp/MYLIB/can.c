#include "can.h"

//uint8_t CanResetSendBuf[128][8]; 		// 重发缓冲区
//uint8_t ResetBuffLen = 0;					 	// 重发缓冲区位置
// 结构体数据变量
static CAN_POWER_SUPPLY power_supply; 
static CAN_INPUT_VC input_vc;
static CAN_TIME canTime;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8;
static CAN_FLAG canFlag;
static CAN_LAMP_DATA lampData[LAMP_NUM]; // 512个灯具

// 结构体历史数据变量
static CAN_POWER_SUPPLY power_supply_tmp;
static CAN_INPUT_VC input_vc_tmp;
static CAN_TIME canTime_tmp;
static CAN_LOOP_VOL_CUR_14 VolCur_1To4_tmp;
static CAN_LOOP_VOL_CUR_58 VolCur_5To8_tmp;
static CAN_FLAG canFlag_tmp;
//static CAN_LAMP_DATA lampData_tmp;
static CAN_LAMP_DATA lampData_tmp[LAMP_NUM];

#if HEARTBEAT_EN
static const uint8_t heartbeatArrgy[8] = HEARTBEATARRGY; // 心跳数据变量
#endif

CanRxMsg CanSendBuff;

#if REVC_SIGNAL
uint8_t revcSendOkSignal = FALSE;		// 主机回复标志位
#endif
#if REVC_LAMP_SIGNAL
uint8_t LampRevcSendOkSignal = FALSE;// 灯具回复标志位
#endif

extern uint8_t FirstSend; //首次全部发送标志
extern unsigned short LampSet ; //控制灯具设置状态

// 模拟灯具数据列表
extern uint16_t lampAddr[250];
extern uint8_t lampState[250];

// 静态函数
//  SEND_TYPE_CAN_POWER_SUPPLY_DATA,
//	SEND_TYPE_CAN_INPUT_VC_DATA,
//	SEND_TYPE_CAN_TIME_DATA,
//	SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA,
//	SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA,
//	SEND_TYPE_CAN_FLAG_DATA,
//	SEND_TYPE_CAN_LAMP_DATA_DATA

// 该函数用于初始化FIFO1的过滤器，F103标准库的过滤器初始化函数没有这部分
void CAN_FilterInit1(CAN_FilterInitTypeDef* CAN_FilterInitStruct)
{
  uint32_t filter_number_bit_pos = 0;
  /* Check the parameters */
  assert_param(IS_CAN_FILTER_NUMBER(CAN_FilterInitStruct->CAN_FilterNumber));
  assert_param(IS_CAN_FILTER_MODE(CAN_FilterInitStruct->CAN_FilterMode));
  assert_param(IS_CAN_FILTER_SCALE(CAN_FilterInitStruct->CAN_FilterScale));
  assert_param(IS_CAN_FILTER_FIFO(CAN_FilterInitStruct->CAN_FilterFIFOAssignment));
  assert_param(IS_FUNCTIONAL_STATE(CAN_FilterInitStruct->CAN_FilterActivation));
  filter_number_bit_pos = ((uint32_t)1) << CAN_FilterInitStruct->CAN_FilterNumber;
  /* Initialisation mode for the filter */
  CAN2->FMR |= 0x00000001;
  /* Filter Deactivation */
  CAN2->FA1R &= ~(uint32_t)filter_number_bit_pos;
  /* Filter Scale */
  if (CAN_FilterInitStruct->CAN_FilterScale == CAN_FilterScale_16bit)
  {
    /* 16-bit scale for the filter */
    CAN2->FS1R &= ~(uint32_t)filter_number_bit_pos;
    /* First 16-bit identifier and First 16-bit mask */
    /* Or First 16-bit identifier and Second 16-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR1 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdLow) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdLow);
    /* Second 16-bit identifier and Second 16-bit mask */
    /* Or Third 16-bit identifier and Fourth 16-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR2 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdHigh);
  }
  if (CAN_FilterInitStruct->CAN_FilterScale == CAN_FilterScale_32bit)
  {
    /* 32-bit scale for the filter */
    CAN2->FS1R |= filter_number_bit_pos;
    /* 32-bit identifier or First 32-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR1 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterIdLow);
    /* 32-bit mask or Second 32-bit identifier */
    CAN2->sFilterRegister[CAN_FilterInitStruct->CAN_FilterNumber].FR2 = 
    ((0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdHigh) << 16) |
        (0x0000FFFF & (uint32_t)CAN_FilterInitStruct->CAN_FilterMaskIdLow);
  }
  /* Filter Mode */
  if (CAN_FilterInitStruct->CAN_FilterMode == CAN_FilterMode_IdMask)
  {
    /*Id/Mask mode for the filter*/
    CAN2->FM1R &= ~(uint32_t)filter_number_bit_pos;
  }
  else /* CAN_FilterInitStruct->CAN_FilterMode == CAN_FilterMode_IdList */
  {
    /*Identifier list mode for the filter*/
    CAN2->FM1R |= (uint32_t)filter_number_bit_pos;
  }
  /* Filter FIFO assignment */
  if (CAN_FilterInitStruct->CAN_FilterFIFOAssignment == CAN_Filter_FIFO0)
  {
    /* FIFO 0 assignation for the filter */
    CAN2->FFA1R &= ~(uint32_t)filter_number_bit_pos;
  }
  if (CAN_FilterInitStruct->CAN_FilterFIFOAssignment == CAN_Filter_FIFO1)
  {
    /* FIFO 1 assignation for the filter */
    CAN2->FFA1R |= (uint32_t)filter_number_bit_pos;
  }
  /* Filter activation */
  if (CAN_FilterInitStruct->CAN_FilterActivation == ENABLE)
  {
    CAN2->FA1R |= filter_number_bit_pos;
  }
  /* Leave the initialisation mode for the filter */
  CAN2->FMR &= ~0x00000001;
}


#if CAN1_ENABLE
/*
	CAN_SJW:		重新同步跳跃时间单元.范围:CAN_SJW_1tq~ CAN_SJW_4tq
	CAN_BS2:		时间段2的时间单元.   范围:CAN_BS2_1tq~CAN_BS2_8tq;
	CAN_BS1:		时间段1的时间单元.   范围:CAN_BS1_1tq ~CAN_BS1_16tq
	Prescaler :	波特率分频器.范围:1~1024;  tq=(brp)*tpclk1
	CanMode：		CAN_Mode_LoopBack（回环模式） CAN_Mode_Normal（正常模式）
	波特率=Fpclk1 / ((BS1 + tBS2 + 1) * Pre); // 总线是时钟频率 / （时间段1 + 时间段2 + 1） * CAN分频

	CAN_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,CAN_Mode_LoopBack);
	波特率为:36M/((8+9+1)*4)=500Kbps 模式为回环模式
	返回值:0,初始化OK;   其他,初始化失败;    
*/
uint8_t CAN1_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode)
{
	GPIO_InitTypeDef 				GPIO_InitStructure; 
	CAN_InitTypeDef        	CAN_InitStructure;
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
	NVIC_InitTypeDef  			NVIC_InitStructure;

	// 时钟初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//使能PORTA时钟	                   											 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);	//使能CAN1时钟	

	// GPIO初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;				//复用推挽GPIO_Mode_AF_PP
	GPIO_Init(GPIOA, &GPIO_InitStructure);								//初始化IO

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;					//上拉输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);								//初始化IO

	// CAN单元设置
	CAN_InitStructure.CAN_TTCM=DISABLE;			//非时间触发通信模式  
	CAN_InitStructure.CAN_ABOM=DISABLE;			//软件自动离线管理	 
	CAN_InitStructure.CAN_AWUM=DISABLE;			//睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStructure.CAN_NART=ENABLE;			//禁止报文自动传送 
	CAN_InitStructure.CAN_RFLM=DISABLE;		 	//报文不锁定,新的覆盖旧的  
	CAN_InitStructure.CAN_TXFP=ENABLE;			//优先级由报文标识符决定 
	CAN_InitStructure.CAN_Mode= mode;	  		//模式设置： mode:0,普通模式;1,回环模式; 
	
	// 设置波特率
	CAN_InitStructure.CAN_SJW=tsjw;					//重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  CAN_SJW_1tq	 CAN_SJW_2tq CAN_SJW_3tq CAN_SJW_4tq
	CAN_InitStructure.CAN_BS1=tbs1; 				//Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2=tbs2;					//Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler=brp;		//分频系数(Fdiv)为brp+1	
	CAN_Init(CAN1, &CAN_InitStructure);     //初始化CAN1 
	
	// 配置过滤器
	CAN_FilterInitStructure.CAN_FilterNumber=0;													//过滤器14
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 			//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; 			//32位宽 
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;										//32位ID
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;								//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO0;	//过滤器14关联到FIFO1
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;								//激活过滤器1
	CAN_FilterInit(&CAN_FilterInitStructure);			//滤波器初始化
	
	// 配置FIFO1为CAN2接收器
	CAN_ITConfig(CAN1,CAN_IT_FMP0, ENABLE);				//FIFO1消息挂号中断允许.		    
	
	// 配置中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn; // USB_LP_CAN1_RX0_IRQn  CAN1_RX1_IRQn
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;     // 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;            // 次优先级为1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_SCE_IRQn;	   //CAN RX中断
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  /*CAN通信中断使能*/
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);		//接收信号量建好后再开启

	return 0;
}

// 0表示发送失败
// 1表示发送成功
static uint8_t Can_Send(CanTxMsg *TxMessage)
{
	uint8_t res;
	uint16_t i = 0;
	
	// 可以在这里添加一个喂狗函数
	IWDG_Feed();		  //喂狗	
	
#if CAN1_ENABLE
	res= CAN_Transmit(CAN1, TxMessage);	// 发送数据
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	if(i>=0XFFF)  return 0; //发送失败
	i = 0;
#endif
	
#if CAN2_ENABLE
	res= CAN_Transmit(CAN2, TxMessage);	// 发送数据
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	if(i>=0XFFF)  return 0; //发送失败
#endif
	
#if REVC_SIGNAL
	i = 0;
	revcSendOkSignal = 0; // 清楚接受标志
	memcpy(&CanSendBuff,TxMessage,sizeof(CanTxMsg));
	while(!revcSendOkSignal)
	{
		SysTickDelayMs(1);
		if(i++ > REVC_TIMEOUT_MS) return 0; // 等待接受信息超时
	}
#endif
	
#if REVC_LAMP_SIGNAL
	if(TxMessage->ExtId == CAN_LAMP_DATA_EXTID)
	{
		i = 0;
		LampRevcSendOkSignal = 0; // 清楚接受标志
		memcpy(&CanSendBuff,TxMessage,sizeof(CanTxMsg));
		while(!LampRevcSendOkSignal)
		{
			SysTickDelayMs(1);
			if(i++ > LAMP_REVC_TIMEOUT_MS) return 0; // 等待接受信息超时
		}
	}
#endif
	return 1;
}

static int SEND_TYPE_CAN_POWER_SUPPLY_DATA(CAN_POWER_SUPPLY power_supply)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_POWER_SUPPLY_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_POWER_SUPPLY_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&power_supply,CAN_POWER_SUPPLY_DLC * sizeof(uint8_t));
	
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_INPUT_VC_DATA(CAN_INPUT_VC input_vc)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_INPUT_VC_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_INPUT_VC_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&input_vc,CAN_INPUT_VC_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_TIME_DATA(CAN_TIME canTime)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_TIME_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_TIME_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canTime,CAN_TIME_DLC * sizeof(uint8_t));
	
	return Can_Send(&TxMessage);	
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(CAN_LOOP_VOL_CUR_14 loop_vol_cur_14)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_LOOP_VOL_CUR_14_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LOOP_VOL_CUR_14_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_14,CAN_LOOP_VOL_CUR_14_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(CAN_LOOP_VOL_CUR_58 loop_vol_cur_58)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_LOOP_VOL_CUR_58_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LOOP_VOL_CUR_58_DLC ;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&loop_vol_cur_58,CAN_LOOP_VOL_CUR_58_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}
static int SEND_TYPE_CAN_FLAG_DATA(CAN_FLAG canflag)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_FLAG_EXTID;				// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_FLAG_DLC;							// 要发送的数据长度
	
	// 填充数据
	memcpy(TxMessage.Data,&canflag,CAN_FLAG_DLC * sizeof(uint8_t));
	 
	return Can_Send(&TxMessage);
}

static int SEND_TYPE_CAN_LAMP_DATA_DATA(CAN_LAMP_DATA lamp_data)
{
	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_LAMP_DATA_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC = CAN_LAMP_DATA_DLC ;							// 要发送的数据长度
	
	// 填充数据
	TxMessage.Data[0] = lamp_data.LAMP_ID_H;
	TxMessage.Data[1] = lamp_data.LAMP_ID_L;
	TxMessage.Data[2] = lamp_data.LAMP_DATA;
	
	return Can_Send(&TxMessage);
}


/*
	can发送一组数据(固定格式:ID为0X12,标准帧,数据帧)	
	len:数据长度(最大为8)				     
	message:数据指针,最大为8个字节.
	返回值:0,成功; 其他,失败;
*/
uint8_t Can1_Send_Msg(CanTxMsg TxMessage)
{
	uint8_t res;
	uint16_t i=0;
	
	res= CAN_Transmit(CAN1, &TxMessage);	// 发送数据
	
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	
	if(i>=0XFFF)
			return 1;
	return 0;	 
}

//  TYPE_CAN_POWER_SUPPLY,
//	TYPE_CAN_INPUT_VC,
//	TYPE_CAN_TIME,
//	TYPE_CAN_LOOP_VOL_CUR_14,
//	TYPE_CAN_LOOP_VOL_CUR_58,
//	TYPE_CAN_FLAG,
//	TYPE_CAN_LAMP_DATA

//	CAN_POWER_SUPPLY 
//	CAN_INPUT_VC 
//	CAN_TIME 
//	CAN_LOOP_VOL_CUR_14 
//	CAN_LOOP_VOL_CUR_58 
//	CAN_FLAG 
//	CAN_LAMP_DATA 

/*
 * can心跳任务需要一直发送数据包到控制器保证通信是正常在线状态
 * 发送成功：0，发送失败：1
 */
uint8_t CanHeartBeatPoll(void)
{
#if HEARTBEAT_EN
	uint8_t res;
	int i=0;

	// 发送的数据结构
	CanTxMsg TxMessage;
	TxMessage.StdId=0x12;						// 标准标识符 
	TxMessage.ExtId=CAN_HEARTBEAT_EXTID;						// 设置扩展标示符 
	TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
	TxMessage.RTR=CAN_RTR_Data;			// 数据帧
	TxMessage.DLC=CAN_HEARTBEAT_DLC;							  // 要发送的数据长度
	
	// 填充数据
	for(i=8;i >= 0;i--)
		TxMessage.Data[i]=heartbeatArrgy[i];	 

#if CAN2_ENABLE
	res = CAN_Transmit(CAN2, &TxMessage);	// CAN2发送数据
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
	{
			i++;															//等待发送结束
	}
	i = 0;
	res = 0;
#endif

#if CAN1_ENABLE
	res = CAN_Transmit(CAN1, &TxMessage);	// CAN1发送数据
	while((CAN_TransmitStatus(CAN1, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
	{
			i++;															//等待发送结束
	}
#endif

	if(i>=0XFFF)
			return 1;
#endif
	return 0;	
}

/*
 * 初始化发送数据并打开标志位
 * 传入参数:传入结构体类型，这是个枚举
 * 传入需要更新发送的数据应该为小于8位的数组
 * 这里可能需要注意的是这里拼接结构体的时候需要注意顺序
 */

void upCanDataChar(CAN_DATA_TYPE type,void *data)
{
	switch(type)
	{
		case TYPE_CAN_POWER_SUPPLY:
		{	
				memcpy(&power_supply,data,sizeof(power_supply)- sizeof(uint8_t));
			
				power_supply.sendStatus = TRUE;
					break;
		}
		case TYPE_CAN_INPUT_VC:
		{
			memcpy(&input_vc,data,sizeof(input_vc) - sizeof(uint8_t));
			
			input_vc.sendStatus = TRUE;
			
			break;
		}
		case TYPE_CAN_TIME:
		{
		
			memcpy(&canTime,data,sizeof(canTime) - sizeof(uint8_t));
			
			canTime.sendStatus = TRUE;
			
			break;
		}
		case TYPE_CAN_LOOP_VOL_CUR_14:
		{
			memcpy(&VolCur_1To4,data,sizeof(VolCur_1To4) - sizeof(uint8_t));
			
			VolCur_1To4.sendStatus = TRUE;
			
			break;
		}
		case TYPE_CAN_LOOP_VOL_CUR_58:
		{
			memcpy(&VolCur_5To8,data,sizeof(VolCur_5To8) - sizeof(uint8_t));
			
			VolCur_5To8.sendStatus = TRUE;
			
			break;
		}
		case TYPE_CAN_FLAG:
		{
			memcpy(&canFlag,data,sizeof(canFlag) - sizeof(uint8_t));
			
			canFlag.sendStatus = TRUE;
			
			break;
		}
		default:break;
	}
}

/*
* 装载灯具数据:传入第一个参数为id数组，第二个参数为灯具数据
 */
void upCanLampChar(uint16_t *id,uint8_t *data)
{
	int i = 0;
	for(i = 0;i < LAMP_NUM;i++)
	{
		lampData[i].LAMP_ID_H = GETINT16_HIGH(data[i]);
		lampData[i].LAMP_ID_L = GETINT16_LOW(id[i]);
		lampData[i].LAMP_DATA = data[i];
	}
}

/*
 * can数据轮询发送任务
 */

//  SEND_TYPE_CAN_POWER_SUPPLY_DATA,
//	SEND_TYPE_CAN_INPUT_VC_DATA,
//	SEND_TYPE_CAN_TIME_DATA,
//	SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA,
//	SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA,
//	SEND_TYPE_CAN_FLAG_DATA,
//	SEND_TYPE_CAN_LAMP_DATA_DATA

void CanPoll(void)
{
	int byte_tmp[4]; 	// 用来保存高低字节合并的上一次数据
	int byte[4];			// 用于保存高低字节新的数据
	int i = 0;
	static RESET_SEND resetSend; //  重新发送标志
	
	if(FirstSend) 
	{
		/* 全部发送标志，这里用到首次发送上面，首次发送过后会有一个标志位
			表示发送首次发送是否成功保证主机已经收到该数据	
		*/
		memcpy(&power_supply_tmp, &power_supply ,sizeof(power_supply));
		resetSend.power = !SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		memcpy(&input_vc_tmp, &input_vc ,sizeof(input_vc));
		resetSend.input = !SEND_TYPE_CAN_INPUT_VC_DATA(input_vc);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		memcpy(&canFlag_tmp, &canFlag ,sizeof(canFlag));
		resetSend.flag = !SEND_TYPE_CAN_FLAG_DATA(canFlag);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		memcpy(&VolCur_1To4_tmp, &VolCur_1To4 ,sizeof(VolCur_1To4));
		resetSend.vout14 = !SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		memcpy(&VolCur_5To8_tmp, &VolCur_5To8 ,sizeof(VolCur_5To8));
		resetSend.vout58 = !SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		memcpy(&canTime_tmp, &canTime ,sizeof(canTime));
		resetSend.time = !SEND_TYPE_CAN_TIME_DATA(canTime);
		SysTickDelayMs(CAN_SEND_DELAY_MS);
		
		for(i= 0;i < LAMP_NUM; i++)
		{
			lampData_tmp[i].LAMP_ID_H = lampData[i].LAMP_ID_H;
			lampData_tmp[i].LAMP_ID_L = lampData[i].LAMP_ID_L;
			lampData_tmp[i].LAMP_DATA = lampData[i].LAMP_DATA;
			if(!SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[i]))
			{
				lampData_tmp[i].LAMP_DATA = 5; // 重发标志
			} 
			SysTickDelayMs(CAN_SEND_DELAY_MS);
		}
		FirstSend = FALSE;		// 首次发送标志关闭
	}
	
	//电源相关 
	if (TRUE == power_supply.sendStatus)
	{
		byte_tmp[0] = MERGE_HL(power_supply_tmp.MAIN_VOL_H,power_supply_tmp.MAIN_VOL_L);
		byte[0] = MERGE_HL(power_supply.MAIN_VOL_H,power_supply.MAIN_VOL_L);
		
		byte_tmp[1] = MERGE_HL(power_supply_tmp.OUTPUT_VOL_H,power_supply_tmp.OUTPUT_VOL_L);
		byte[1] = MERGE_HL(power_supply.OUTPUT_VOL_H,power_supply.OUTPUT_VOL_L);
		
		byte_tmp[2] = MERGE_HL(power_supply_tmp.OUTPUT_CUR_H,power_supply_tmp.OUTPUT_CUR_L);
		byte[2] = MERGE_HL(power_supply.OUTPUT_CUR_H,power_supply.OUTPUT_CUR_L);
		
		byte_tmp[3] = MERGE_HL(power_supply_tmp.BATTERY_VOL_ALL_H,power_supply_tmp.BATTERY_VOL_ALL_L);
		byte[3] = MERGE_HL(power_supply.BATTERY_VOL_ALL_H,power_supply.BATTERY_VOL_ALL_L);
		
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) || FirstSend ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.power)
			{
				if(SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply)) //1为发送成功
				{
					// 发送数据成功过后、将标注位关闭并保存上次发送的数据
					memcpy(&power_supply_tmp, &power_supply ,sizeof(power_supply));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					power_supply.sendStatus = FALSE;
					resetSend.power = FALSE;
				}else
				{
					resetSend.power = TRUE; // 重发标志
					CAN_Init_All(BOUND20KB);
				}
			}
			else{
				// 数据相同不上传
				// Can_Send_Msg(heartbeatArrgy,8);//发送8个字节
			}	
	}
	
	//充电电池相关
	if (TRUE == input_vc.sendStatus)
	{
		byte_tmp[0] = MERGE_HL(input_vc_tmp.BATTERY_VOL_1_H,input_vc_tmp.BATTERY_VOL_1_L);
		byte[0] = MERGE_HL(input_vc.BATTERY_VOL_1_H,input_vc.BATTERY_VOL_1_L);
		
		byte_tmp[1] = MERGE_HL(input_vc_tmp.BATTERY_VOL_2_H,input_vc_tmp.BATTERY_VOL_2_L);
		byte[1] = MERGE_HL(input_vc.BATTERY_VOL_2_H,input_vc.BATTERY_VOL_2_L);
		
		byte_tmp[2] = MERGE_HL(input_vc_tmp.INPUT_CUR_H,input_vc_tmp.INPUT_CUR_L);
		byte[2] = MERGE_HL(input_vc.INPUT_CUR_H,input_vc.INPUT_CUR_L);
		
		byte_tmp[3] = MERGE_HL(input_vc_tmp.INPUT_VOL_H,input_vc_tmp.INPUT_VOL_L);
		byte[3] = MERGE_HL(input_vc.INPUT_VOL_H,input_vc.INPUT_VOL_L);
		
		if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.input)
		  {
				if(SEND_TYPE_CAN_INPUT_VC_DATA(input_vc))
				{
					// 保存上次数据
					memcpy(&input_vc_tmp, &input_vc ,sizeof(input_vc));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					input_vc.sendStatus = FALSE;
					resetSend.input = FALSE;
				}else{
					resetSend.input = TRUE; // 重发标志
					CAN_Init_All(BOUND20KB);
				}
			}
			else{
				// 数据相同不上传
			}	
	}
	//电池和应急相关 
	if (TRUE == canTime.sendStatus)
	{
		byte_tmp[0] = MERGE_HL(canTime_tmp.BATTERY_VOL_3_H,canTime_tmp.BATTERY_VOL_3_L);
		byte[0] = MERGE_HL(canTime.BATTERY_VOL_3_H,canTime.BATTERY_VOL_3_L);
		byte_tmp[1] = MERGE_HL(canTime_tmp.HEGTM_H,canTime_tmp.HEGTM_L);
		byte[1] = MERGE_HL(canTime.HEGTM_H,canTime.HEGTM_L);

			if(byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER) ||
					byte[1] > (byte_tmp[1] + HEGTM_SEND_TIME_S) || byte[1] < (byte_tmp[1]) || resetSend.time) // 定时更新一次应急时间，如果状态从新计数表示重新应急需要发送一次
			{
				if(SEND_TYPE_CAN_TIME_DATA(canTime)) 
				{
					// 保存上次数据
					memcpy(&canTime_tmp, &canTime ,sizeof(canTime));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					canTime.sendStatus = FALSE;
					resetSend.time = FALSE;
				}else{
					resetSend.time = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
			else{
				// 数据相同不上传
			}
	}
	//1-4路回路电压电流
	if (TRUE == VolCur_1To4.sendStatus)
	{
			byte_tmp[0] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_1_H,VolCur_1To4_tmp.LOOP_VOL_CUR_1_L);
			byte[0] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_1_H,VolCur_1To4.LOOP_VOL_CUR_1_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_2_H,VolCur_1To4_tmp.LOOP_VOL_CUR_2_L);
			byte[1] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_2_H,VolCur_1To4.LOOP_VOL_CUR_2_L);
			
			byte_tmp[2] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_3_H,VolCur_1To4_tmp.LOOP_VOL_CUR_3_L);
			byte[2] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_3_H,VolCur_1To4.LOOP_VOL_CUR_3_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_1To4_tmp.LOOP_VOL_CUR_4_H,VolCur_1To4_tmp.LOOP_VOL_CUR_4_L);
			byte[3] = MERGE_HL(VolCur_1To4.LOOP_VOL_CUR_4_H,VolCur_1To4.LOOP_VOL_CUR_4_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.vout14)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4))
				{
					// 保存上次数据
					memcpy(&VolCur_1To4_tmp, &VolCur_1To4 ,sizeof(VolCur_1To4));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					VolCur_1To4.sendStatus = FALSE;
					resetSend.vout14 = FALSE;
				}else{
					resetSend.vout14 = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
			else{
				// 数据相同不上传
			}	
	}
	//5-8路回路电压电流
	if (TRUE == VolCur_5To8.sendStatus)
	{
			byte_tmp[0] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_5_H,VolCur_5To8_tmp.LOOP_VOL_CUR_5_L);
			byte[0] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_5_H,VolCur_5To8.LOOP_VOL_CUR_5_L);
			
			byte_tmp[1] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_6_H,VolCur_5To8_tmp.LOOP_VOL_CUR_6_L);
			byte[1] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_6_H,VolCur_5To8.LOOP_VOL_CUR_6_L);
			
			byte_tmp[2] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_7_H,VolCur_5To8_tmp.LOOP_VOL_CUR_7_L);
			byte[2] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_7_H,VolCur_5To8.LOOP_VOL_CUR_7_L);
			
			byte_tmp[3] = MERGE_HL(VolCur_5To8_tmp.LOOP_VOL_CUR_8_H,VolCur_5To8_tmp.LOOP_VOL_CUR_8_L);
			byte[3] = MERGE_HL(VolCur_5To8.LOOP_VOL_CUR_8_H,VolCur_5To8.LOOP_VOL_CUR_8_L);
		
			if((byte[0]>(byte_tmp[0]+(byte_tmp[0]/100)*VC_PER) || byte[0]<(byte_tmp[0]-(byte_tmp[0]/100)*VC_PER)) ||
			 (byte[1]>(byte_tmp[1]+(byte_tmp[1]/100)*VC_PER) || byte[1]<(byte_tmp[1]-(byte_tmp[1]/100)*VC_PER)) || 
			 (byte[2]>(byte_tmp[2]+(byte_tmp[2]/100)*VC_PER) || byte[2]<(byte_tmp[2]-(byte_tmp[2]/100)*VC_PER)) ||
			 (byte[3]>(byte_tmp[3]+(byte_tmp[3]/100)*VC_PER) || byte[3]<(byte_tmp[3]-(byte_tmp[3]/100)*VC_PER)) || resetSend.vout58)
		  {
				if(SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8)) // 1为发送成功
				{
					// 保存上次数据
					memcpy(&VolCur_5To8_tmp, &VolCur_5To8 ,sizeof(VolCur_5To8));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					VolCur_5To8.sendStatus = FALSE;
					resetSend.vout58 = FALSE;
				}else
				{
					resetSend.vout58 = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}
			else{
				// 数据相同不上传
			}	
	}
	// 状态相关
	if (TRUE == canFlag.sendStatus)
	{
			if(canFlag_tmp.FAULT_FLAG_H == canFlag.FAULT_FLAG_H &&
					canFlag_tmp.FAULT_FLAG_L == canFlag.FAULT_FLAG_L &&
					canFlag_tmp.WORK_FLAG_H == canFlag.WORK_FLAG_H &&
					canFlag_tmp.WORK_FLAG_L == canFlag.WORK_FLAG_L && resetSend.flag == FALSE)
			{
				// 数据相同不上传
			}
			else{
				if(SEND_TYPE_CAN_FLAG_DATA(canFlag)) 
				{
					// 发送上传数据
					memcpy(&canFlag_tmp, &canFlag ,sizeof(canFlag));
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					canFlag.sendStatus = FALSE;
					resetSend.flag = FALSE;
				}else{
					resetSend.flag = TRUE;
					CAN_Init_All(BOUND20KB);
				}
			}	
	}
	// 灯具轮询开始
	if (TRUE)
	{
		for(i= 0;i < LAMP_NUM;i++)
		{
			if((lampData_tmp[i].LAMP_DATA == lampData[i].LAMP_DATA) && (lampData_tmp[i].LAMP_DATA != 5))
			{
				// 数据相同不上传
//				SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[i]);
//				SysTickDelayMs(CAN_SEND_DELAY_MS);
			}
			else if(SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[i]))
			{
					SysTickDelayMs(CAN_SEND_DELAY_MS);
					// 发送上传数据并将标注位关闭	
					lampData_tmp[i].LAMP_DATA = lampData[i].LAMP_DATA;
			} 
			else if(lampData_tmp[i].LAMP_DATA != lampData[i].LAMP_DATA)
			{
					lampData_tmp[i].LAMP_DATA = 5; // 重发标志
			}
		} 		// for循环	
	}				// 轮询灯具的if
	//灯具轮询结束
}

#endif


#if CAN2_ENABLE
////////////////////////////////////////////////////////////// can2
uint8_t CAN2_Mode_Init(uint8_t tsjw,uint8_t tbs2,uint8_t tbs1,uint16_t brp,uint8_t mode)
{
	GPIO_InitTypeDef 				GPIO_InitStructure; 
	CAN_InitTypeDef        	CAN_InitStructure;
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
	NVIC_InitTypeDef  			NVIC_InitStructure;

	// 时钟初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//使能PORTA时钟	                   											 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);	//使能CAN1时钟	

	// GPIO初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;				//复用推挽	GPIO_Mode_AF_PP
	GPIO_Init(GPIOB, &GPIO_InitStructure);					

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;					//上拉输入 GPIO_Mode_IPU
	GPIO_Init(GPIOB, &GPIO_InitStructure);						

	// CAN单元设置
	CAN_InitStructure.CAN_TTCM=DISABLE;			//非时间触发通信模式  
	CAN_InitStructure.CAN_ABOM=DISABLE;			//软件自动离线管理	 
	CAN_InitStructure.CAN_AWUM=DISABLE;			//睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStructure.CAN_NART=ENABLE;			//禁止报文自动传送 
	CAN_InitStructure.CAN_RFLM=DISABLE;		 	//报文不锁定,新的覆盖旧的  
	CAN_InitStructure.CAN_TXFP=ENABLE;			//优先级由报文标识符决定 
	CAN_InitStructure.CAN_Mode= mode;	  		//模式设置： mode:0,普通模式;1,回环模式; 
	
	// 设置波特率
	CAN_InitStructure.CAN_SJW=tsjw;					//重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  CAN_SJW_1tq	 CAN_SJW_2tq CAN_SJW_3tq CAN_SJW_4tq
	CAN_InitStructure.CAN_BS1=tbs1; 				//Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2=tbs2;					//Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler=brp;		//分频系数(Fdiv)为brp+1	
	
	CAN_Init(CAN2, &CAN_InitStructure);     //初始化CAN2
	
	// 配置过滤器
	CAN_FilterInitStructure.CAN_FilterNumber=1;												//过滤器14
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 			//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; 			//32位宽 
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;										//32位ID
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;								//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO1;	//过滤器14关联到FIFO1
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;								//激活过滤器1
	CAN_FilterInit1(&CAN_FilterInitStructure);			//滤波器初始化
	
	// 配置中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX1_IRQn; // CAN2_RX1_IRQn    USB_LP_CAN2_RX0_IRQn
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;     // 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;            // 次优先级为1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	// 配置FIFO1为CAN2接收器
	CAN_ITConfig(CAN2,CAN_IT_FMP1, ENABLE);				//FIFO1消息挂号中断允许.		 

	return 0;
}

/*
	can发送一组数据(固定格式:ID为0X12,标准帧,数据帧)	
	len:数据长度(最大为8)				     
	message:数据指针,最大为8个字节.
	返回值:0,成功; 其他,失败;
*/
uint8_t Can2_Send_Msg(CanTxMsg TxMessage)
{	

	uint8_t res;
	uint16_t i=0;
	 
	res= CAN_Transmit(CAN2, &TxMessage);	// 发送数据
	
	while((CAN_TransmitStatus(CAN2, res)==CAN_TxStatus_Failed)&&(i<0XFFF))
			i++;															//等待发送结束
	
	if(i>=0XFFF)
			return 1;
	return 0;	 
}

#endif

#if CAN1_ENABLE|CAN2_ENABLE

//	BOUND10KB = 1,
//	BOUND20KB,
//	BOUND50KB,
//	BOUND100KB,
//	BOUND200KB,
//	BOUND500KB,
//	BOUND1M,

// 同时初始化两个can并重新初始化波特率
void CAN_Init_All(CAN_BOUND bound)
{
	uint16_t time = 100;
	// 默认为20k
	switch((uint8_t)bound)
	{
		case BOUND10KB:time = 200;
			break;
		case BOUND20KB:time = 100;
			break;
		case BOUND50KB:time = 40;
			break;
		case BOUND100KB:time = 20;
			break;
		case BOUND200KB:time = 10;
			break;
		case BOUND500KB:time = 4;
			break;
		case BOUND1M:time = 2;
			break;
		default: time = 200; // 默认20K
			break;
	}
#if CAN1_ENABLE
	CAN1_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,time,CAN_Mode_Normal);
#endif
#if CAN2_ENABLE
	CAN2_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,time,CAN_Mode_Normal);
#endif
}

// 处理主机命令事件 广播帧处理
void Process_host_msg(CanRxMsg ISR_RxMessage)
{
	int i = 0,j = 0; // 用来循环，j为临时变量
	
	// 查询信息指令
	if(ISR_RxMessage.Data[0] == CMD_CAN_QUERY)
	{
		// 电源查询
		if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_POWER)
		{
			SEND_TYPE_CAN_POWER_SUPPLY_DATA(power_supply);
		}
		
		// 工作状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_DEV_STATE) 
		{
			SEND_TYPE_CAN_FLAG_DATA(canFlag);
		}
		
		// 灯具状态查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_LAMP) 
		{
				// 该种方式会影响主循环的运行
				// while(SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[ISR_RxMessage.Data[2]])); // 等待发送成功
			// 采用下次轮询发送来实现重发
			if(!SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[ISR_RxMessage.Data[2]]))
					{
						lampData_tmp[ISR_RxMessage.Data[2]].LAMP_DATA = 5;// 重发标志
					}						
					SysTickDelayMs(CAN_SEND_DELAY_MS);
		}
		
		// 批量灯具查询
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_RANGE_LAMP) 
		{
			// REVC_LAMP_SIGNAL
			i = MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3]); // 查询首地址
			j = MERGE_HL(ISR_RxMessage.Data[4],ISR_RxMessage.Data[5]); // 查询尾地址
			
			if (j > LAMP_NUM) return;// 查询长度超过灯具总数
			
			for(;i <= j;i++)
			{
					if(!SEND_TYPE_CAN_LAMP_DATA_DATA(lampData[i]))
					{
						lampData_tmp[i].LAMP_DATA = 5;// 重发标志
					}						
					SysTickDelayMs(CAN_SEND_DELAY_MS);	
			}
		}
		
		// 查询1-4路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL14) 
		{
			SEND_TYPE_CAN_LOOP_VOL_CUR_14_DATA(VolCur_1To4);
		}
		
		// 查询5-8路电压
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_VOL58)
		{
			SEND_TYPE_CAN_LOOP_VOL_CUR_58_DATA(VolCur_5To8);
		}
		
		// 查询应急时间
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_HEGTM)
		{
			SEND_TYPE_CAN_TIME_DATA(canTime);
		}
		// 查询版本信息
		else if(ISR_RxMessage.Data[1] == CMD_CAN_QUERY_HEGTM)
		{
			// 发送的数据结构
			CanTxMsg TxMessage;
			TxMessage.StdId=0x12;						// 标准标识符 
			TxMessage.ExtId=0x12;						// 设置扩展标示符 
			TxMessage.IDE=CAN_Id_Extended; 	// 扩展帧
			TxMessage.RTR=CAN_RTR_Data;			// 数据帧
			TxMessage.DLC = 0x02 ;					// 要发送的数据长度
			
			// 填充数据
			TxMessage.Data[0] = 0x00;
			TxMessage.Data[1] = 0x01;
		
			Can_Send(&TxMessage);
		}
	} 
	
	// 修改指令
	else if(ISR_RxMessage.Data[0] == CMD_CAN_EDIT)
	{
		// 改变灯具状态
		if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_LAMP) 				
		{
			// ISR_RxMessage.Data[2]; // 灯具ID高字节
			// ISR_RxMessage.Data[3]; // 灯具ID低字节
			// ISR_RxMessage.Data[4]; // 左亮，右亮，熄灭
			lampState[MERGE_HL(ISR_RxMessage.Data[2],ISR_RxMessage.Data[3])] = ISR_RxMessage.Data[4];
		}
		
		// 重启复位：应该是全部重发
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_RESET) 
		{
			FirstSend = TRUE; // 重发标志
			EpsState.bReset = TRUE;	//复位
		}
		
		// 消音
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_SILENCING) 
		{
			SysInd.bMute = ISR_RxMessage.Data[2]; //消音标志
		}
		
//应急
//#define	AUTO_EMGY		0x00		//自动应急
//#define	MANU_EMGY		0x01		//手动应急
//#define	FORC_EMGY		0x02		//强制应急
//#define	AMCK_EMGY		0X03		//自动月检应急
//#define	AYCK_EMGY		0X04		//自动年检应急
//#define	MMCK_EMGY		0x05		//手动月检应急
//#define	MYCK_EMGY   0x06		//手动年检应急
//#define	EMGY_IDLE		0x07		//设备空闲
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MANUAL) 
		{
			EpsState.bCtrlSig &= ~(1<<ISR_RxMessage.Data[2]); 
		}
		
		// 月检
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_MCHK) 
		{
			EpsState.bSelfSig &= ~(1<<ISR_RxMessage.Data[2]);
		}
		
		// 强制启动开关
		else if(ISR_RxMessage.Data[1] == CMD_CAN_EDIT_EVENT_FEMGY) 
		{
			EpsState.bSelfSig &= ~(1<<ISR_RxMessage.Data[2]);
		}
	}
	
#if 0
	// 首先确认是否是主机消息还是从机消息，从机消息应该直接转发不过此函数
	
	// 判断消息类型是查询还是修改
	
	// 对应的功能参数
	
	// 灯具查询单个，多个回复的实现
	
	// 应急状态和应急指令  广播帧
	
	//电源信息查询
	if(ISR_RxMessage.StdId == 0x12) // 查询指令
	{
		// 灯具查询
		// 应急状态查询
		// 回路电压 1-8路
		// 主电电压
		// 电池电压，电流
	}else if(ISR_RxMessage.StdId == 0x13) // 修改指令
	{
		// 更改灯具状态
		// 改变应急状态
	}	
	
	///////////////////协议处理///////////////////
	if(ISR_RxMessage.Data[0] == FRM_TYPE_BROAD)
	{
		if(ISR_RxMessage.Data[0] == CMD_QUERY_LABLE) 			ReadLable(); 							//读电子标签
		else if(ISR_RxMessage.Data[0] == CMD_WRITE_LABLE) WriteLable();							//写电子标签
		else if(ISR_RxMessage.Data[0] == CMD_SET_STAE) 		SetDevState(FALSE,TRUE);	//状态设置
		else if(ISR_RxMessage.Data[0] == CMD_PRF_SYS) 		PrinfSysDat();						//系统参数打印
	}
	else if(ISR_RxMessage.Data[0] == FRM_TYPE_DATA)
	{
		SetDevState(FALSE,FALSE);
		if(MyFrmNode.frmRx.nSlaver == MyLable.nAddress)
		{
			if(ISR_RxMessage.Data[0] == CMD_QUERY_POWER)		QueryPower(); //查询电源参数
			else if(ISR_RxMessage.Data[0] == CMD_POW_EVENT)	DoPowEvent(); //处理电源事件
		}
	}//else
#endif
}

/**
  * @brief  This function handles can1
  * @param  None
  * @retval None
	* CAN1 中断服务函数
*/

//extern uint8_t revcSendOkSignal;
//extern uint8_t LampRevcSendOkSignal; 
//extern CanRxMsg CanSendBuff;

//void CAN1_RX1_IRQHandler(void)
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  CanRxMsg ISR_RxMessage;
	CanTxMsg TxMessage;
  CAN_Receive(CAN1, 0, &ISR_RxMessage);
	
#if REVC_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC))
	{
		revcSendOkSignal = TRUE;
		return;
	}
#endif
	
#if REVC_LAMP_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC) && !memcmp(&CanSendBuff.StdId,&ISR_RxMessage.StdId,sizeof(CanSendBuff.StdId)))
	{
		LampRevcSendOkSignal = TRUE;
		return;
	}
#endif
	
	
	memcpy(&TxMessage,&ISR_RxMessage,sizeof(CanTxMsg));
#if CAN2_ENABLE
	Can2_Send_Msg(TxMessage);
#endif
	
	Process_host_msg(ISR_RxMessage); // 处理主机消息
}


/**
  * @brief  This function handles can2
  * @param  None
  * @retval None
	* CAN2 中断服务函数
*/
void CAN2_RX1_IRQHandler(void)
// void USB_LP_CAN2_RX0_IRQHandler(void)
{
  CanRxMsg ISR_RxMessage;
	CanTxMsg TxMessage;
  CAN_Receive(CAN2, 1, &ISR_RxMessage);
		
#if REVC_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC) && !memcmp(&CanSendBuff.StdId,&ISR_RxMessage.StdId,sizeof(CanSendBuff.StdId)))
	{
		revcSendOkSignal = TRUE;
		return;
	}
#endif
	
#if REVC_LAMP_SIGNAL
	// 接受到和发送信号一样的信息表示主机接受信息完成
	if(!memcmp(CanSendBuff.Data,ISR_RxMessage.Data,CanSendBuff.DLC) && !memcmp(&CanSendBuff.StdId,&ISR_RxMessage.StdId,sizeof(CanSendBuff.StdId)))
	{
		LampRevcSendOkSignal = TRUE;
		return;
	}
#endif
	
	memcpy(&TxMessage,&ISR_RxMessage,sizeof(CanTxMsg));
#if CAN2_ENABLE
	Can1_Send_Msg(TxMessage);
#endif	
	Process_host_msg(ISR_RxMessage); // 处理主机消息
}


void TIM1_UP_IRQHandler(void)
{
	static int i = 0;
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) 
		{			
			TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );
			if(i++ >= HEARTBEAT_DELAY && FirstSend!=1)
			{
				CanHeartBeatPoll();		
				i = 0;
			}
		}
}

#endif



