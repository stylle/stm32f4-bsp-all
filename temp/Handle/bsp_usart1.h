#ifndef  _USART_1_H_
#define  _USART_1_H_

#include "main.h"

/*******************************************************/
#define USART_RX_GPIO_PORT                GPIOA
#define USART_RX_GPIO_CLK                 RCC_AHB1Periph_GPIOA
#define USART_RX_PIN                      GPIO_Pin_10
#define USART_RX_AF                       GPIO_AF_USART1
#define USART_RX_SOURCE                   GPIO_PinSource10

#define USART_TX_GPIO_PORT                GPIOA
#define USART_TX_GPIO_CLK                 RCC_AHB1Periph_GPIOA
#define USART_TX_PIN                      GPIO_Pin_9
#define USART_TX_AF                       GPIO_AF_USART1
#define USART_TX_SOURCE                   GPIO_PinSource9
/************************************************************/

#define MAX_BUFF_LEN 128

void Usart1_Init(uint32_t BOUND);
void Usart_SendByte( USART_TypeDef * pUSARTx, uint8_t ch);
void Usart_SendString( USART_TypeDef * pUSARTx, char *str);

void Usart_SendHalfWord( USART_TypeDef * pUSARTx, uint16_t ch);

#endif
