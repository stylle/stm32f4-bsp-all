#ifndef __MAIN_H
#define __MAIN_H

#define STM32_Fx 4

#if STM32_Fx == 4
#include "stm32f4xx.h"
#elif STM32_fX == 1
#include "stm32f1xx.h"
#endif


//16位
#define BSWAP_16(x) \
    (uint16_t)((((uint16_t)(x) & 0x00ff) << 8) | \
               (((uint16_t)(x) & 0xff00) >> 8) )
             
//32位               
#define BSWAP_32(x) \
    (uint32_t)(
                (((uint32_t)(x) & 0xff000000) >> 24) | \
                (((uint32_t)(x) & 0x00ff0000) >> 8 ) | \
                (((uint32_t)(x) & 0x0000ff00) << 8 ) | \
                (((uint32_t)(x) & 0x000000ff) << 24)  ) 

// printf("%#x\n",BSWAP_16(0x1234)) ;  // 0x3412
// printf("%#x\n",BSWAP_32(0x12345678));  // 0x78563412

#include "string.h"


#include <stdio.h>
#include "bsp_key.h"
#include "bsp_led.h"
#include "bsp_usart1.h"

void *,mymemcpy(void *desc,const void * src,size_t size)
{ 
	unsigned char *desc1 = (unsigned char*)desc; 
	unsigned char *src1 = (unsigned char*)src; 
	if((desc == NULL) && (src == NULL))//判断desc和src是不是空的。 
	{  
		return NULL; 
	} 

	while(size > 0) 
	{  
		*desc1 = *src1;  
		desc1++;  
		src1++; 
		size--;
	} 
	return desc;
}

// 内存对比
int mymemcmp(void *desc,const void *src, size_t size)
{
	unsigned char *desc1 = (unsigned char*)desc; 
	unsigned char *src1 = (unsigned char*)src;
	
	if((desc == NULL) && (src == NULL))//判断desc和src是不是空的。 
	{  
		return 0; 
	} 
	
	while(size > 0)
	{
		if(*desc1 != *src1)
			return 0;
		desc1++;
		src1++;
		size--;
	}
	return 1;
    
}
void *memcpy1(void *desc,const void * src,size_t size)
{ 
	unsigned char *desc1 = (unsigned char*)desc; 
	unsigned char *src1 = (unsigned char*)src; 
	if((desc == NULL) && (src == NULL))//判断desc和src是不是空的。 
	{  
		return NULL; 
	} 

	while(size > 0) 
	{  
		*desc1 = *src1;  
		desc1++;  
		src1++; 
		size--;
	} 
	return desc;
}

//memset
void mymemset(void *desc,int num, size_t size)
{
	unsigned char *desc1 = (unsigned char*)desc; 

	if(desc == NULL)//判断desc和src是不是空的。 
	{  
		return; 
	} 
	
	while(size > 0)
	{
		*desc1 = (unsigned char)num;
		desc1++;
		size--;
	}
}


#endif

