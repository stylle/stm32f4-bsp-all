#include "bsp_key.h"

KeyRegister pKeyRegister = 0;

static void KeyDelay(__IO uint32_t nCount)	 //简单的延时函数
{
	for(; nCount != 0; nCount--);
}

#if STM32_Fx == 1
void Key_Init(void)
{
	
}
#elif STM32_Fx == 4
void Key_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	
	RCC_AHB1PeriphClockCmd(KEY1_GPIO_CLK|KEY2_GPIO_CLK|KEY3_GPIO_CLK,ENABLE);/*开启按键GPIO口的时钟*/
	
	GPIO_InitStructure.GPIO_Pin = KEY1_PIN; 				/*选择按键的引脚*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN; 		/*设置引脚为输入模式*/
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;/*设置引脚不上拉也不下拉*/
	
	GPIO_Init(KEY1_GPIO_PORT, &GPIO_InitStructure); /*使用上面的结构体初始化按键*/ 
	
	GPIO_InitStructure.GPIO_Pin = KEY2_PIN; 				/*选择按键的引脚*/
	GPIO_Init(KEY2_GPIO_PORT, &GPIO_InitStructure); /*使用上面的结构体初始化按键*/
	
	GPIO_InitStructure.GPIO_Pin = KEY3_PIN; 				/*选择按键的引脚*/
	GPIO_Init(KEY3_GPIO_PORT, &GPIO_InitStructure); /*使用上面的结构体初始化按键*/
}
#endif


void Key_Poll(void)
{
	int i = 0;
	if(GPIO_ReadInputDataBit(KEY1_GPIO_PORT,KEY1_PIN) == (uint32_t)Bit_RESET)  
	{	 
		KeyDelay(0xffff);
		if(GPIO_ReadInputDataBit(KEY1_GPIO_PORT,KEY1_PIN) == (uint32_t)Bit_RESET)
		{
			while(GPIO_ReadInputDataBit(KEY1_GPIO_PORT,KEY1_PIN) == (uint32_t)Bit_RESET)
			{
				i++;
				KeyDelay(0xffff);
			}
			if(i > 100 * LONG_PRESS_TIMEOUT) pKeyRegister(KEY1,KEY_LONG_PRESS);
			else pKeyRegister(KEY1,KEY_PRESS);
		}			
	}
	
	if(GPIO_ReadInputDataBit(KEY2_GPIO_PORT,KEY2_PIN) == (uint32_t)Bit_RESET)  
	{	 
		KeyDelay(0xffff);
		if(GPIO_ReadInputDataBit(KEY2_GPIO_PORT,KEY2_PIN) == (uint32_t)Bit_RESET)
		{
			while(GPIO_ReadInputDataBit(KEY2_GPIO_PORT,KEY2_PIN) == (uint32_t)Bit_RESET)
			{
				i++;
				KeyDelay(0xffff);
			}
			if(i > 100 * LONG_PRESS_TIMEOUT) pKeyRegister(KEY2,KEY_LONG_PRESS);
			else pKeyRegister(KEY2,KEY_PRESS);
		}			
	}
	
	if(GPIO_ReadInputDataBit(KEY3_GPIO_PORT,KEY3_PIN) == (uint32_t)Bit_RESET)  
	{	 
		KeyDelay(0xffff);
		if(GPIO_ReadInputDataBit(KEY3_GPIO_PORT,KEY3_PIN) == (uint32_t)Bit_RESET)
		{
			while(GPIO_ReadInputDataBit(KEY3_GPIO_PORT,KEY3_PIN) == (uint32_t)Bit_RESET)
			{
				i++;
				KeyDelay(0xffff);
			}
			if(i > 100 * LONG_PRESS_TIMEOUT) pKeyRegister(KEY3,KEY_LONG_PRESS);
			else pKeyRegister(KEY3,KEY_PRESS);
		}			
	}
	
}
void Key_Register(KeyRegister pcb)
{
	if(pKeyRegister == 0)
	{
		pKeyRegister = pcb;
	}
}

