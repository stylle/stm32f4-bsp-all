#ifndef __BSP_KEY_H
#define __BSP_KEY_H
#include "main.h"

#define LONG_PRESS_TIMEOUT 1 //按一秒表示长按

typedef enum{
	KEY1 = 0,
	KEY2,
	KEY3,
}KEYx_TYPE;

typedef enum{
	KEY_PRESS = 1,	// 短按
	KEY_LONG_PRESS,	// 长按
}KEY_STATE_TYPEDEF;


//引脚定义
/*******************************************************/
#define KEY1_PIN                  GPIO_Pin_10               
#define KEY1_GPIO_PORT            GPIOE                      
#define KEY1_GPIO_CLK             RCC_AHB1Periph_GPIOE

#define KEY2_PIN                  GPIO_Pin_11                 
#define KEY2_GPIO_PORT            GPIOE                      
#define KEY2_GPIO_CLK             RCC_AHB1Periph_GPIOE

#define KEY3_PIN                  GPIO_Pin_12                 
#define KEY3_GPIO_PORT            GPIOE                      
#define KEY3_GPIO_CLK             RCC_AHB1Periph_GPIOE
/*******************************************************/

typedef void (*KeyRegister)(KEYx_TYPE key, KEY_STATE_TYPEDEF state);

void Key_Init(void);
void Key_Poll(void);
void Key_Register(KeyRegister pcb); // 注册函数


#endif


