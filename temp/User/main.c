/***********************************************************************
文件名称：main.C
功    能：板子上的 3个LED轮流闪烁 
实验平台：基于STM32F407VCT6 开发板
库版本  ：V1.2.1 
***********************************************************************/
#include "main.h"	
	

void Key_Handle(KEYx_TYPE id,KEY_STATE_TYPEDEF state)
{
	if(id == KEY1 && state == KEY_LONG_PRESS)
	{
		LED1_ON;
	}
	if(id == KEY2 && state == KEY_PRESS)
	{
		LED1_OFF;
		printf("nihao\r\n");
	}
	if(id == KEY3 && state == KEY_PRESS)
	{
		LED3_ON;
	}
}


int main(void)
{
	/*
		系统时钟配置在system_stm32f4xx.c 文件中的SystemInit()函数中实现，在复位后直接在启动文件中运行
	*/
	Led_Init(); //LED 端口初始化 
	Key_Init();
	Usart1_Init(115200);
	
	Key_Register(Key_Handle);
	while (1)
	{
		Key_Poll();
	}
}

