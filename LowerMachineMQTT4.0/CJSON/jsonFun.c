#include "jsonFun.h"
#include "cJSON.h"
#include "TTL_usart.h"     //包含需要的头文件
#include "malloc.h"
#include "bsp_user_lib.h"
#include "main.h"

extern int g_nThisDevId;
extern int g_nDevBCCId;	
extern int g_nAskType;

extern uint16_t sDevData[4][150];
extern int nDevReply[4];

extern AskDevInfo g_stAskDevInfo[3]; //从服务器上得到的命令
extern int g_nAskDevInfoNum;				//命令个数

//旧数据，用于比较
uint16_t sOldDevData[4][150];
uint16_t g_nInf0Num = 0;

char* writeToJson( char * pJsonBuf)
{
	char *result;
	int nIndex = 0;
	int nDevIndex = 0;
	cJSON *root;
	cJSON* InfoArrayObj;
	cJSON* InfoItemObj;
	cJSON* dataInfoObj;
	char temp[10]; 
	uint16_t crc;	
	
	uint16_t nDataType = 0; //设备类型
    uint16_t nFunType = 0; //命令功能
    uint16_t nDataLen = 0; //数据长度
	
//	//添加测试数据
//	//************************************
//	for( ; nIndex < 100 ; nIndex++)
//	{
//		sDevData[ENUM_EPS][nIndex]	= nIndex;
//		sDevData[ENUM_PLC][nIndex]	= nIndex + 1;
//		sDevData[ENUM_BMS][nIndex]	= nIndex + 2;
//	}
//	nIndex = 0;
//	//************************************
	
	root=cJSON_CreateObject();	
	if(root)
	{
		cJSON_AddNumberToObject(root,"DevBCCId",g_nThisDevId);
		cJSON_AddNumberToObject(root,"AskType", g_nAskType);
		cJSON_AddNumberToObject(root,"InfoNum" , g_nInf0Num++);
			
		InfoArrayObj = cJSON_CreateArray();
		//循环每个设备的需求信息
		for( ; nDevIndex < g_nAskDevInfoNum ; nDevIndex++)
		{
			nDataType = g_stAskDevInfo[nDevIndex].nDataType;
			nFunType = g_stAskDevInfo[nDevIndex].nFunType;
			nDataLen = g_stAskDevInfo[nDevIndex].nDataLen;
			//查看设备的请求是不是数据
			if((nFunType == 1) || (nFunType == 2))
			{
				//如是是请求更新数据
				if(nFunType == 1)
				{	
					InfoItemObj = cJSON_CreateObject();	
					
					cJSON_AddNumberToObject(InfoItemObj,"DataType",nDataType);
					cJSON_AddNumberToObject(InfoItemObj,"isReply" , nDevReply[nDataType]);
					cJSON_AddNumberToObject(InfoItemObj,"DataLen" , nDataLen);
					
					dataInfoObj = cJSON_CreateObject();	
					//除以二是因为上传的是short数据
					for( ; nIndex < nDataLen/2 ; nIndex++)
					{
						if(sDevData[nDataType][nIndex] != sOldDevData[nDataType][nIndex])
						{			
							sOldDevData[nDataType][nIndex] = sDevData[nDataType][nIndex];
							sprintf(temp,"%d",nIndex);	
							cJSON_AddNumberToObject(dataInfoObj,temp,sOldDevData[nDataType][nIndex]);				
						}	
					}
					
					memset(sOldDevData[nDataType] , 0 , 150);
					memcpy(sOldDevData[nDataType] , sDevData[nDataType], nDataLen);
					
					crc = CRC16_Modbus((uint8_t *)(sDevData[nDataType]), nDataLen);
					cJSON_AddNumberToObject(InfoItemObj , "CRCData",crc);
					
					cJSON_AddItemToObject(InfoItemObj,"Data" , dataInfoObj);
//					cJSON_Delete(dataInfoObj);
//					dataInfoObj = NULL;
					
					cJSON_AddItemToArray(InfoArrayObj , InfoItemObj);
//					cJSON_Delete(InfoItemObj);
//					InfoItemObj = NULL;
														
					nIndex = 0;
					nDataType = 0;
					nFunType = 0;
					nDataLen = 0;
					crc = 0;
				}
				//如果是请求全部数据
				else if(nFunType == 2)
				{
					InfoItemObj = cJSON_CreateObject();	
					
					cJSON_AddNumberToObject(InfoItemObj,"DataType",nDataType);
					cJSON_AddNumberToObject(InfoItemObj,"isReply" , nDevReply[nDataType]);
					cJSON_AddNumberToObject(InfoItemObj,"DataLen" , nDataLen);
					
					dataInfoObj = cJSON_CreateObject();	
					for( ; nIndex < nDataLen/2 ; nIndex++)
					//这里只测试上传10个数据
					//for( ; nIndex < 10/2 ; nIndex++)
					{												
						sOldDevData[nDataType][nIndex] = sDevData[nDataType][nIndex];
						sprintf(temp,"%d",nIndex);	
						cJSON_AddNumberToObject(dataInfoObj,temp,sOldDevData[nDataType][nIndex]);						
					}	

					memset(sOldDevData[nDataType] , 0 , 150);
					memcpy(sOldDevData[nDataType] , sDevData[nDataType], nDataLen);
					
					crc = CRC16_Modbus((uint8_t *)(sDevData[nDataType]), nDataLen);
					cJSON_AddNumberToObject(InfoItemObj , "CRCData",crc);
					
					cJSON_AddItemToObject(InfoItemObj,"Data" , dataInfoObj);
//					cJSON_Delete(dataInfoObj);
//					dataInfoObj = NULL;
					
					cJSON_AddItemToArray(InfoArrayObj , InfoItemObj);
//					cJSON_Delete(InfoItemObj);
//					InfoItemObj = NULL;
										
					nIndex = 0;
					nDataType = 0;
					nFunType = 0;
					nDataLen = 0;	
					crc = 0;					
				}								
			}						
		}
		
		cJSON_AddItemToObject(root , "AllData",InfoArrayObj);
//		cJSON_Delete(InfoArrayObj);
//		InfoArrayObj = NULL;
		
		result=cJSON_PrintUnformatted(root);
		strcpy(pJsonBuf,result);
		cJSON_Delete(root);
		myfree(result);
	}
	else
	{
		ttl_printf("生产JSON失败\r\n");	
	}
	
	memset(g_stAskDevInfo,0,3 * sizeof(AskDevInfo));
	
    return pJsonBuf;
}

void GetJsonInfo(char * pData)
{  
    cJSON *root;
	cJSON *DataListJson = NULL;
	cJSON *DataListItemJson = NULL;
	int nArrayLen = 0;
	int nIndex = 0;
	
	ttl_printf("GetJsonInfo\n");
	root = cJSON_Parse(pData);
    if(!root) 
    {
       ttl_printf("没有JSON内容\r\n");
    } 
    else
    {
        g_nDevBCCId = cJSON_GetObjectItem(root, "DevBCCId")->valueint;
		g_nAskType = cJSON_GetObjectItem(root, "AskType")->valueint;		
		
		DataListJson = cJSON_GetObjectItem(root,"TaskList");
		if(DataListJson->type == cJSON_Array)
		{
			nArrayLen = cJSON_GetArraySize(DataListJson);
			g_nAskDevInfoNum = nArrayLen;
			for( ; nIndex < nArrayLen ; nIndex++)
			{
				DataListItemJson = cJSON_GetArrayItem(DataListJson, nIndex);

				g_stAskDevInfo[nIndex].nDataType = cJSON_GetObjectItem(DataListItemJson, "DataType")->valueint;
				g_stAskDevInfo[nIndex].nFunType = cJSON_GetObjectItem(DataListItemJson, "FunType")->valueint;
				g_stAskDevInfo[nIndex].nDataLen = cJSON_GetObjectItem(DataListItemJson, "DataLen")->valueint;
				
				ttl_printf("jSON内容 nIndex: %d\r\n" , nIndex);		
				ttl_printf("jSON内容 DataType: %d\r\n" , g_stAskDevInfo[nIndex].nDataType);	
				ttl_printf("jSON内容 FunType: %d\r\n" , g_stAskDevInfo[nIndex].nFunType);	
				ttl_printf("jSON内容 DataLen: %d\r\n" , g_stAskDevInfo[nIndex].nDataLen);	
				
				cJSON_Delete(DataListItemJson);
				DataListItemJson = NULL;
			}
		}
		cJSON_Delete(DataListJson);
		DataListJson = NULL;
		
		cJSON_Delete(root);	
		root =	NULL;		
	}
 }

 //{"DevBCCId": 0,"TaskList":[{"DataType":1,"FunType":2,"DataLen":28},{"DataType":2,"FunType":2,"DataLen":30}]}