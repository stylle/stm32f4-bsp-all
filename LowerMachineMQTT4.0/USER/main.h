/*-----------------------------------------------------*/
/*              超纬电子STM32系列开发板                */
/*-----------------------------------------------------*/
/*                     程序结构                        */
/*-----------------------------------------------------*/
/*USER     ：包含程序的main函数，是整个程序的入口      */
/*HARDWARE ：包含开发板各种功能外设的驱动程序          */
/*CORE     ：包含STM32的核心程序，官方提供，我们不修改 */
/*STLIB    ：官方提供的库文件，我们不修改              */
/*-----------------------------------------------------*/
/*                                                     */
/*           程序main函数，入口函数头文件              */
/*                                                     */
/*-----------------------------------------------------*/

#ifndef __MAIN_H
#define __MAIN_H

#include "stdio.h"
#include "string.h"
#include "stdint.h"

typedef struct
{
	uint8_t Rxlen;
	char RxBuf[128];
	uint8_t Txlen;
	char TxBuf[128];
}PRINT_MODS_T;

//逆变数据
typedef struct 
{
    uint16_t nState;          //状态码      1:0001  //这个状态码就是flags的信息
    uint16_t nElectricityA_V; //主电A相电压   220:00DC
    uint16_t nElectricityB_V; //主电B相电压   220:00DC
    uint16_t nElectricityC_V; //主电C相电压   220:00DC
    uint16_t nOutputA_V;      //输出A相电压   220:00DC
    uint16_t nOutputB_V;      //输出B相电压   220:00DC
    uint16_t nOutputC_V;      //输出C相电压   220:00DC
    uint16_t nOutputA_A;      //输出电流A相   10:000A
    uint16_t nOutputB_A;      //输出电流B相   10:000A
    uint16_t nOutputC_A;      //输出电流C相   10:000A
    uint16_t nBatteryV;       //电池电压      24:0018
    uint16_t nChargeA;        //充电电流      10:000A
    uint16_t nEpsVersion;     //EPS软件版本   1:0001
    uint16_t nLcdVersion;     //液晶软件版本   1:0001
    uint16_t nCheckBatteryV[50];    //巡检电池数量 暂时不用

}EpsInfo;

typedef struct 
{
    uint16_t nD800;    
    uint16_t nD801;   
    uint16_t nD802;   
    uint16_t nD803; 
    uint16_t nD804;        
    uint16_t nD805;        
    uint16_t nD806;
    uint16_t nD807;
    uint16_t nD808;
    uint16_t nD809;
    uint16_t nD810;
    uint16_t nD811;
    uint16_t nD812;
    uint16_t nD813;
    uint16_t nD814;
    uint16_t nD815;
    uint16_t nD816;
    uint16_t nD817;
    uint16_t nD818;
    uint16_t nD819;
    uint16_t nD820;
    uint16_t nD821;
    uint16_t nD822;
    uint16_t nD823;
    uint16_t nD824;
    uint16_t nD825;
    uint16_t nD826;
    uint16_t nD827;
}PlcMsgInfo;

typedef struct 
{
    uint16_t nMODS_state; //对应bms_state_t
    uint16_t nSystemWarningH; //警告状态码高字节
    uint16_t nSystemWarningL; //警告状态码高字节
    uint16_t nBatteryParameterV;        //电池包总压
    uint16_t nCellsAmount;              // bms电池数量
    uint16_t nBatteryParameterA;        //bms电流
    uint16_t nBatteryParameterSoc;      //SOC
    uint16_t nBatteryParameterAh;       //剩余电量Ah
    uint16_t nBatteryParameterSOH;      //SOH
    uint16_t BatteryParameterVersion;   //软件版本
    uint8_t BatteryParameterCellTemp[16]; //电池温度
    uint16_t BatteryParameterCellRes[32]; //电池电压
}BmsMsgInfo;

typedef enum 
{
	ENUM_EPS = 1,	
	ENUM_PLC = 2,	
	ENUM_BMS = 3
}ENUM_DEV_TYPE;

//获取电气设备信息的需求
typedef struct 
{
    uint16_t nDataType; //设备类型
    uint16_t nFunType; //命令功能
    uint16_t nDataLen; //数据长度
}AskDevInfo;

extern PRINT_MODS_T g_tPrint;

extern unsigned int SystemTimer; //变量声明

void Data_State(void);           //函数声明
void pollDevFun(void);
void updataInfo();			//更新部分数据

#endif











