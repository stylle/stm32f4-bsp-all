/*-----------------------------------------------------*/
/*              超纬电子STM32系列开发板                */
/*-----------------------------------------------------*/
/*                     程序结构                        */
/*-----------------------------------------------------*/
/*USER     ：包含程序的main函数，是整个程序的入口      */
/*HARDWARE ：包含开发板各种功能外设的驱动程序          */
/*CORE     ：包含STM32的核心程序，官方提供，我们不修改 */
/*STLIB    ：官方提供的库文件，我们不修改              */
/*HMAC     ：各种加密算法                              */
/*-----------------------------------------------------*/
/*                                                     */
/*           程序main函数，入口函数源文件              */
/*                                                     */
/*-----------------------------------------------------*/

#include "stm32f10x.h"  //包含需要的头文件
#include "main.h"       //包含需要的头文件
#include "TTL_usart.h"     //包含需要的头文件
#include "8266_usart.h"     //包含需要的头文件
#include "timer2.h"     //包含需要的头文件
#include "timer3.h"     //包含需要的头文件
#include "wifi.h"	    //包含需要的头文件
#include "led.h"        //包含需要的头文件
#include "mqtt.h"       //包含需要的头文件
#include "bsp.h"
#include "iwdg.h"       //包含需要的头文件
#include "modbus_host.h"
#include "cJSON.h"
#include "jsonFun.h"
#include "malloc.h"

static void DispMenu(void);
static uint16_t BigSmallChange(uint16_t nData);

unsigned int SystemTimer = 0;   //用于全局计时的变量           单位秒
unsigned int DataSendTimer = 0; //用于数据发送的计时的变量   单位秒
unsigned int ADCTimer = 0;      //用于ADC采集的计时的变量      单位秒

int g_nThisDevId = 1; 	//本设备ID
 
int g_nDevBCCId = -1;	//此数据来源时从服务器上获取
int g_nAskType = -1;
AskDevInfo g_stAskDevInfo[3] = {0}; //从服务器上得到的命令
int g_nAskDevInfoNum = 0;				//命令个数

//所有的电气设备
uint16_t sDevData[4][150];
//所有电气设备数据长度
int nDevDataLen[3] = {14 , 26 , 54};
//设备数据的长度
char sDevDataLenght[3];
int nDevReply[4] = {0 , 0 , 0 ,0};

PRINT_MODS_T g_tPrint;

int main(void) 
{	
	MSG_T ucMsg;					/* 消息代码 用于打印*/
	IWDG_Init(7,625);               //独立看门狗 时间2s 
	bsp_Init();						//初始化485
	bsp_LedOn(1);
	mem_init();
	Usart3_Init(9600);              //初始化打印串口
	WiFi_ResetIO_Init();            //初始化WiFi的复位IO
	AliIoT_Parameter_Init();	    //初始化连接阿里云IoT平台MQTT服务器的参数			
	
	while(1)                        //主循环
	{		
		//esp8266 mqtt
		/*--------------------------------------------------------------------*/
		/*   Connect_flag=1同服务器建立了连接,我们可以发布数据和接收推送了    */
		/*--------------------------------------------------------------------*/
		if(Connect_flag==1)
		{     
			/*-------------------------------------------------------------*/
			/*                     处理发送缓冲区数据                      */
			/*-------------------------------------------------------------*/
			if(MQTT_TxDataOutPtr != MQTT_TxDataInPtr)
			{                    //if成立的话，说明发送缓冲区有数据了
				//3种情况可进入if
				//第1种：0x10 连接报文
				//第2种：0x82 订阅报文，且ConnectPack_flag置位，表示连接报文成功
				//第3种：SubcribePack_flag置位，说明连接和订阅均成功，其他报文可发
				if((MQTT_TxDataOutPtr[2]==0x10)||((MQTT_TxDataOutPtr[2]==0x82)&&(ConnectPack_flag==1))||(SubcribePack_flag==1))
				{    
					ttl_printf("发送数据:0x%x\r\n",MQTT_TxDataOutPtr[2]);  //串口提示信息
					
					MQTT_TxData(MQTT_TxDataOutPtr);                       //发送数据
					MQTT_TxDataOutPtr += TBUFF_UNIT;                      //指针下移
					if(MQTT_TxDataOutPtr==MQTT_TxDataEndPtr)              //如果指针到缓冲区尾部了
					{
						MQTT_TxDataOutPtr = MQTT_TxDataBuf[0];            //指针归位到缓冲区开头
					}					
				} 				
			}//处理发送缓冲区数据的else if分支结尾
			/*-------------------------------------------------------------*/
			/*                     处理接收缓冲区数据                      */
			/*-------------------------------------------------------------*/
			if(MQTT_RxDataOutPtr != MQTT_RxDataInPtr)
			{                //if成立的话，说明接收缓冲区有数据了														
				ttl_printf("接收到数据:");                             //串口提示信息
				/*-----------------------------------------------------*/
				/*                    处理CONNACK报文                  */
				/*-----------------------------------------------------*/				
				//if判断，如果第一个字节是0x20，表示收到的是CONNACK报文
				//接着我们要判断第4个字节，看看CONNECT报文是否成功
				if(MQTT_RxDataOutPtr[2]==0x20)
				{             			
				    switch(MQTT_RxDataOutPtr[5])
					{					
						case 0x00 : ttl_printf("CONNECT报文成功\r\n");                            //串口输出信息	
								    ConnectPack_flag = 1;                                        //CONNECT报文成功
									break;                                                       //跳出分支case 0x00                                              
						case 0x01 : ttl_printf("连接已拒绝，不支持的协议版本，准备重启\r\n");  //串口输出信息
									Connect_flag = 0;                                            //Connect_flag置零，重启连接
									break;                                                       //跳出分支case 0x01   
						case 0x02 : ttl_printf("连接已拒绝，不合格的客户端标识符，准备重启\r\n"); //串口输出信息
									Connect_flag = 0;                                            //Connect_flag置零，重启连接
									break;                                                       //跳出分支case 0x02 
						case 0x03 : ttl_printf("连接已拒绝，服务端不可用，准备重启\r\n");         //串口输出信息
									Connect_flag = 0;                                            //Connect_flag置零，重启连接
									break;                                                       //跳出分支case 0x03
						case 0x04 : ttl_printf("连接已拒绝，无效的用户名或密码，准备重启\r\n");   //串口输出信息
									Connect_flag = 0;                                            //Connect_flag置零，重启连接						
									break;                                                       //跳出分支case 0x04
						case 0x05 : ttl_printf("连接已拒绝，未授权，准备重启\r\n");               //串口输出信息
									Connect_flag = 0;                                            //Connect_flag置零，重启连接						
									break;                                                       //跳出分支case 0x05 		
						default   : ttl_printf("连接已拒绝，未知状态，准备重启\r\n");             //串口输出信息 
									Connect_flag = 0;                                            //Connect_flag置零，重启连接					
									break;                                                       //跳出分支case default 								
					}				
				}
				/*-----------------------------------------------------*/
				/*                    处理SUBACK报文                   */
				/*-----------------------------------------------------*/				
				//if判断，第一个字节是0x90，表示收到的是SUBACK报文
				//接着我们要判断订阅回复，看看是不是成功
				else if(MQTT_RxDataOutPtr[2]==0x90)
				{ 
						switch(MQTT_RxDataOutPtr[6])
						{					
						case 0x00 :
						case 0x01 : ttl_printf("订阅成功\r\n");            //串口输出信息
							        SubcribePack_flag = 1;                //SubcribePack_flag置1，表示订阅报文成功送
									break;                                //跳出分支                                             
						default   : ttl_printf("订阅失败，准备重启\r\n");  //串口输出信息 
									Connect_flag = 0;                     //Connect_flag置零，重启连接
									break;                                //跳出分支 								
					}					
				}
				/*-----------------------------------------------------*/
				/*                  处理PINGRESP报文                   */
				/*-----------------------------------------------------*/
				//if判断，第一个字节是0xD0，表示收到的是PINGRESP报文
				else if(MQTT_RxDataOutPtr[2]==0xD0)
				{ 
					ttl_printf("PING报文回复\r\n"); 		  //串口输出信息 
					if(Ping_flag==1)
					{                     //如果Ping_flag=1，表示第一次发送
						 Ping_flag = 0;    				  //要清除Ping_flag标志
					}
					else if(Ping_flag>1)
					{ 				  //如果Ping_flag>1，表示是多次发送了，而且是2s间隔的快速发送
						Ping_flag = 0;     				  //要清除Ping_flag标志
						TIM3_ENABLE_30S(); 				  //PING定时器重回30s的时间
					}				
				}
				/*-----------------------------------------------------*/
				/*                  处理数据推送报文                   */
				/*-----------------------------------------------------*/				
				//if判断，如果第一个字节是0x30，表示收到的是服务器发来的推送数据
				//我们要提取控制命令
				else if((MQTT_RxDataOutPtr[2]==0x30))
				{ 
					ttl_printf("服务器等级0推送\r\n"); 		   //串口输出信息 
					MQTT_DealPushdata_Qs0(MQTT_RxDataOutPtr);  //处理等级0推送数据
				}				
				
				MQTT_RxDataOutPtr +=RBUFF_UNIT;                     //接收指针下移
				if(MQTT_RxDataOutPtr==MQTT_RxDataEndPtr)            //如果接收指针到接收缓冲区尾部了
				{
					MQTT_RxDataOutPtr = MQTT_RxDataBuf[0];          //接收指针归位到接收缓冲区开头  
				}					
			}
			
			/*-------------------------------------------------------------*/
			/*                     处理命令缓冲区数据                      */
			/*-------------------------------------------------------------*/
			//if成立的话，说明命令缓冲区有数据了
			if(MQTT_CMDOutPtr != MQTT_CMDInPtr)
			{                             			       
				ttl_printf("接收MQTT数据:%s\r\n",&MQTT_CMDOutPtr[2]);                 //串口输出信息
				//这里对数据做处理，根据定义的值，做出不同的反应
				GetJsonInfo(&MQTT_CMDOutPtr[2]);				
				
				MQTT_CMDOutPtr += CBUFF_UNIT;                             	 //指针下移
				if(MQTT_CMDOutPtr==MQTT_CMDEndPtr)
				{					
					//如果指针到缓冲区尾部了
					//指针归位到缓冲区开头
					MQTT_CMDOutPtr = MQTT_CMDBuf[0];          	             	
				}					
			}//处理命令缓冲区数据的else if分支结尾	
			
			/*-------------------------------------------------------------*/
			/*                     处理定时上传任务                        */
			/*-------------------------------------------------------------*/
			Data_State();         //定时上传各种数据的任务
			ttl_printf("Data_State end\r\n");
			bsp_DelayMS(1);          //延时1ms，必须有，里面有喂狗，不然看门狗会溢出，造成复位				
		}	
		/*--------------------------------------------------------------------*/
		/*         Connect_flag=0同服务器断开了连接,我们要连接服务器          */
		/*--------------------------------------------------------------------*/
		else
		{ 
			ttl_printf("准备连接服务器\r\n");                  //串口输出信息
			Usart1_Init(115200);                             //串口1功能初始化，波特率115200	
			TIM_Cmd(TIM4,DISABLE);                           //关闭TIM4 
			TIM_Cmd(TIM3,DISABLE);                           //关闭TIM3  
			TIM_Cmd(TIM2,DISABLE);                           //关闭TIM2    
			WiFi_RxCounter=0;                                //WiFi接收数据量变量清零                        
			memset(WiFi_RX_BUF,0,WiFi_RXBUFF_SIZE);          //清空WiFi接收缓冲区 
			if(WiFi_Connect_IoTServer()==0)
			{   
				//如果WiFi连接云服务器函数返回0，表示正确，进入if
				ttl_printf("连接服务器成功\r\n");              //串口输出信息
				Usart1_IDELInit(115200);                     //串口1 开启DMA 和 空闲中断
				Connect_flag = 1;                            //Connect_flag置1，表示连接成功	
				SystemTimer = 0;                             //全局计时时间变量清0
				DataSendTimer = 0;                           //温湿度计时时间变量清0
				ADCTimer = 0;                                //ADC计时时间变量清0
				WiFi_RxCounter=0;                            //WiFi接收数据量变量清零                        
				memset(WiFi_RX_BUF,0,WiFi_RXBUFF_SIZE);      //清空WiFi接收缓冲区 
				
				//初始化，里面包含CONNECT报文
				MQTT_Buff_Init();                            //初始化发送接收命令缓冲区 
				TIM3_ENABLE_30S();                           //启动定时器3 30s的PING保活定时器
				TIM2_ENABLE_1S();                            //启动定时器2 1s的定时器
				
				bsp_LedOn(2);
			}
			else
			{
				bsp_LedOff(2);
			}
		}
		
		bsp_LedOn(6);	
		//485 modbus
		bsp_Idle();					/* 这个函数在bsp.c文件。用户可以修改这个函数实现CPU休眠和喂狗 */				
		pollDevFun();		
		
		if (bsp_GetMsg(&ucMsg))		/* 读取消息代码 */
		{
			switch (ucMsg.MsgCode)
			{
				case MSG_MODS:		
					DispMenu();		/* 打印实验结果 */
					break;
				
				default:
					break;
			}
		}
		bsp_LedOff(6);
		
		bsp_DelayMS(100);
	}
}

/*-------------------------------------------------*/
/*函数名：定时上传各种数据的任务                   */
/*参  数：无                                       */
/*返回值：无                                       */
/*-------------------------------------------------*/
void Data_State(void)
{				
	if((g_nDevBCCId == 0) || (g_nDevBCCId == g_nThisDevId))
	{
		ttl_printf("updataInfo\r\n");
		updataInfo();

		g_nDevBCCId = -1;	
		g_nAskType = -1;
	}			
}

//更新部分数据
void updataInfo(int nDataType , int nDataLen)
{
	char temp[1024];             //缓冲区	
	memset(temp , 0 , 1024);
	
	writeToJson(temp);	
	ttl_printf("updataInfo 发送数据长度temp:%d\r\n",strlen(temp));
	if(strlen(temp) > 536)
	{
		//这里通过测试，数据大于536，容易导致模块崩溃，原因未知
		return;
	}
	MQTT_PublishQs0(P_TOPIC_NAME,temp,strlen(temp));                  //添加数据到发送缓冲区	
		
	ttl_printf("updataInfo 发送数据:%s\r\n",temp);			
}

u8 nDevIndex = 0; //对设备的轮询
u8 isRecvDev = 0;
extern uint8_t SlaveAddr;

void pollDevFun(void)
{	
	isRecvDev = 0;
	if(nDevIndex == 0)
	{
		bsp_PutMsg(MSG_MODS, 0); 		
		SlaveAddr = 0x01; //从机地址
		//向从机读取数据
		isRecvDev = MODH_ReadParam_03H(REG_P01, nDevDataLen[0]);
		nDevIndex = 1;
		
		if(isRecvDev == 0)
		{
			bsp_LedOff(3);
			ttl_printf("\r\n逆变器通信超时\r\n");
			nDevReply[ENUM_EPS] = 0;
		}
		else
		{
			bsp_LedOn(3);
			nDevReply[ENUM_EPS] = 1;
			memset(sDevData[ENUM_EPS] , 0 , 150);
			
			memcpy(sDevData[ENUM_EPS] , g_tModH.RxBuf + 3, g_tModH.RxBuf[2]);
			
		}	
	}
	else if(nDevIndex == 1)
	{
		bsp_PutMsg(MSG_MODS, 0); 		
		SlaveAddr = 0x10; //从机地址
		//向从机读取数据		
		isRecvDev = MODH_ReadParam_03H(REG_P02, nDevDataLen[1]);
		nDevIndex = 2;
		
		if(isRecvDev == 0)
		{
			bsp_LedOff(4);
			ttl_printf("\r\nPLC通信超时\r\n");
			nDevReply[ENUM_PLC] = 0;
		}
		else
		{
			bsp_LedOn(4);
			nDevReply[ENUM_PLC] = 1;
			memset(sDevData[ENUM_PLC] , 0 , 150);
			
			memcpy(sDevData[ENUM_PLC] , g_tModH.RxBuf + 3, g_tModH.RxBuf[2]);
		}
	}
	else if(nDevIndex == 2)
	{
		bsp_PutMsg(MSG_MODS, 0); 		
		SlaveAddr = 0x20; //从机地址
		//向从机读取数据
		isRecvDev = MODH_ReadParam_03H(REG_P03, nDevDataLen[2]);
		nDevIndex = 0;
		
		if(isRecvDev == 0)
		{
			bsp_LedOff(5);
			ttl_printf("\r\nBMS通信超时\r\n");
			nDevReply[ENUM_BMS] = 0;
		}
		else
		{
			bsp_LedOn(5);
			nDevReply[ENUM_BMS] = 1;
			memset(sDevData[ENUM_BMS] , 0 , 150);
			
			memcpy(sDevData[ENUM_BMS] , g_tModH.RxBuf + 3, g_tModH.RxBuf[2]);
		}
	}
	else
	{
		nDevIndex = 0;
	}			
}

/*
*********************************************************************************************************
*	函 数 名: DispMenu
*	功能说明: 打印例程结果
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
static void DispMenu(void)
{	
	uint8_t i;
	
	ttl_printf("\r\n"); 
	
	ttl_printf("发送的命令 : 0x");				/* 打印发送命令 */
	for (i = 0; i < g_tPrint.Txlen; i++)
	{
		ttl_printf(" %02X", g_tPrint.TxBuf[i]);
	}
	//打印之后清零
	g_tPrint.Txlen = 0;
	memset(g_tPrint.TxBuf , 0 , 128);

	ttl_printf("\r\n");
	
	ttl_printf("接收的命令 : 0x");				/* 打印接收命令 */
	for (i = 0; i < g_tPrint.Rxlen; i++)
	{
		ttl_printf(" %02X", g_tPrint.RxBuf[i]);
	}
	
	g_tPrint.Rxlen = 0;
	memset(g_tPrint.RxBuf , 0 , 128);
	
	ttl_printf("\r\n");
}

uint16_t BigSmallChange(uint16_t nData)
{
	uint16_t nTemp1 = nData & 0x00ff;
	uint16_t nTemp2 = nData & 0xff00;
	uint16_t nTemp3 = (nTemp1 << 8) | (nTemp2 >> 8);
	return nTemp3;
}