/*-------------------------------------------------*/
/*            超纬电子STM32系列开发板              */
/*-------------------------------------------------*/
/*                                                 */
/*              实现LED功能的源文件                */
/*                                                 */
/*-------------------------------------------------*/

#include "stm32f10x.h"  //包含需要的头文件
#include "led.h"        //包含需要的头文件

/* 按键口对应的RCC时钟 */
#define RCC_ALL_LED 	(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOF|RCC_APB2Periph_GPIOG)

#define GPIO_PORT_LED1  GPIOD
#define GPIO_PIN_LED1	GPIO_Pin_0

#define GPIO_PORT_LED2  GPIOD
#define GPIO_PIN_LED2	GPIO_Pin_1

#define GPIO_PORT_LED3  GPIOD
#define GPIO_PIN_LED3	GPIO_Pin_5

#define GPIO_PORT_LED4  GPIOG
#define GPIO_PIN_LED4	GPIO_Pin_11

#define GPIO_PORT_LED5  GPIOG
#define GPIO_PIN_LED5	GPIO_Pin_15

#define GPIO_PORT_LED6  GPIOB
#define GPIO_PIN_LED6	GPIO_Pin_9

/*
*********************************************************************************************************
*	函 数 名: bsp_InitLed
*	功能说明: 配置LED指示灯相关的GPIO,  该函数被 bsp_Init() 调用。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitLed(void)
{    	 
	GPIO_InitTypeDef GPIO_InitStructure;

	/* 打开GPIO时钟 */
	RCC_APB2PeriphClockCmd(RCC_ALL_LED, ENABLE);
	/*
		配置所有的LED指示灯GPIO为推挽输出模式
		由于将GPIO设置为输出时，GPIO输出寄存器的值缺省是0，因此会驱动LED点亮.
		这是我不希望的，因此在改变GPIO为输出前，先关闭LED指示灯
	*/
	bsp_LedOff(1);
	bsp_LedOff(2);
	bsp_LedOff(3);
	bsp_LedOff(4);
	bsp_LedOff(5);
	bsp_LedOff(6);

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	/* 推挽输出模式 */
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED1;
	GPIO_Init(GPIO_PORT_LED1, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED2;
	GPIO_Init(GPIO_PORT_LED2, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED3;
	GPIO_Init(GPIO_PORT_LED3, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED4;
	GPIO_Init(GPIO_PORT_LED4, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED5;
	GPIO_Init(GPIO_PORT_LED5, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_LED6;
	GPIO_Init(GPIO_PORT_LED6, &GPIO_InitStructure);                                            //所有LED熄灭
}
//蜂鸣器初始化
void bsp_BeepInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);	 //使能GPIOE端口时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;				 //BEEP-->PE0 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	 //速度为50MHz
	GPIO_Init(GPIOE, &GPIO_InitStructure);	 //根据参数初始化GPIOE0

	GPIO_ResetBits(GPIOE,GPIO_Pin_0);//输出0，关闭蜂鸣器输出
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedOn
*	功能说明: 点亮指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_LedOn(uint8_t _no)
{
	if (_no == 1)
	{
		GPIO_PORT_LED1->BRR = GPIO_PIN_LED1;
	}
	else if (_no == 2)
	{
		GPIO_PORT_LED2->BRR = GPIO_PIN_LED2;
	}
	else if (_no == 3)
	{
		GPIO_PORT_LED3->BRR = GPIO_PIN_LED3;
	}
	else if (_no == 4)
	{
		GPIO_PORT_LED4->BRR = GPIO_PIN_LED4;
	}
	else if (_no == 5)
	{
		GPIO_PORT_LED5->BRR = GPIO_PIN_LED5;
	}
	else if (_no == 6)
	{
		GPIO_PORT_LED6->BRR = GPIO_PIN_LED6;
	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedOff
*	功能说明: 熄灭指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_LedOff(uint8_t _no)
{
	if (_no == 1)
	{
		GPIO_PORT_LED1->BSRR = GPIO_PIN_LED1;
	}
	else if (_no == 2)
	{
		GPIO_PORT_LED2->BSRR = GPIO_PIN_LED2;
	}
	else if (_no == 3)
	{
		GPIO_PORT_LED3->BSRR = GPIO_PIN_LED3;
	}
	else if (_no == 4)
	{
		GPIO_PORT_LED4->BSRR = GPIO_PIN_LED4;
	}
	else if (_no == 5)
	{
		GPIO_PORT_LED5->BSRR = GPIO_PIN_LED5;
	}
	else if (_no == 6)
	{
		GPIO_PORT_LED6->BSRR = GPIO_PIN_LED6;
	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedToggle
*	功能说明: 翻转指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 按键代码
*********************************************************************************************************
*/
void bsp_LedToggle(uint8_t _no)
{
//	if (_no == 1)
//	{
//		GPIO_PORT_LED1->ODR ^= GPIO_PIN_LED1;
//	}
//	else if (_no == 2)
//	{
//		GPIO_PORT_LED2->ODR ^= GPIO_PIN_LED2;
//	}
//	else if (_no == 3)
//	{
//		GPIO_PORT_LED3->ODR ^= GPIO_PIN_LED3;
//	}
//	else if (_no == 4)
//	{
//		GPIO_PORT_LED4->ODR ^= GPIO_PIN_LED4;
//	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_IsLedOn
*	功能说明: 判断LED指示灯是否已经点亮。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 1表示已经点亮，0表示未点亮
*********************************************************************************************************
*/
uint8_t bsp_IsLedOn(uint8_t _no)
{
//	if (_no == 1)
//	{
//		if ((GPIO_PORT_LED1->ODR & GPIO_PIN_LED1) == 0)
//		{
//			return 1;
//		}
//		return 0;
//	}
//	else if (_no == 2)
//	{
//		if ((GPIO_PORT_LED2->ODR & GPIO_PIN_LED2) == 0)
//		{
//			return 1;
//		}
//		return 0;
//	}
//	else if (_no == 3)
//	{
//		if ((GPIO_PORT_LED3->ODR & GPIO_PIN_LED3) == 0)
//		{
//			return 1;
//		}
//		return 0;
//	}
//	else if (_no == 4)
//	{
//		if ((GPIO_PORT_LED4->ODR & GPIO_PIN_LED4) == 0)
//		{
//			return 1;
//		}
//		return 0;
//	}

	return 0;
}



