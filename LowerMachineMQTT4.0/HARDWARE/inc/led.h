/*-------------------------------------------------*/
/*            超纬电子STM32系列开发板              */
/*-------------------------------------------------*/
/*                                                 */
/*              实现LED功能的头文件                */
/*                                                 */
/*-------------------------------------------------*/

#ifndef __LED_H
#define __LED_H

#include "stdint.h"

/* 供外部调用的函数声明 */
void bsp_InitLed(void);
void bsp_BeepInit(void);
void bsp_LedOn(uint8_t _no);
void bsp_LedOff(uint8_t _no);
void bsp_LedToggle(uint8_t _no);

uint8_t bsp_IsLedOn(uint8_t _no);

#endif
