/*-------------------------------------------------*/
/*            超纬电子STM32系列开发板              */
/*-------------------------------------------------*/
/*                                                 */
/*              操作Wifi功能的头文件               */
/*                                                 */
/*-------------------------------------------------*/

#ifndef __WIFI_H
#define __WIFI_H

#include "8266_usart.h"	    //包含需要的头文件

#define RESET_IO(x)    GPIO_WriteBit(GPIOA, GPIO_Pin_15, (BitAction)x)  //PA控制WiFi的复位

#define WiFi_printf       u1_printf           //串口1控制 WiFi
#define WiFi_RxCounter    Usart1_RxCounter    //串口1控制 WiFi
#define WiFi_RX_BUF       Usart1_RxBuff       //串口1控制 WiFi
#define WiFi_RXBUFF_SIZE  USART1_RXBUFF_SIZE  //串口1控制 WiFi

//#define SSID   "Redmi"            //路由器SSID名称
//#define SSID   "CH_Bridge_Net"            //路由器SSID名称
//#define SSID   "HUAWEI-B311-2032"            //路由器SSID名称
#define SSID   "CH_Research"            //路由器SSID名称
//#define PASS   "12344321"                 //路由器密码
#define PASS   "12344321_W"                 //路由器密码

void WiFi_ResetIO_Init(void);
char WiFi_SendCmd(char *cmd, int timeout);
char WiFi_Reset(int timeout);
char WiFi_JoinAP(int timeout);
char WiFi_Connect_Server(int timeout);
char WiFi_Smartconfig(int timeout);
char WiFi_WaitAP(int timeout);
char WiFi_Connect_IoTServer(void);


#endif


